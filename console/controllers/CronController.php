<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\models\User;
use frontend\modules\trip\models\Trip;
use frontend\modules\customer\models\Contact;
use frontend\modules\vendor\models\GpsTracking;
use frontend\modules\master\models\State;
use yii\helpers\ArrayHelper;
use frontend\modules\customer\models\Customer;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\vendor\models\Vendor;

class CronController extends Controller
{
    public function actionIndex()
    {
        die("coron service check index action");
        return $this->render('index');
    }

    public function actionDailyactivities(){
      $this->actionDropInactiveTrips();
    }

    public function actionHalfNHourCron()
    {
      $this->actionSendTripUpdates();
    }

    // update next client metting scheduled
    public function actionDropInactiveTrips(){

        return false;

//      $expiredTrips = Trip::find()
//        ->select(['id'])
//        ->where(['<','status',Trip::STATUS_ORDER_CONFIRMED])
//        // ->andWhere(['OR',['vendor_bids'=>NULL],['vendor_bids'=>'']])
//        ->andWhere(['OR',['negotiation_remark'=>NULL],['negotiation_remark'=>'']])
//        // ->andWhere(['OR',['recipients'=>NULL],['recipients'=>'']])
//        ->andWhere(['<','updated_at',date('Y-m-d',strtotime("3 days ago"))])
//        ->asArray()
//        ->all();
//        if (!empty($expiredTrips)) {
//          foreach($expiredTrips as $trip){
//            $modelTrip = Trip::findOne($trip['id']);
//            $modelTrip->status = Trip::STATUS_DELETED;
//            if($modelTrip->save(false)){
//                Yii::$app->activity->add($modelTrip->id,'frontend\modules\trip\models\Trip','Enquiry Auto Dropped');
//            }
//          }
//        }

    }

    // To send customer email updates about trip
    public function actionSendTripUpdates()
    {
        $trips = Trip::find()
                    ->select(['id','updated_at','last_notified_at'])
                    ->where(['OR',
                              ['AND',
                                ['<=','last_notified_at','NOW() - INTERVAL notification_duration hour'],
                                ['<=','updated_at','last_notified_at']
                              ],
                              ['last_notified_at'=>NULL]
                    ])
                    ->andWhere(['NOT',['notification_duration'=>NULL]])
                    ->andWhere(['NOT',['remarks'=>NULL]])
                    //->andWhere(['>=','status',Trip::STATUS_ORDER_CONFIRMED])
                    ->asArray()->all();

        if (!empty($trips)) {
          foreach ($trips as $trip) {
                $modelTrip = Trip::findOne(['id'=>$trip['id']]);

                $contactPerson = Contact::findOne(['id'=>$modelTrip->contact_person_id]);
                if (empty($contactPerson->email) && empty($modelTrip->email)) {
                  continue;
                } else {
                  $email = [];
                  if (!empty($contactPerson->email)) {
                    array_push($email,$contactPerson->email);
                  }
                  if (!empty($modelTrip->email)) {
                    array_push($email,$modelTrip->email);
                  }
                }

                $lastNotifiedAt = empty($modelTrip->last_notified_at)?NULL:date_create($modelTrip->last_notified_at);
                $allRemarks = json_decode($modelTrip->remarks,true);
                $remarks=[];
                foreach ($allRemarks as $remark) {
                    $datetime = date_create($remark["on"]);
                    if (empty($lastNotifiedAt)) {
                      $remarks[]=$remark;
                    } else if($datetime >= $lastNotifiedAt) {
                      $remarks[]=$remark;
                    }
                }
                // Send email
                // $arr = [];
                // $emailList = explode(",",$sendModel->recipients);
                // foreach($emailList as $eml){ array_push($arr,$eml);}
                if (!empty($remarks)) {
                    $path = \Yii::$app->mailer->setViewPath("@common/mail");
                    $sendNotification = \Yii::$app->mailer->compose(['html' => $path.'trip_status_notification'], ['modelTrip' => $modelTrip,'contactPerson'=>$contactPerson,'remarks'=>$remarks])
                    ->setFrom(\Yii::$app->params['adminEmail'])
                    ->setTo($email)
                    ->setSubject('Trip notification for '.$modelTrip->number);
                    if($sendNotification->send()){
                      // Update last_notified_at
                      $modelTrip->last_notified_at = date('Y-m-d H:i:s');
                      $modelTrip->save(false);
                    }
                }
          }
        }

    }

    public function actionAlTracking()
    {
        // Get GPS data Ashokleyland
        //$apiUrl = 'https://ialert2.ashokleyland.com/ialert/daas/api/getdata?token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJOakExTlRBPVxyXG4iLCJhdWQiOiJEQUFTIn0.GEb06jOmj-7hkV17uUubjo3kPxcrOA8pB9z-mkdAaqo';
        $apiUrl = "https://ialertelite.ashokleyland.com/ialert/daas/api/getdata?token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJOakExTlRBPVxyXG4iLCJhdWQiOiJEQUFTIn0.GEb06jOmj-7hkV17uUubjo3kPxcrOA8pB9z-mkdAaqo";
        $response = $this->fetchGpsData($apiUrl);

        if (!empty($response)) {
          $truckDetails = json_decode($response, true);
          // Storing data to gps_tracking table
          if (!empty($truckDetails)) {
            foreach ($truckDetails as $truck) {
                $this->updateGpsData($truck,'AL');
            }
          }
        }

        // Get GPS data Telematics4u
        $apiUrl = 'http://api.telematics4u.com/TelematicsRESTService/services/ServiceProcess/getAssetLiveData?UserId=middlemileapi&Password=Admin@321&Format=JSON';

        $response = $this->fetchGpsData($apiUrl);

        if (!empty($response)) {
          $truckDetails = json_decode($response, true);
          // Storing data to gps_tracking table
          if (!empty($truckDetails)) {
            foreach ($truckDetails as $truck) {
                // Data adjustment for compatibility
                $truck['vehicleregnumber'] = $truck['vname'];
                $truck['latitude'] = $truck['lat'];
                $truck['longitude'] = $truck['lngt'];
                $truck['datetime'] = $truck['dttime'];
                $truck['altitude'] = '';
                $this->updateGpsData($truck,'T4U');
            }
          }
        }
    }

    protected function reverseGeoCoding($lat,$long)
    {
      $apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&sensor=true&key='.\Yii::$app->params['gmapKey'];
      try {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
      } catch (\Exception $e) {}
      if (isset($response) && !empty($response)) {
          $data = json_decode($response,true);
          $addressLength = count($data['results']);
          // Get state name
          $state = '';
          try {
            $state = isset($data['results'][($addressLength - 2)]['address_components'][0]['long_name'])?$data['results'][($addressLength - 2)]['address_components'][0]['long_name']:'';
          } catch (\Exception $e) {}
          $address = '';
          try {
            $address = (isset($data['results'][0]['formatted_address'])?$data['results'][0]['formatted_address']:'');
          } catch (\Exception $e) {}

          return [
            'raw_data' =>json_encode($data,true),
            'state' =>$state,
            'address'=> $address
          ];
      }
      return [
        'raw_data' =>NULL,
        'state' =>NULL,
        'address'=> NULL,
      ];
    }

    protected function fetchGpsData($apiUrl)
    {
      $response = NULL;
      try {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);
      } catch (\Exception $e) {$response = NULL;}

      return $response;
    }

    protected function updateGpsData($truck,$api)
    {
        // Checking for records with same lat long with engine status off
        // Adjustment for GPS accuracy
        $lat = str_split($truck['latitude']);
        array_pop($lat);
        array_pop($lat);
        $long = str_split($truck['longitude']);
        array_pop($long);
        array_pop($long);
        $model = GpsTracking::find()
                            ->andWhere([
                                'registration_number'=>$truck['vehicleregnumber'],
                                'ignition'=>0,
                              ])
                            ->andWhere(['like','lat',$lat])
                            ->andWhere(['like','long',$long])
                            ->orderBy(['id'=>SORT_DESC])
                            ->one();

        // If no record then it is new entry else only change updated at
        if (empty($model)) {

//            try {
//              $reverseGeoCoding = $this->reverseGeoCoding($truck['latitude'],$truck['longitude']);
//            } catch (\Exception $e) {}
            
            $model = new GpsTracking();
            $model->api = $api;
            $model->registration_number = $truck['vehicleregnumber'];
            
            $data = $truck;
            if (is_array($data)) {
              $data =json_encode($data,true);
            }
            $model->data = $data;
            $model->ignition = $truck['ignition'];

            $model->reverse_geocoding = '';// $reverseGeoCoding['raw_data'];
            $model->address = "-"; //$reverseGeoCoding['address'];

//            $state = State::find()->where(['like','UPPER(name)',strtoupper($reverseGeoCoding['state'])])->one();
//            if (!empty($state)) {
//              $model->state_code = $state->gst_state_code;
//              $model->zone = $state->zone;
//            }

            $model->lat = $truck['latitude'];
            $model->long = $truck['longitude'];
            $model->created_at = date('Y-m-d H:i:s');
        }
        $model->updated_at = date('Y-m-d H:i:s');
        $model->save(false);
    }


  
        public function actionDelhiveryUpdates()
    {
        // Finding customer by name
        $delhiveryIds = \frontend\modules\customer\models\Customer::find()->select(['id'])->where(['like','name','Delhivery'])->asArray()->all();
        
        if (!empty($delhiveryIds)) {
            \Yii::$app->db->createCommand('SET SESSION group_concat_max_len = 100000;')->execute();
            $delhiveryIds = \yii\helpers\ArrayHelper::getColumn($delhiveryIds,'id');

            // Getting Tracking details for Trip of Delhivery JSON_UNQUOTE(JSON_EXTRACT(GT.data,"$.speed"))
            $trip = Trip::find()->from([Trip::tableName().' T'])
                        ->select(['distinct(registration_number) vregid'])
                            ->leftJoin(Vehicle::tableName().' V','V.id = T.vehicle_id')
                            ->andWhere(['T.company_id'=>$delhiveryIds])
                            ->andWhere(['<=','T.trip_date',date('Y-m-d H:i:s')])
                            ->andWhere(['not', ['tracking_status' => null]])
                            ->andWhere(['tracking_status' =>[1,2,3,4,5,6,9]])
                            ->andWhere(['T.status'=>Trip::STATUS_ORDER_CONFIRMED])
                            ->asArray()->all();

            
            if(!empty($trip)){
                    $trackingdata = GpsTracking::find()->select(['*'])->where(['registration_number'=> array_column($trip,'vregid'),'sent_to_delhivery'=>NULL])->limit(50)->orderBy('id DESC')->asArray()->all();
                    $deliveryData = ["vendor_name"=>"MMP","count"=>0,"coordinates"=>[]];
                    $coordinates = [];
                    $markAsSent = [];
                                        
                    if(!empty($trackingdata)){
                        foreach ($trackingdata as $data){
                            $deviceData = json_decode($data['data'],true);
                            $coordinates[] = [
                                                          'epoch'=>strtotime($deviceData['datetime']) . '000',
                                                          'lat'=> number_format($data['lat'],12),
                                                          'lon'=> number_format($data['long'],12),
                                                          'ignition'=>$deviceData['ignition'],
                                                          'spd'=>$deviceData['speed'],
                                                         "server_timestamp"=>time().'000',
                                                         "vendor_device_id"=>"",
                                                         "vehicle_number"=>$deviceData['vehicleregnumber'],
                                                         "gps_status"=>"valid",
                                                        "gps_packet_type"=> "current"
                                                        ];
                            $markAsSent[]=$data['id'];
                        }


                        GpsTracking::updateAll(['sent_to_delhivery'=>1],['id'=>$markAsSent]);

                        $deliveryData['coordinates'] = $coordinates;
                        $deliveryData['count']= count($coordinates);

//                        echo(json_encode($trackingdata));
                        $deliveryData = json_encode($deliveryData,JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://mts-staging.delhivery.com/v2/location-data",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 0,
                          CURLOPT_FOLLOWLOCATION => true,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS =>  $deliveryData,
                          CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json",
                            "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1pZG1pbGUtbWlkZGxlbWlsZXBybyIsInRva2VuX25hbWUiOiJtaWRtaWxlLW1pZGRsZW1pbGVwcm8iLCJjZW50ZXIiOlsiSU5EMTIyMDAzQUFCIl0sInVzZXJfdHlwZSI6Ik5GIiwiYXBwX2lkIjo3OCwiYXVkIjoiLmRlbGhpdmVyeS5jb20iLCJmaXJzdF9uYW1lIjoibWlkbWlsZS1taWRkbGVtaWxlcHJvIiwic3ViIjoidW1zOjp1c2VyOjoxNzA2ODYyMi00N2U4LTExZWEtYTVjOS0wNmJlMWMzZTIxMzIiLCJleHAiOjE2NDM5NTkyNzMsImFwcF9uYW1lIjoiTVRTIiwiYXBpX3ZlcnNpb24iOiJ2MiJ9.QKg020ULAleSp-IlEOuTFeI4oVkit53F-ShZfSgmaKY"
                          ),
                        ));

                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        $res = curl_exec($curl);

                        // Check if any error occurred
                        if(curl_errno($curl))
                        {
                            echo 'Curl error: ' . curl_error($curl);
                        }else{
                             var_dump($res);
                        }
                        
                        curl_close($curl);

                            }
                     }
              }
        }
}

<?php

use yii\db\Migration;

/**
 * Class m190618_130403_alter_tbl_master_state
 */
class m190618_130403_alter_tbl_master_state extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `master_states` ADD `gst_state_code` INT NULL DEFAULT NULL AFTER `name`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190618_130403_alter_tbl_master_state cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190618_130403_alter_tbl_master_state cannot be reverted.\n";

        return false;
    }
    */
}

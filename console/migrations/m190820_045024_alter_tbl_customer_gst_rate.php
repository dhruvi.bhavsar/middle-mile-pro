<?php

use yii\db\Migration;

/**
 * Class m190820_045024_alter_tbl_customer_gst_rate
 */
class m190820_045024_alter_tbl_customer_gst_rate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `customer` CHANGE `gst_rate_applicable` `gst_rate` FLOAT(10,2) NOT NULL DEFAULT '0';
          ALTER TABLE `customer` ADD `state_code` INT NULL DEFAULT NULL AFTER `preferred_notification_duration`;
          ALTER TABLE `customer` ADD `detention_rate_1` FLOAT(10,2) NULL DEFAULT NULL AFTER `state_code`, ADD `detention_rate_2` FLOAT(10,2) NULL DEFAULT NULL AFTER `detention_rate_1`, ADD `detention_rate_3` FLOAT(10,2) NULL DEFAULT NULL AFTER `detention_rate_2`;
          ALTER TABLE `customer` ADD `internal_reference` VARCHAR(255) NULL DEFAULT NULL AFTER `detention_rate_3`;
          ALTER TABLE `vendor` ADD `state_code` INT NULL DEFAULT NULL AFTER `mobile`;
          ALTER TABLE `vendor` ADD `address` VARCHAR(255) NULL DEFAULT NULL AFTER `state_code`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190820_045024_alter_tbl_customer_gst_rate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190820_045024_alter_tbl_customer_gst_rate cannot be reverted.\n";

        return false;
    }
    */
}

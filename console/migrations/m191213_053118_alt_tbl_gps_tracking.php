<?php

use yii\db\Migration;

/**
 * Class m191213_053118_alt_tbl_gps_tracking
 */
class m191213_053118_alt_tbl_gps_tracking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `gps_tracking` ADD `reverse_geocoding` TEXT NULL DEFAULT NULL AFTER `long`;
            ALTER TABLE `gps_tracking` ADD `address` VARCHAR(255) NULL DEFAULT NULL AFTER `reverse_geocoding`;
            ALTER TABLE `gps_tracking` ADD `state_code` INT NULL DEFAULT NULL AFTER `address`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191213_053118_alt_tbl_gps_tracking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191213_053118_alt_tbl_gps_tracking cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190905_081621_create_tbl_tyre
 */
class m190905_081621_create_tbl_tyre extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `tyre`(
              `id` INT NOT NULL AUTO_INCREMENT,
              `company` VARCHAR(255) NOT NULL,
              `serial_number` VARCHAR(255) NOT NULL,
              `price` DECIMAL(10, 2) NOT NULL,
              `total_repair_charges` DECIMAL(10, 2) NULL DEFAULT NULL,
              `current_vehicle_id` INT NOT NULL,
              `total_km_run` INT NULL DEFAULT '0',
              `purchase_date` DATE NULL DEFAULT NULL,
              `scraped_date` DATE NULL DEFAULT NULL,
              `data_usage` TEXT NULL DEFAULT NULL,
              `status` INT NOT NULL DEFAULT '1',
              PRIMARY KEY(`id`)
            ) ENGINE = InnoDB;
            ALTER TABLE `tyre` CHANGE `current_vehicle_id` `current_vehicle_id` INT(11) NULL DEFAULT NULL;
            ALTER TABLE `tyre` ADD `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `data_usage`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190905_081621_create_tbl_tyre cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190905_081621_create_tbl_tyre cannot be reverted.\n";

        return false;
    }
    */
}

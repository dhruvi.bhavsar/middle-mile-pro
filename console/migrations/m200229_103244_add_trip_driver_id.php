<?php

use yii\db\Migration;

/**
 * Class m200229_103244_add_trip_driver_id
 */
class m200229_103244_add_trip_driver_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `trip_driver` ADD `driver_id` INT NULL DEFAULT NULL AFTER `trip_id`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200229_103244_add_trip_driver_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200229_103244_add_trip_driver_id cannot be reverted.\n";

        return false;
    }
    */
}

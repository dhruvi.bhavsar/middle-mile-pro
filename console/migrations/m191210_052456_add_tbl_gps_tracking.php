<?php

use yii\db\Migration;

/**
 * Class m191210_052456_add_tbl_gps_tracking
 */
class m191210_052456_add_tbl_gps_tracking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          CREATE TABLE `gps_tracking`(
              `id` INT NOT NULL AUTO_INCREMENT,
              `registration_number` VARCHAR(255) NOT NULL,
              `data` TEXT NOT NULL,
              `ignition` INT NOT NULL,
              `lat` FLOAT NOT NULL,
              `long` FLOAT NOT NULL,
              `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY(`id`)
          ) ENGINE = InnoDB;
          ALTER TABLE `gps_tracking` ADD `api` VARCHAR(255) NOT NULL DEFAULT 'AL' AFTER `id`;
          ALTER TABLE `gps_tracking` CHANGE `lat` `lat` DOUBLE NOT NULL, CHANGE `long` `long` DOUBLE NOT NULL;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191210_052456_add_tbl_gps_tracking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191210_052456_add_tbl_gps_tracking cannot be reverted.\n";

        return false;
    }
    */
}

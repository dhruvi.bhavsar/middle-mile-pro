<?php

use yii\db\Migration;

/**
 * Class m191214_054533_alt_tbl_gps_tracking_add_col_zone
 */
class m191214_054533_alt_tbl_gps_tracking_add_col_zone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `gps_tracking` ADD `zone` INT NULL DEFAULT NULL AFTER `state_code`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191214_054533_alt_tbl_gps_tracking_add_col_zone cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191214_054533_alt_tbl_gps_tracking_add_col_zone cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190903_064123_alter_tbl_customer_add_password_cols
 */
class m190903_064123_alter_tbl_customer_add_password_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `customer` ADD `auth_key` VARCHAR(255) NULL DEFAULT NULL AFTER `email`, ADD `password_hash` VARCHAR(255) NULL DEFAULT NULL AFTER `auth_key`, ADD `password_reset_token` VARCHAR(255) NULL DEFAULT NULL AFTER `password_hash`;
        ALTER TABLE `customer_persons` ADD `auth_key` VARCHAR(255) NULL DEFAULT NULL AFTER `email`, ADD `password_hash` VARCHAR(255) NULL DEFAULT NULL AFTER `auth_key`, ADD `password_reset_token` VARCHAR(255) NULL DEFAULT NULL AFTER `password_hash`;
        ALTER TABLE `customer_persons` ADD `is_admin` INT NULL DEFAULT '0' AFTER `designation`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190903_064123_alter_tbl_customer_add_password_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190903_064123_alter_tbl_customer_add_password_cols cannot be reverted.\n";

        return false;
    }
    */
}

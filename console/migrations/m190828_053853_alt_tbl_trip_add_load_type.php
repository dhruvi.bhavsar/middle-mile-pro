<?php

use yii\db\Migration;

/**
 * Class m190828_053853_alt_tbl_trip_add_load_type
 */
class m190828_053853_alt_tbl_trip_add_load_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `trip` ADD `load_type` INT NULL DEFAULT NULL AFTER `service_type`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190828_053853_alt_tbl_trip_add_load_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190828_053853_alt_tbl_trip_add_load_type cannot be reverted.\n";

        return false;
    }
    */
}

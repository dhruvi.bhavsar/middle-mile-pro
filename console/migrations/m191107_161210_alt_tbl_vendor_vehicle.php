<?php

use yii\db\Migration;

/**
 * Class m191107_161210_alt_tbl_vendor_vehicle
 */
class m191107_161210_alt_tbl_vendor_vehicle extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `vendor_vehicle` CHANGE `chassi_number` `chassi_number` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
        ")
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191107_161210_alt_tbl_vendor_vehicle cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191107_161210_alt_tbl_vendor_vehicle cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191120_095244_alter_tbl_transcations_add_cols_payment_type_detention_days
 */
class m191120_095244_alter_tbl_transcations_add_cols_payment_type_detention_days extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
          ALTER TABLE `transactions`
                  ADD `payment_type` INT NULL DEFAULT NULL AFTER `mamul_type`,
                  ADD `detention_days` DECIMAL(10,1) NULL DEFAULT NULL AFTER `payment_type`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191120_095244_alter_tbl_transcations_add_cols_payment_type_detention_days cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191120_095244_alter_tbl_transcations_add_cols_payment_type_detention_days cannot be reverted.\n";

        return false;
    }
    */
}

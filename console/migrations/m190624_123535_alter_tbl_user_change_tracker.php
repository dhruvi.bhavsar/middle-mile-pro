<?php

use yii\db\Migration;

/**
 * Class m190624_123535_alter_tbl_user_change_tracker
 */
class m190624_123535_alter_tbl_user_change_tracker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `user` ADD `designation` VARCHAR(255) NULL DEFAULT NULL AFTER `full_name`;
        ALTER TABLE `user` ADD `work_address` TINYTEXT NULL DEFAULT NULL AFTER `address`;
        ALTER TABLE `user` ADD `contact_number_personal` VARCHAR(20) NULL DEFAULT NULL AFTER `mobile`;
        ALTER TABLE `user` ADD `pan` VARCHAR(50) NULL DEFAULT NULL AFTER `bank_account_details`, ADD `adhaar` VARCHAR(50) NULL DEFAULT NULL AFTER `pan`;
        ALTER TABLE `user` ADD `end_date` DATE NULL DEFAULT NULL AFTER `joining_date`;
        ALTER TABLE `user` ADD `salary_details` TEXT NULL DEFAULT NULL AFTER `performance_review_history`, ADD `training_record` TEXT NULL DEFAULT NULL AFTER `salary_details`;
        ALTER TABLE `customer` ADD `code` VARCHAR(50) NULL DEFAULT NULL AFTER `type`;
        ALTER TABLE `customer` ADD `gst_rate_applicable` INT NOT NULL DEFAULT '1' AFTER `note`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190624_123535_alter_tbl_user_change_tracker cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190624_123535_alter_tbl_user_change_tracker cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190701_115741_alter_tbl_trip
 */
class m190701_115741_alter_tbl_trip extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `trip` CHANGE `origin` `origin` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `destination` `destination` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190701_115741_alter_tbl_trip cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190701_115741_alter_tbl_trip cannot be reverted.\n";

        return false;
    }
    */
}

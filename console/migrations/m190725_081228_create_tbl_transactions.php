<?php

use yii\db\Migration;

/**
 * Class m190725_081228_create_tbl_transactions
 */
class m190725_081228_create_tbl_transactions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
      CREATE TABLE `transactions`(
        `id` INT NOT NULL AUTO_INCREMENT,
        `trip_id` INT NOT NULL,
        `transaction_id` VARCHAR(100) NULL DEFAULT NULL,
        `amount` FLOAT(10, 2) NOT NULL DEFAULT '0',
        `account_details` INT NOT NULL,
        `note` TINYTEXT NULL DEFAULT NULL,
        `status` INT NOT NULL DEFAULT '1',
        PRIMARY KEY(`id`)
      ) ENGINE = InnoDB;
      ALTER TABLE `customer` ADD `preferred_notification_duration` FLOAT NOT NULL DEFAULT '0' AFTER `credit_period`;
      ALTER TABLE `transactions` ADD `datetime` DATETIME NOT NULL AFTER `note`, ADD `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `datetime`, ADD `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190725_081228_create_tbl_transactions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190725_081228_create_tbl_transactions cannot be reverted.\n";

        return false;
    }
    */
}

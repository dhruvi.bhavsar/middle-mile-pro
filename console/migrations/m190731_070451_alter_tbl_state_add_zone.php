<?php

use yii\db\Migration;

/**
 * Class m190731_070451_alter_tbl_state_add_zone
 */
class m190731_070451_alter_tbl_state_add_zone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `master_states` ADD `zone` INT(11) NULL DEFAULT NULL AFTER `gst_state_code`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190731_070451_alter_tbl_state_add_zone cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190731_070451_alter_tbl_state_add_zone cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190722_104606_create_table_trip_documents
 */
class m190722_104606_create_table_trip_documents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
          CREATE TABLE `trip_documents`(
            `id` INT NOT NULL AUTO_INCREMENT,
            `trip_id` INT NOT NULL,
            `data_document` TEXT NULL DEFAULT NULL,
            `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `status` INT NOT NULL DEFAULT '1',
            PRIMARY KEY(`id`)
          ) ENGINE = InnoDB;
          ALTER TABLE `trip` ADD `pod_receival_date` DATE NULL DEFAULT NULL AFTER `notification_duration`;
          ALTER TABLE `trip` ADD `final_bill_date` DATE NULL DEFAULT NULL AFTER `pod_receival_date`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190722_104606_create_table_trip_documents cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190722_104606_create_table_trip_documents cannot be reverted.\n";

        return false;
    }
    */
}

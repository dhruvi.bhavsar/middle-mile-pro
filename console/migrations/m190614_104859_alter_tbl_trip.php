<?php

use yii\db\Migration;

/**
 * Class m190614_104859_alter_tbl_trip
 */
class m190614_104859_alter_tbl_trip extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `trip` ADD `targated_buying` FLOAT(10,2) NULL DEFAULT NULL AFTER `targated_rate`;
          ALTER TABLE `trip` ADD `vendor_bids` TEXT NULL DEFAULT NULL AFTER `vehicle_type_id`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190614_104859_alter_tbl_trip cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190614_104859_alter_tbl_trip cannot be reverted.\n";

        return false;
    }
    */
}

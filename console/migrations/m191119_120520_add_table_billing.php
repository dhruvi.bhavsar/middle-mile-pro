<?php

use yii\db\Migration;

/**
 * Class m191119_120520_add_table_billing
 */
class m191119_120520_add_table_billing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `trip` ADD `billing_id` INT NULL DEFAULT NULL AFTER `recipients`;
            CREATE TABLE `billing`(
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                `company_id` INT NOT NULL,
                `date` DATE NOT NULL,
                `status` INT NOT NULL DEFAULT '1',
                PRIMARY KEY(`id`)
            ) ENGINE = InnoDB;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191119_120520_add_table_billing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191119_120520_add_table_billing cannot be reverted.\n";

        return false;
    }
    */
}

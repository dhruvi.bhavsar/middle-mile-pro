<?php

use yii\db\Migration;

/**
 * Class m190612_071302_alter_tbl_user_add_new_cols
 */
class m190612_071302_alter_tbl_user_add_new_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
      ALTER TABLE
        `user` ADD `blood_group` VARCHAR(50) NULL DEFAULT NULL AFTER `team`,
        ADD `address` TINYTEXT NULL DEFAULT NULL AFTER `blood_group`,
        ADD `emergency_number` VARCHAR(20) NULL DEFAULT NULL AFTER `address`,
        ADD `dob` DATE NULL DEFAULT NULL AFTER `emergency_number`,
        ADD `wedding_anniversary` DATE NULL DEFAULT NULL AFTER `dob`,
        ADD `spouse_name` VARCHAR(100) NULL DEFAULT NULL AFTER `wedding_anniversary`,
        ADD `child_1_name` VARCHAR(100) NULL DEFAULT NULL AFTER `spouse_name`,
        ADD `child_2_name` VARCHAR(100) NULL DEFAULT NULL AFTER `child_1_name`,
        ADD `bank_account_details` TINYTEXT NULL DEFAULT NULL AFTER `child_2_name`,
        ADD `joining_date` DATE NULL DEFAULT NULL AFTER `bank_account_details`,
        ADD `employment_histroy` TEXT NULL DEFAULT NULL AFTER `joining_date`,
        ADD `performance_review_history` TEXT NULL DEFAULT NULL AFTER `employment_histroy`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190612_071302_alter_tbl_user_add_new_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190612_071302_alter_tbl_user_add_new_cols cannot be reverted.\n";

        return false;
    }
    */
}

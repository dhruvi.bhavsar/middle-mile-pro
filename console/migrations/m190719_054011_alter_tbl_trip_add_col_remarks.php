<?php

use yii\db\Migration;

/**
 * Class m190719_054011_alter_tbl_trip_add_col_remarks
 */
class m190719_054011_alter_tbl_trip_add_col_remarks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `trip` ADD `remarks` TEXT NULL DEFAULT NULL AFTER `cancellation`;
        ALTER TABLE `trip` ADD `update_duration` FLOAT NULL DEFAULT NULL AFTER `remarks`;
        ALTER TABLE `trip` ADD `last_updated_at` DATETIME NULL DEFAULT NULL AFTER `update_duration`;
        ALTER TABLE `trip` CHANGE `last_updated_at` `last_notified_at` DATETIME NULL DEFAULT NULL;
        ALTER TABLE `trip` CHANGE `update_duration` `notification_duration` FLOAT NULL DEFAULT NULL;
        ALTER TABLE `trip` CHANGE `last_notified_at` `last_notified_at` DATETIME NULL DEFAULT NULL COMMENT 'Last notified date & time';
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190719_054011_alter_tbl_trip_add_col_remarks cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190719_054011_alter_tbl_trip_add_col_remarks cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m190722_073839_alter_tbl_vendor_vehicle
 */
class m190722_073839_alter_tbl_vendor_vehicle extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute("
        ALTER TABLE `vendor_vehicle` ADD `vehicle_owner` VARCHAR(255) NULL DEFAULT NULL AFTER `vendor_id`, ADD `vehicle_owner_mobile` VARCHAR(255) NULL DEFAULT NULL AFTER `vehicle_owner`;
        ALTER TABLE `customer` ADD `credit_period` INT NOT NULL DEFAULT '0' AFTER `bank_name`;
      ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190722_073839_alter_tbl_vendor_vehicle cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190722_073839_alter_tbl_vendor_vehicle cannot be reverted.\n";

        return false;
    }
    */
}

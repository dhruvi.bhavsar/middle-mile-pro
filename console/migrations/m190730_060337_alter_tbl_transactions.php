<?php

use yii\db\Migration;

/**
 * Class m190730_060337_alter_tbl_transactions
 */
class m190730_060337_alter_tbl_transactions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `transactions` CHANGE `account_details` `transaction_type` INT(11) NOT NULL;
          ALTER TABLE `transactions` ADD `mamul` FLOAT NULL DEFAULT NULL AFTER `transaction_type`, ADD `mamul_type` INT NULL DEFAULT NULL AFTER `mamul`;
          ALTER TABLE `trip` ADD `unloaded_datetime` DATETIME NULL DEFAULT NULL AFTER `notification_duration`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190730_060337_alter_tbl_transactions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190730_060337_alter_tbl_transactions cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m200301_104308_add_tracking_status_to_trip_tbl
 */
class m200301_104308_add_tracking_status_to_trip_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          ALTER TABLE `trip` ADD `tracking_status` INT NULL DEFAULT NULL AFTER `remarks`;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200301_104308_add_tracking_status_to_trip_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200301_104308_add_tracking_status_to_trip_tbl cannot be reverted.\n";

        return false;
    }
    */
}

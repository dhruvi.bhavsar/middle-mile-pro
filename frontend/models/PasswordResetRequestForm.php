<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use frontend\modules\customer\models\Customer;
use frontend\modules\customer\models\Contact;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email',function ($attribute, $params){
                $user = User::findOne(['email'=>$this->$attribute,'status'=>User::STATUS_ACTIVE]);
                if (empty($user)) {
                  $user = Contact::findOne(['email'=>$this->$attribute,'status'=>Contact::STATUS_ACTIVE]);
                }
                if (empty($user)) {
                  $this->addError($attribute,'There is no user with this email address.');
                }
            }],
          ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);
        if (!$user) {
            $user = Contact::findOne([
                'status' => Contact::STATUS_ACTIVE,
                'email' => $this->email,
            ]);

            if (!$user) { return false; }
            if (empty($user->auth_key)) {
              $user->auth_key = Yii::$app->security->generateRandomString();
            }
            if (!Contact::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
                if (!$user->save()) {
                    return false;
                }
            }
        } else {
          if (empty($user->auth_key)) {
            $user->auth_key = Yii::$app->security->generateRandomString();
          }
          if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
              $user->generatePasswordResetToken();
              if (!$user->save()) {
                  return false;
              }
          }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }
}

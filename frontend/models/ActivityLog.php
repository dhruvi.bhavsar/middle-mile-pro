<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "activity_log".
 *
 * @property integer $id
 * @property integer $performed_by
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $log
 * @property string $created_at
 */
class ActivityLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['performed_by', 'entity_type', 'log', 'created_at'], 'required'],
            [['performed_by', 'entity_id'], 'integer'],
            [['entity_type', 'log'], 'string'],
            [['created_at'], 'safe'],

            // HTMLPurifier
            [['id','performed_by','entity_type','log','created_at', 'entity_id'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'performed_by' => 'Performed By',
            'entity_id' => 'Entity ID',
            'entity_type' => 'Entity Type',
            'log' => 'Log',
            'created_at' => 'Created At',
        ];
    }
}

<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\modules\trip\models\Trip;
use frontend\modules\trip\models\TripSearch;
use frontend\modules\trip\models\Transactions;
use yii\data\ActiveDataProvider;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\vendor\models\VehicleSearch;
use frontend\modules\vendor\models\VehicleStatusTwo;
use frontend\modules\vendor\models\GpsTrackingSearch;
use frontend\modules\trip\models\TripDocuments;
use yii\web\UploadedFile;
use frontend\modules\vendor\models\GpsTracking;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','signup','contact','about'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','index','contact','about','delhivery-updates','vehicletrackingcsv'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }

        if (Yii::$app->user->identity->role == 0) {
            return $this->redirect('/tracking/manage/index');
        }

        // For Driver

        if (Yii::$app->user->identity->role == 1) {
          $model = Trip::driverCurrentTrip(\Yii::$app->user->identity->id);
          
          if(empty($model)){
              $addRemarks=null;
              $dataTransactions=null;
              $modelTransactions=null;
              $modelDocumentUpload = null;
          }else{
                    $addRemarks = new Trip(['scenario'=>'addRemarks']);
                    $dataTransactions = new ActiveDataProvider([
                        'query' => Transactions::tripTransactionDetails($model->id,3),
                        'pagination' => [ 'pageSize' => 10, ],
                    ]);
                    $modelDocumentUpload = TripDocuments::findOne(['trip_id'=>$model->id]);
                    if (empty($modelDocumentUpload)) {
                      $modelDocumentUpload = new TripDocuments();
                      $modelDocumentUpload->trip_id = $model->id;
                      $modelDocumentUpload->created_at = date('Y-m-d H:i:s');
                      $modelDocumentUpload->updated_at = date('Y-m-d H:i:s');
                      $modelDocumentUpload->status = 1;
                    }
                    if ($modelDocumentUpload->load(Yii::$app->request->post())) {
                      $modelDocumentUpload->document = UploadedFile::getInstance($modelDocumentUpload, 'document');
                      if ($modelDocumentUpload->validate()) {
                        $modelDocumentUpload->uploadDocument();
                        $modelDocumentUpload->save(false);
                        $this->refresh();
                      }
                    }
                    $modelTransactions = new Transactions();
                    if ($modelTransactions->load(Yii::$app->request->post()) && !empty($modelTransactions->id)) {
                      $modelTransactions = Transactions::findOne($modelTransactions->id);
                    }
                    if ($modelTransactions->load(Yii::$app->request->post()) && $modelTransactions->processData($model)) {
                      $modelTransactions->save(false);
                      $modelTransactions->sendSmsToVendor($model);
                      $this->refresh();
                    }
          }
          return $this->render('driver', [
              'model'=>$model,
              'addRemarks'=>$addRemarks,
              'dataTransactions'=>$dataTransactions,
              'modelTransactions'=>$modelTransactions,
              'modelDocumentUpload'=>$modelDocumentUpload,
          ]);
        }

        $searchModel = new TripSearch();
        $dataProvider = $searchModel->searchFindTasks(Yii::$app->request->queryParams);

        $vehicleSearchModel = new VehicleSearch();
        $vehicleDataProvider = $vehicleSearchModel->searchVehicle(Yii::$app->request->queryParams);

        $gspTrackingSearch = new GpsTrackingSearch();
        $gspTrackingData = $gspTrackingSearch->search(Yii::$app->request->queryParams);

        $vehicleTrackingTwoSearch = new VehicleStatusTwo();
        $vehicleTrackingTwoDataProvider = $vehicleTrackingTwoSearch->searchVehicleStatusTwo(Yii::$app->request->queryParams);

        // $closedOrderDataProvider = new ActivedataProvider([
        //     'query'=>Trip::closedOrders(),
        //     'pagination' =>[ 'pageSize'=>10 ],
        // ]);

        return $this->render('index', [
            'searchModel'=>$searchModel,
            'dataProvider' => $dataProvider,
            'vehicleSearchModel'=>$vehicleSearchModel,
            'vehicleDataProvider' => $vehicleDataProvider,
            'gspTrackingSearch' => $gspTrackingSearch,
            'gspTrackingData' => $gspTrackingData,
            'vehicleTrackingTwoSearch' => $vehicleTrackingTwoSearch,
            'vehicleTrackingTwoDataProvider' => $vehicleTrackingTwoDataProvider,
            // 'closedOrderDataProvider' => $closedOrderDataProvider,
            'tripCounts'=>Trip::tripCounts()
        ]);
    }


    public function actionVehicletrackingcsv(){

        $vehicleTrackingTwoSearch = new VehicleStatusTwo();
        $vehicleTrackingTwoDataProvider = $vehicleTrackingTwoSearch->searchVehicleStatusTwo(Yii::$app->request->queryParams);
        $vehicleTrackingTwoSearch->load(Yii::$app->request->queryParams);

                    $startDate = new \DateTime($vehicleTrackingTwoSearch->from_date);
                    $endDate = new \DateTime(date("Y-m-d 23:59:59",strtotime($vehicleTrackingTwoSearch->to_date)));


                    $header1 =[''];
                    $data[0] =['Vehicle'];
                    // Preparing headers
                    for ($i=0; $startDate <= $endDate; $startDate->modify('+1 day')) {
                        array_push($header1,$startDate->format('d-M-Y \(D\)'),"","","");
                        array_push($data[0],"From","To","Status","Customer");
                    }

                    $vehicleData = $vehicleTrackingTwoDataProvider->allModels;
                    if(!empty($vehicleData)){
                        $i=1;
                        foreach($vehicleData as $vehicle){
                            $trip_ids = explode(',', $vehicle['trip_ids']);
                            $trip_dates = explode(',', $vehicle['trip_dates']);
                            $customer = explode(',', $vehicle['customer_name']);
                            $unloaded_datetimes = explode(',', $vehicle['unloaded_datetimes']);
                            $origin = explode(',', $vehicle['origin']);
                            $dest = explode(',', $vehicle['dest']);

                              if(!is_array($trip_dates)) {$trip_dates=[$trip_dates];}
                              if(!is_array($unloaded_datetimes)) {$unloaded_datetimes=[$unloaded_datetimes];}

                            // Vehicle data preparation started here
                            $data[$i] = [$vehicle['registration_number']];
                            $startDate = new \DateTime($vehicleTrackingTwoSearch->from_date);
                            for ($j=0; $startDate <= $endDate; $startDate->modify('+1 day')) {

                                $selectedTrip = 0;
                                foreach ($trip_dates as $index => $tripDate) {
                                    $thisTrip = date_create(date("Y-m-d 00:00:00",strtotime($tripDate)));
                                    if ($thisTrip > $startDate) { break; }
                                    $selectedTrip = ($index + 1);
                                }

                                if (empty($trip_ids[0]) || empty($selectedTrip)) {
                                 // No record then show avalible
                                 array_push($data[$i],'-');
                                 array_push($data[$i],'-');
                                 array_push($data[$i],'Available');
                                 array_push($data[$i],'-');
                                 
                                } else {
                                 $thisIndex = ($selectedTrip - 1);
                                 $vehicle_status = "[".$vehicle['vehicle_status']."]";
                                 $vehicle_status = json_decode($vehicle_status,true);
                                 $vehicle_status = $vehicle_status[$thisIndex];

                                 foreach ($vehicle_status as $vehicleStatus) {
                                   $vehicleStatusDate = date_create(date("Y-m-d 00:00:00",strtotime($vehicleStatus['datetime'])));
                                   if ($vehicleStatusDate > $startDate) {
                                     break;
                                   }else{
                                       $vehicleStatusEvent = (int)$vehicleStatus['trip_event'];
                                   }
                                 }

                                 if(($vehicleStatusEvent==7 && $vehicleStatusDate<$startDate)){
                                        array_push($data[$i],'-');
                                        array_push($data[$i],'-');
                                        array_push($data[$i],\Yii::$app->params['vehicle_status_color_code']['']['label']);
                                        array_push($data[$i],'-');
                                   } else if(date_diff($vehicleStatusDate, $startDate)->format("%a")>3){
                                        array_push($data[$i],'-');
                                        array_push($data[$i],'-');
                                        array_push($data[$i],'-');
                                        array_push($data[$i],'-');
                                   }else{
                                       array_push($data[$i],$origin[$thisIndex]);
                                       array_push($data[$i],$dest[$thisIndex]);
                                       array_push($data[$i],\Yii::$app->params['vehicle_status_color_code'][$vehicleStatusEvent]['label']);
                                       array_push($data[$i],$customer[$thisIndex]);
                                   }
                                }
                              }
                           $i++;
                        }
                    }
                    

                    $file = \Yii::$app->commonFunction->exportCSV($header1,$data,NULL,'vehicle_status.csv');
                    return Yii::$app->response->sendFile($file);

    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'guest';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/site/index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        unset($session);
        return $this->redirect('/site/login');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'guest';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect('/site/index');
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'guest';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'guest';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    
    
    public function actionDelhiveryUpdates()
    {
        // Finding customer by name
        $delhiveryIds = \frontend\modules\customer\models\Customer::find()->select(['id'])->where(['like','name','Delhivery'])->asArray()->all();

        if (!empty($delhiveryIds)) {
            \Yii::$app->db->createCommand('SET SESSION group_concat_max_len = 100000;')->execute();
            $delhiveryIds = \yii\helpers\ArrayHelper::getColumn($delhiveryIds,'id');

            // Getting Tracking details for Trip of Delhivery JSON_UNQUOTE(JSON_EXTRACT(GT.data,"$.speed"))
            $trip = Trip::find()->from([Trip::tableName().' T'])
                        ->select(['distinct(registration_number) vregid'])
                            ->leftJoin(Vehicle::tableName().' V','V.id = T.vehicle_id')
                            ->andWhere(['T.company_id'=>$delhiveryIds])
                            ->andWhere(['<=','T.trip_date',date('Y-m-d H:i:s')])
                            ->andWhere(['not', ['tracking_status' => null]])
                            ->andWhere(['tracking_status' =>[1,2,3,4,5,6,9]])
                            ->andWhere(['T.status'=>Trip::STATUS_ORDER_CONFIRMED])
                            ->asArray()->all();


            if(!empty($trip)){
                    $trackingdata = GpsTracking::find()->select(['*'])->where(['registration_number'=> array_column($trip,'vregid'),'sent_to_delhivery'=>NULL])->limit(1)->asArray()->all();
                    $deliveryData = ["vendor_name"=>"MMP","count"=>0,"coordinates"=>[]];
                    $coordinates = [];
                    $markAsSent = [];
                    if(!empty($trackingdata)){
                        foreach ($trackingdata as $data){
                            $deviceData = json_decode($data['data'],true);
                            $coordinates[] = [
                                                          'epoch'=>strtotime($deviceData['datetime']) . '000',
                                                          'lat'=> number_format($data['lat'],12),
                                                          'lon'=> number_format($data['long'],12),
                                                          'ignition'=>$deviceData['ignition'],
                                                          'spd'=>$deviceData['speed'],
                                                         "server_timestamp"=>time().'000',
                                                         "vendor_device_id"=>"",
                                                         "vehicle_number"=>$deviceData['vehicleregnumber'],
                                                         "gps_status"=>"valid",
                                                        "gps_packet_type"=> "current"
                                                        ];
                            $markAsSent[]=$data['id'];
                        }


                        /// GpsTracking::updateAll(['sent_to_delhivery'=>1],['id'=>$markAsSent]);

                        $deliveryData['coordinates'] = $coordinates;
                        $deliveryData['count']= count($coordinates);

//                        echo(json_encode($trackingdata));
                        $deliveryData = json_encode($deliveryData,JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://mts-staging.delhivery.com/v2/location-data",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 0,
                          CURLOPT_FOLLOWLOCATION => true,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS =>  $deliveryData,
                          CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json",
                            "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1pZG1pbGUtbWlkZGxlbWlsZXBybyIsInRva2VuX25hbWUiOiJtaWRtaWxlLW1pZGRsZW1pbGVwcm8iLCJjZW50ZXIiOlsiSU5EMTIyMDAzQUFCIl0sInVzZXJfdHlwZSI6Ik5GIiwiYXBwX2lkIjo3OCwiYXVkIjoiLmRlbGhpdmVyeS5jb20iLCJmaXJzdF9uYW1lIjoibWlkbWlsZS1taWRkbGVtaWxlcHJvIiwic3ViIjoidW1zOjp1c2VyOjoxNzA2ODYyMi00N2U4LTExZWEtYTVjOS0wNmJlMWMzZTIxMzIiLCJleHAiOjE2NDM5NTkyNzMsImFwcF9uYW1lIjoiTVRTIiwiYXBpX3ZlcnNpb24iOiJ2MiJ9.QKg020ULAleSp-IlEOuTFeI4oVkit53F-ShZfSgmaKY"
                          ),
                        ));

                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        $res = curl_exec($curl);

                        // Check if any error occurred
                        if(curl_errno($curl))
                        {
                            echo 'Curl error: ' . curl_error($curl);
                        }else{
                            // var_dump($res);
                        }
                        
                        curl_close($curl);

                            }
                     }
              }
        }
    
}

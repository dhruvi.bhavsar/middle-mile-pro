<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class GuestAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme_ampleadmin/css/animate.css',
        'theme_ampleadmin/css/style.css?v11',
        'theme_ampleadmin/css/colors/default.css?v11',
    ];
    public $js = [
      'js/pwa.js?v11',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

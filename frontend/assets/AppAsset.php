<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css?v11',
        'plugins/bower_components/toast-master/css/jquery.toast.css',
        //'plugins/bower_components/morrisjs/morris.css',
        'plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
        'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css',
        'plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.css',
        'plugins/bower_components/timepicker/bootstrap-timepicker.min.css',
        'plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css',
        'theme_ampleadmin/css/animate.css',
        'theme_ampleadmin/css/style.css?v11',
        'theme_ampleadmin/css/colors/default.css?v11',
    ];
    public $js = [
        'theme_ampleadmin/js/sidebar-nav.min.js',
        'theme_ampleadmin/js/jquery.slimscroll.js',
        'theme_ampleadmin/js/waves.js',
        'theme_ampleadmin/js/jquery.toast.js',
        'theme_ampleadmin/js/chartist.min.js',
        'theme_ampleadmin/js/tooltip.min.js',
        'plugins/bower_components/moment/moment.js',
        'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js',
        'plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
        'plugins/bower_components/timepicker/bootstrap-timepicker.min.js',
        'plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        'theme_ampleadmin/js/custom.js?v11',
        'js/pwa.js?v11',
        //'theme_ampleadmin/js/dashboard1.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

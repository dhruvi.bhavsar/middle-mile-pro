<?php
/*
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="wrapper" class="new-login-register">
      <div class="lg-info-panel">
              <div class="inner-panel">
                  <a class="p-20 di">
                            <h4  style="display: inline-block;color:#FFF;">
                                <strong>MIDDLE MILE </strong> PRO
                            </h4>
                  </a>
                  <div class="lg-content">
                      <h2>Reliable Transport Partner</h2>
                      <p class="text-muted">Middlemilepro offers end-to-end full truck load line haul services across all India.</p>
                  </div>
              </div>
      </div>
      <div class="new-login-box">
                <div class="white-box">
                  <?= common\widgets\Alert::widget() ?>
                    <h3 class="box-title m-b-0">Log In to Account</h3>
                    <small>Enter your details below</small>
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <div class="form-group">
                      <div class="col-md-12">
                        <div class="checkbox checkbox-info pull-left p-t-0">
                        </div>
                        <!--<a href="'/site/request-password-reset'" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a>-->
                      </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                      <div class="col-xs-12">
                        <?= Html::submitButton('Log In', ['class' => 'btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light', 'name' => 'login-button']) ?>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <a href="/site/request-password-reset" class="pull-right"> Reset Password</a>
                    </div>

                    <!--
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                        <div class="social">
                            <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook">
                                <i aria-hidden="true" class="fa fa-facebook"></i>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google">
                                <i aria-hidden="true" class="fa fa-google-plus"></i>
                            </a>
                        </div>
                      </div>
                    </div>
                   -->

                  <?php ActiveForm::end(); ?>

                </div>
      </div>
</section>

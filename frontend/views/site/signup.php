

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="wrapper" class="new-login-register">
      <div class="lg-info-panel">
              <div class="inner-panel">
                  <a href="javascript:void(0)" class="p-20 di"><img src="/images/logo.png"></a>
                  <div class="lg-content">
                      <h2>The ULTIMATE Society manager</h2>
                      <p class="text-muted">Keep society management transparent and crystal clear to all members</p>
                  </div>
              </div>
      </div>
      <div class="new-login-box">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Sign In to Account</h3>
                    <small>Enter your details below</small>
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Name'); ?>

                    <?= $form->field($model, 'email'); ?>
                    
                    <?= $form->field($model, 'mobile'); ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>


                    <div class="form-group text-center m-t-20">
                      <div class="col-xs-12">
                        <?= Html::submitButton('Signup', ['class' => 'btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light', 'name' => 'signup-button']) ?>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                        <div class="social">
                            <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook">
                                <i aria-hidden="true" class="fa fa-facebook"></i>
                            </a>
                            <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google">
                                <i aria-hidden="true" class="fa fa-google-plus"></i>
                            </a>
                        </div>
                      </div>
                    </div>

                    <div class="form-group m-b-0">
                      <div class="col-sm-12 text-center">
                        <p>Already have an account? <a href="/site/login" class="text-primary m-l-5"><b>Log in</b></a></p>
                      </div>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
      </div>
</section>

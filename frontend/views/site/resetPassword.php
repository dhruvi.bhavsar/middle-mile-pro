<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="wrapper" class="new-login-register">
      <div class="lg-info-panel">
              <div class="inner-panel">
                  <a class="p-20 di"> <h4  style="display: inline-block;color:#FFF;"><strong>MIDDLE MILE </strong> PRO </h4> </a>
                  <div class="lg-content">
                      <h2>Reliable Transport Partner</h2>
                      <p class="text-muted">Middlemilepro offers end-to-end full truck load line haul services across all India.</p>
                  </div>
              </div>
      </div>
      <div class="new-login-box">
                <div class="white-box">
                  <h3 class="box-title m-b-0">Reset Password</h3>
                  <small>Enter your details below</small>
                  <?= common\widgets\Alert::widget() ?>
                  <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                      <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label('Enter New Password'); ?>
                      <div class="form-group"><?= Html::submitButton('Save', ['class' => 'btn btn-info btn-block']) ?></div>
                  <?php ActiveForm::end(); ?>
            </div>
    </div>
</section>

<?php
  $trip_ids = explode(',', $vehicle['trip_ids']);
  $trip_dates = explode(',', $vehicle['trip_dates']);
  $customer = explode(',', $vehicle['customer_name']);
  $unloaded_datetimes = explode(',', $vehicle['unloaded_datetimes']);
  $origin = explode(',', $vehicle['origin']);
  $dest = explode(',', $vehicle['dest']);

    if(!is_array($trip_dates)) {$trip_dates=[$trip_dates];}
    if(!is_array($unloaded_datetimes)) {$unloaded_datetimes=[$unloaded_datetimes];}

  ?>
  <tr>
    <td class="headcol border-right"><?= $vehicle['registration_number']; ?> </td>

    <?php
    $startDate = new \DateTime($vehicleTrackingTwoSearch->from_date);
    $endDate = new \DateTime(date("Y-m-d 23:59:59",strtotime($vehicleTrackingTwoSearch->to_date)));
    for ($i=0; $startDate <= $endDate; $startDate->modify('+1 day')) { ?>

      <?php
      $selectedTrip = 0;
      foreach ($trip_dates as $index => $tripDate) {
        $thisTrip = date_create(date("Y-m-d 00:00:00",strtotime($tripDate)));
        if ($thisTrip > $startDate) {
          break;
        }

        $selectedTrip = ($index + 1);
      }

      ?>

      <?php
      if (empty($trip_ids[0]) || empty($selectedTrip)) { ?>
        <!-- No record then avalible -->
        <td class=""> -  </td>
        <td class=""> - </td>
        <td >
          <span class="label label-rouded" style="background-color:<?= \Yii::$app->params['vehicle_status_color_code'][''] ['color']?>"><?= \Yii::$app->params['vehicle_status_color_code']['']['label'] ?></span>
        </td>
        <td class="border-right">- </td>
      <?php } else {
        $thisIndex = ($selectedTrip - 1);
        $vehicle_status = "[".$vehicle['vehicle_status']."]";
        $vehicle_status = json_decode($vehicle_status,true);
        $vehicle_status = $vehicle_status[$thisIndex];


        foreach ($vehicle_status as $vehicleStatus) {
          $vehicleStatusDate = date_create(date("Y-m-d 00:00:00",strtotime($vehicleStatus['datetime'])));
          if ($vehicleStatusDate > $startDate) {
            break;
          }else{
              $vehicleStatusEvent = (int)$vehicleStatus['trip_event'];
          }
        }

        if(($vehicleStatusEvent==7 && $vehicleStatusDate<$startDate)){ ?>

        <td class=""> -  </td>
        <td class=""> - </td>
        <td >
          <span class="label label-rouded" style="background-color:<?= \Yii::$app->params['vehicle_status_color_code'][''] ['color']?>"><?= \Yii::$app->params['vehicle_status_color_code']['']['label'] ?></span>
        </td>
        <td class="border-right">- </td>

        <?php }else if(date_diff($vehicleStatusDate, $startDate)->format("%a")>3){ ?>
            <td class=""> -  </td>
            <td class=""> - </td>
            <td class=""> - </td>
            <td class="border-right">- </td>
        <?php } else{ ?>
        
        <td class=""> <?= $origin[$thisIndex]; ?>  </td>
        <td class=""> <?= $dest[$thisIndex]; ?> </td>
        <td >
            <span class="label label-rouded" style="background-color:<?= \Yii::$app->params['vehicle_status_color_code'][$vehicleStatusEvent] ['color']?>"><?= \Yii::$app->params['vehicle_status_color_code'][$vehicleStatusEvent]['label'] ?></span>
        </td>
        <td class="border-right"> <?= $customer[$thisIndex]; ?> </td>
        <?php }
       }
      ?>


    <?php }?>
  </tr>

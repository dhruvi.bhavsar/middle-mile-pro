<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use frontend\modules\master\models\VehicleType;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\TripSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
    <div class="col-md-3">
      <?= $form->field($model, 'registration_number')->label("Reg #") ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'address')->label("City / Location") ?>
    </div>
    <div class="col-md-2">
      <?= $form->field($model, 'v_type_id')->dropdownList(VehicleType::VehicleTypeList(),['prompt'=>'Select Vehicle Type'])->label("Vehicle Type"); ?>
    </div>
    <div class="col-md-2">
      <?= $form->field($model, 'zone')->dropdownList(\Yii::$app->params['zone'],['prompt'=>'Select Zone'])->label("Zone"); ?>
    </div>
    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/site/index"']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
</div>

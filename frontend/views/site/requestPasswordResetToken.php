<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="wrapper" class="new-login-register">
      <div class="lg-info-panel">
              <div class="inner-panel">
                  <a class="p-20 di"><h4  style="display: inline-block;color:#FFF;"> <strong>MIDDLE MILE </strong> PRO </h4> </a>
                  <div class="lg-content">
                      <h2>Reliable Transport Partner</h2>
                      <p class="text-muted">Middlemilepro offers end-to-end full truck load line haul services across all India.</p>
                  </div>
              </div>
      </div>
      <div class="new-login-box">
                <div class="white-box">
                    <div class="site-request-password-reset">
                        <h1><?= Html::encode($this->title) ?></h1>
                        <small>Please fill out your email. A link to reset password will be sent there.</small>
                        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                            <div class="form-group"><?= Html::submitButton('Send', ['class' => 'btn btn-info btn-block']) ?></div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
        </div>
</section>

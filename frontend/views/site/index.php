<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vehicle;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
$this->title = "Dashboard";

$label = [Trip::STATUS_VENDOR_ASSIGN_PENDING=>'danger',Trip::STATUS_CUSTOMER_CONFIRM_PENDING=>'warning',Trip::STATUS_VENDOR_CONFIRM_PENDING=>'info',Trip::STATUS_ORDER_CONFIRMED=>'success',Trip::STATUS_VENDOR_RE_ASSIGN=>'danger'];
$this->registerCss("
    tbody tr {cursor: pointer;}
    tbody tr:hover {cursor: pointer; color:#000;}
    ");

$this->registerJsFile('@web/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<?php if(Yii::$app->user->identity->role == 10){ ?>
<div class="row">
<div class="col-lg-2 col-sm-6 col-xs-12">
        <div class="white-box analytics-info p-10">
                <h3 class="box-title">Total Orders</h3>
                <ul class="list-inline two-part">
                        <li>
                                <div id="sparklinedash3"><canvas width="30"  style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-info"></i> <span class="counter text-info"><?= $tripCounts['total'] ?></span></li>
                </ul>
        </div>
</div>
<div class="col-lg-2 col-sm-6 col-xs-12">
        <div class="white-box analytics-info p-10">
                <h3 class="box-title">Pending</h3>
                <ul class="list-inline two-part">
                        <li>
                                <div id="sparklinedash2"><canvas width="67" height="30" style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span class="counter text-purple"><?= $tripCounts['pending'] ?></span></li>
                </ul>
        </div>
</div>
<div class="col-lg-2 col-sm-6 col-xs-12">
        <div class="white-box analytics-info p-10">
                <h3 class="box-title">Active</h3>
                <ul class="list-inline two-part">
                        <li>
                                <div id="sparklinedash"><canvas width="67" height="30" style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i> <span class="counter text-success"><?= $tripCounts['active'] ?></span></li>
                </ul>
        </div>
</div>
<div class="col-lg-2 col-sm-6 col-xs-12">
        <div class="white-box analytics-info p-10">
                <h3 class="box-title">Delivered</h3>
                <ul class="list-inline two-part">
                        <li>
                                <div id="sparklinedash5"><canvas width="67" height="30" style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i> <span class="counter text-success"><?= $tripCounts['delivered'] ?></span></li>
                </ul>
        </div>
</div>
<div class="col-lg-2 col-sm-6 col-xs-12">
        <div class="white-box analytics-info p-10">
                <h3 class="box-title">Dropped</h3>
                <ul class="list-inline two-part">
                        <li>
                                <div id="sparklinedash4"><canvas width="67" height="30" style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas></div>
                        </li>
                        <li class="text-right"><i class="ti-arrow-down text-danger"></i> <span class="text-danger"><?= $tripCounts['closed'] ?></span></li>
                </ul>
        </div>
</div>

</div>
<?php } ?>

<div class="white-box">

  <nav class="">
    <ul class="nav nav-tabs customtab" role="tablist">
      <li class="active"><a data-toggle="tab" href="#dashboard-pendingenquiries" aria-expanded="false">
        <div class="hidden-xs ">Pending Enquiries</div>
        <div class="hidden-lg hidden-md hidden-sm"><i class="mdi mdi-truck-delivery"></i></div>
      </a></li>
      <li class=""><a data-toggle="tab" href="#dashboard-vehicledetails" aria-expanded="false">
        <div class="hidden-xs ">Vehicle Status</div>
        <div class="hidden-lg hidden-md hidden-sm"><i class="mdi mdi-truck"></i></div>
      </a></li>
      <li class=""><a data-toggle="tab" href="#vehicle-tracking" aria-expanded="false">
        <div class="hidden-xs ">Vehicles Tracking</div>
        <div class="hidden-lg hidden-md hidden-sm"><i class="mdi mdi-map"></i></div>
      </a></li>
      <li class=""><a data-toggle="tab" onclick="setContainerHeight()" href="#dashboard-vehicles_tracking_two" aria-expanded="false">
        <div class="hidden-xs ">Vehicle Tracking 2</div>
        <div class="hidden-lg hidden-md hidden-sm"><i class="mdi mdi-map"></i></div>
      </a></li>
    </ul>
  </nav>

  <div class="tab-content">
        <div id="dashboard-pendingenquiries" class="tab-pane fade in active">
          <?php Pjax::begin(['id'=>'pjax-pendingenquiries']); ?>
              <?= $this->render('_search', ['model' => $searchModel]); ?>
              <div class="clearfix"></div>
              <?php
              $dataProvider->setSort([
                'attributes' => [
                  'Customer' => [
                    'asc' => ['custName' => SORT_ASC],'desc' => ['custName' => SORT_DESC],'default' => SORT_ASC
                  ],
                  'Enquiry Id' => [
                    'asc' => ['number' => SORT_ASC],'desc' => ['number' => SORT_DESC],'default' => SORT_ASC
                  ],
                  'Route' => [
                    'asc' => ['origin_location' => SORT_ASC,'destination_location' => SORT_ASC],'desc' => ['origin_location' => SORT_DESC,'destination_location' => SORT_ASC],'default' => SORT_ASC
                  ],
                  'Trip Date' => [
                    'asc' => ['trip_date' => SORT_ASC],'desc' => ['trip_date' => SORT_DESC],'default' => SORT_DESC
                  ],
                  'Bids' => [
                    'asc' => ['vendor_bids' => SORT_ASC],'desc' => ['vendor_bids' => SORT_DESC],'default' => SORT_DESC
                  ],
                  'Vendor' => [
                    'asc' => ['vendor' => SORT_ASC],'desc' => ['vendor' => SORT_DESC],'default' => SORT_DESC
                  ],
                  'Vehicle' => [
                    'asc' => ['vehicle_type' => SORT_ASC],'desc' => ['vehicle_type' => SORT_DESC],'default' => SORT_ASC
                  ],
                  'Enquired on' => [
                    'asc' => ['created_at' => SORT_ASC],'desc' => ['created_at' => SORT_DESC],'default' => SORT_DESC
                  ],
                  'Last Updated on' => [
                    'asc' => ['updated_at' => SORT_ASC],'desc' => ['updated_at' => SORT_DESC],'default' => SORT_DESC
                  ],
                  'Status' => [
                    'asc' => ['status' => SORT_ASC],'desc' => ['status' => SORT_DESC],'default' => SORT_DESC
                  ],
                ],
                'defaultOrder' => [
                  'Enquired on' => SORT_ASC
                  //'Status' => SORT_ASC
                ]
              ]);
              ?>
              <?= GridView::widget([
                'options' => ['class'=>'table-responsive','style'=>'font-size:12px'],
                'tableOptions' => ['class'=>'table'],
                'emptyText' => '<center>No records</center>',
                'dataProvider' => $dataProvider,
                'rowOptions'   => function ($model, $key, $index, $grid) {
                  return [
                    'data-id' => $model->id,
                    'onclick' => 'location.href="/trip/manage/view?id="+$(this).data("id");',
                  ];
                },
                'layout' => "{items}\n<div align='center'>{pager}</div>",
                'pager'=>[
                'linkOptions'=>['class'=>'btn-circle m-r-10'],
                'disabledListItemSubTagOptions' => ['class' => 'btn btn-circle disable  m-r-10']
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                      'attribute'=>'Customer',
                      'value'=>function($model){
                        return $model->custName;
                      }
                    ],
                    [
                      'attribute'=>'Enquiry Id',
                      'value'=>function($model){
                        return $model->number;
                      }
                    ],
                    [
                      'attribute'=>'Route',
                      'format' => 'raw',
                      'value'=>function($model){
                        return ($model->origin_location.' - '.$model->destination_location)
                        .'<br> <p style="max-width:200px;font-size:12px">Loading: '.($model->loading_point).'<br> Unloading: '.($model->unloading_point).'</p>';
                      }
                    ],
                    [
                      'format'=>'raw',
                      'attribute' => 'Trip Date',
                      'value' => function($model){
                        return '<label class="hidden-lg hidden-md text-muted">Trip Date : </label>'.date('d-m-Y',strtotime($model->trip_date));
                      }
                    ],
                    [
                      'attribute'=>'Bids',
                      'format' => 'raw',
                      'value'=>function($model){
                        $bids = json_decode($model->vendor_bids,true);
                        $bidDetails = '-';
                        if (!empty($bids)) {
                          $count = count($bids);
                          $lowest = \Yii::$app->commonFunction->lowestBid($bids);
                          $bidDetails = $count .'/'. $lowest;
                        }
                        return '<label class="hidden-lg hidden-md text-muted">Bids : </label>'.$bidDetails;
                      }
                    ],
                    [
                      'attribute'=>'Vendor',
                      'format' => 'raw',
                      'value'=>function($model){
                        return '<label class="hidden-lg hidden-md text-muted">Vendor : </label>'.(!empty($model->vendor)?$model->vendor:'-');
                      }
                    ],
                    [
                    'attribute'=>'Vehicle',
                    'format' => 'raw',
                    'value'=>function($model){
                      return '<label class="hidden-lg hidden-md text-muted">Vehicle : </label>'.$model->vehicle_type;
                    }
                    ],
                    [
                    'attribute' => 'Enquired on',
                    'format' => 'raw',
                    'value' => function($model){
                      return '<label class="hidden-lg hidden-md text-muted">Enquired on : </label>'.date('d-m-Y',strtotime($model->created_at));
                    }
                    ],
                    [
                    'attribute' => 'Last Updated on',
                    'format' => 'raw',
                    'value' => function($model){
                      return '<label class="hidden-lg hidden-md text-muted">Last Updated on : </label>'.date('d-m-Y',strtotime($model->updated_at));
                    }
                    ],
                    [
                    'attribute' => 'Status',
                    'format' => 'html',
                    'value' => function($model){
                      return Yii::$app->params['tripStatusLabel'][$model->status];
                    }
                    ],
                ],
                ]); ?>
          <?php Pjax::end(); ?>
        </div>
        <div id="dashboard-vehicledetails" class="tab-pane fade">
          <?php Pjax::begin(['id'=>'pjax-vehicledetails']); ?>
              <?= $this->render('_vehicle_search', ['model' => $vehicleSearchModel]); ?>
              <?php global $vehicleStatus; ?>
              <?= GridView::widget([
                'options' => ['class'=>'table-responsive','style'=>'font-size:12px'],
                'tableOptions' => ['class'=>'table'],
                'emptyText' => '<center>No records</center>',
                'dataProvider' => $vehicleDataProvider,
                'rowOptions'   => function ($model, $key, $index, $grid) {
                  $GLOBALS['vehicleStatus'] = Trip::vehicleStatus($model['id']);
                  return [
                    'data-id' => $model['id'],
                    // 'onclick' => 'location.href="/trip/manage/view?id="+$(this).data("id");',
                  ];
                },
                'layout' => "{items}\n<div align='center'>{pager}</div>",
                'pager'=>[
                'linkOptions'=>['class'=>'btn-circle m-r-10'],
                'disabledListItemSubTagOptions' => ['class' => 'btn btn-circle disable  m-r-10']
                ],
                'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  [
                    'attribute'=>'registration_number',
                    'label'=>'Vehicle #',
                    'format'=>'raw',
                    'value'=>function($model){
                      return $model['registration_number'].'<br>'.$model['vehicle_type'];
                    }
                  ],
                  [
                    'attribute'=>'vehicle_owner',
                    'format'=>'raw',
                    'value'=>function($model){
                      return $model['vehicle_owner'].'<br>'.$model['vehicle_owner_mobile'];
                    }
                  ],
                  'customer_name',
                  'origin',
                  'destination',
                  [
                        'attribute' => 'Loading point',
                        'format' => 'raw',
                        'value' => function($model) {
                            return $model['loading_point'].' / '.ucwords($model['origin']);
                        }
                    ],
                    [
                        'attribute' => 'Unloading point',
                        'format' => 'raw',
                        'value' => function($model) {
                            return $model['unloading_point'].' / '.ucwords($model['destination']);
                        }
                    ],
                  [
                    'attribute'=>'trip_date',
                    'format'=>'raw',
                    'value'=>function($model){
                      if (empty($model['trip_date'])) {
                        return '';
                      }
                      // $currentVehicleStatus['type'].' : '.
                      return date('d-m-Y h:i A',strtotime($model['trip_date']));
                    }
                  ],
                  [
                    'attribute'=>'status',
                    'format'=>'raw',
                    'value'=>function($model){
                      if (empty($model['status'])) {
                        return $model['trip_status'];
                      }
                      // $currentVehicleStatus['type'].' : '.
                      return Yii::$app->params['tripStatusLabel'][$model['status']].'<br/> '.$model['trip_status'].' <br> '.$model['number'];
                    }
                  ],
                  [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => '{view}',
                      'buttons'=>[
                          'view'=>function ($url, $model, $key) {
                            return Html::a('<i class="mdi mdi-calendar-text" style="line-height: 0.8;"></i>', ['/vendor/vehicle/vehiclestatus','id'=>$model['id']],['class'=>'btn btn-circle btn-flat waves-effect','title' => 'View Vehicle Status','data-pjax'=>'0']);
                          },
                      ],
                  ],
                ]]); ?>
          <?php Pjax::end(); ?>
        </div>
        <div id="vehicle-tracking" class="tab-pane fade">

            <?= $this->render('_search_gps_tracking', ['model' => $gspTrackingSearch]); ?>
            <br>
             <div id="map-container" style="width:100%; height:400px">Loading</div>

            <?php
            $this->registerCss("#map_canvas { height: 100% }");
            $this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.Yii::$app->params['gmapKey'],['depends' => [\yii\web\JqueryAsset::className()]]);

            //$dataGps = frontend\modules\vendor\models\GpsTracking::getCurrentVehicleLocation();
            //echo "<pre>";var_dump($dataGps); die();
            $location = [];
            // $zone = frontend\modules\master\models\State::getZoneFromStateId();
            $zone = \Yii::$app->params['zone'];
            if (!empty($gspTrackingData)) {

              foreach ($gspTrackingData as $data) {
                $location[] = array_merge(
                  json_decode($data['data'],true),
                  [
                    'api'=>$data['api'],
                    'v_type'=>$data['v_type'],
                    'location'=>$data['address'],
                   // 'zone'=>(isset($zone[$data['zone']])?$zone[$data['zone']]:''),
                  ]);
              }
            }

            $this->registerJs("

              var locations = ".json_encode($location,true).";
              //console.log(locations);
              $(function(){
                if (locations != undefined && locations != '') {
                  initialize(locations);
                }
              });


              function initialize(locations) {

                var myOptions = {
                  center: new google.maps.LatLng(21.1458, 79.0882),
                  zoom: 4,
                  mapTypeId: google.maps.MapTypeId.ROADMAP

                };

                var map = new google.maps.Map(document.getElementById('map-container'),
                    myOptions);

                setMarkers(map,locations)
              }


              function setMarkers(map,locations){

                  var marker, i;
                  if (locations != undefined) {
                    for (i = 0; i < locations.length; i++)
                    {
                      var vehicle = locations[i]['v_type']+'-'+locations[i]['vehicleregnumber'];
                      var lat = locations[i]['latitude'];
                      var long = locations[i]['longitude'];
                      var location =  locations[i]['location'];
                      var datentime =  locations[i]['datetime'];
                      var zone =  locations[i]['zone'];
                      var ignition =  locations[i]['ignition'];
                      var speed =  locations[i]['speed'];
                      var altitude =  locations[i]['altitude'];


                      latlngset = new google.maps.LatLng(lat, long);

                      var marker = new google.maps.Marker({
                        map: map,
                        title: vehicle ,
                        label: {
                          text: vehicle,
                          color: 'eb3a44',
                          fontSize: '16px',
                          fontWeight: 'bold'
                        },
                        position: latlngset,
                        // icon:{}
                        });
                        map.setCenter(marker.getPosition())


                        var content = '<h3>Vehicle Number: ' + vehicle +  '</h3>'
                        + 'Last tracking : ' + datentime + '<br />'
                        + 'Location : ' + location + '<br />'
                        + 'Zone : ' + zone + '<br />'
                        + 'Ignition : ' + ignition + '<br />'
                        + 'Speed : ' + speed + '<br />'
                        + 'Altitude : ' + altitude + '<br />';


                        var infowindow = new google.maps.InfoWindow()

                        google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
                          return function() {
                            infowindow.setContent(content);
                            infowindow.open(map,marker);
                          };
                        })(marker,content,infowindow));

                      }
                  }
                }

      initialize();
      ", yii\web\View::POS_END);

        ?>

        </div>
        <div id="dashboard-vehicles_tracking_two" class="tab-pane fade">
          <?= $this->render('_vehicle_status_two', ['model' => $vehicleTrackingTwoSearch]); ?>
          <?php Pjax::begin(['id'=>'pjax-pendingenquiries']); ?>
              <div class="position-responsive">
                <div class="vehicleStatusTable">
                  <div class="innerdiv drag-scroll">
                    <?php
                    $startDate = new \DateTime($vehicleTrackingTwoSearch->from_date);
                    $endDate = new \DateTime($vehicleTrackingTwoSearch->to_date);
                    $header1 ='<tr><th class="headcol border-right"> &nbsp; </th>';
                    $header2 ='<tr><th class="headcol border-right"> Vehicle # </th>';
                    for ($i=0; $startDate <= $endDate; $startDate->modify('+1 day')) { ?>
                      <?php
                      // Generating Table Header
                      $header1 .='<th class="border-right" colspan="4">'.$startDate->format('d-M-Y \(D\)').'</th>';
                      $header2 .='<th>From</th>
                      <th>To</th>
                      <th>Status</th>
                      <th class="border-right">Customer</th>';
                      ?>
                    <?php }
                    $header1 .="</tr>";
                    $header2 .="</tr>";
                    ?>

                    <table>
                      <thead>
                        <?= $header1; ?>
                        <?= $header2; ?>
                      </thead>
                      <tbody>
                        <?php Pjax::begin(['id'=>'timesheet-index']); ?>
                         <?= ListView::widget([
                              'dataProvider' => $vehicleTrackingTwoDataProvider,
                              'options' => ['tag' => 'tbody','class' => 'list-wrapper','id' => 'list-wrapper',],
                              'pager'=>[
                                'linkOptions'=>['class'=>'btn-circle m-r-10'],
                                'disabledListItemSubTagOptions' => ['class' => 'btn btn-circle disable  m-r-10'],
                                'maxButtonCount' => 5,
                              ],
                              'itemOptions'=>['class'=>''],
                              'emptyText' => '<center>No records</center>',
                              'layout' => "{items}\n<tr align='left'  style='clear:both'><td style=\"width: 400px !important;\" class=\"headcol border-right\" colspan=\"4\">{pager}</td></div>",
                              'itemView' => function ($model, $key, $index, $widget) use($vehicleTrackingTwoSearch) {
                                  return $this->render('_vehicle_tracking_two_row',['vehicle' => $model,'index'=>$index,'vehicleTrackingTwoSearch'=>$vehicleTrackingTwoSearch]);
                              },
                              //'viewParams' => ['counter' => $propertyCount],
                          ]); ?>
                        <?php Pjax::end(); ?>

                      </tbody>


                    </table>
                  </div>
                </div>
              </div>
          <?php Pjax::end(); ?>

              <script type="text/javascript">
              function setContainerHeight()
              {
                setTimeout(function () {
                  let vehicleStatusTable = document.querySelector('.vehicleStatusTable');
                  let height = vehicleStatusTable.offsetHeight;
                  document.querySelector('.position-responsive').style.height = (height + Number(60))+'px';
                  console.log(vehicleStatusTable.offsetHeight);
                }, 400);

              }
                const slider = document.querySelector('.drag-scroll');
                      let isDown = false;
                      let startX;
                      let scrollLeft;

                      slider.addEventListener('mousedown', (e) => {
                      isDown = true;
                      slider.classList.add('active');
                      startX = e.pageX - slider.offsetLeft;
                      scrollLeft = slider.scrollLeft;
                      });
                      slider.addEventListener('mouseleave', () => {
                      isDown = false;
                      slider.classList.remove('active');
                      });
                      slider.addEventListener('mouseup', () => {
                      isDown = false;
                      slider.classList.remove('active');
                      });
                      slider.addEventListener('mousemove', (e) => {
                      if(!isDown) return;
                      e.preventDefault();
                      const x = e.pageX - slider.offsetLeft;
                      const walk = (x - startX) * 3; //scroll-fast
                      slider.scrollLeft = scrollLeft - walk;
                  });
              </script>
              <style media="screen">
              .position-responsive{
                position: relative;
              }
              .vehicleStatusTable {
                position: absolute;
                top: 0;
                left: 0;
                right: 150px;
              }
              .vehicleStatusTable td,.vehicleStatusTable th{
                white-space:nowrap;
                border-bottom: 1px solid #efefef;
                margin:0px;
              }
              .vehicleStatusTable td{
                padding: 10px;
              }
              .vehicleStatusTable th{
                padding: 10px 10px 0px 10px;
              }
              .innerdiv {
                width: 100%;
                overflow-x:scroll;
                margin-left: 150px;
                overflow-y:visible;
                padding-bottom:1px;
              }
              .headcol {
                position:absolute;
                width:150px;
                left:0;
                top:auto;
              }
              .border-right{
                border-right: 1px solid #efefef;
              }
              </style>
        </div>


  </div>

</div>

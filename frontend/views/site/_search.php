<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\TripSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="col-md-3">
      <?= $form->field($model, 'origin_state')->widget(Select2::classname(), [
        'data' => \frontend\modules\master\models\State::StateList(),
        'options' => [
          'placeholder' => 'Select Origin State ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'origin_city')->widget(Select2::classname(), [
        'data' => \frontend\modules\master\models\City::CityList(),
        'options' => [
          'placeholder' => 'Select Origin City ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'destination_state')->widget(Select2::classname(), [
        'data' => \frontend\modules\master\models\State::StateList(),
        'options' => [
          'placeholder' => 'Select Destination State ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'destination_city')->widget(Select2::classname(), [
        'data' => \frontend\modules\master\models\City::CityList(),
        'options' => [
          'placeholder' => 'Select Destination City ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'company_id')->widget(Select2::classname(), [
        'data' =>  \frontend\modules\customer\models\Customer::CustomerList(),
        'options' => [
          'placeholder' => 'Select Customer ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ])->label("Customer");
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'number')->label("Enquiry Id") ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'vendor_id')->widget(Select2::classname(), [
        'data' =>  \frontend\modules\vendor\models\Vendor::VendorList(),
        'options' => [
          'placeholder' => 'Select Customer ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'vehicle_type_id')->widget(Select2::classname(), [
        'data' => \frontend\modules\master\models\VehicleType::vehicletypelist(),
        'options' => ['placeholder' => 'Select a Vehicle Type ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-3">
    <?= $form->field($model, 'trip_date')->textInput(['class'=>'datepicker form-control','autocomplete'=>'off']) ?>
    </div>
    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/site/index"']) ?>
        <?php if(Yii::$app->commonFunction->accessToElement('createTrip')){ ?>
          <a href="/trip/manage/create" class="btn btn-info">Add New Enquiry</a>
          <?php } ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
</div>

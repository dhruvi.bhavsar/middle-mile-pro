<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vehicle;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
if (empty($model)) {
	$this->title = '<center>No Trip Assigned</center>';
	return true;
} else {
	$this->title = $model->number;
}

?>

<div class="trip-view">
	<div class="row">
	  <div class="col-md-4 col-sm-12">
	    <section class="white-box">
	      <div class="user-btm-box p-t-0">
					<div class="row  m-t-10">
	          <div class="col-sm-12"><p><strong class="text-muted">Travel:</strong> From <?= ($model->originDetails->name) ?> to <?= ($model->destinationDetails->name) ?></p></div>
	        </div>
	        <hr>
					<div class="row  m-t-10">
	          <div class="col-sm-6 b-r"><p><strong class="text-muted">Loading:</strong><?= (!empty($model->loading_point))?$model->loading_point:'-'; ?></p></div>
	          <div class="col-sm-6"><p><strong class="text-muted">Unloading:</strong><?= (!empty($model->unloading_point))?$model->unloading_point:'-'; ?></p></div>
	        </div>
	      </div>
	    </section>
	  </div>

	  <div class="col-md-8 col-sm-12 ">
	    <section class="well p-t-10 mob-p-t-0">
  	      <nav class="">
  	        <ul class="nav nav-tabs customtab" role="tablist">
              <li class="active"><a data-toggle="tab" href="#trip-tracking" aria-expanded="false"><span> Tracking</span></a></li>
              <li class=""><a data-toggle="tab" href="#trip-expense" aria-expanded="false"><span> Expense</span></a></li>
              <li class=""><a data-toggle="tab" href="#trip-document" aria-expanded="false"><span> Documents</span></a></li>
  	        </ul>
  	      </nav>

  	      <div class="tab-content">
						<div id="trip-tracking" class="tab-pane fade in active">
							<?= $this->render('@frontend/modules/trip/views/manage/_manage_tracking', [
									'model' => $model,
									'addRemarks'=>$addRemarks,
									'forDriver'=>1,
							]) ?>
						</div>
              <div id="trip-expense" class="tab-pane fade">
								<a class="btn btn-primary waves-effect waves-light btnAddEditTransaction"  title="Tax Invoice" aria-label="Tax Invoice" data-pjax="0">Add Transaction</a>
  							<div class="row">
  								<div class="col-md-12">
										<?= $this->render('@frontend/modules/trip/views/manage/_trip_transaction', [
												'dataTransactions' => $dataTransactions,
												'hideCTA'=>1
										]) ?>
  								</div>
  							</div>
  		        </div>
							<div id="trip-document" class="tab-pane fade">
								<?= $this->render('@frontend/modules/trip/views/manage/_trip_document', [
										'model' => $model,
										'modelDocumentUpload'=>$modelDocumentUpload,
								]) ?>
							</div>

  	      </div>
	    </section>

	  </div>
	</div>

</div>

<?= $this->render('@frontend/modules/trip/views/manage/_modalCreateEditTransaction',[
	'modelTransactions'=>$modelTransactions,
	'forDriver'=>1,
	]) ?>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\TripSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'id'=>'tracking-status-2'
        ],
    ]); ?>
    <div class="col-md-3">
      <?= $form->field($model, 'vendor_id')->widget(Select2::classname(), [
        'data' =>  \frontend\modules\vendor\models\Vendor::VendorList(),
        'options' => [
          'placeholder' => 'Select Vendor ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'from_date')->textInput(['class'=>'form-control datepicker'])->label("From Date"); ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'to_date')->textInput(['class'=>'form-control datepicker'])->label("To Date"); ?>
    </div>
    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/site/index"']) ?> | 
        <?= Html::button('CSV', ['class' => 'btn btn-inverse','onclick'=>"$('#tracking-status-2').attr('action', '/site/vehicletrackingcsv').submit();"]) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
</div>

<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

$user = Yii::$app->user->identity;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="/js/manifest.webmanifest"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fix-header hide-sidebar">
    <?php $this->beginBody() ?>
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader" style="display: none;">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>

    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?= $this->render('header',['user'=>$user]); ?>
        <!-- End Top Navigation -->

      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <?php if(!\Yii::$app->user->isGuest){?>
        <?= $this->render('sidebar'); ?>
      <?php } else {
        $this->registerCss("
          #page-wrapper{
            margin-left: 0px !important;
          }
        ");
      } ?>
      <!-- End Left Sidebar -->

  <div id="page-wrapper">
        <div class="container-fluid">
          <!--  Page Title Section -->
          <?php if (!isset(Yii::$app->controller->action->defaultName) || Yii::$app->controller->action->defaultName != "Error") { ?>
          <div class="row bg-title"><div class="col-xs-12">
            <?php //  var_dump($this->params['backUrl']) ?>
            <div class="pull-left m-r-10" style="min-height:30px;min-width:10px">
              <?php if (isset($this->params['backUrl']) && !\Yii::$app->user->isGuest) { ?>
                <a class="btn btn-flat btn-outline btn-circle waves-effect waves-light  pull-left" href="<?= $this->params['backUrl']; ?>"><i style="margin-top:-11px;display: inline-block;font-size: 30px;" class="mdi mdi-chevron-left text-info"></i></a>
              <?php } ?>
            </div>

            <h4 class="page-title"><?= $this->title; ?></h4>
          </div></div>
          <?php } ?>
          <?= Alert::widget() ?>
           <!-- End of Page Title Section -->
            <div class="row m-0 hidden-xs hidden-sm">&nbsp;</div>
            <?= $content; ?>
        </div>
   </div>
   </div>


        <?php
        if (!Yii::$app->user->isGuest) {
        $this->registerJs("
                                  function getNotificationCount(){
                                      $.ajax({
                                        dataType: 'json',
                                        url: '/notification/manage/count',
                                        context: document.body
                                      }).done(function(data) {
                                            if(Number(data.count)>0){
                                                $('.notifications-count-notifications').show().text(data.count);
                                            }else{
                                                 $('.notifications-count-notifications').hide().text('');
                                            }

                                            if(data.notifications !=''){
                                                $('.notification-items-list').html(data.notifications);
                                            }

                                      });
                                  }

                                 // get notification count on load
                                 getNotificationCount();

                                // update notification count on every 30 sec
                                setInterval(function(){
                                    getNotificationCount()
                                },10000);

            ", yii\web\View::POS_READY, 'my-options');
    }
   ?>

    <footer class="footer text-center"> <?= date('Y') ?> &copy; <?= Yii::$app->name; ?> </footer>
    
    <div class="hidden" hidden>
      <div class="col-sm-12 text-left p-0 modal-add-button text-center">
        <br>
        <button type="button" class="btn btn-sm btn-info btn-rounded m-0 btn-lg  text-uppercase waves-effect waves-light add-button" name="add-button">Add To Home Screen</button>
        <button type="button" class="btn btn-sm btn-inverse btn-rounded m-0 btn-lg  text-uppercase waves-effect waves-light " name="cancel" onclick="$('.modal-add-button').hide();">Close</button>
        <br><br>
      </div>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

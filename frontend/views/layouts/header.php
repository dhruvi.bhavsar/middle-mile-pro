<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
      <?php /*
        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="/">
                <!-- Logo icon image, you can use font-icon also --><b>
                <!--This is dark logo icon-->
                <!--<img src="/images/logo.png" alt="home" class="dark-logo" />-->
                <!--This is light logo icon-->
                <!--<img src="/images/logo.png" alt="home" class="light-logo" />-->
             </b>
                <!-- Logo text image you can use text also -->
                <span class="hidden-xs">
                    <h4  style="display: inline-block;">
                        <strong>MIDDLEMILE </strong> PRO
                    </h4>
                </span>
                <span class="hidden-sm hidden-md hidden-lg">
                    <h4  style="display: inline-block;">
                        <strong>MM</strong>P
                    </h4>
                </span>
            </a>
        </div>
        */ ?>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        <ul class="nav navbar-top-links navbar-left">
            <li >
              <?php if(!\Yii::$app->user->isGuest && \Yii::$app->user->identity->role != 1){ ?>
                <a style="padding-left:20px;padding-right:20px" href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a>
              <?php } ?>
            </li>
            <li style="margin: 7px 30px;">
              <span class="hidden-xs"><h4  style="display: inline-block;color:#fff !important"> <strong>MIDDLEMILE </strong> PRO </h4></span>
              <span class="hidden-sm hidden-md hidden-lg"><h4  style="display: inline-block;color:#fff !important">  <strong>MM</strong>P</h4></span>
            </li>
        </ul>
        <?php if(!\Yii::$app->user->isGuest){?>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li title="Net trip request"><a href="/trip/manage/create"><i class="fa-fw mdi mdi-library-plus"></i><span class="hidden-sm">New enquiry</span></a></li>
            
            <li title="Trip Requests"><a href="/trip/manage"><i class="fa-fw mdi mdi-truck-delivery"></i></a></li>
            
            <li class="dropdown">
                 <a class="dropdown-toggle"  id="notification-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa-fw mdi mdi-bell-ring"></i>
                    <?php
                    $count = \frontend\modules\notification\models\Notifications::count();
                    if(!empty(intval($count))){ ?>
                        <span class="badge badge-danger notifications-count-notifications" ><?= $count; ?></span>
                    <?php }else{ ?>
                        <span class="badge badge-danger notifications-count-notifications" style="display: none;"></span>
                    <?php } ?>
                    <span class="caret"></span>
                  </a>
                
                <ul class="dropdown-menu dropdown-menu-right  animated flipInX" style="width:300px">
                      <li class="notification-items-list">
                        
                    </li>
                     <li role="separator" class="divider"></li>
                     <li class="show-all-link" ><a href="/notification/manage/index" class="text-center">All Notifications</a></li>
                  </ul>
            </li>
            

            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                    <!--<img src="../plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle">-->
                    <b class="hidden-xs"><?= ((isset($user->username) && !empty($user->username))?$user->username:$user->name); ?></b> <i class="ti-user hidden-lg hidden-md hidden-sm"></i> <span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <!--<div class="u-img"><img src="../plugins/images/users/varun.jpg" alt="user" /></div>-->
                            <div class="u-text">
                                <h4><?= ((isset($user->username) && !empty($user->username))?$user->username:$user->name); ?></h4>
                                <p class="text-muted"><?= $user->email; ?></p>
                            </div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/site/logout" data-method="post" ><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <?php } ?>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>

<?php
function generateTableRow($tableRow,$isHeader = NULL)
{
	$tagOpen = !empty($isHeader)?'<th>':'<td>';
	$tagClose = !empty($isHeader)?'</th>':'</td>';
	$html = '';
	if (is_array($tableRow[0])) {
		foreach ($tableRow as $subRow) {
			$html .= '<tr>';
			foreach ($subRow as $column) {
				$html .= $tagOpen.$column.$tagClose;
			}
			$html .= '</tr>';
		}
	} else {
		$html .= '<tr>';
		foreach ($tableRow as $column) {
			$html .= $tagOpen.$column.$tagClose;
		}
		$html .= '</tr>';
	}
	return $html;
}
?>
<div class="trip-view">
		<div class="row">
			<div class="col-md-12">
				<div class="white-box printableArea">
					<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive">
								<table class="table table-invoice">
										<?php
										if (isset($header) && !empty($header)) {
											echo '<thead>'.generateTableRow($header,1).'</thead>';
										}
										if (isset($body) && !empty($body)) {
											echo '<tbody>'.generateTableRow($body).'</tbody>';
										}
										if (isset($footer) && !empty($footer)) {
											echo '<tfoot>'.generateTableRow($footer).'</tfoot>';
										}
										?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>

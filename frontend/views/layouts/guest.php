<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\GuestAsset;
use common\widgets\Alert;

GuestAsset::register($this);
if (isset($_GET['webview'])) {
  $cookies = Yii::$app->response->cookies;
  // add a new cookie to the response to be sent
  $cookies->add(new \yii\web\Cookie([
      'name' => 'webview',
      'value' => true,
  ]));
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="/js/manifest.webmanifest"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <!-- Preloader -->
<!--    <div class="preloader">
      <div class="cssload-speeding-wheel"></div>
    </div>-->

    <?php // Alert::widget() ?>
    <?= $content ?>
    <div class="modal-add-button">
      <div class="col-sm-12 text-left p-0 text-center message-box">
        <br>
        <h4>Add MMP to your home screen.</h4>
        <br>
        <div class="row m-0">
          <div class="col-xs-6">
            <button type="button" class="btn btn-md btn-block btn-info btn-rounded m-0 text-uppercase waves-effect waves-light add-button" name="add-button">Add </button>
          </div>
          <div class="col-xs-6">
            <button type="button" class="btn btn-md btn-block btn-inverse btn-rounded m-0 text-uppercase waves-effect waves-light " name="cancel" onclick="$('.modal-add-button').hide();">Close</button>
          </div>
        </div>
        <br>
      </div>
    </div>

    <style media="screen">
      @media only screen and (max-width: 900px) {
        .modal-add-button{
          display:<?= isset($_GET['webview'])?'none !important':'block' ?>;
          position: fixed;
          bottom: 0px;
          background: rgba(16, 16, 16, 0.60);
          left: 0px;
          top:0px;
          right: 0px;
        }
      }
      @media only screen and (min-width: 901px) {
        .modal-add-button{
          display:none !important;
        }
      }

      .message-box{
        position: absolute;
        top: 40vh;
        left: 30px;
        right: 30px;
        background: #fff;
      }
    </style>

    <?php $this->registerJs("

    function getCookie(name) {
        var dc = document.cookie;
        var prefix = name + '=';
        var begin = dc.indexOf('; ' + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else
        {
            begin += 2;
            var end = document.cookie.indexOf(';', begin);
            if (end == -1) {
            end = dc.length;
            }
        }
        // because unescape has been deprecated, replaced with decodeURI
        //return unescape(dc.substring(begin + prefix.length, end));
        return decodeURI(dc.substring(begin + prefix.length, end));
    }

    function toggleA2HS() {
        var myCookie = getCookie('webview');
        console.log(myCookie);
        if (myCookie != null || myCookie !='' ) {
            $('.modal-add-button').hide();
        }
    }
    toggleA2HS();
    ") ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

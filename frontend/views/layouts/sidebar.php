<?php

 ?>
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
                </div>

                <!-- <div class="user-profile"></div> -->

                <ul class="nav" id="side-menu">

                    <?php if(!\Yii::$app->user->isGuest && \Yii::$app->user->identity->role != 1){?>
                    
                            <?php if(Yii::$app->commonFunction->accessToElement('dashboard')){ ?>
                            <li><a href="/site/index" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"></i> <span class="hide-menu">Dashboard</span></a></li>
                            <?php } ?>
                            <?php if(Yii::$app->commonFunction->accessToElement('tripRequest')){ ?>
                            <li><a href="/trip/manage" class="waves-effect"><i class="fa-fw mdi mdi-truck-delivery"></i> <span class="hide-menu">Trip Requests</span></a></li>
                            <li><a href="/trip/mytrip/index" class="waves-effect"><i class="fa-fw mdi mdi-account-check"></i> <span class="hide-menu">Driver Account Settlement</span></a></li>
                            <?php } ?>
                            <!--<li><a href="/order/manage/index" class="waves-effect"><i class="mdi mdi-account-multiple"></i> <span class="hide-menu">Orders</span></a></li> -->
                            <?php if(Yii::$app->commonFunction->accessToElement('customers')){ ?>
                            <li><a href="/customer/manage" class="waves-effect"><i class="mdi mdi-factory fa-fw"></i> <span class="hide-menu">Customers</span></a></li>
                            <?php } ?>

                            <?php if(Yii::$app->commonFunction->accessToElement('vendors')){ ?>
                            <li><a href="#" class="waves-effect"><i class="mdi mdi-ferry fa-fw"></i> <span class="hide-menu">Vendors<span class="fa arrow"></span></span></a>
                                <ul class="nav nav-second-level">
                                    <?php if(Yii::$app->commonFunction->accessToElement('vendorList')){ ?>
                                    <li><a href="/vendor/manage"><i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">Vendor List</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('vendorType')){ ?>
                                    <li><a href="/vendor/type"><i class="mdi mdi-format-list-bulleted-type fa-fw"></i> <span class="hide-menu">Vendor Type</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('stops')){ ?>
                                    <li><a href="/master/vendorcategory"><i class="mdi mdi-format-list-bulleted-type fa-fw"></i> <span class="hide-menu">Vendor Category</span></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if(Yii::$app->commonFunction->accessToElement('staff')){ ?>
                             <li><a href="/staff/manage" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i> <span class="hide-menu">Staff</span></a></li>
                            <?php } ?>
                            <?php if(Yii::$app->commonFunction->accessToElement('inventory')){ ?>
                              <li> <a href="#" class="waves-effect"><i class="mdi mdi-clipboard-text fa-fw"></i> <span class="hide-menu">Inventory<span class="fa arrow"></span></span></a>
                                  <ul class="nav nav-second-level">
                                      <li><a href="/inventory/tyre"><i  class="mdi mdi-checkbox-multiple-blank-circle fa-fw"></i> <span class="hide-menu">Tyres</span></a></li>
                                  </ul>
                              </li>
                            <?php } ?>
                            <?php if(Yii::$app->commonFunction->accessToElement('masterSettings')){ ?>
                            <li> <a href="#" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Master Settings<span class="fa arrow"></span></span></a>
                                <ul class="nav nav-second-level">
                                    <?php if(Yii::$app->commonFunction->accessToElement('states')){ ?>
                                    <li><a href="/master/state"><i  class="mdi mdi-google-maps fa-fw"></i> <span class="hide-menu">States</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('city')){ ?>
                                    <li><a href="/master/city"><i class="mdi mdi-map-marker fa-fw"></i> <span class="hide-menu">City</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('route')){ ?>
                                    <li><a href="/master/route"><i class="mdi mdi-road-variant fa-fw"></i> <span class="hide-menu">Route / Lane</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('vehicleType')){ ?>
                                    <li><a href="/master/vehicletype"><i class="mdi mdi-truck-trailer fa-fw"></i> <span class="hide-menu">Vehicle Type</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('stops')){ ?>
                                    <li><a href="/master/stop"><i class="mdi mdi-map-marker fa-fw"></i> <span class="hide-menu">Stops</span></a></li>
                                    <?php } ?>
                                    <?php if(Yii::$app->commonFunction->accessToElement('stops')){ ?>
                                    <li><a href="/master/customertype"><i class="mdi mdi-format-list-bulleted-type fa-fw"></i> <span class="hide-menu">Customer Type</span></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>
                            <?php if(Yii::$app->commonFunction->accessToElement('accounts')){ ?>
                            <li><a href="#" class="waves-effect"><i class="mdi mdi-cash-usd fa-fw"></i> <span class="hide-menu">Accounts<span class="fa arrow"></span></span></a>
                                <ul class="nav nav-second-level">
                                    <?php if(Yii::$app->commonFunction->accessToElement('account')){ ?>
                                    <li><a href="/account/manage/payable"><i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">Payable</span></a></li>
                                    <li><a href="/account/manage/receivable"><i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">Receivable</span></a></li>
                                    <!-- <li><a href="/account/manage/index"><i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">Balance Payable</span></a></li> -->
                                  <?php } ?>
                                </ul>
                            </li>
                            <li><a href="#" class="waves-effect"><i class="mdi mdi-receipt fa-fw"></i> <span class="hide-menu">POD<span class="fa arrow"></span></span></a>
                              <ul class="nav nav-second-level">
                                  <?php if(Yii::$app->commonFunction->accessToElement('pod')){ ?>
                                  <li><a href="/pod/manage/index"><i class="mdi mdi-format-list-bulleted-type fa-fw"></i> <span class="hide-menu">POD's pending</span></a></li>
                                  <li><a href="/pod/manage/collection"><i class="mdi mdi-format-list-bulleted-type fa-fw"></i> <span class="hide-menu">POD's collected</span></a></li>
                                  <li><a href="/pod/manage/manifests"><i class="mdi mdi-format-list-bulleted-type fa-fw"></i> <span class="hide-menu">POD's Manifest</span></a></li>
                                  <?php } ?>
                              </ul>
                            </li>
                            <?php } ?>
                            <?php if(Yii::$app->commonFunction->accessToElement('viewTracking')){ ?>
                              <li><a href="/tracking/manage" class="waves-effect"><i class="fa-fw mdi mdi-map"></i> <span class="hide-menu">Tracking</span></a></li>
                            <?php } ?>
                      
                    <?php } else  {?>
                            
                                <li><a href="/site/index" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"></i> <span class="hide-menu">Dashboard</span></a></li>
                                <li><a href="/trip/mytrip/index" class="waves-effect"><i class="fa-fw mdi mdi-truck-delivery"></i> <span class="hide-menu">My trips</span></a></li>
                                <li><a href="/trip/mytrip/index" class="waves-effect"><i class="fa-fw mdi mdi-all-inclusive"></i> <span class="hide-menu">New Ferry trip</span></a></li>
                           
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->

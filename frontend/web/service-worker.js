// Add cache
const newCache = '?v11';
const cacheStuff = [
  // Fixed CSS
  '/plugins/bower_components/toast-master/css/jquery.toast.css',
  '/plugins/bower_components/morrisjs/morris.css',
  '/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
  '/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css',
  '/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.css',
  '/plugins/bower_components/timepicker/bootstrap-timepicker.min.css',
  '/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css',
  '/theme_ampleadmin/css/animate.css',
  // CSS Dynamic/ Changable under version
  '/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css'+newCache,
  '/theme_ampleadmin/css/style.css'+newCache,
  '/theme_ampleadmin/css/colors/default.css'+newCache,
  '/theme_ampleadmin/css/spinners.css',
  // Fixed JS
  '/theme_ampleadmin/js/sidebar-nav.min.js',
  '/theme_ampleadmin/js/jquery.slimscroll.js',
  '/theme_ampleadmin/js/waves.js',
  '/theme_ampleadmin/js/jquery.toast.js',
  '/theme_ampleadmin/js/chartist.min.js',
  '/theme_ampleadmin/js/tooltip.min.js',
  '/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js',
  '/plugins/bower_components/moment/moment.js',
  '/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js',
  '/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
  '/plugins/bower_components/timepicker/bootstrap-timepicker.min.js',
  '/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js',
  // JS Dynamic/ Changable under version
  'theme_ampleadmin/js/custom.js'+newCache,
  //Images
  '/favicon.ico',
  '/images/middlemilrprologo.png',
  //'/images/login-register.jpg',
  // webmenifest
  '/js/manifest.webmanifest',
  // Fonts
  'https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900',
  '/theme_ampleadmin/css/icons/material-design-iconic-font/fonts/materialdesignicons-webfontdc99.html?v=1.8.36',
  '/theme_ampleadmin/less/icons/themify-icons/fonts/themify9f24.woff?-fvbane',
  '/theme_ampleadmin/less/icons/font-awesome/fonts/fontawesome-webfont3295.html?v=4.5.0',
  '/theme_ampleadmin/css/icons/material-design-iconic-font/css/materialdesignicons.min.css'
];

self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open(newCache).then(function(cache) {
       return cache.addAll(cacheStuff);
   })
 );
},function(err){
  console.log(err);
  return true;
});
// Clear outdated cache and keep only new cache
caches.keys().then(function(names) {
    for (let name of names){
      if (newCache !== name) {caches.delete(name);}
    }
});
self.addEventListener('fetch', function(e) {
  // console.log(e.request.url);
    e.respondWith(
      caches.open(newCache).then(function(cache) {
        return cache.match(e.request).then(function (response) {
          return response || fetch(e.request).then(function(response) {
            if (e.request.url.indexOf('https://fonts.gstatic.com') >= 0 || e.request.url.indexOf('/theme_ampleadmin/')  >= 0 || e.request.url.indexOf('/plugins/')  >= 0 || e.request.url.indexOf('.woff2')  >= 0  || e.request.url.indexOf('.woff')  >= 0  || e.request.url.indexOf('.ttf')  >= 0 || e.request.url.indexOf('.eot')  >= 0) {
                cache.put(e.request, response.clone());
            }
            return response;
          });
        });
      })
    );
});

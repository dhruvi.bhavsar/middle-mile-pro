
if('serviceWorker' in navigator) {
  // Registering service worker
  navigator.serviceWorker
           .register('/service-worker.js?v11')
           .then(function() { console.log('Service Worker Registered'); },function(error){
            console.log('service worker registration failed:',error);
          });

  // removing old service worker if any
  navigator.serviceWorker
      .getRegistrations().then(function(registrations) {
          for(let registration of registrations) {
              if (registration.waiting.scriptURL !== '') {
                registration.unregister();
              }
          }
      }).catch(function(err) {
        // console.log(err)
      });
}

// Code to handle install prompt on desktop

let deferredPrompt;
const addBtn = document.querySelector('.add-button');
const modalAddBtn = document.querySelector('.modal-add-button');
// addBtn.style.display = 'none';
  window.addEventListener('beforeinstallprompt', (e) => {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    // Update UI to notify the user they can add to home screen
    modalAddBtn.style.display = 'block';

    addBtn.addEventListener('click', (e) => {
      // hide our user interface that shows our A2HS button
      modalAddBtn.style.display = 'none';
      // Show the prompt
      deferredPrompt.prompt();
      // Wait for the user to respond to the prompt
      deferredPrompt.userChoice.then((choiceResult) => {
          if (choiceResult.outcome === 'accepted') {
            console.log('User accepted the A2HS prompt');
          } else {
            console.log('User dismissed the A2HS prompt');
          }
          deferredPrompt = null;
        });
    });
  });

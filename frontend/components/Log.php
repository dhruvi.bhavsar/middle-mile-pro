<?php
namespace frontend\components;
use Yii;
use yii\base\Component;
use frontend\models\ActivityLog;

class Log extends Component{

	public $performed_by;
	public $created_at;
	public $entity_id;
	public $entity_type;
	public $log;

	public function init(){
		parent::init();
		try {
				$this->performed_by = Yii::$app->user->identity->id;
		} catch (\Exception $e) {
				$this->performed_by = 0;
		}
		$this->created_at = date('Y-m-d H:i:s');
	}

	public function add($id=null,$type=null,$log=null){
		$this->entity_id = $id;
		$this->entity_type = $type;
		$this->log = $log;
		$model = new ActivityLog();
		$model->performed_by = $this->performed_by;
		$model->created_at = $this->created_at;
		$model->entity_id = $this->entity_id;
		$model->entity_type = $this->entity_type;
		$model->log = $this->log;
		$model->save(false);
	}
}

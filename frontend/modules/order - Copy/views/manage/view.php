<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\order\models\Orderslip */

$this->title = "#".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orderslip-view">

    <p>
        <?= Html::a('Cancel', ['index'],['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Id',
                'value' => '#' . $model->id
            ],
            'company',
            [
                'label' => 'Vendor',
                'value' => $model->vendor->name
            ],
            [
                'label' => 'Lane',
                'value' => $model->lane->name
            ],
            'origin',
            'destination',
            'company_rate',
            'company_advance',
            'company_balance',
            'vendor_rate',
            'vendor_advance',
            'vendor_balance',
            'truck_type',
            'truck_number',
            'placement_datetime',
            'driver_name',
            'driver_mobile',
            'truck_other_details',
            'note:ntext',
            'transist_time',
            'special_instructions:ntext',
        ],
    ]) ?>

</div>

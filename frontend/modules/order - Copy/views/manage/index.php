<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orderslip-index">

    

    <p>
        <?= Html::a('Add Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
                'attribute' => 'Number',
                'value' => function($model) {
                    return $model->id;
                }
            ],
            'company',
             [
                'attribute' => 'Vendor',
                'value' => function($model) {
                    return $model->vendor->name;
                }
            ],
             [
                'attribute' => 'Lane',
                'value' => function($model) {
                    return $model->lane->name;
                }
            ],
            'origin',
            'destination',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

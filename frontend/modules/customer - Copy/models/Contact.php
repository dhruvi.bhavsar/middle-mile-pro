<?php

namespace frontend\modules\customer\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "customer_persons".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $name
 * @property string $designation
 * @property string $email
 * @property string $mobile
 * @property integer $status
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_persons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'name'], 'required'],
            [['customer_id', 'status'], 'integer'],
            [['name', 'designation', 'email', 'mobile'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'name' => 'Name',
            'designation' => 'Designation',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'status' => 'Status',
        ];
    }

    public static function customerContacts($cust_id){ 
       $query = self::find()->where(['status'=>1,'customer_id'=>$cust_id]); 
       $contacts = new ActiveDataProvider([ 
           'query' => $query, 
           'sort' => false, 
       ]); 
       return $contacts; 
    }

    public static function ContactList($cust_id){
        $data= self::find()->where(['status'=>1,'customer_id'=>$cust_id]);
        $data = $data->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }
}

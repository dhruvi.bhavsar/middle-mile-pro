<?php

namespace frontend\modules\customer;
use Yii;

/**
 * customer module definition class
 */
class Customer extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\customer\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->commonFunction->accessToAction('customers');
        parent::init();

        // custom initialization code goes here
    }
}

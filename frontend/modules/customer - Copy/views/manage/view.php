<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\customer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-view">

	<div class="white-box">
		<p class="text-muted m-b-40"> <?= Html::a('Back', ['index'],['class' => 'btn btn-default']) ?></code></p>
		<!-- Nav tabs -->
		<ul class="nav nav-tabs customtab" role="tablist">
			<li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Details</span></a></li>
			<li role="presentation" class=""><a href="#contact-person" aria-controls="contact-person" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Contact Person</span></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="details">
			<p class="text-muted m-b-40">
				<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
				<?= Html::a('Delete', ['delete', 'id' => $model->id], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => 'Are you sure you want to delete this item?',
						'method' => 'post',
					],
				]) ?>
			</p>
			<?= DetailView::widget([
				'model' => $model,
				'attributes' => [
					//'id',
					'name',
					'address',
					//'status',
				],
			]) ?>
				<div class="clearfix"></div>
			</div>
			<div role="tabpanel" class="tab-pane" id="contact-person">
			<p class="text-muted m-b-40"> <?= Html::a('Add Contact', ['/customer/contact/create','cust' => $model->id], ['class' => 'btn btn-success']) ?> </p>
    <?= GridView::widget([
        'dataProvider' => $contactModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /* 'id', */
            /* 'customer_id', */
            'name',
            'mobile',
            'email:email',
            'designation',
            // 'status',

            [
			'class'    => 'yii\grid\ActionColumn',
			'template' => '{update} {delete}',
			'buttons'  => [
				'update' => function ($url, $model) {
					$url = Url::to(['/customer/contact/update', 'id' => $model->id,'cust'=>$model->customer_id]);
					return Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => 'update']);
				},
				'delete' => function ($url, $model) {
					$url = Url::to(['/customer/contact/delete', 'id' => $model->id,'cust'=>$model->customer_id]);
					return Html::a('<span class="fa fa-trash"></span>', $url, [
						'title'        => 'delete',
						'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
						'data-method'  => 'post',
					]);
				},
				]
			],
        ],
    ]); ?>
			</div>
		</div>
	</div>

</div>

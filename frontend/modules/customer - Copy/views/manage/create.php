<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\customer */

$this->title = 'Add Customer';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

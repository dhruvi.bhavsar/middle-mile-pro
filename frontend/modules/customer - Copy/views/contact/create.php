<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\Contact */

$this->title = 'Add Contact';
$this->params['breadcrumbs'][] = ['label' => 'Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace frontend\modules\notification\Models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property int $sender_id
 * @property int $receiver_id
 * @property string $message
 * @property string $url
 * @property int $status
 * @property string $datetime
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender_id', 'receiver_id', 'message', 'url', 'datetime'], 'required'],
            [['sender_id', 'receiver_id', 'status'], 'integer'],
            [['message', 'url'], 'string'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_id' => 'Sender ID',
            'receiver_id' => 'Receiver ID',
            'message' => 'Message',
            'url' => 'Url',
            'status' => 'Status',
            'datetime' => 'Datetime',
        ];
    }

    public static function newEntry($receiver_id,$title,$message,$url,$params=[]){
        $model = new Notifications();
        $model->sender_id = Yii::$app->user->id;
        $model->receiver_id = $receiver_id;
        $model->title = $title;
        $model->message = $message;
        $model->url = $url;
        $model->datetime = date("Y-m-d H:i:s");
        $model->status = 0;
        $model->save(false);
    }

    public static function count(){
       return self::find()->where(['receiver_id'=>Yii::$app->user->id,'status'=>'0'])->count();
    }

}

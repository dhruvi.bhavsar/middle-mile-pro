<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="white-box notifications-index col-md-8 col-md-offset-2 col-sm-12">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showHeader'=> false,
        'tableOptions' => ['class' => 'table '],
        'columns' => [
            [
                  'attribute' => '',
                  'format' => 'html',
                  'value' => function($model) {
                          return "<a href='/notification/manage/redirect?url=" . $model->url . "&id=" . $model->id . "'><strong>" . $model->title ."</strong> - " . $model->message . "</a>";
                      }
            ]            
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showHeader'=> false,
        'layout' => '{items}',
        'tableOptions' => ['class' => 'table '],
        'columns' => [
            [
                  'attribute' => '',
                  'format' => 'html',
                  'value' => function($model) {
                          return "<a href='/notification/manage/redirect?url=" . $model->url . "&id=" . $model->id . "'><strong>" . $model->title ."</strong> - " . $model->message . "</a>";
                      }
           ]            
        ],
 ]); ?>

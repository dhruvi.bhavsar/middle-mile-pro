<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>
	<?php
		if(!empty(Yii::$app->request->get('cust'))){
			$model->customer_id = Yii::$app->request->get('cust');
		}else{
			return Yii::$app->getResponse()->redirect('/customer/manage/index');
		}
	?>
    <?= $form->field($model, 'customer_id')->hiddenInput(['value'=>$model->customer_id])->label(false) ?>

<div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'name')->textInput() ?>
	</div>
	<div class="col-md-6">
    <?= $form->field($model, 'designation')->textInput() ?>
	</div>
</div>

<div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	</div>
	<div class="col-md-6">
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
	</div>
</div>
<div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'is_admin')->checkbox() ?>
	</div>
	<div class="col-md-6"></div>
</div>



    <?= $form->field($model, 'status')->hiddenInput(['value'=>1])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
    tbody tr {cursor: pointer;}
    tbody tr:hover {cursor: pointer; color:#000;}
    ");
?>
<div class="customer-index">

    <p>
        <?= Html::a('Add Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'rowOptions'   => function ($model, $key, $index, $grid) {
            return [
                'data-id' => $model->id,
                'onclick' => 'location.href="/customer/manage/view?id="+$(this).data("id");',
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute'=>'id',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">MMP Account No : </label>'.$model->id;
              }
            ],
            [
              'attribute'=>'name',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Name : </label>'.$model->name;
              }
            ],
            [
              'attribute'=>'code',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Code : </label>'.$model->code;
              }
            ],
            [
              'attribute'=>'address',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Address : </label>'.$model->address;
              }
            ],
            [
              'attribute'=>'email',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Email : </label>'.$model->email;
              }
            ],
            [
              'attribute'=>'mobile',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Mobile : </label>'.$model->mobile;
              }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update} {delete}'],
        ],
    ]); ?>
</div>

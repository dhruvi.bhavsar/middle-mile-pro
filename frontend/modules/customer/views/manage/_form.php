<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
         <?= $form->field($model, 'intended_name')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
         <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    </div>
</div>
    
<div class="row">
    <div class="col-md-4">
      <?= $form->field($model, 'state_code')->dropdownList(frontend\modules\master\models\State::gstStateList(),['prompt'=>'Select State']) ?>
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'gst_no')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
      <?php if (empty($model->gst_rate)) {
        $model->gst_rate = 0;
      } ?>
    <?= $form->field($model, 'gst_rate')->textInput() ?>
    </div>
<!--    <div class="col-md-6">
    <?= $form->field($model, 'pan_no')->textInput(['maxlength' => true]) ?>
    </div>-->
</div>

<!--<div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'bank_ac_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'bank_ac')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'bank_ifsc')->textInput(['maxlength' => true]) ?>
    </div>
</div>-->

<div class="row">
    <div class="col-md-6">

	<?= $form->field($model, 'type')->widget(Select2::classname(), [
		'data' => \frontend\modules\master\models\CustomerType::CustomerTypeList(),
		'options' => ['placeholder' => 'Select a Customer ...',
				'onChange' => "$('#trip-contact_person_id').load('/customer/contact/list',{cust_id:$('#trip-company_id').val()});",
		],
		'pluginOptions' => [
			'allowClear' => true,
		],
	]);
	?>

    </div>
    <div class="col-md-6">
      <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
</div>
  <div class="row">
    <div class="col-md-6">
      <?= $form->field($model, 'credit_period')->textInput(['maxlength' => true]) ?>
    </div>
<!--    <div class="col-md-6">
        <?= $form->field($model, 'preferred_notification_duration')->textInput() ?>
    </div>-->
  </div>
  <div class="row">
      <div class="col-md-4">
      <?= $form->field($model, 'detention_rate_1')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-4">
      <?= $form->field($model, 'detention_rate_2')->textInput(['maxlength' => true]) ?>
      </div>
    <div class="col-md-4">
    <?= $form->field($model, 'detention_rate_3')->textInput(['maxlength' => true]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'internal_reference')->textArea(['maxlength' => true,'rows'=>4]) ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'note')->textArea(['maxlength' => true,'rows'=>4]) ?>
    </div>
  </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div></div></div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="col-md-3">
      <?= $form->field($model, 'id') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'name') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'code') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'address') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'email') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'mobile') ?>
    </div>
    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'gst_no') ?>

    <?php // echo $form->field($model, 'pan_no') ?>

    <?php // echo $form->field($model, 'bank_ac') ?>

    <?php // echo $form->field($model, 'bank_ac_name') ?>

    <?php // echo $form->field($model, 'bank_ifsc') ?>

    <?php // echo $form->field($model, 'bank_name') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/customer/manage/index"']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

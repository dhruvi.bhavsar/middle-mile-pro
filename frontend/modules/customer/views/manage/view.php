<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\customer\models\customer */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-view">

    <div class="white-box">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs customtab" role="tablist">
            <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Details</span></a></li>
            <li role="presentation" class=""><a href="#contact-person" aria-controls="contact-person" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Contact Person</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="details">
                <p class="text-muted m-b-40">
                    <?= Html::a('Update', ['update', 'id' => $model->id],
                        ['class' => 'btn btn-primary']) ?>
                    <?=
                    Html::a('Delete', ['delete', 'id' => $model->id],
                        [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </p>


                <div class="row">
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Name</label><br><?= $model->name ?></div>
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Intended Name</label><br><?= $model->intended_name ?></div>
                </div>

                <div class="row">
                    <div class="col-md-12"><label class="control-label" for="trip-trip_date">Address</label><br><?= $model->address ?></div>
                </div>

                <div class="row">
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Customer Type</label><br><?= $model->customerType->name ?></div>
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Code</label><br><?= $model->code ?></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Email</label><br><?= $model->email ?></div>
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Mobile</label><br><?= $model->mobile ?></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Gst No</label><br><?= $model->gst_no ?></div>
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Gst Rate</label><br><?= $model->gst_rate ?> % </div>
<!--<div class="col-md-6"><label class="control-label" for="trip-trip_date">Pan No</label><br><?= $model->pan_no ?></div>-->
                </div>
                
                <div class="row">

                    <div class="col-md-6">
<?php $gstStateList                  = frontend\modules\master\models\State::gstStateList(); ?>
                        <label class="control-label" for="trip-trip_date">State & GST State Code</label><br><?= !empty($model->state_code)
        ? $gstStateList[$model->state_code].' ('.$model->state_code.')' : ''; ?>
                    </div>
                </div>

                <!--		<div class="row">
                                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Account Name</label><br><?= $model->bank_ac_name ?></div>
                                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Account Number</label><br><?= $model->bank_ac ?></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Bank Name</label><br><?= $model->bank_name ?></div>
                                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Branch (IFSC)</label><br><?= $model->bank_ifsc ?></div>
                                </div>-->
                <hr/>
                <div class="row">
                    <div class="col-md-6"><label class="control-label" for="trip-trip_date">Credit Period</label><br><?= $model->credit_period ?> days</div>
                                <!--<div class="col-md-6"><label class="control-label" for="trip-trip_date">Preferred Notification Duration</label><br> Every <?= $model->preferred_notification_duration ?> hours </div>-->
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-4"><label class="control-label" for="trip-trip_date">Detention Rate(1-2 days)</label><br><?= $model->detention_rate_1 ?></div>
                    <div class="col-md-4"><label class="control-label" for="trip-trip_date">Detention Rate(3-4 days)</label><br> <?= $model->detention_rate_2 ?> </div>
                    <div class="col-md-4"><label class="control-label" for="trip-trip_date">Detention Rate(>4 days)</label><br><?= $model->detention_rate_3 ?> days</div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12"><label class="control-label" for="trip-trip_date">Internal Reference</label><br><?= $model->internal_reference ?></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><label class="control-label" for="trip-trip_date">Note</label><br><?= $model->note ?></div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="contact-person">
                <p class="text-muted m-b-40"> <?= Html::a('Add Contact',
    ['/customer/contact/create', 'cust' => $model->id],
    ['class' => 'btn btn-success']) ?> </p>
                <?=
                GridView::widget([
                    'options' => ['class' => 'white-box'],
                    'tableOptions' => ['class' => 'table table-responsive'],
                    'emptyText' => '<center>No records</center>',
                    'dataProvider' => $contactModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        /* 'id', */
                        /* 'customer_id', */
                        'name',
                        'mobile',
                        'email:email',
                        'designation',
                        // 'status',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    $url = Url::to(['/customer/contact/update', 'id' => $model->id,
                                            'cust' => $model->customer_id]);
                                    return Html::a('<i class="mdi mdi-pencil" style="line-height: 0.8;"></i>',
                                            $url,
                                            ['class' => 'btn btn-circle btn-flat waves-effect',
                                            'title' => 'update']);
                                },
                                'delete' => function ($url, $model) {
                                    $url = Url::to(['/customer/contact/delete', 'id' => $model->id,
                                            'cust' => $model->customer_id]);
                                    return Html::a('<i class="mdi mdi-delete text-danger" style="line-height: 0.8;"></i>',
                                            $url,
                                            [
                                            'title' => 'delete',
                                            'class' => 'btn btn-circle btn-flat waves-effect',
                                            'data-confirm' => Yii::t('yii',
                                                'Are you sure you want to delete this item?'),
                                            'data-method' => 'post',
                                    ]);
                                },
                            ]
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>

</div>

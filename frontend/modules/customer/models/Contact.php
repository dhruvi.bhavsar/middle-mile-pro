<?php

namespace frontend\modules\customer\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "customer_persons".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $name
 * @property string $designation
 * @property string $email
 * @property string $mobile
 * @property integer $status
 */
class Contact extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $role = 0;
    public $team = 0;
    public $username;
    const STATUS_ACTIVE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_persons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'name'], 'required'],
            [['customer_id', 'status'], 'integer'],
            [['name', 'designation', 'email', 'mobile'], 'string', 'max' => 100],
            [['role','team','is_admin'], 'safe'],
            // HTMLPurifier
            [['id','customer_id','name','designation','email', 'mobile','is_admin','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'name' => 'Name',
            'designation' => 'Designation',
            'email' => 'Email',
            'is_admin' => 'Is Admin',
            'mobile' => 'Mobile',
            'status' => 'Status',
        ];
    }

    public static function customerContacts($cust_id){
       $query = self::find()->where(['status'=>1,'customer_id'=>$cust_id]);
       $contacts = new ActiveDataProvider([
           'query' => $query,
           'sort' => false,
       ]);
       return $contacts;
    }

    public static function ContactList($cust_id){
        $data= self::find()->where(['status'=>1,'customer_id'=>$cust_id])->asArray()->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id'=>$id, 'status'=>self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (empty($this->password_hash)) {

          $model = new \frontend\models\PasswordResetRequestForm();
          $model->load(['PasswordResetRequestForm'=>['email'=>$this->email]]);
          if ($model->sendEmail()) {
              \Yii::$app->session->setFlash('success', 'Welcome to Middle Mile Pro. We have sent you email to set password. Kindly check your inbox.');
          } else {
              \Yii::$app->session->setFlash('error', 'Sorry, we are unable process your request.');
          }
          return false;
        }
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}

<?php

namespace frontend\modules\customer\models;

use Yii;
use frontend\modules\master\models\CustomerType;
use frontend\modules\master\models\MasterVendorType;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $mobile
 * @property integer $type
 * @property string $note
 * @property string $gst_no
 * @property string $pan_no
 * @property string $bank_ac
 * @property string $bank_ac_name
 * @property string $bank_ifsc
 * @property string $bank_name
 * @property integer $status
 frontend\modules\customer\models\Customer
 */
class Customer extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $role = 0;
    public $team = 0;
    public $username;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address','intended_name', 'type'], 'required'],
            ['bank_ac', 'unique', 'targetClass' => 'frontend\modules\customer\models\Customer', 'message' => 'This Account No. has already been taken.'],
            [['type', 'status','credit_period','state_code'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['address', 'note'], 'string', 'max' => 200],
            [['internal_reference'], 'string', 'max' => 255],
            [['email','code', 'mobile'], 'string', 'max' => 50],
            [['preferred_notification_duration'],'number'],
            [['gst_no', 'pan_no', 'bank_ac', 'bank_ac_name', 'bank_ifsc', 'bank_name'], 'string', 'max' => 30],
            [['gst_rate','detention_rate_1','detention_rate_2','detention_rate_3'],'number'],
            ['gst_rate', 'default', 'value' => 0],
            [['preferred_notification_duration'], 'default', 'value' => 1],
            [['credit_period','detention_rate_1','detention_rate_2','detention_rate_3'], 'default', 'value' => 0],
            [['auth_key','password_hash','password_reset_token'], 'string', 'max' => 255],
            [['role','team'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'MMP Account No',
            'code' => 'Code',
            'name' => 'Name',
            'address' => 'Address',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'type' => 'Type',
            'note' => 'Note',
            'gst_rate' => 'GST Rate (%)',
            'gst_no' => 'Gst No',
            'pan_no' => 'Pan No',
            'bank_ac' => 'Account Number',
            'bank_ac_name' => 'Account Name',
            'bank_ifsc' => 'Branch (IFSC)',
            'bank_name' => 'Bank Name',
            'credit_period' => 'Credit Period(Days)',
            'preferred_notification_duration'=>'Preferred Notification Duration',
            'state_code'=>'State',
            'detention_rate_1'=>'Detention Rate(1-2 days)',
            'detention_rate_2'=>'Detention Rate(3-4 days)',
            'detention_rate_3'=>'Detention Rate(>4 days)',
            'internal_reference'=>'Internal Reference',
            'status' => 'Status',
        ];
    }

    public function getCustomerType()
    {
        return $this->hasOne(CustomerType::className(), ['id' => 'type']);
    }

    public static function CustomerList($params=[]){
        $data= self::find()
        ->select(['(case when (address <> "") THEN CONCAT(name," (",address,")") ELSE  name END) name','id'])
        ->where(['status'=>1]);
        $data = $data->asArray()->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

    public static function customerNotificationDuration(){
        $data= self::find()
        ->select(['preferred_notification_duration','id'])
        ->where(['status'=>1])
        ->asArray()->all();
        return \yii\helpers\ArrayHelper::map($data,'id','preferred_notification_duration');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (empty($this->password_hash)) {

          $model = new \frontend\models\PasswordResetRequestForm();
          $model->load(['PasswordResetRequestForm'=>['email'=>$this->email]]);
          if ($model->sendEmail()) {
              \Yii::$app->session->setFlash('success', 'Welcome to Middle Mile Pro. We have sent you email to set password. Kindly check your inbox.');
          } else {
              \Yii::$app->session->setFlash('error', 'Sorry, we are unable process your request.');
          }
          return false;
        }
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}

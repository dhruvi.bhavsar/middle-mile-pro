<?php

namespace frontend\modules\customer\controllers;

use Yii;
use frontend\modules\customer\models\Contact;
use frontend\modules\customer\models\Customer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller
{
    public $model_path = 'frontend\modules\customer\models\Contact';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action){
        if($action->id == 'list'){$this->enableCsrfValidation = false;}
        return parent::beforeAction($action);
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Contact::find()->where(['status'=>1]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!empty(Yii::$app->request->get('cust'))){
            $customer = $this->findCustomer(Yii::$app->request->get('cust'));
        }

        $model = new Contact();
        $model->auth_key = Yii::$app->security->generateRandomString();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->activity->add($model->id,$this->model_path,'Customer Contact Person Created');
            return $this->redirect(['/customer/manage/view', 'id' => Yii::$app->request->get('cust')]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$cust)
    {
        if(!empty(Yii::$app->request->get('cust'))){
            $customer = $this->findCustomer(Yii::$app->request->get('cust'));
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             Yii::$app->activity->add($id,$this->model_path,'Contact Person Details Updated');
            return $this->redirect(['/customer/manage/view', 'id' => $cust]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$cust)
    {
        if(!empty(Yii::$app->request->get('cust'))){
            $customer = $this->findCustomer(Yii::$app->request->get('cust'));
        }
        $model = $this->findModel($id);
        $model->status = 2;
        if($model->save(false)){
            Yii::$app->activity->add($id,$this->model_path,'Contact Person Deleted');
        }
        return $this->redirect(['/customer/manage/view', 'id' => $cust]);
    }

    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCustomer($cust){
        $model = Customer::findOne(['status'=>1,'id'=>$cust]);
        if($model !== null){
            return $model;
        }else{
            return $this->redirect('\customer\manage\index');
        }
    }

   public function actionList1()
   {
       // $post = Yii::$app->request->post();
       // $options = "<option>Select Customer</option>";
       // $where = ['customer_persons.status'=>1];
       // $model = Contact::find()->select(['id','name']);

       // if(!empty($post) && isset($post['cust_id'])){
       //     $where['customer_persons.customer_id'] = $post['cust_id'];
       // }
       // $model = $model->where($where)->asArray()->all();

       //     if(!empty($model)){
       //     foreach ($model as $obj){
       //         $options.="<option value='" . $obj['id'] ."' >" .($obj['name']). "</option>";
       //         }
       //     }
       //     var_dump($options);
           // return $options;

   }

   public function actionList($id){
Yii::$app->response->format = Response::FORMAT_JSON;
       $post = Yii::$app->request->post();
       $options = "<option>Select Customer</option>";
       $where = ['customer_persons.status'=>1];
       $model = Contact::find();
        $data = [['id' => '', 'text' => '']];

       if(!empty($id) && isset($id)){
           $where['customer_persons.customer_id'] = $id;
       }
       $model = $model->where($where)->asArray()->all();

           if(!empty($model)){
           foreach ($model as $obj){
               $data[] = ['id' => $obj['id'], 'text' => ($obj['name'])];
               }
           }
           // echo "<pre>";print_r($data); die();
       return ['data' => $data];




        // Yii::$app->response->format = Response::FORMAT_JSON;
        // $model = Contact::find()->andWhere(['customer_id' => $id])->all();
        // $data = [['id' => '', 'name' => '']];
        // foreach ($model as $person) {
        //     $data[] = ['id' => $person->id, 'name' => $person->code];
        // }
        // // return \yii\helpers\ArrayHelper::map($data,'id','name');
        // return ['data' => $data];
   }

}

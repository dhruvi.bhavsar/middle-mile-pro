<?php

namespace frontend\modules\order\models;

use Yii;

/**
 * This is the model class for table "orderslip".
 *
 * @property integer $id
 * @property string $number
 * @property integer $vendor
 * @property integer $company
 * @property integer $email
 * @property string $origin
 * @property double $loading_lat
 * @property double $loading_lon
 * @property string $destination
 * @property double $unloading_lat
 * @property double $unloading_lon
 * @property integer $lane
 * @property integer $truck_type
 * @property string $truck_number
 * @property string $placement_datetime
 * @property string $driver_name
 * @property string $driver_mobile
 * @property string $truck_other_details
 * @property double $rate
 * @property double $advance
 * @property double $balance
 * @property string $note
 * @property string $transist_time
 * @property string $special_instructions
 */
class Orderslip extends \yii\db\ActiveRecord
{
    public $vendor_code;
    public $note = "The freight rate agreed is all inclusive and no additional charges to be paid OD to be submitted within 5 days of delivery. In case of delay , penalty of Rs 200/day will be charged.";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orderslip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // 'truck_type', 'truck_number', 'placement_datetime', 'driver_name', 'driver_mobile', 'transist_time'
            [[ 'vendor_id', 'company', 'lane_id',   'company_rate',   'vendor_rate'], 'required'],
            [[  'lane_id'], 'integer'],
            [['loading_lat', 'loading_lon', 'unloading_lat', 'unloading_lon', 'company_rate','vendor_rate','company_balance','vendor_balance'], 'number'],
            [['placement_datetime','company_advance','vendor_advance'], 'safe'],
            [['note', 'special_instructions'], 'string'],
            [['number', 'driver_name', 'driver_mobile', 'transist_time'], 'string', 'max' => 50],
            [[ 'origin', 'destination'], 'string', 'max' => 100],
            [['truck_number'], 'string', 'max' => 20],
            [['truck_other_details'], 'string', 'max' => 200],

            // HTMLPurifier
            [['id','number','vendor_id','company','origin','loading_lat','loading_lon','destination','unloading_lat','unloading_lon','lane_id','truck_type','truck_number','placement_datetime','driver_name','driver_mobile','truck_other_details','note','transist_time','special_instructions'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'vendor_id' => 'Vendor Name',
            'company' => 'Company',
            'origin' => 'Loading Location',
            'loading_lat' => 'Loading Lat',
            'loading_lon' => 'Loading Lon',
            'destination' => 'Destination',
            'unloading_lat' => 'Unloading Lat',
            'unloading_lon' => 'Unloading Lon',
            'lane_id' => 'Lane',
            'truck_type' => 'Truck Type',
            'truck_number' => 'Truck Number',
            'placement_datetime' => 'Placement Datetime',
            'driver_name' => 'Driver Name',
            'driver_mobile' => 'Driver Mobile',
            'truck_other_details' => 'Truck Other Details',
            'note' => 'Note',
            'transist_time' => 'Transist Time',
            'special_instructions' => 'Special Instructions',
        ];
    }


    public function getVendor(){
        return $this->hasOne(\frontend\modules\master\models\Vendor::className(), ['id' => 'vendor_id']);
     }

    public function getLane(){
        return $this->hasOne(\frontend\modules\master\models\Route::className(), ['id' => 'lane_id']);
     }


}

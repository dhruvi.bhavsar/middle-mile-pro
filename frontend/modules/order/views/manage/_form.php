<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model frontend\modules\order\models\Orderslip */
/* @var $form yii\widgets\ActiveForm */

$vendorList = frontend\modules\master\models\Vendor::VendorList(true);

?>

<div class="orderslip-form">

    <?php $form = ActiveForm::begin(); ?>

   <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'company')->textInput() ?>
        </div>
       <div class="col-md-6">
          <?=  $form->field($model, 'vendor_id')->widget(Select2::classname(), [
                 'data'=> \yii\helpers\ArrayHelper::map($vendorList,'id','text'),
                 'options' => ['placeholder' => 'Select vendor','multiple'=>false],
                 'pluginOptions' => [
                     'allowClear' => true,
                     'tags' => true,
                             'templateSelection' => new JsExpression('function (selected) {
                                 console.log(selected);
                                 return selected.text;
                     }'),
                 ],
             ]);
        ?>
       </div>
   </div>



   <div class="row">
        <div class="col-md-6">
          <?= $form->field($model, 'company_rate')->textInput() ?>
          <?= $form->field($model, 'company_advance')->textInput(['data-calculate'=>'company']) ?>
          <?= $form->field($model, 'company_balance')->textInput() ?>
        </div>

        <div class="col-md-6">
             <?= $form->field($model, 'vendor_rate')->textInput() ?>
             <?= $form->field($model, 'vendor_advance')->textInput(['data-calculate'=>'vendor']) ?>
             <?= $form->field($model, 'vendor_balance')->textInput() ?>
        </div>
   </div>


    <?= $form->field($model, 'lane_id')->dropDownList(frontend\modules\master\models\Route::RouteList(),['prompt'=>"Select Lane/Route"]); ?>

    <hr />

    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'origin')->textInput(['maxlength' => true]) ?>
        <?php /* $form->field($model, 'loading_lat')->textInput() ?>
        <?= $form->field($model, 'loading_lon')->textInput() */ ?>
       </div>
       <div class="col-md-6">
        <?= $form->field($model, 'destination')->textInput(['maxlength' => true]) ?>
        <?php /* $form->field($model, 'unloading_lat')->textInput() ?>
        <?= $form->field($model, 'unloading_lon')->textInput()  */ ?>
      </div>
    </div>

   <div class="row">
      <div class="col-md-6">
            <?= $form->field($model, 'truck_type')->textInput() ?>
       </div>
       <div class="col-md-6">
            <?= $form->field($model, 'truck_number')->textInput(['maxlength' => true]) ?>
      </div>
    </div>

    <?= $form->field($model, 'truck_other_details')->textInput(['maxlength' => true]) ?>

   <div class="row">
       <div class="col-md-6">
        <?= $form->field($model, 'driver_name')->textInput(['maxlength' => true]) ?>
       </div>
       <div class="col-md-6">
        <?= $form->field($model, 'driver_mobile')->textInput(['maxlength' => true]) ?>
      </div>
    </div>

    <hr />

   <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'placement_datetime')->textInput(['class'=>'form-control datepicker'])->label("Placement Date"); ?>
       </div>
       <div class="col-md-6">
        <?= $form->field($model, 'transist_time')->textInput(['maxlength' => true]) ?>
       </div>
    </div>

   <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
       </div>
       <div class="col-md-6">
    <?= $form->field($model, 'special_instructions')->textarea(['rows' => 6]) ?>
       </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$this->registerJs("

    window.vendorList = " .json_encode($vendorList) . ";

    $('#orderslip-company_advance,#orderslip-vendor_advance').on('keyup',function(){
        var elem = $(this);

        var updateFieldPrefix = elem.attr('data-calculate');

        var rate = Number($('#orderslip-' + updateFieldPrefix + '_rate').val());
        var balance = $('#orderslip-' + updateFieldPrefix + '_balance');
        var value = Number(elem.val().replace('%',''));

        if(String(value).match(/[^0-9%]/g)){
            balance.val(rate);
            return false;
        }

        if(elem.val().substr(-1)=='%'){
            if( value > 100 ){
                bal = rate;
            }else{
                bal = (rate - (rate * value/100));
            }
        }else{
            bal = rate - value;
        }
        balance.val(bal);
    });



    ", \yii\web\View::POS_END);

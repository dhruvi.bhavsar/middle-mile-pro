<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\order\models\Orderslip */

$this->title = 'Update Order: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="orderslip-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

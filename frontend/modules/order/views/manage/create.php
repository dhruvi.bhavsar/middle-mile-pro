<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\order\models\Orderslip */

$this->title = 'Add Order';
$this->params['breadcrumbs'][] = ['label' => 'Order', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orderslip-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Driver Trips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="index">
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <h2>Load trips</h2>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive mobile-table'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $loadtrips,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Name',
                'value' => function ($model) {
                    return $model['username'] . " - ". $model['mobile'];
                }
            ],
            [
                'label' => 'From',
                'value' => function ($model) {
                    return $model['from'];
                }
            ],
            [
                'label' => 'To',
                'value' => function ($model) {
                     return $model['to'];
                }
            ],
            [
                'label' => 'Vehicle',
                'value' => function ($model) {
                     return $model['registration_number'];
                }
            ],
            [
                'label' => 'Date',
                'value' => function ($model) {
                     return date("d M Y",strtotime($model['trip_date']));
                }
            ],
            [
                'label' => 'Total Kms / Charges',
                'value' => function ($model) {
                    $trackingEvents = json_decode($model['remarks'],true);
                    if(!empty($trackingEvents)){
                        $kmdiffrence = 0;
                        $kmreading = 0;
                        foreach($trackingEvents as $data){
                            if(!empty($data['km_reading'])){
                                if(empty($kmreading)){
                                    $kmreading = $data['km_reading'];
                                }else{
                                    $kmdiffrence +=($data['km_reading'] - $kmreading);
                                    $kmreading = $data['km_reading'];
                                }
                            }
                        }
                    }
                        
                    return $kmdiffrence . "km / Rs" .($model['load_rate']) * $kmdiffrence;


                }
            ],
        ],
    ]); ?>

    
    <h2>Ferry trips</h2>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive mobile-table'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $ferrytrips,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Name',
                'value' => function ($model) {
                    return $model['username'] . " - ". $model['mobile'];
                }
            ],
            [
                'label' => 'From',
                'value' => function ($model) {
                    return $model['from_location'];
                }
            ],
            [
                'label' => 'To',
                'value' => function ($model) {
                     return $model['to_location'];
                }
            ],
            [
                'label' => 'Vehicle',
                'value' => function ($model) {
                     return $model['registration_number'];
                }
            ],
            [
                'label' => 'Date',
                'value' => function ($model) {
                     return date("d M Y",strtotime($model['start_date_time']));
                }
            ],
            [
                'label' => 'Total Kms / Charges',
                'value' => function ($model) {
                    $kms = ($model['to_km_reading']-$model['from_km_reading']);
                    return $kms . "km / Rs" .($model['ferry_rate']) * $kms;
                }
            ],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title                   = "Order ID - " . $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$colseOrders                   = '';
?>
<div class="trip-view">

    <div class="row">
        <div class=""></div>

    </div>

    <div class="row">
        <div class="col-md-4 col-sm-12">
            <section class="white-box">
                <div class="user-btm-box p-t-0">
                                          <!-- <div class="row  m-t-10"><div class="col-sm-12"><strong>TRIP DETAILS</strong><br/><br/></div></div> -->
<!--                    <div class="row  m-t-10">
                        <div class="col-sm-12"><strong>Order Id</strong><p><?= $model->number ?></p></div>
                    </div>-->

                    <div class="row  m-t-10">
                        <div class="col-sm-6"><strong>Customer</strong><p><?= $model->companyName->name ?></p></div>
                        <div class="col-sm-6 b-l"><strong>Contact Person</strong>
                            <p class="m-0"><?= (!empty($model->contactPersonName->name)) ? ($model->contactPersonName->name): ''; ?></p>
                            <p class="font-12 m-0"><?=
                                (!empty($model->contactPersonName->designation))
                                        ? $model->contactPersonName->designation
                                        : '';
                                ?></p>
                            <p class="font-12 m-0">
                                <?=
                                (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile
                                        : '';
                                ?>
                                <?=
                                (!empty($model->contactPersonName->email)) ? '  '.$model->contactPersonName->email
                                        : '';
                                ?>
                            </p>
                        </div>
                    </div>
                    <hr>
                    
<?php /*
 *                  <div class="row  m-t-10">
                        <div class="col-sm-6 b-r"><strong>Origin</strong><p><?= ($model->originDetails->name) ?></p></div>
                        <div class="col-sm-6"><strong>Destination</strong><p><?= ($model->destinationDetails->name) ?></p></div>
                    </div>
                    <hr> */
?>
                    
                    <div class="row  m-t-10">
                        <div class="col-sm-6 b-r"><strong>Loading Point</strong><p><?=
                                (!empty($model->loading_point)) ? $model->loading_point
                                        : '-';
                                ?></p></div>
                        <div class="col-sm-6"><strong>Unloading Point</strong><p><?=
                                (!empty($model->unloading_point)) ? $model->unloading_point
                                        : '-';
                                ?></p></div>
                    </div>

                    <?php if($model->trip_type == 2){ ?>
                    <div class="row  m-t-10 ">
                        <div class="col-sm-12 b-r "><strong>Intermittent Stops</strong></div>
                        <?php $i = 1; foreach ($stopsModel as  $stop){ ?>
                            <div class="col-sm-12 b-r">
                                <span><?= $i . ") " .$stop['point']; ?></span>
                            </div>
                        <?php $i++;  } ?>
                        
                    </div>
                    <?php } ?>
                    <hr>

                    <?php if(!empty($model->approx_distance)){ ?>
                    <div class="row  m-t-10 ">
                        <div class="col-sm-12 b-r"><strong>Trip Distance </strong> <?= $model->approx_distance; ?>Km</div>
                    </div>
                    <?php } ?>
                    
                    <hr>

                    <div class="row  m-t-10">
                        <div class="col-sm-6 b-r"><strong>Vehicle Type</strong><p><?= $model->vehicleType->name ?></p></div>
                        <div class="col-sm-6 "><strong>Vehicle Placement Date & Time</strong> <p><?=date('d-m-Y h:i A', strtotime($model->trip_date)); ?></p></div>
                    </div>

                    <hr>
                    <div class="row  m-t-10">
                        <div class="col-sm-6 b-r"><strong>Service Type</strong><p><?= Yii::$app->params['serviceType'][$model->service_type] ?></p></div>
                        <div class="col-sm-6"><strong>Load Type</strong><p><?= (!empty($model->load_type))?Yii::$app->params['load_type'][$model->load_type]:"-" ?></p></div>
                    </div>

                    <hr>
                    
                    <div class="row  m-t-10">
                        <div class="col-sm-12"><strong>Targeted  Buying</strong><p>
                        <?=  isset($model->targated_buying) ? $model->targated_buying: ' -';?></p></div>

                    </div>
                    <hr>

                    <div class="row  m-t-10">
                        <div class="col-sm-12"><strong>Special Instructions</strong><p>
                        <?=  isset($model->special_instructions) ? $model->special_instructions: ' -';?></p></div>

                    </div>
                    <?php /*
                    <hr>
                    <div class="row  m-t-10">
                        <div class="col-sm-6 b-r"><strong>Buying Rate</strong><p>
                                <?=
                                (isset($model->buying_rate)) ? $model->buying_rate
                                        : ' -';
                                ?>
                                <?php
                                if ($model->status < Trip::STATUS_ORDER_CONFIRMED
                                    && $model->status >= Trip::STATUS_VENDOR_RE_ASSIGN) {
                                    ?>
                                    &nbsp;&nbsp;<?=
                                    Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                        ['/trip/manage/assignvendor', 'id' => $model->id]);
                                    ?>
                                <?php } ?>
                            </p></div>
                        <div class="col-sm-6"><strong>Selling Rate</strong><p>
                                <?= (isset($model->selling_rate))
                                        ? $model->selling_rate : ' -';
                                ?>
                                <?php if ($model->status
                                    === Trip::STATUS_VENDOR_CONFIRM_PENDING) { ?>
                                    &nbsp;&nbsp;<?=
                                    Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                        ['/trip/manage/customerconfirm', 'id' => $model->id]);
                                    ?>
<?php } ?>
                            </p></div>
                    </div>
                    <hr>
                    <div class="row  m-t-10">
                        <div class="col-sm-6 b-r"><strong>Origin Handling Charges</strong><p><?=
(!empty($model->origin_handling_charges)) ? $model->origin_handling_charges : '-'
?></p></div>
                        <div class="col-sm-6"><strong>Destination Handling Charges</strong><p><?=
                                (!empty($model->destination_handling_charges)) ? $model->destination_handling_charges
                                        : '-';
                                ?></p></div>
                    </div>

                    */ ?>
                    <?php if ($model->status == Trip::STATUS_DELETED) { ?>
                        <hr>
                        <div class="row  m-t-10"><div class="col-sm-12"><strong>Cancellation Reason</strong><p><?= $model->cancellation ?></p></div></div>
                    <?php } ?>

                    <?php
                    $colseOrder = '';
                    if (Yii::$app->commonFunction->accessToElement('deleteTrip')) {
                        $colseOrders = Html::a('Drop Order',
                                ['closeorder', 'id' => $model->id],
                                ['class' => 'btn btn-danger btn-block waves-effect waves-light']);
                    }
                    ?>
                    <hr>
                    <div class="row text-center m-t-10">
                        <div class="col-xs-6 b-r">
                            <?php if (Yii::$app->commonFunction->accessToElement('createTrip')) { ?>
                                <?=
                                Html::a('Update',
                                    ['update', 'id' => $model->id],
                                    ['title' => 'Update', 'aria-label' => 'Update',
                                    'data-pjax' => '0', 'class' => 'btn btn-info btn-block waves-effect waves-light'])
                                ?>
                    <?php } ?>
                        </div>
                        <div class="col-xs-6"><?= $colseOrders; ?></div>
                    </div>
                    <?php // }  ?>

<?php
if ($model->status == Trip::STATUS_ORDER_CONFIRMED) {
    ?>
                        <hr>
                        <div class="row text-center m-t-10">
                            <div class="col-xs-6 b-r">
                                <?=
                                Html::a('Booking slip',
                                    ['/trip/manage/bookingslip', 'id' => $model->id],
                                    ['title' => 'Booking slip', 'aria-label' => 'Booking slip',
                                    'data-pjax' => '0', 'class' => 'btn btn-primary btn-block waves-effect waves-light'])
                                ?>
                            </div>
                            <div class="col-xs-6">
                                <?=
                                Html::a('Order slip',
                                    ['/trip/manage/orderslip', 'id' => $model->id],
                                    ['title' => 'Order slip', 'aria-label' => 'Order slip',
                                    'data-pjax' => '0', 'class' => 'btn btn-primary btn-block waves-effect waves-light'])
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row text-center m-t-10">
                            <div class="col-xs-6 b-r">
                                <?=
                                Html::a('Tax Invoice',
                                    ['/trip/manage/taxinvoice', 'id' => $model->id],
                                    ['title' => 'Tax Invoice', 'aria-label' => 'Tax Invoice',
                                    'data-pjax' => '0', 'class' => 'btn btn-success btn-block waves-effect waves-light'])
                                ?>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                if (!empty($model->billing)) {
                                    echo Html::a('View Billing',
                                        ['viewinvoice', 'billing' => $model->billing_id],
                                        [
                                        'title' => 'View Billing',
                                        'aria-label' => 'View Billing',
                                        'data-pjax' => '0',
                                        'class' => 'btn btn-success btn-block waves-effect waves-light',
                                        'target' => '_blank'
                                    ]);
                                }
                                ?>
                        <?php //= $colseOrders; ?>
                            </div>
                        </div>
<?php } ?>
                            <?php if (Yii::$app->commonFunction->accessToElement('customerConfirm')) { ?>
                        <hr>
                        <div class="row text-center m-t-10">
                            <div class="col-xs-6 b-r">

                                <?php if ($model->status < Trip::STATUS_ORDER_CONFIRMED) { ?>
                                    <?=
                                    Html::a('Edit Vendor',
                                        ['/trip/manage/assignvendor', 'id' => $model->id],
                                        ['title' => 'Edit', 'aria-label' => 'Edit',
                                        'data-pjax' => '0',
                                        'class' => 'btn btn-warning btn-block waves-effect waves-light'])
                                    ?>
                                    &nbsp;
                                    <?php
                                } else if ($model->status >= Trip::STATUS_CUSTOMER_CONFIRM_PENDING) {
                                    echo Html::a('Re-Assign Vendor',
                                        '#frmReAssignVendor',
                                        ['title' => 'Re-assign vendor', 'aria-label' => 'Re-Assign Vendor',
                                        'data-pjax' => '0', 'data-toggle' => 'modal',
                                        'class' => 'btn btn-warning btn-block waves-effect waves-light']);
                                }
                                ?>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                if ($model->status == Trip::STATUS_CUSTOMER_CONFIRM_PENDING) {
                                    echo Html::a('Confirm Customer',
                                        ['/trip/manage/customerconfirm', 'id' => $model->id],
                                        ['title' => 'Confirm Customer', 'aria-label' => 'Confirm Customer',
                                        'data-pjax' => '0',
                                        'class' => 'btn btn-primary btn-block waves-effect waves-light']);
                                }
                                ?>
                            </div>
                        </div>
        <?php } ?>
                </div>
            </section>
        </div>

        <?php /*
          <?php if($model->status == Trip::STATUS_CUSTOMER_CONFIRM_PENDING && Yii::$app->commonFunction->accessToElement('customerConfirm')){ ?>
          <li class=""><a data-toggle="tab" href="#customer-confirmation" aria-expanded="false">Customer Confirmation</a></li>
          <?php } ?>

          if($model->status == Trip::STATUS_CUSTOMER_CONFIRM_PENDING && Yii::$app->commonFunction->accessToElement('customerConfirm')){ ?>
          <div id="customer-confirmation" class="tab-pane fade">
          <div class="row">
          <div class="col-md-12">
          <div class="row">
          <div class="col-md-12">
          <p>Customer confirmation is pending. Click here to <a href="/trip/manage/customerconfirm?id=<?= $model->id ?>" class="btn btn-info btn-rounded">Confirm customer</a></p>
          <p style=""><span style="text-align:center">&nbsp;&nbsp;OR&nbsp;&nbsp;</span></p>
          <p>Customer not convinced. Click here to <a class="btn btn-info btn-rounded" data-toggle="modal" href="#frmReAssignVendor">Re-Assign Vendor</a></p>
          </div>
          </div>
          </div>
          </div>
          </div>
          <?php } */
        ?>

        <div class="col-md-8 col-sm-12 ">
            <section class="well p-t-10 mob-p-t-0">
                <nav class="">
                    <ul class="nav nav-tabs customtab" role="tablist">

                        <li class="active"><a data-toggle="tab" href="#vendor-details" aria-expanded="false">
                                <span class="visible-xs"><i class="mdi mdi-ferry"></i></span>
                                <span class="hidden-xs"> Vendor & Vehicle Details</span>
                            </a></li>
<?php if ($model->status >= Trip::STATUS_ORDER_CONFIRMED) { ?>
                            <li class=""><a data-toggle="tab" href="#trip-tracking" aria-expanded="false">
                                    <span class="visible-xs"><i class="mdi mdi-map"></i></span>
                                    <span class="hidden-xs"> Tracking</span>
                                </a></li>
                            <li class=""><a data-toggle="tab" href="#trip-documents" aria-expanded="false">
                                    <span class="visible-xs"><i class="mdi mdi-receipt"></i></span>
                                    <span class="hidden-xs"> Documents</span>
                                </a></li>

                            <li class=""><a data-toggle="tab" href="#trip-transaction" aria-expanded="false">
                                    <span class="visible-xs"><i class="mdi mdi-currency-inr"></i></span>
                                    <span class="hidden-xs"> Transactions</span>
                                </a></li>

                    <?php } ?>
                    </ul>
                </nav>

                <div class="tab-content">
<?php /*
if ($model->trip_type == 2) {
    ?>
                        <div id="trip-multistop" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive table-custom mobile-table">
                                        <table class="table color-table table-hover"> <!--  muted-table -->
                                            <thead>
                                                <tr>
                                                    <th>Stop Name</th>
                                                    <th>Handling Charges</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($stopsModel as $stop) { ?>
                                                    <tr>
                                                        <td><?= $stop['name'] ?></td>
                                                        <td><?= $stop['handling_charges'] ?></td>
                                                    </tr>
    <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <?php } */ ?>

                    <div id="vendor-details" class="tab-pane fade in active" >
                        <div class="row">
                            <div class="col-md-12">
<?php if ($model->status >= Trip::STATUS_CUSTOMER_CONFIRM_PENDING) { ?>
                                    <div class="row">
                                        <div class="p-10 bg-success text-white m-b-10 " ><strong>Vendor</strong></div>
                                        <div class="col-md-6"><p><b>Vendor Name :</b> <?= $model->vendorDetails->name ?></p></div>
                                        <div class="col-md-6"><p><b>Mobile :</b> <?= $model->vendorDetails->mobile ?></p></div>
                                        <div class="col-md-6"><p><b>Email :</b> <?= $model->vendorDetails->email ?></p></div>
                                        <div class="col-md-6"><p><b>Category :</b> <?= $model->vendorDetails->vendorCategory->name ?></p></div>
                                        <div class="col-md-6"><p><b>Type :</b> <?= $model->vendorDetails->vendorType->name ?></p></div>
                                        <div class="col-md-6">
                                            <p><b>Buying Rate  :</b> <?php
                                                    echo (isset($model->buying_rate))
                                                            ? $model->buying_rate
                                                            : ' -';
                                                    ?>
                                                &nbsp;&nbsp;<span>(<?=
                                                    ($model->buying_rate_incl_handling_charges)
                                                            ? 'Including' : 'Excluding'
                                                    ?> Handling Charges)</span>
                                            </p>
                                        </div>
                                        <div class="col-md-12">
    <?php if (!empty($model->origin_lat)) { ?>
                                                <button type="button"
                                                        name="button"
                                                        title="SMS to Vendor"
                                                        class="btn btn-primary waves-effect waves-light js-sms-origin-destination m-t-20"
                                                        data-mob="<?= $model->vendorDetails->mobile ?>"
                                                        data-msg="Dear <?= $model->vendorDetails->name; ?>,
                                                        Loading Location : https://maps.google.com/?q=<?= $model->origin_lat.','.$model->origin_lon ?>
                                                        "
                                                        >
                                                    SMS Loading Location</button> &nbsp;
    <?php } ?>
    <?php if (!empty($model->destination_lat)) { ?>
                                                <button type="button"
                                                        name="button"
                                                        title="SMS to Vendor"
                                                        class="btn btn-primary waves-effect waves-light js-sms-origin-destination m-t-20"
                                                        data-mob="<?= $model->vendorDetails->mobile ?>"
                                                        data-msg="Dear <?= $model->vendorDetails->name; ?>,
                                                        Unloading Location: https://www.google.com/maps/search/?api=1&query=<?= $model->destination_lat.','.$model->destination_lon ?>
                                                        "
                                                        >
                                                    SMS Unloading Location</button>
                                    <?php } ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"> <br> </div>
<?php } else if ($model->status == Trip::STATUS_VENDOR_ASSIGN_PENDING) {
    ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>Vendor assignment is pending. Click here to <a href="/trip/manage/assignvendor?id=<?= $model->id ?>" class="btn btn-info btn-rounded">Assign Vendor</a></p>
                                        </div>
                                    </div>
<?php } else if ($model->status == Trip::STATUS_VENDOR_RE_ASSIGN) {
    ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>Customer is not convinced. Vendor Re-assignment is pending. Click here to <a href="/trip/manage/assignvendor?id=<?= $model->id ?>" class="btn btn-info btn-rounded">Assign Vendor</a></p>
                                        </div>
                                    </div>
<?php } ?>
<?php if ($model->status >= Trip::STATUS_ORDER_CONFIRMED) { ?>
                                    <div class="row">
                                       <div class="p-10 bg-success text-white m-b-10 col-md-12" ><strong>Driver</strong></div>

                                        <div class="col-md-6">
                                            <p><b>Owner :</b> <?= $model->vehicleDetails->vehicle_owner ?></p>
                                            <p><b>Registration Number :</b> <?= $model->vehicleDetails->registration_number ?></p>
                                            <p><b>Chassi Number :</b> <?= $model->vehicleDetails->chassi_number ?></p>
                                            <p><b>Body Number :</b> <?= $model->vehicleDetails->body_number ?></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><b>Owner's Mobile :</b> <?= $model->vehicleDetails->vehicle_owner_mobile ?></p>
                                            <p><b>Vehicle type :</b> <?= $model->vehicleType->name ?></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><b>Driver Details :</b></p>
                                            <?php foreach ($driverList as $driver) { ?>
                                                <p class="text-muted m-l-15"><?= $driver['name'] ?> / <?= $driver['mobile'] ?></p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-12">
                                            <?php if (Yii::$app->user->identity->role
                                                == 10) { ?>
                                                <?=
                                                Html::a('Edit',
                                                    ['/trip/manage/vendorconfirm',
                                                    'id' => $model->id],
                                                    ['title' => 'Update', 'aria-label' => 'Update',
                                                    'data-pjax' => '0', 'class' => 'btn btn-primary waves-effect waves-light m-t-20 pull-right'])
                                                ?> &nbsp;
    <?php } ?>
    <?php if (!empty($model->origin_lat)) { ?>
                                                <button type="button"
                                                        name="button"
                                                        class="btn btn-primary waves-effect waves-light js-sms-origin-destination m-t-20"
                                                        data-mob="<?= $driver['mobile'] ?>"
                                                        data-msg="Dear <?= ($driver['name']); ?>,
                                                        Loading Location : https://maps.google.com/?q=<?= $model->origin_lat.','.$model->origin_lon ?>
                                                        "
                                                        >
                                                    SMS Loading Location</button>&nbsp;
    <?php } ?>
    <?php if (!empty($model->destination_lat)) { ?>
                                                <button type="button"
                                                        name="button"
                                                        class="btn btn-primary waves-effect waves-light js-sms-origin-destination m-t-20"
                                                        data-mob="<?= $driver['mobile'] ?>"
                                                        data-msg="Dear <?= ($driver['name']); ?>,
                                                        Unloading Location: https://www.google.com/maps/search/?api=1&query=<?= $model->destination_lat.','.$model->destination_lon ?>
                                                        "
                                                        >
                                                    SMS Unloading Location</button>
                                    <?php } ?>
                                        </div>
                                    </div>
<?php } else if ($model->status == Trip::STATUS_VENDOR_CONFIRM_PENDING) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>Vehicle details pending. Click here to <a href="/trip/manage/vendorconfirm?id=<?= $model->id ?>" class="btn btn-info btn-rounded">Update vehicle details</a></p>
                                        </div>
                                    </div>
                        <?php } ?>
                            </div>
                        </div>
                    </div>

                        <?php if ($model->status >= Trip::STATUS_ORDER_CONFIRMED) { ?>
                        <div id="trip-tracking" class="tab-pane fade">
                            <?=
                            $this->render('_manage_tracking',
                                [
                                'model' => $model,
                                'addRemarks' => $addRemarks,
                            ])
                            ?>
                        </div>

                        <div id="trip-documents" class="tab-pane fade">
    <?=
    $this->render('_trip_document',
        [
        'model' => $model,
        'modelDocumentUpload' => $modelDocumentUpload,
    ])
    ?>
                        </div>

                        
                        <div id="trip-transaction" class="tab-pane fade">
                            <?php Pjax::begin(['id'=>'transaction-listing']); ?>
                            <div class="row text-center">
                            <a class="btn btn-primary waves-effect waves-light btnAddEditTransaction"  title="Tax Invoice" aria-label="Tax Invoice" data-pjax="0">Add Transaction</a>
                            <br><br>
                            </div>
                            <div class="row">
                                <div class="col-md-6 b-r">
                                    <label for="">Vendor</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <tbody>
                                                    <tr><th class="">Buying Rate</th><td align="right"><?= $model->buying_rate; ?></td></tr>
                                                    <tr><th class="">Adv. Payable</th><td align="right"><?= $model->advance_buying_charges; ?></td></tr>
                                                    <tr><th class="">Paid</th><td align="right"><?= (float) $totalTransaction['paid']; ?></td></tr>
                                                    <tr><th class="">Ancillary charges 
                                                                                    <i class="mdi mdi-information-outline" data-toggle="tooltip" title="Detention charges, other additional charges" ></i>
                                                            </th><td align="right"><?= (float) $totalTransaction['vendor_ancillary_charges']; ?></td>
                                                    </tr>
                                                    <tr><th class="">Commission</th><td align="right"><?= (float) $totalTransaction['vendor_commission']; ?></td></tr>
                                                    <tr><th class="">Mamul</th><td align="right"><?= (float) $totalTransaction['vendor_mamul']; ?></td></tr>
                                                    <tr><th class="">TDS</th><td align="right"><?= (float) $totalTransaction['vendor_tds']; ?></td></tr>
                                                    <tr><th class="">Debit</th><td align="right"><?= (float) $totalTransaction['vendor_debit']; ?></td></tr>

    <?php
    $payable         = (float) $model->buying_rate + (float) $totalTransaction['vendor_ancillary_charges']
                                - (float) $totalTransaction['paid']
                                - (float) $totalTransaction['vendor_mamul']
                                - (float) $totalTransaction['vendor_commission']
                                - (float) $totalTransaction['vendor_tds']
                                - (float) $totalTransaction['debit'];
    // if (!empty($model->buying_rate_incl_handling_charges)) {
    // 	$payable += (float)$model->origin_handling_charges + (float)$model->destination_handling_charges;
    // }
    ?>
                                                    <tr><th class="">Balance</th><td align="right"><?= (float) $payable; ?></td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 b-l">
                                    <label for="">Customer  </label>
    <?php
    //$receivalbe      = \Yii::$app->commonFunction->getReceivables($model->id);
    // var_dump($receivalbe['total_receivables']);
    // var_dump($receivalbe['amount_paid']);
    // var_dump($receivalbe);
    ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <tbody>
                                                    <tr><th class="">Selling Rate</th><td align="right"><?= $model->selling_rate; ?></td></tr>
                                                    <tr><th class="">Adv. Receivable</th><td align="right"><?= $model->advance_selling_charges; ?></td></tr>
                                                    <tr><th class="">Received</th><td align="right"><?= (float) $totalTransaction['received']; ?></td></tr>
                                                    <tr><th class="">Ancillary charges
                                                            <i class="mdi mdi-information-outline" data-toggle="tooltip" title="Detention charges, other additional charges" ></i></th>
                                                            <td align="right"><?= (float) $totalTransaction['customer_ancillary_charges']; ?></td>
                                                    </tr>
                                                    <?php
                                                    $totalReceivable = (float) $model->selling_rate + (float) $totalTransaction['customer_ancillary_charges']
                                                                                    - (float) $totalTransaction['received']
                                                                                    - (float) $totalTransaction['customer_commission']
                                                                                    - (float) $totalTransaction['customer_mamul']
                                                                                    - (float) $totalTransaction['customer_tds']
                                                                                    - (float) $totalTransaction['writeOff'];
                                                    ?>
<!--                                                    <tr><th class="">Origin Handling Charges</th><td align="right"><?= (float) $model->origin_handling_charges; ?></td></tr>
                                                    <tr><th class="">Destination Handling Charges</th><td align="right"><?= (float) $model->destination_handling_charges; ?></td></tr>-->
                                                    <tr><th class="">Commission</th><td align="right"><?= (float) $totalTransaction['customer_commission']; ?></td></tr>
                                                    <tr><th class="">Mamul payable</th><td align="right"><?= (float) $totalTransaction['customer_mamul']; ?></td></tr>
                                                    <tr><th class="">TDS</th><td align="right"><?= (float) $totalTransaction['customer_tds']; ?></td></tr>
                                                    <tr><th class="">Write Off</th><td align="right"><?= (float) $totalTransaction['writeOff']; ?></td></tr>
                                                    <tr><th class="">Balance</th><td align="right" class=""><?= (float) $totalReceivable; ?></td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- bank transaction entries -->
                            <div class="row text-center bg-success text-white"><h2 class="text-white">Transaction summary</h2></div>
                                <?=
                                $this->render('_trip_transaction',
                                    [
                                    'dataTransactions' => $dataTransactions,
                                    'hideCTA' => 0
                                ]);
                                ?>
                       
                       <?php Pjax::end(); ?>
                       </div>

<?php } ?>
                </div>
            </section>

        </div>
    </div>

</div>

<?php
// $this->registerJs("
// 	$('#btn-re-assign').click(function(){
// 		$('#frmReAssignVendor').modal();
// 	});
// ",View::POS_END);
// if($model->status != Trip::STATUS_ORDER_CONFIRMED){
// $this->registerJs("
// 	$(function(){
// 		try {
// 			$(window).load(function() {
// 				$('html, body').animate({ scrollTop: $(document).height() }, 1000);
// 			});
// 		} catch (err) {}
// 	})
// ",View::POS_END);
// }
?>
<p class="jsReader" hidden></p>
<?php $this->registerJs("
// To send sms to vendor
$('.js-sms-origin-destination').click(function(){
  var mob = $(this).data('mob');
  var msg = $(this).data('msg');
  var confirmation = confirm('Do you want to send this sms? '+ msg);
  if (confirmation == true) {
    $.post('/trip/manage/sendquotationsms', {mobile:mob, message:msg},function(data) {
      //Set message
      $('body').append('<div  class=\"alert-success myadmin-alert alert myadmin-alert-top-right block fade in\" style=\"display:block\"><button type=\"button\" class=\"m-l-10 close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button><h4>Message Sent Successfully.</h4></div>');
      // Auto close alert
      window.setTimeout(function() {
        $('.myadmin-alert').fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
        });
      }, 4000);
    });
  }
});

"); ?>
<script>
    function initAutocomplete() {
        // Create the search box and link it to the UI element.
        var trip_location = document.getElementById('tripLocation');
//        var searchBox = new google.maps.places.Autocomplete(trip_location,{'fields':['geometry']});
        var searchBox = new google.maps.places.Autocomplete(
                                                                                               document.getElementById('tripLocation'),
                                                                                                {types: ['geocode'],componentRestrictions:{'country': 'in'}});
                                                                         searchBox.setFields(['address_component']);
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['gmapKey'] ?>&libraries=places&callback=initAutocomplete"
async defer></script>



<!-- Modal Reassign Vendor -->
<?php
$form = ActiveForm::begin([
        'action' => ['/trip/manage/vendor-re-assign', 'id' => $model->id],
    ]);
?>
<div id="frmReAssignVendor" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Re-Assign Vendor</h4>
            </div>
            <div class="modal-body">
<?= $form->field($reAssignModel, 'negotiation_remark')->textArea(['row' => 4]); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<?=
Html::submitButton('Save', ['class' => 'btn btn-success', 'id' => 'send-button'])
?>
            </div>
        </div>
    </div>
</div>
<?php $form = ActiveForm::end(); ?>
<?=
$this->render('_modalCreateEditTransaction',
    ['modelTransactions' => $modelTransactions])
?>
<?php $this->registerJs('
$(document).on("pjax:complete","#trip-transaction-table", function(event) {
		createEditTransaction();
});
'); ?>

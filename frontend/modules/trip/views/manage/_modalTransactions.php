<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

// $userList = \common\models\User::find()->select(['id','username'])->asArray()->all();
// $userList = ArrayHelper::map($userList, 'id', 'username');
/* @var $this yii\web\View */
/* @var $model frontend\modules\timesheet\models\ResumePlacements */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
  var transactionType = <?= json_encode(\Yii::$app->params['transaction_type'],true); ?>;
  var mamulType = <?= json_encode(\Yii::$app->params['mamul_type'],true); ?>;
  var paymentType = <?= json_encode(\Yii::$app->params['payment_type'],true); ?>;
</script>
<div class="modal fade" id="modalTransactions" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header"><h4 class="modalTransactionsTitle">Transactions</h4></div>
        <div class="modal-body" style="height:70vh;overflow: auto;">
            <div class=" table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th> <label for="">Date &amp; Time</label> </th>
                    <th> <label for="">Transaction ID</label> </th>
                    <th> <label for="">Amount</label> </th>
                    <th> <label for="">Transaction Type</label> </th>
                    <th> <label for="">Note</label> </th>
                  </tr>
                </thead>
                <tbody class="addTransactionListing"></tbody>
              </table>
            </div>
        </div>
        <div class="modal-footer"><a class="btn btn-inverse m-r-10 pull-right" data-dismiss="modal">Close</a></div>
        </div>
    </div>
  </div>
</div>
<?php $this->registerJs("
  $(document).ready(function(){
    $('.jsShowTransationDetails').click(function(){
      $('.addTransactionListing').html('');
      var elem = $(this);
      var dataJson = JSON.parse(elem.find('.jsonData').text());
      var dataTitle = elem.data('title');
      $('.modalTransactionsTitle').text(dataTitle);
      var list_transaction_rows ='';
      $.each(dataJson,function(i,data){
        var mamul = '';
        if (data.mamul_type != null) {
            mamul += '<br><small>'+mamulType[data.mamul_type]+' : '+data.mamul+'</small>';
           // console.log(mamul);
        }
        var payment_type = '';
        if (data.payment_type != null) {
            payment_type += '<br><small><label class=\"hidden-lg hidden-md text-muted\">Payment Type : </label>'+paymentType[data.payment_type]+'</small>';
        }
        if (data.payment_type != null) {
            payment_type += '<br><small><label class=\"text-muted\">Detention Days : </label>'+data.detention_days+'</small>';
        }
        list_transaction_rows += '<tr>'+
                                    '<td><label class=\"hidden-lg hidden-md text-muted\">Trip Date : </label>'+getDateTimePhpStyle(data.datetime,1)+'</td>'+
                                    '<td><label class=\"hidden-lg hidden-md text-muted\">Transaction ID : </label>'+data.transaction_id+'</td>'+
                                    '<td><label class=\"hidden-lg hidden-md text-muted\">Amount : </label>'+(data.amount * 1)+mamul+'</td>'+
                                    '<td><label class=\"hidden-lg hidden-md text-muted\">Transaction Type : </label>'+transactionType[data.transaction_type]+payment_type+'</td>'+
                                    '<td><label class=\"hidden-lg hidden-md text-muted\">Note : </label>'+data.note+'</td>'+
                                '</tr>';
      });
      if (list_transaction_rows == '') {
        list_transaction_rows = '<tr><td colspan=\"5\" class=\"text-center\">No Records</td></tr>';
      }
      $('.addTransactionListing').html(list_transaction_rows);
      $('#modalTransactions').modal();
    });
  });
"); ?>

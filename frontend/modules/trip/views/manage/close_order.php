<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="trip-view">


<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<h3><b>TRIP DETAILS</b>
			<span class="pull-right">
			</span></h3>
			<hr>
			<div class="row">
                <div class="col-md-6">
                    <p><b>Date :</b> <?= date('d-m-Y',strtotime($model->trip_date)); ?></p>
					<p><b>Vehicle Type :</b> <?= $model->vehicleType->name ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Company :</b> <?= $model->companyName->name ?></p>
                    <p class="text-muted m-l-5">
						<?= (!empty($model->contactPersonName->name)) ? $model->contactPersonName->name : ''; ?>
						<?= (!empty($model->contactPersonName->designation)) ? ' / '.$model->contactPersonName->designation : ''; ?>
					</p>
                    <p class="text-muted m-l-5">
						<?= (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile : ''; ?>
						<?= (!empty($model->contactPersonName->email)) ? ' / '.$model->contactPersonName->email : ''; ?>
					</p>
                </div>

                <div class="col-md-6">
                    <p><b>Origin :</b> <?= $model->originDetails->name ?></p>
                    <p><b>Loading Point :</b> <?php echo (!empty($model->loading_point))?$model->loading_point:'-'; ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Destination :</b> <?= $model->destinationDetails->name ?></p>
                    <p><b>Unloading Point :</b> <?php echo (!empty($model->unloading_point))?$model->unloading_point:'-'; ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Service Type :</b> <?= Yii::$app->params['serviceType'][$model->service_type] ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Targeted Rate :</b><?php echo (isset($model->targated_rate))?$model->targated_rate:' -'; ?></p>
                    <p><b>Buying Rate :</b><?php echo (isset($model->buying_rate))?$model->buying_rate:' -'; ?>
					</p>
                    <p><b>Selling Rate :</b><?php echo (isset($model->selling_rate))?$model->selling_rate:' -'; ?>
					</p>
                </div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Origin Handling Charges :</b> <?php echo (!empty($model->origin_handling_charges))?$model->origin_handling_charges:'-' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Destination Handling Charges :</b> <?php echo (!empty($model->destination_handling_charges))?$model->destination_handling_charges:'-';  ?></p>
                </div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Trip Type :</b> <?= Yii::$app->params['tripType'][$model->trip_type] ?></p>
                </div>

				<?php if($model->trip_type == 2){ ?>
                <div class="col-md-12">
					<div class="table-responsive">
						<table class="table color-table muted-table">
							<thead>
								<tr>
									<th>Stop Name</th>
									<th>Handling Charges</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach($stopsModel as $stop){ ?>
								<tr>
									<td><?= $stop['name'] ?></td>
									<td><?= $stop['handling_charges'] ?></td>
								</tr>
							<?php }
							?>
							</tbody>
						</table>
					</div>
                </div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>


<?php if($model->status >= Trip::STATUS_CUSTOMER_CONFIRM_PENDING){ ?>
<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">

			<h3><b>VENDOR DETAILS</b>
			<span class="pull-right">
			</span></h3>
			<hr>
			<div class="row">
                <div class="col-md-6">
                    <p><b>Vendor Name :</b> <?= $model->vendorDetails->name ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Mobile :</b> <?= $model->vendorDetails->mobile ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Email :</b> <?= $model->vendorDetails->email ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Category :</b> <?= $model->vendorDetails->vendorCategory->name ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Type :</b> <?= $model->vendorDetails->vendorType->name ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Buying Rate  :</b> <?php echo (isset($model->buying_rate))?$model->buying_rate:' -'; ?>
					&nbsp;&nbsp;<span>(<?= ($model->buying_rate_incl_handling_charges)?'Including':'Excluding' ?> Handling Charges)</span>
					</p>
                </div>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php if($model->status >= Trip::STATUS_ORDER_CONFIRMED){ ?>
<div class="row">
	<div class="col-md-12">
		<div class="white-box ">
			<h3><b>VEHICLE DETAILS</b>
						<span class="pull-right">
			<?php if(Yii::$app->user->identity->role == 10){ ?>
				<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/trip/manage/vendorconfirm', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0']) ?>
			<?php } ?>

			</span>

			</h3>
			<hr>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Registration Number :</b> <?= $model->vehicleDetails->registration_number ?></p>
                    <p><b>Chassi Number :</b> <?= $model->vehicleDetails->chassi_number ?></p>
                    <p><b>Body Number :</b> <?= $model->vehicleDetails->body_number ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Vehicle type :</b> <?= $model->vehicleType->name ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Driver Details :</b></p>
					<?php foreach($driverList as $driver){ ?>
					<p class="text-muted m-l-15"><?= $driver['name'] ?> / <?= $driver['mobile'] ?></p>
					<?php } ?>
                </div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div class="row">
	<div class="col-md-12">
		<div class="white-box ">
			<h3><b>Drop order</b> <span class="pull-right"></span></h3>
			<hr>
			<?php $form = ActiveForm::begin(); ?>
			<div class="row">
				<div class="col-md-6">
                    <?= $form->field($newModel, 'cancellation')->textArea(['value'=>$model->cancellation,'rows'=>4]) ?>
                </div>

			</div>
				<div class="form-group">
					<?= Html::submitButton($newModel->isNewRecord ? 'Close' : 'Update', ['class' => $newModel->isNewRecord ? 'btn btn-warning' : 'btn btn-primary']) ?>
					<?= Html::a('Cancel', ['view','id'=>$model->id],['class' => 'btn btn-default']) ?>
				</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>



</div>

<?php

$this->registerJs("
	$(window).load(function() {
	  $('html, body').animate({ scrollTop: $(document).height() }, 1000);
	});
",View::POS_END);

?>

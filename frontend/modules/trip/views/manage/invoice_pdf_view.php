<?php
$this->registerCss("
/* td {border:1px #ccc solid !important; padding:0px !important;} */
/* td {padding:0px !important;} */
");


//return true;


// $driverList = TripDriver::tripDriverListforReport($id);
// $stopsModel = TripStops::tripStopListByNameforReport($id);
?>
<?php
/* var_dump($model->companyName); die();  */


/* echo Yii::$app->commonFunction->getIndianCurrency(100); die(); */
// 'customePrice' = number_format((float)$model->selling_rate, 2, '.', '');

$annexure = [];
?>
<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<?php // Header logo and address middlemilrprologo.png  invoice-logo.jpg ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-invoice" style="width:90%">
							<colgroup>
									<col style="width:250px;">
									<col style="">
									<col style="width:450px;">
							</colgroup>
							<tr style="border-style:hidden">
								<td rowspan="3" style="text-align:center;padding:0px;border-style:hidden;"><img src="/images/middlemilrprologo.png" style="width:210px;"/></td>
								<td style="text-align:center;padding:0px;border-style:hidden"><b>MiddleMile Pro LLP</b></td>
								<td rowspan="3" style="text-align:center;padding:0px;width:100px;border-style:hidden"><b> &nbsp; </b></td>
							</tr>
							<tr style="border-style:hidden">
								<td style="text-align:center;padding:0px;border-style:hidden">B-47, Pravasi Industrial Estate Goregaon East Mumbai-400063</td>
							</tr>
							<tr style="border-style:hidden">
								<td style="text-align:center;padding:0px;border-style:hidden">Registered Address: C103, Rock Garden II,Dahisar West Mumbai-400068.</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?php // End Header logo and address  ?>
			<br>
			<?php // bill section ?>
			<?php //   ?>
					<?php // Details of Seller (Billed by) ?>
					<div style="font-size:12px">
						<div style="text-align:center;padding:2px;border:#e0e0e0 3px outset;display:block">
							<table class="table table-invoice" style="width:100%;border:hidden;font-size:12px" cellspacing="4" cellpadding="2">
								<tr>
									<th style="background-color:#ccc;border:#444 1px solid;width:350px"><p><b>Details of Seller (Billed by)</b></p></th>
									<th align="left" style="font-size:18px; padding-left:120px"><p><b>Bill of Supply</b></p></th>
								</tr>
							</table>
							<table class="table table-invoice" style="width:100%;border:hidden;font-size:12px" cellspacing="4" cellpadding="2">
								<tr>
									<th align='left' style=""><b>Invoice Number:</b></th>
									<th style="font-weight:500;">MMP-<?= \Yii::$app->commonFunction->newInvoiceNumber(); ?></th>
									<th colspan="6" style="border:0px;"></th>
									<th align='left' style=""><b>Date:</b></th>
									<th style="font-weight:500;"><?= date('d-M-Y'); ?></th>
								</tr>
								<tr>
									<th align='left' style="border:#444 1px solid;"><b>State :</b></th>
									<th style="border:#444 1px solid;font-weight:500;">Maharashtra</th>
									<th align='left' style="border:#444 1px solid;"><b>State Code:</b></th>
									<th style="border:#444 1px solid;font-weight:500;">27</th>
									<th align='left' style="border:#444 1px solid;"><b>PAN:</b></th>
									<th style="border:#444 1px solid;font-weight:500;">ABEFM5858P</th>
									<th style="border:0px;"></th>
									<th style="border:0px;"></th>
								</tr>
								<tr>
									<th align='left' style="border:#444 1px solid;"><b>Vendor Code:</b></th>
									<th style="border:#444 1px solid;font-weight:500;">NA</th>
									<th align='left' style="border:#444 1px solid;"><b>SAC Code:</b></th>
									<th style="border:#444 1px solid;font-weight:500;">996512</th>
									<th align='left' style="border:#444 1px solid;"><b>GSTIN:</b></th>
									<th style="border:#444 1px solid;font-weight:500;">27ABEFM5858P1ZF</th>
									<th style="border:0px; width:90px"></th>
									<th align='left' colspan="2" style="border:#444 1px solid; width:190px"><b>Reverse Charge Applicable</b></th>
									<th style="border:#444 1px solid;font-weight:500;">No</th>
								</tr>

							</table>
						</div>
						<div style="text-align:center;padding:2px;border:#e0e0e0 3px outset;display:block">
							<?php // Details of Seller (Billed by) ?>
							<?php
							$company = $model[0]->companyName;
							$contactPerson = $model[0]->contactPersonName;
							//var_dump($contactPerson);
							?>
							<table class="table table-invoice" style="width:100%;border:hidden;font-size:12px" cellspacing="4" cellpadding="2">
								<tr>
									<th style="background-color:#ccc;border:#444 1px solid;width:350px"><p><b>Details of Buyer (Billed to)</b></p></th>
									<th ></th>
								</tr>
							</table>
							<table class="table table-invoice" style="width:100%;border:hidden;font-size:12px" cellspacing="4" cellpadding="2">
								<tr>
									<th align='left' style="border:#444 1px solid; width:125px;"><b>Company Name:</b></th>
									<th align='left' colspan="2" style="border:#444 1px solid;font-weight:500;"><?= !empty($company)?($company->name):''; ?></th>
									<th align='left' style="border:#444 1px solid;"><b>GSTIN:</b></th>
									<th style="border:#444 1px solid;font-weight:500;"><?= !empty($company)?$company->gst_no:''; ?></th>
									<th align='left' style="border:#444 1px solid;"><b>State Code:</b></th>
									<th style="border:#444 1px solid;font-weight:500;"><b><?= !empty($company)?$company->state_code:''; ?></b></th>
									<th colspan="2"></th>
									<th align='right' >Kind Attn :</th>
									<th align='left' ><?= !empty($contactPerson)?($contactPerson->name):''; ?></th>
								</tr>
								<tr>
									<td align='left' style="border:#444 1px solid;width:125px;"><b>Address:</b> <br><br> </td>
									<td align='left' colspan="6" style="border:#444 1px solid;font-weight:500;vertical-align:top"><?= !empty($company)?$company->address:''; ?></td>
									<th colspan="2"></th>
									<th align='right' >Contact # :</th>
									<th align='left'><?= !empty($contactPerson)?$contactPerson->mobile:''; ?></th>
								</tr>
							</table>
						</div>
						<div style="text-align:center;padding:2px;border:#e0e0e0 3px outset;display:block">
						<table class="table table-invoice" style="width:100%;border: #333 1px solid;font-size:12px" cellspacing="0" cellpadding="2">
							<tr>
								<th style="background-color:#ccc;border: #333 1px solid"><b>S. No.</b></th>
								<th style="background-color:#ccc;border: #333 1px solid"><b>Consignment Note Number</b></th>
								<th style="background-color:#ccc;border: #333 1px solid"><b>Consignment Note Date</b></th>
								<th style="background-color:#ccc;border: #333 1px solid"><b>Truck No.</b></th>
								<th style="background-color:#ccc;border: #333 1px solid"><b>Origin</b></th>
								<th style="background-color:#ccc;border: #333 1px solid"><b>Destination</b></th>
								<th style="border: #333 1px solid"><b>Vehicle Freight</b></th>
								<th style="border: #333 1px solid"><b>Detention Charges</b></th>
								<th style="border: #333 1px solid"><b>Loading Charges</b></th>
								<th style="border: #333 1px solid"><b>Unloading Charges</b></th>
								<th style="border: #333 1px solid"><b>Toll & Other	 Charges</b></th>
								<th style="border: #333 1px solid"><b>Total Amount</b></th>
							</tr>
							<?php
							$totalSellingRate = $totalDetentionCharges = $totalLoadingCharges = $totalUnloadingCharges = 0;
							$totalTollCharges = $totalWriteOff = $netAmount = 0;
							$rowCount = 1;
							?>
							<?php foreach ($model as $srno => $trip) { ?>
								<?php
								$receivables = \Yii::$app->commonFunction->getReceivables($trip->id);
								// $loadingCharge = 0;
								// $unloadingCharge = 0;
								// if (empty($trip->buying_rate_incl_handling_charges)) {
								// }
								$loadingCharge = $trip->origin_handling_charges;
								$unloadingCharge = $trip->destination_handling_charges;
								$totalAmount = $trip->selling_rate + $receivables['toll_charges'] + $receivables['total_detention_charges'] + $loadingCharge + $unloadingCharge;
								$totalSellingRate += $trip->selling_rate;
								$totalDetentionCharges += $receivables['total_detention_charges'];
								$totalLoadingCharges += $loadingCharge;
								$totalUnloadingCharges += $unloadingCharge;
								$totalTollCharges += $receivables['toll_charges'];
								$totalWriteOff += $receivables['write_off'];
								$netAmount += $totalAmount;
								$rowCount ++;

								// Generating $annexure
								$annexure[$srno]['number'] = $trip['number'];
								$annexure[$srno]['trip_id'] = $trip['id'];
								$annexure[$srno]['trip_date'] = date('d-M-Y',strtotime($trip['trip_date']));
								$annexure[$srno]['selling_price'] = $trip->selling_rate + $loadingCharge + $unloadingCharge;
								$annexure[$srno]['totalAmount'] = $totalAmount;
								$annexure[$srno]['receivables'] = $receivables;
								$annexure[$srno]['destination'] = (!empty($trip->destinationDetails)?$trip->destinationDetails->name:'');
								$annexure[$srno]['origin'] =  (!empty($trip->originDetails)?$trip->originDetails->name:'');
								$annexure[$srno]['registration_number'] = !empty($trip->vehicleDetails)?$trip->vehicleDetails->registration_number:'';
								?>
								<tr>
									<th align='center' style="border: #333 1px solid;font-weight:500;"><?= $srno + 1; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $trip['number']; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $annexure[$srno]['trip_date'];?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $annexure[$srno]['registration_number'] ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $annexure[$srno]['origin']; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $annexure[$srno]['destination']; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $trip->selling_rate ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $receivables['total_detention_charges']; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $loadingCharge; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"><?= $unloadingCharge; ?></th>
									<th style="border: #333 1px solid;font-weight:500;"> <?= $receivables['toll_charges']; ?> </th>
									<th style="border: #333 1px solid;font-weight:500;"> <?= $totalAmount; ?> </th>
								</tr>
							<?php } ?>
							<?php if ($rowCount < 10) {
								for ($i=$rowCount; $i < 10; $i++) { ?>
									<tr> <th align='center' style="border: #333 1px solid;font-weight:500;"> </th> <th style="border: #333 1px solid;font-weight:500;"> <p> &nbsp; </p> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;"> </th><th style="border: #333 1px solid;font-weight:500;">   </th><th style="border: #333 1px solid;font-weight:500;">  </th></tr>
								<?php	}
							} ?>
							<tr>
								<th align='center' style="border: #333 1px solid;font-weight:500;"> </th>
								<th colspan="5" style="border: #333 1px solid;font-weight:500;"> <b>Annexure attached for details (if any)</b> </th>
								<th colspan="6" style="border: #333 1px solid;font-weight:500;"></th>
							</tr>
							<tr>
								<th align='center' style="border: #333 1px solid;font-weight:500;"> </th>
								<th colspan="5" style="border: #333 1px solid;font-weight:500;"> <b>Total</b> </th>
								<th style="border: #333 1px solid;font-weight:500;"><?= $totalSellingRate ?></th>
								<th style="border: #333 1px solid;font-weight:500;"><?= $totalDetentionCharges; ?></th>
								<th style="border: #333 1px solid;font-weight:500;"><?= $totalLoadingCharges; ?></th>
								<th style="border: #333 1px solid;font-weight:500;"><?= $totalUnloadingCharges; ?></th>
								<th style="border: #333 1px solid;font-weight:500;"> <?= $totalTollCharges; ?> </th>
								<th style="border: #333 1px solid;font-weight:500;"> <?= $netAmount; ?> </th>
							</tr>
							<tr>
								<th colspan="12" align='center' style="border: 0px;font-weight:500;"> <br> </th>
							</tr>
							<?php
							$netPayable = $netAmount;
							if (!empty($totalWriteOff)) { ?>
								<tr >
									<th align='center' style="border: 0px;font-weight:500;"></th>
									<th colspan="5" style="border: 0px;font-weight:500; vertical-align:bottom;text-align:left"> <b>Write Off / Discount</b> </th>
									<th style="border: 0px;font-weight:500;"></th>
									<th style="border: 0px;font-weight:500;"></th>
									<th style="border: 0px;font-weight:500;"></th>
									<th style="border: 0px;font-weight:500;"></th>
									<th style="border: 0px;font-weight:500;"></th>
									<th style="border: #333 1px solid;font-weight:500;"> <?= $totalWriteOff; ?> </th>
								</tr>
								<?php $netPayable -=  $totalWriteOff; ?>
							<?php } ?>
							<tr >
								<th align='center' style="border: 0px;font-weight:500;"></th>
								<th colspan="5" style="border: 0px;font-weight:500; vertical-align:bottom;text-align:left"> <b>Final Bill Amount</b> </th>
								<th style="border: 0px;font-weight:500;"></th>
								<th style="border: 0px;font-weight:500;"></th>
								<th style="border: 0px;font-weight:500;"></th>
								<th style="border: 0px;font-weight:500;"></th>
								<th style="border: 0px;font-weight:500;"></th>
								<th style="border: #333 1px solid;font-weight:500;"> <?= $netPayable; ?> </th>
							</tr>
						</table>
						<?php
						$AdvanceAmount = $receivables['amount_paid'];
						$netPayable -= $AdvanceAmount;
						?>
						<table class="table table-invoice" style="width:100%;border: #333 1px solid;font-size:12px" cellspacing="0" cellpadding="2">
							<tr>
								<th style="border: 0px;text-align:left"> <b> For Middlemile Pro LLP</b></th>
								<th style="border: 0px; font-weight:500">Invoice amount in words</th>
							</tr>
							<tr>
								<th style="border: 0px;text-align:left"> </th>
								<th style="border: #333 2px solid; font-weight:500;"> <h4 style="margin-top:10px;margin-bottom:10px;"><?= \Yii::$app->commonFunction->getIndianCurrency($netPayable); ?></h4> </th>
							</tr>
							<tr>
								<th style="border: 0px;text-align:left"> </th>
								<th style="border: 0px; font-weight:500"></th>
							</tr>
							<tr>
								<th style="border: 0px;text-align:left;font-weight:500;">Ms. Shilpa Patil </th>
								<th style="border: 0px; font-weight:500">
									<table class="table table-invoice" style="width:100%;border: 0px;" cellspacing="0" cellpadding="2">
										<tr>
											<th style="border: 0px;text-align:left;"> </th>
											<th align="right" style="border: 0px; font-weight:500;width:160px">Advance Received</th>
											<th style="border: #333 1px solid;width:80px; font-weight:500;"><?= (float)$AdvanceAmount; ?></th>
										</tr>
									</table>
								</th>
							</tr>
							<tr>
								<th style="border: 0px;text-align:left;"> <b>Finance Executive</b> </th>
								<th style="border: 0px; font-weight:500;">
									<table class="table table-invoice" style="width:100%;border: 0px;" cellspacing="0" cellpadding="2">
										<tr>
											<th style="border: 0px;text-align:left;"> </th>
											<th align="right" style="border: 0px;width:160px "> <b>Net Payable</b> </th>
											<th style="border: #333 1px solid;width:80px; font-weight:500;"><?= $netPayable; ?></th>
										</tr>
									</table>
								</th>
							</tr>
						</table>
						<br>
						<?php // Bank Details ?>
						<table class="table table-invoice" style="width:100%;border:#444 2px solid; text-align: center;font-size:12px" cellspacing="0" cellpadding="2">
										<tr>
											<th colspan="5" style="background-color:#ccc;border:#444 1px solid;"><p><b>Bank Details</b></p></th>
										</tr>
										<tr >
											<th style="border:#444 1px solid;"><b>Name of Account Holder</b></th>
											<th style="border:#444 1px solid;"><b>Bank Name</b></th>
											<th style="border:#444 1px solid;"><b>Account Number</b></th>
											<th style="border:#444 1px solid;"><b>IFSC Code</b></th>
											<th style="border:#444 1px solid;"><b>Branch Address</b></th>
										</tr>
										<tr>
											<td style="border:#444 1px solid;">MiddleMile Pro LLP</td>
											<td style="border:#444 1px solid;">Kotak Mahindra Bank</td>
											<td style="border:#444 1px solid;">0812171004</td>
											<td style="border:#444 1px solid;">KKBK0000652</td>
											<td style="border:#444 1px solid;">Santa Cruz, Mumbai</td>
										</tr>
									</table>
					</div>
					</div>


		</div>
	</div>
</div>

<!-- <div class="page-break"> </div> -->
<!-- NEW PAGE -->
<pagebreak />

<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<?php // Header logo and address middlemilrprologo.png  invoice-logo.jpg ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-invoice" style="width:90%">
							<colgroup>
									<col style="width:250px;">
									<col style="">
									<col style="width:450px;">
							</colgroup>
							<tr style="border-style:hidden">
								<td rowspan="3" style="text-align:center;padding:0px;border-style:hidden;"><img src="/images/middlemilrprologo.png" style="width:210px;"/></td>
								<td style="text-align:center;padding:0px;border-style:hidden"><b>MiddleMile Pro LLP</b></td>
								<td rowspan="3" style="text-align:center;padding:0px;width:100px;border-style:hidden"><b> &nbsp; </b></td>
							</tr>
							<tr style="border-style:hidden">
								<td style="text-align:center;padding:0px;border-style:hidden">B-47, Pravasi Industrial Estate Goregaon East Mumbai-400063</td>
							</tr>
							<tr style="border-style:hidden">
								<td style="text-align:center;padding:0px;border-style:hidden">Registered Address: C103, Rock Garden II,Dahisar West Mumbai-400068.</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<h3 align="center"> <u>Annexure to Invoice</u> </h3>


			<div style="font-size:12px">

				<?php $tripCount = 0;
				foreach ($annexure as $key => $trip){
					if ($tripCount == 0) {
						echo '<div>
										<div style="width:180px;display:inline-block;float: left;">
											<div style="height:20px;"></div>
											<!-- <div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Indent No.</b></div> -->
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Consignment Note No</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Consignment Note Date</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Vehicle Registration No</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Origin</b></div>
											<!-- <div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Intermediary Stops</b></div> -->
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Destination</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Sale Price</b></div>
											<!-- <div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Vehicle Hire Charegs</b></div> -->
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Destination Charges</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Toll & Other Charges</b></div>
											<!-- <div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Remarks for Other Charges</b></div> -->
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Origin Detn Days</b></div>
											<!-- <div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Origin Reporting Date/Time</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Departure Date & Time</b></div> -->
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Dest. Detn Days</b></div>
											<!-- <div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Destination Arrival Date</b></div>
											<div align="left" style="height:20px;border:#444 1px solid;background-color:#ccc;"><b>Unloading Completion Date</b></div> -->
									</div>
									<div style="width:100px;display:inline-block;float: left;">
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>UoM</b></div>
											<!-- <div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>#</b></div> -->
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>#</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Date</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>#</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Place</b></div>
											<!-- <div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Place</b></div> -->
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Place</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>INR</b></div>
											<!--<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>INR</b></div> -->
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>INR</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>INR</b></div>
											<!-- <div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Text</b></div> -->
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Days</b></div>
											<!-- <div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Date</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Date</b></div> -->
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Days</b></div>
											<!-- <div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Date</b></div>
											<div align="center" style="height:20px;border:#444 1px solid;color:#5494cc;"><b>Date</b></div> -->
									</div>
									';
					}
					echo '<div style="width:180px;display:inline-block;float: left;">';
				?>
				<?php // var_dump($trip['receivables']) ?>
							<div align="center" style="height:20px;border:#444 1px solid">Trip-<?= $key +1 ?></div>
							<!-- <div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['number'] ?></div> -->
							<div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['number'] ?></div>
							<div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['trip_date'] ?></div>
							<div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['registration_number']; ?></div>
							<div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['origin']; ?></div>
							<!-- <div align="center" style="height:20px;border:#444 1px solid;"></div> -->
							<div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['destination']; ?></div>
							<div align="center" style="height:20px;border:#444 1px solid;"><?= $trip['selling_price']; ?></div>
							<!-- <div align="center" style="height:20px;border:#444 1px solid;"></div> -->
							<div align="center" style="height:20px;border:#444 1px solid;"><?= (float)$trip['receivables']['origin_detention_charges'] ?></div>
							<div align="center" style="height:20px;border:#444 1px solid;"><?= (float)$trip['receivables']['toll_charges'] ?></div>
							<!-- <div align="center" style="height:20px;border:#444 1px solid;"></div> -->
							<div align="center" style="height:20px;border:#444 1px solid;"><?= (float)$trip['receivables']['origin_detention_days'] ?></div>
							<!-- <div align="center" style="height:20px;border:#444 1px solid;"></div>
							<div align="center" style="height:20px;border:#444 1px solid;"></div> -->
							<div align="center" style="height:20px;border:#444 1px solid;"><?= (float)$trip['receivables']['destination_detention_days'] ?></div>
							<!-- <div align="center" style="height:20px;border:#444 1px solid;"></div>
							<div align="center" style="height:20px;border:#444 1px solid;"></div> -->
				<?php
					echo '</div>';
					if ($tripCount == 3) {
						echo "</div> <pagebreak />";
					}
				 	$tripCount = ($tripCount == 3)?0:($tripCount+1);
				} ?>
				<?php if (count($model)%3 != 0) {
					 echo '</div>';
				}; ?>
			</div>
		</div>
	</div>
</div>
<?php // die(); ?>

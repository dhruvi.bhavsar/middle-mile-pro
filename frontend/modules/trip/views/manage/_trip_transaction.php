<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
/* @var $form yii\widgets\ActiveForm */
if (!isset($hideCTA)) {
  $hideCTA =  0;
}

?>
<?php  //Pjax::begin(['id'=>'trip-transaction-table']); ?>
    <?= GridView::widget([ // white-box
    'options' => ['class'=>' table-responsive'],
    'tableOptions' => ['class'=>'table'],
    'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataTransactions,
    'layout' => "{items}\n<div align=\"center\">{pager}</div>",
    'pager'=>[
          'linkOptions'=>['class'=>'btn-circle m-r-10'],
          'disabledListItemSubTagOptions' => ['class' => 'btn btn-circle disable  m-r-10']
        ],
        'columns' => [
            [
              'attribute' => 'datetime',
              'format'=>'raw',
              'value' => function($model) {
                return '<label class="hidden-lg hidden-md text-muted">Date & Time : </label>'.date('d-m-Y \<\b\r\> h:i A',strtotime($model->datetime));
              }
            ],
            [
              'attribute' => 'amount',
              'format'=>'raw',
              'label'=>'Amt',
              'value' => function($model) { //
                if($model->receipt == 0 ){
                    return '<span class="text-warning" title="Receipt not received"> '  . $model['amount'] .  ' <br /><i class="fa-fw mdi success mdi-alert-circle"></i> Receipt pending <span>';
                }else if($model->receipt == 1){
                     return '<span class="text-success" title="Receipt received"> '  . $model['amount'] .  ' <br /><i class="fa-fw mdi success mdi-check-circle"></i> Receipt received<span>';
                }else {
                    return $model['amount'];
                }
              }
            ],
            [
              'attribute' => 'transaction_type',
              'label'=>'TXN Type',
              'format'=>'raw',
              'value' => function($model) {
                $data = "";
                switch($model->applicable_for){
                    case 1:
                    $data = "<label class='label label-warning lb-sm'>Payable</label>";
                    break;
                    case 2:
                    $data = "<label class='label label-info lb-sm'>Receivable</label>";
                    break;
                }
                $data .= '<br/><small>'.\Yii::$app->params['transaction_type'][$model->transaction_type]."</small>";
                
                if(!empty($model->detention)){
                    $detension = json_decode($model->detention,true);
                    if(!empty($detension['days'])){
                        $data.= "<br/><small>".$detension['days']." day(s)</small>". '<i class="mdi mdi-information-outline" data-toggle="tooltip" title="" data-original-title="' . $detension['entry'] . ' To ' . $detension['exit'] . '"></i>';
                    }
                }
                return $data;
              }
            ],
            [
              'attribute' => 'note',
              'format'=>'raw',
              'value' => function($model) {
                return $model->note;
              }
            ],
            [
              'attribute' => 'status',
              'format'=>'raw',
              'value' => function($model) {
                if(empty($model->approval)){
                     $data = '<p class="text-danger">Operations approval pending</p>';
                     if(Yii::$app->user->id == $model->assigned_to) {
                        $data.='<div class="text-center">'.
                            Html::a('Approve','/trip/manage/approve-transaction?id='.$model->id,['title' => 'Approve','class'=>'btn btn-success btn-xs waves-effect','data-method'=>'POST','data-confirm'=>'Are you sure want to approve transaction ?']). ' ' .
                            Html::a('Reject','/trip/manage/reject-transaction?id='.$model->id,['title' => 'Reject','class'=>'btn btn-danger btn-xs waves-effect','data-method'=>'POST','data-confirm'=>'Are you sure want to reject transaction ?']). ' ' .
                        '</div>';
                     }
                     return $data;
                }else if($model->approval== \frontend\modules\trip\models\Transactions::REJECTED){
                        return '<p class="text-default text-center">' . Yii::$app->params['transactionsApproval'][$model->approval] . '</p>';
                }else{
                    switch ($model->status){
                        case 0:
                            $data = '<p class="text-danger">' . Yii::$app->params['transactionStatus'][$model->status] . '</p>';
                            if(Yii::$app->user->identity->team == 5){
                                        $data .='<div class="text-center">'.
                                                        Html::a('Confirm','/trip/manage/confirm-transaction?id='.$model->id,['title' => 'Confirm','class'=>'btn btn-success btn-xs waves-effect','data-method'=>'POST','data-confirm'=>'Are you sure you checked transaction in bank record ?'])
                                      . '</div>';
                            }
                             return $data;
                        break;
                        case 1:
                            return '<p class="text-success text-center">' . Yii::$app->params['transactionStatus'][$model->status] . '</p>';
                        break;
                        case 2:
                            return '<p class="text-danger text-center">' . Yii::$app->params['transactionStatus'][$model->status] . '</p>';
                        break;
                        case 5:
                            return '<p class="text-default text-center">' . Yii::$app->params['transactionStatus'][$model->status] . '</p>';
                        break;
                    }

                }
                return $model->status;
              }
            ],
            [
            'label' => '',
            'format'=>'raw',
            'value' => function($model) use ($hideCTA){
              if (!empty($hideCTA) || in_array($model->status,[1,2,5]) || $model->approval == 2) {
                return '';
              }

              //Html::a('<i class="mdi mdi-pencil" style="line-height: 0.8;"></i>',NULL, [
//                'title' => 'Edit',
//                'class'=>'btnAddEditTransaction btn btn-circle btn-flat waves-effect m-r-10',
//                'data'=>$model->attributes,
//              ]).
              return 
                  Html::a('<i class="mdi mdi-delete text-danger" style="line-height: 0.8;"></i>',['delete-transaction','id'=>$model->id], [
                'title' => 'Delete',
                'class'=>'btn btn-circle btn-flat waves-effect',
                'data'=>['method'=>'post','pjax'=>'0','confirm'=>'Are you want to delete this transaction?']
              ]);
            }
          ]
        ],
    ]); ?>

  <?php // Pjax::end(); ?>

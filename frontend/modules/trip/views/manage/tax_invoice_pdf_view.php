<?php
$this->registerCss("
/* td {border:1px #ccc solid !important; padding:0px !important;} */
/* td {padding:0px !important;} */
");
?>
<?php 
/* var_dump($model->companyName); die();  */


/* echo Yii::$app->commonFunction->getIndianCurrency(100); die(); */
$customePrice = number_format((float)$model->selling_rate, 2, '.', '');
?>
<div class="trip-view">
<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-invoice">
							<tr><td style="text-align:center;padding:0px;"><b>Tax Invoice</b></td></tr>
							<tr><td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
									<td rowspan="3" style="border:1px solid #000;width:50%;padding:0px;">
										<table class="table table-responsive" style="">
											<tr><td style=""><img src="/images/invoice-logo.jpg" style="width:100px;"/></td>
											<td style="">
											<p><b>Middle Mile Pro LLP</b><br>
											 B-47, Pravasi Industrial Estate,<br>
											Goregaon East Mumbai-400063<br>
											<span style="font-size:12px;">Reg. 103,Rock Garden,Dahisar West Mumbai-400068</span><br>
											GSTIN/UIN: 27ABEFM5858P1ZF<br>
											Contact : 9892593578<br>
											E-Mail : asagore@middlemilepro.com<br>
											www.middlemilepro.com</p>
											</td></tr>
										</table>
									</td>
									<td style="border:1px solid #000;">
										Invoice No.<br><b><?= $model->number ?></b>
									</td>
									<td style="border:1px solid #000;">
										Dated<br><b><?= date('d-M-Y') ?></b>
									</td>
								</tr>
								<tr>
									<td style="border:1px solid #000;">Delivery Note<br><b></b></td>
									<td style="border:1px solid #000;">Mode/Terms of Payment<br><b></b></td>
								</tr>
								<tr>
									<td style="border:1px solid #000;">Supplier's Ref.<br><b></b></td>
									<td style="border:1px solid #000;">Other Reference(s)<br><b style="font-size:10px;"></b></td>
								</tr>
								<tr>
								<td rowspan="5" style="border:1px solid #000;padding:0px;vertical-align: top;">
								Buyer<br>
								<?= $model->companyName->name ?>
								<?php $address = explode(",",$model->companyName->address) ?>
								<?php foreach($address as $line){ ?>
								<?= $line ?>,<br>
								<?php } ?>
								<!--Future Chain Supply Solution Ltd. (BOM)<br>
								701-705,7TH FLOR,349 BUSINES POINT,<br>
								WESTEREN EXPRESS HIGHWAY,<br>
								ANDHERI( EAST)-MUMBAI-400069<br>
								State Name :Maharashtra, Code : 27<br>
									GSTIN/UIN:27AAACF9650N1Z4<br>
									PAN/IT No : <br>
									Place of Supply :Maharashtra--></td>
								<td style="border:1px solid #000;">Buyer's Order No.<br><b>&nbsp;</b></td>
								<td style="border:1px solid #000;">Dated<br><b>&nbsp;</b></td>
								</tr>
								<tr>
								<td style="border:1px solid #000;">Despatch Document No.<br><b>&nbsp;</b></td>
								<td style="border:1px solid #000;">Delivery Note Date<br><b>&nbsp;</b></td>
								</tr>
								<tr>
								<td style="border:1px solid #000;">Despatched through<br><b>BY ROAD</b></td>
								<td style="border:1px solid #000;">Destination<br><b><?= $model->originDetails->name ?> To <?= $model->destinationDetails->name ?></b></td>
								</tr>
								<tr>
								<td style="border:1px solid #000;">Bill of Lading/LR-RR No.<br><b style="font-size:9px;">VH/BWDH/17_18/03407 dt. 23-Sep-2017</b></td>
								<td style="border:1px solid #000;">Motor Vehicle No.<br><b><?= $model->vehicleDetails->registration_number ?></b></td>
								</tr>
								<tr><td colspan="2" style="border:1px solid #000;">Terms of Delivery<br><br><br><br><br><br></td></tr>
							</table>
							</td></tr>
							
							<tr><td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
								<td style="border:1px solid #000;">Sl No.</td>
								<td style="border:1px solid #000;">Description of Goods</td>
								<td style="border:1px solid #000;">HSN/SAC</td>
								<td style="border:1px solid #000;">GST Rate</td>
								<td style="border:1px solid #000;">Part No.</td>
								<td style="border:1px solid #000;">Quantity</td>
								<td style="border:1px solid #000;">Rate</td>
								<td style="border:1px solid #000;">per</td>
								<td style="border:1px solid #000;">Amount</td>
								</tr>
								
								<tr>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">1</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><b>Vehicle Hire Charges</b> Vehicle Type  <?= $model->vehicleType->name ?></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">9973</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">0 %</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><b>1 Nos</b></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><?= $customePrice ?></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">Nos</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><b><?= $customePrice ?></b></td>
								</tr>
								
								<tr>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">&nbsp;</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								</tr>
								
								<tr>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;">Total</td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"><b>1 Nos</b></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"><b>₹ <?= $customePrice ?></b></td>
								</tr>
							</table>
							</td></tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
							<tr><td style="border-right:none;border-top:1px solid #000;border-left:1px solid #000;">Amount Chargeable (in words)</td><td style="border-top:1px solid #000;border-right:1px solid #000;text-align:right;">E. & O.E</td></tr>
							<tr><td colspan="2" style="border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;"><b><?= ucwords(Yii::$app->commonFunction->getIndianCurrency($customePrice))." Only"; ?></b></td></tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
								<td style="width:80%;text-align:center;border:1px solid #000;">HSN/SAC</td>
								<td style="border:1px solid #000;">Taxable Value</td>
								</tr>
								
								<tr>
								<td style="border:1px solid #000;">9973</td>
								<td style="border:1px solid #000;"><?= $customePrice ?></td>
								</tr>
								
								<tr>
								<td style="border:1px solid #000;text-align:right;"><b>Total</b></td>
								<td style="border:1px solid #000;"><b><?= $customePrice ?></b></td>
								</tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;border-left:1px solid #000;border-right:1px solid #000;">
							<tr><td style="" style="">Tax Amount (in words)  : NIL<br><br>&nbsp;</td></tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;border:1px solid #000;">
								<tr><td style="width:50%;padding:0px;border-left:1px solid #000;">
								<table class="table table-responsive">
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>Remarks:</td></tr>
									<tr><td>23-Sep  VH/BWDH/17_18/03407</td></tr>
								</table>
								</td>
								<td style="width:50%;padding:0px;border-right:1px solid #000;">
								<table class="table table-responsive">
									<tr><td colspan="2">Company's Bank Details</td></tr>
									<tr><td>Bank Name</td><td>: <b style="font-size:12px;">Kotak Mahindra Bank 0812171004</b></td></tr>
									<tr><td>A/c No.</td><td>: <b style="font-size:12px;">0812171004</b></td></tr>
									<tr><td>Branch & IFS Code</td><td>: <b style="font-size:12px;">Santacruz & KKBK0000652</b></td></tr>
								</table>
								</td></tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
								<td style="width:50%;border-left:1px solid #000;border-bottom:1px solid #000;"><table class="table table-responsive"><tr><td>Company's PAN</td><td>:<b> ABEFM5858P</b></td></tr>
									<tr><td colspan="2">Declaration</td></tr>
									<tr><td colspan="2">We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</td>
									</tr></table>
								</td>
								<td style="width:50%;border:1px solid #000;padding:0px;"><table class="table table-responsive"><tr><td>for Middle Mile Pro LLP</td></tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr><td>Authorised Signatory</td>
									</tr></table>
								</td>
								</tr>
							</table>
							</td>
							</tr>
							
							<tr><td style="text-align:center;">SUBJECT TO MUMBAI JURISDICTION</td></tr>
							<tr><td style="text-align:center;">This is a Computer Generated Invoice</td></tr>

							

							</tbody>
						</table> <!-- TABLE COLSE -->
					</div>
				</div>
			</div>	<!-- row close -->
		</div>
	</div>
</div>


</div>
<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

// $userList = \common\models\User::find()->select(['id','username'])->asArray()->all();
// $userList = ArrayHelper::map($userList, 'id', 'username');
/* @var $this yii\web\View */
/* @var $model frontend\modules\timesheet\models\ResumePlacements */
/* @var $form yii\widgets\ActiveForm */
if (!isset($forDriver)) {
  $forDriver = 0;
}
?>
<?php $form = ActiveForm::begin(); ?>
<div class="modal fade" id="modalCreateEditTransaction" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"><h4 class="modalTransactionsTitle">Transaction</h4></div>
        <div class="modal-body" >
          <div class="hidden" hidden>
              <?= $form->field($modelTransactions, 'id')->hiddenInput() ?>
          </div>
          <div class="row">

              <div class="col-md-12" <?= !empty($forDriver)?'hidden':'' ?>>
                <?= $form->field($modelTransactions, 'transaction_type')->dropdownList(\Yii::$app->params['transaction_type'],['prompt'=>'Select Transaction Type']) ?>
              </div>

              <div class="col-md-12 transaction-amount" hidden>
                  <div class="col-md-6">
                        <?= $form->field($modelTransactions, 'amount')->textInput() ?>
                  </div>
                  <div class="col-md-6 jsMamul" hidden>
                    <?= $form->field($modelTransactions, 'mamul')->textInput() ?>
                  </div>
              </div>
              <div class="col-md-12 p-l-30 p-r-30 transaction-amount" hidden >
                      <?= $form->field($modelTransactions, 'note')->textInput()->label('Remark'); ?>
              </div>
            <?php /*
            <div class="clearfix"></div>
            <div class="col-md-6" hidden>
              <?= $form->field($modelTransactions, 'datetime')->textInput(['class'=>'form-control datetimepicker']) ?>
            </div>            
            <div class="col-md-6">
              <?= $form->field($modelTransactions, 'transaction_id')->textInput()->label("Bank transaction id") ?>
            </div> */ ?>

              <!--Detention details-->
              <div id="detention-details" style="display:none;">
                    <div class="clearfix"></div>
                      <div class="col-md-5">
                        <?= $form->field($modelTransactions, 'detention[entry]')->textInput(['class'=>'form-control datetimepicker'])->label('Entry date time') ?>
                      </div>
                      <div class="col-md-5">
                        <?= $form->field($modelTransactions, 'detention[exit]')->textInput(['class'=>'form-control datetimepicker'])->label('Exit date time') ?>
                      </div>
                      <div class="col-md-2">
                        <?= $form->field($modelTransactions, 'detention[days]')->textInput()->label("Days") ?>
                      </div>
              </div>
              <div class="clearfix"></div>

              
              <div class="col-md-12 twoway-transaction-amount" hidden>
                 <div class="col-md-3 p-t-10">
                     <input type="checkbox" name="Transactions[is_payable]" id="payables" onclick="$('.payable-fields').toggle();">
                     <label for="payables" id="payable-text">Payable</label>
                 </div>
                 <div class="col-md-3 payable-fields" hidden>
                    <?= $form->field($modelTransactions, 'payable_amount')->textInput(['placeholder'=>'Amount'])->label(false); ?>
                 </div>
                 <div class="col-md-6 payable-fields" hidden>
                    <?= $form->field($modelTransactions, 'payable_remark')->textInput(['placeholder'=>'Remark'])->label(false) ?>
                 </div>
                  <div class="col-md-9 col-md-offset-3 p-t-10 payable-fields" hidden>
                      <input type="checkbox" name="Transactions[receipt]" value="0">
                        <label for="payables" id="payable-text">Receipt required</label>
                  </div>
                  
             </div>
              <hr />
            <div class="col-md-12 twoway-transaction-amount" hidden>
                 <div class="col-md-3 p-t-10">
                     <input type="checkbox" name="Transactions[is_receivable]" id="receivables"  onclick="$('.receivable-fields').toggle();">
                     <label for="receivables" id="receivable-text">Receivable</label>
                 </div>
                 <div class="col-md-4 receivable-fields" hidden>
                    <?= $form->field($modelTransactions, 'receivable_amount')->textInput(['placeholder'=>'Amount'])->label(false); ?>
                 </div>
                 <div class="col-md-5 receivable-fields" hidden>
                    <?= $form->field($modelTransactions, 'receivable_remark')->textInput(['placeholder'=>'Remark'])->label(false) ?>
                 </div>
             </div>


            <?php /*
            <div class="col-md-6 jsParentPaymentType" style="display:none">
              <?= $form->field($modelTransactions, 'payment_type')->dropdownList(\Yii::$app->params['payment_type'],['prompt'=>'Select Payment Type']) ?>
            </div>
            <div class="col-md-6 jsDetentionDays" style="display:none">
              <?= $form->field($modelTransactions, 'detention_days')->textInput() ?>
            </div>
            */ ?>

           
            <div class="clearfix"></div>
           <?php /*
            <br/>
            <br/>
            
            <div class="col-md-12">
              <?= $form->field($modelTransactions, 'note')->textArea() ?>
            </div>
            <div class="clearfix"></div>
            *
            */?>
          </div>
        </div>
          <div class="modal-footer">
            <?= Html::resetButton('Cancel',['class' => 'btn btn-default','data'=>['dismiss'=>'modal']]); ?>
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
          </div>
        </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
<?php $this->registerJs("

 // Toggle Amount fields
  $('#transactions-transaction_type').change(function(){
    if (this.value == 2 || this.value == 3 || this.value == 4 || this.value == 5 || this.value == 33) {
        $('.transaction-amount').show();
        $('.twoway-transaction-amount').hide();
    } else {
        $('.transaction-amount').hide();
        $('.twoway-transaction-amount').show();
    }

    if (this.value == 6 || this.value == 7) {
        $('#detention-details').show();
    }else{
        $('#detention-details').hide();
    }


    if (this.value == 15 || this.value == 16 || this.value == 17) {
        $('#payable-text').text('Vendor');
        $('#receivable-text').text('Customer');
    } else {
        $('#payable-text').text('Payable');
        $('#receivable-text').text('Receivable');
    }
  });

  $('.btnAddEditTransaction').click(function(){
    // $('#transactions-transaction_type,#transactions-amount,#transactions-datetime,#transactions-transaction_id,#transactions-mamul,#transactions-id,#transactions-mamul_type,#transactions-payment_type,#transactions-detention_days,#transactions-note').val('').trigger('change');
    $('#modalCreateEditTransaction').modal();
  });
"); ?>

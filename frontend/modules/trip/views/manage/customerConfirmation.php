<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use frontend\modules\trip\models\Trip;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Alert::widget() ?>
<div class="trip-view">

<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<h3><b>CUSTOMER CONFIRMATION</b> <span class="pull-right"></span></h3>
			<hr>
			<?php $form = ActiveForm::begin(); ?>
			<div class="row">
<!--                <div class="col-md-6">
					<p><b>Targated Rate :</b> <?= $model->targated_rate ?></p>
                </div>-->
                <div class="col-md-2">
					<p><b>Buying Rate :</b> <?= $model->buying_rate ?></p>
                </div>
                <div class="col-md-4">
                    <?= $form->field($newModel, 'selling_rate')->textInput(['value'=>$model->selling_rate]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($newModel, 'advance_selling_charges')->textInput(['value'=>$model->advance_selling_charges]) ?>
                </div>

			</div>
			<div class="row">
				<div class="col-md-6">
				<?php
					$newModel->origin_handling_charges = (!empty($model->origin_handling_charges))?$model->origin_handling_charges:0;
					$newModel->destination_handling_charges = (!empty($model->destination_handling_charges))?$model->destination_handling_charges:0;
				?>
                    <?= $form->field($newModel, 'origin_handling_charges')->textInput(['value'=>$model->origin_handling_charges]) ?>
                </div>
				<div class="col-md-6">
                    <?= $form->field($newModel, 'destination_handling_charges')->textInput(['value'=>$model->destination_handling_charges]) ?>
                </div>
			</div>
				<div class="form-group">
					<?= Html::submitButton($newModel->isNewRecord ? 'Save' : 'Update', ['class' => $newModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					<?= Html::a('Cancel', ['view','id'=>$model->id],['class' => 'btn btn-default']) ?>
				</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<h3><b>TRIP DETAILS</b>
			<span class="pull-right">
			<?php if($model->status < Trip::STATUS_ORDER_CONFIRMED){ ?>
				<?php if(Yii::$app->commonFunction->accessToElement('createTrip')){ ?>
				<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0']) ?>
				<?php } ?>
				&nbsp;
				<?php if(Yii::$app->commonFunction->accessToElement('deleteTrip')){ ?>
				<?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], ['title'=>'Delete','aria-label'=>'Delete','data-pjax'=>'0','data-confirm'=>'Are you sure you want to delete this item?','data-method'=>'post']) ?>
				<?php } ?>
			<?php } ?>
			</span></h3>
			<hr>
			<div class="row">
                <div class="col-md-6">
                    <p><b>Date :</b> <?= date('d-m-Y',strtotime($model->trip_date)); ?></p>
					<p><b>Vehicle Type :</b> <?= $model->vehicleType->name ?></p>
                </div>

                <div class="col-md-6">
                  <p><b>Customer :</b> <?= $model->companyName->name ?></p>
                  <p><b>Contact Person : <?= (!empty($model->contactPersonName->name)) ? $model->contactPersonName->name : ''; ?> </b></p>
                  <p class="text-muted m-l-5">
                	<?= (!empty($model->contactPersonName->designation)) ? $model->contactPersonName->designation : ''; ?>
                	</p>
                  <p class="text-muted m-l-5">
                	<?= (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile : ''; ?>
                	<?= (!empty($model->contactPersonName->email)) ? ' / '.$model->contactPersonName->email : ''; ?>
                	</p>
                </div>

                <div class="col-md-6">
                    <p><b>Origin :</b> <?= $model->originDetails->name ?></p>
                    <p><b>Loading Point :</b> <?php echo (!empty($model->loading_point))?$model->loading_point:'-'; ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Destination :</b> <?= $model->destinationDetails->name ?></p>
                    <p><b>Unloading Point :</b> <?php echo (!empty($model->unloading_point))?$model->unloading_point:'-'; ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Service Type :</b> <?= Yii::$app->params['serviceType'][$model->service_type] ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Targeted Rate :</b><?php echo (isset($model->targated_rate))?$model->targated_rate:' -'; ?></p>
                    <p><b>Buying Rate :</b><?php echo (isset($model->buying_rate))?$model->buying_rate:' -'; ?></p>
                    <p><b>Selling Rate :</b><?php echo (isset($model->selling_rate))?$model->selling_rate:' -'; ?></p>
                </div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Origin Handling Charges :</b> <?php echo (!empty($model->origin_handling_charges))?$model->origin_handling_charges:'-' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Destination Handling Charges :</b> <?php echo (!empty($model->destination_handling_charges))?$model->destination_handling_charges:'-';  ?></p>
                </div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Trip Type :</b> <?= Yii::$app->params['tripType'][$model->trip_type] ?></p>
                </div>

				<?php if($model->trip_type == 2){ ?>
                <div class="col-md-12">
                    <!--p><b>Stops :</b> </p-->
					<div class="table-responsive">
						<table class="table color-table muted-table">
							<thead>
								<tr>
									<th>Stop Name</th>
									<th>Type</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach($stopsModel as $stop){ ?>
								<tr>
									<td><?= $stop['point'] ?></td>
									<td><?= $stop['stop_type'] ?></td>
								</tr>
							<?php }
							?>
							</tbody>
						</table>
					</div>
                </div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

</div>

<?php
$this->registerJs("
$('.vendor_id').click(function(){
	$('#trip-vendor_id').val($(this).val());
});
");
?>

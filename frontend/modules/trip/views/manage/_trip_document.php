<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
/* @var $form yii\widgets\ActiveForm */
if (!isset($hideCTA)) {
  $hideCTA =  0;
}
?>


  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
  <div class="row">
    <div class="col-md-12">
      <?= $form->field($modelDocumentUpload, 'document')->fileInput() ?>
    </div>
    <div class="col-md-6">
      <?= $form->field($modelDocumentUpload, 'reference_number')->textInput(['class'=>'form-control','autocomplete'=>'off']) ?>
    </div>
    <div class="col-md-6">
      <?= $form->field($modelDocumentUpload, 'type')->dropdownList(\Yii::$app->params['documents'],['prompt'=>'Select Document Type']) ?>
    </div>
    <div class="col-md-6">
      <?php $modelDocumentUpload->date = date('d-m-Y') ?>
      <?= $form->field($modelDocumentUpload, 'date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?>
    </div>
    <div class="col-md-6">
      <?= $form->field($modelDocumentUpload, 'expire_date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?>
    </div>
  </div>
  <?= Html::submitButton('Upload', ['class' => 'btn btn-success','id'=>'send-button']) ?>
  <?php $form = ActiveForm::end(); ?>
  <br><br>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr style="line-height:0">
          <th>Document</th>
          <th>Date</th>
          <th>Expire Date</th>
          <th>Details</th>
          <th> </th>
        </tr>
      </thead>
      <tbody>
      <?php if (!empty($modelDocumentUpload->data_document)) {
            $uploadedDocuments = json_decode($modelDocumentUpload->data_document,true);
            foreach ($uploadedDocuments as $document) {
              $user = User::findOne($document['by']);
              ?>
              <tr style="line-height:0.2">
                <td>
                  <?= isset($document['reference_number'])?$document['reference_number']:'' ?>
                  <br>
                  <?= \Yii::$app->params['documents'][$document['type']]; ?>
                </td>
                <td ><?= '<label class="hidden-lg hidden-md text-muted">Date : </label>'.(isset($document['date'])?$document['date']:''); ?></td>
                <td ><?= '<label class="hidden-lg hidden-md text-muted">Expire Date : </label>'.(isset($document['expire_date'])?$document['expire_date']:''); ?></td>
                <td>
                  <?= '<label class=" text-muted">On : </label>'.date('d-m-Y h:i A',strtotime($document['on'])); ?>
                  <br>
                  <?= '<label class=" text-muted">By : </label>'.$user->username; ?>
                </td>
                <td class="grid-action-column text-center">
                  <a class="btn btn-circle btn-flat waves-effect" target="_blank" href="<?= '/uploads/'.$document['document']; ?>" title="View Document"><i class="mdi mdi-eye"></i></a>
                </td>
              </tr>
      <?php	}
        } else{ ?>
        <tr>
          <td colspan="10" class="text-center">No Records...</td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>

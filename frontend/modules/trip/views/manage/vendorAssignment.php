<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use common\widgets\Alert;
use frontend\modules\trip\models\Trip;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title                   = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
.vendor-row label {	font-weight: 400 !important; width: 100%; }
.vendor-row label:hover { color:#000; cursor:pointer; }
");
?>
<?= Alert::widget() ?>
<div class="trip-view">

    <div class="row">
        <div class="col-md-12">
            <div class="white-box printableArea">
                <h3><b>Vendor Assignment</b> <span class="pull-right"></span></h3>
                <hr>
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <?php if ($model->status == Trip::STATUS_VENDOR_RE_ASSIGN) { ?>
                        <div class="col-md-12 alert alert-warning">
                            <p><label class="control-label">Re-Assign vendor (Reason): </label>&nbsp;&nbsp;<span><?= ($model->negotiation_remark) ?><span></p>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-12">
                                        <p><label class="control-label" for="trip-vendor_id">Vendor</label></p>
                                        <div class="table-responsive">
                                            <table class="table color-table muted-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Contact</th>
                                                        <th>Category</th>
                                                        <th>Type</th>
                                                        <th>Bids</th>
                                                        <th>SMS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $bids = json_decode($model->vendor_bids,
                                                        true);
                                                    foreach ($vendors as $obj) {
                                                        ?>
                                                        <tr class="vendor-row">
                                                            <td><input type="radio" name="vendor" class="vendor_id" id="vendor_<?= $obj['id'] ?>" value="<?= $obj['id'] ?>" <?= ($obj['id']
                                                    == $model->vendor_id) ? 'checked'
                                                            : ''; ?>/></td>
                                                            <td><label for="vendor_<?= $obj['id'] ?>"><?= $obj['name'] ?></label></td>
                                                            <td><label for="vendor_<?= $obj['id'] ?>"><?= $obj['mobile'] ?></label></td>
                                                            <td><label for="vendor_<?= $obj['id'] ?>"><?= $obj['category'] ?></label></td>
                                                            <td><label for="vendor_<?= $obj['id'] ?>"><?= $obj['type'] ?></label></td>
                                                            <td  style="max-width:110px;">
                                                                <div class="form-group has-success m-0 input-group input-group-sm" style="max-width:150px;">
                                                                    <input type="number" style="font-size:14px" id="trip-targated_rate" class="form-control" name="Trip[targated_rate]" aria-invalid="false" disabled="true" value="<?= isset($bids[$obj['id']])
                                                            ? $bids[$obj['id']] : 0 ?>">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-default bidSubmitBtn mdi mdi-pencil" data-vendor_id = "<?= $obj['id']; ?>" style="font-size:14px" type="button" onclick="$(this).toggleClass('mdi-pencil mdi-floppy');$(this).parents('.form-group').find('input').removeAttr('disabled');"></button>
                                                                    </div>
                                                                    <div class="help-block m-0"></div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <span class="mytooltip tooltip-effect-3" style="position: absolute;"> <span class="tooltip-item"><button
                                                                            data-vid='<?= $obj['id']; ?>'
                                                                            data-mob="<?= $obj['mobile'] ?>"
                                                                            type="button"
                                                                            class="btn btn-outline  js-send-sms"
                                                                            <?php //data-toggle="tooltip" ?>
                                                                            data-msg="Dear <?= ($obj['name']); ?>,
                                                                            Load available : <?= $model->vehicleType->name; ?> from <?= ($model->originDetails->name) ?> to <?= ($model->destinationDetails->name); ?> call/sms/whatsapp to Mr.Shukla 8356050104"
    <?php //data-placement="left"  ?>
    <?php //title="Send SMS"   ?>
                                                                            ><i class="mdi mdi-message-reply-text text-info"></i></button></span> <span class="tooltip-content clearfix"> <span class="tooltip-text" style="padding:15px 20px 15px 15px">
                                                                            Dear <?= ($obj['name']); ?>, <br>
                                                                            Load available : <?= $model->vehicleType->name; ?> <br>
                                                                            from <?= ($model->originDetails->name) ?> to <?= ($model->destinationDetails->name); ?> <br>
                                                                            call/sms/whatsapp to Mr.Shukla 8356050104
                                                                        </span> </span> </span>

                                                            </td>
                                                        </tr>
<?php } ?>
                                                    <tr>
                                                        <td colspan="5" style="text-align:center;"><button type="button" class="btn btn-primary" id="newVendor">New Vendor</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group" id="divNewVendor" style="display:none;">
                                                <!--input type="text" name="new_vendor" id="new_vendor_name" class="form-control"-->
                                                <?=
                                                Select2::widget([
                                                    'name' => 'new_vendor',
                                                    'data' => \frontend\modules\vendor\models\Vendor::VendorList(),
                                                    'options' => [
                                                        'placeholder' => 'Select Vendor ...',
                                                        'tags' => true,
                                                        'allowClear' => true,
                                                        'id' => 'new_vendor_name',
                                                    ],
                                                    'pluginOptions' => ['tags' => true,
                                                        'allowClear' => true],
                                                    'pluginEvents' => [
                                                        'select2:select' => 'function(e) {
								$("#trip-vendor_id").val($("#new_vendor_name").val());
								}',
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
<?= $form->field($newModel, 'vendor_id')->hiddenInput(['value' => $model->vendor_id])->label(false) ?>

                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($newModel,
                                            'buying_rate')->textInput(['value' => $model->buying_rate]) ?>
                                    <?php $newModel->buying_rate_incl_handling_charges
                                        = $model->buying_rate_incl_handling_charges; ?>
<?= $form->field($newModel, 'buying_rate_incl_handling_charges')->checkbox() ?>
                                    </div>

                                    </div>
                                    <div class="form-group">
<?= Html::submitButton($newModel->isNewRecord ? 'Save' : 'Update',
    ['class' => $newModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<?= Html::a('Cancel', ['view', 'id' => $model->id],
    ['class' => 'btn btn-default']) ?>
                                    </div>
                                                        <?php ActiveForm::end(); ?>
                                    </div>
                                    </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="white-box printableArea">
                                                <h3><b>TRIP DETAILS</b>
                                                    <span class="pull-right">
<?php if ($model->status < Trip::STATUS_ORDER_CONFIRMED) { ?>
    <?php if (Yii::$app->commonFunction->accessToElement('createTrip')) { ?>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>',
            ['update', 'id' => $model->id],
            ['title' => 'Update', 'aria-label' => 'Update', 'data-pjax' => '0']) ?>
    <?php } ?>
                                                            &nbsp;
    <?php if (Yii::$app->commonFunction->accessToElement('deleteTrip')) { ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>',
            ['delete', 'id' => $model->id],
            ['title' => 'Delete', 'aria-label' => 'Delete', 'data-pjax' => '0', 'data-confirm' => 'Are you sure you want to delete this item?',
            'data-method' => 'post']) ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                    </span></h3>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p><b>Date :</b> <?= date('d-m-Y',
                                                                strtotime($model->trip_date)); ?></p>
                                                        <p><b>Vehicle Type :</b> <?= $model->vehicleType->name ?></p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <p><b>Customer :</b> <?= $model->companyName->name ?> <?= !empty($model->companyName->address)
                                                                    ? "(".$model->companyName->address.")"
                                                                    : "" ?></p>
                                                        <p><b>Contact Person : <?= (!empty($model->contactPersonName->name))
                                                                    ? ($model->contactPersonName->name)
                                                                    : ''; ?> </b></p>
                                                        <p class="text-muted m-l-5">
<?= (!empty($model->contactPersonName->designation)) ? $model->contactPersonName->designation
        : ''; ?>
                                                        </p>
                                                        <p class="text-muted m-l-5">
<?= (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile
        : ''; ?>
<?= (!empty($model->contactPersonName->email)) ? ' / '.$model->contactPersonName->email
        : ''; ?>
                                                        </p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <p><b>Origin :</b> <?= (!empty($model->originDetails->name))
        ? $model->originDetails->name : ''; ?></p>
                                                        <p><b>Loading Point :</b> <?= (!empty($model->loading_point))
        ? $model->loading_point : '-'; ?></p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <p><b>Destination :</b> <?= $model->destinationDetails->name ?></p>
                                                        <p><b>Unloading Point :</b> <?php echo (!empty($model->unloading_point))
        ? $model->unloading_point : '-'; ?></p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <p><b>Service Type :</b> <?= Yii::$app->params['serviceType'][$model->service_type] ?></p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <p><b>Targeted Rate :</b><?php echo (isset($model->targated_rate))
        ? $model->targated_rate : ' -'; ?></p>
                                                        <p><b>Buying Rate :</b><?php echo (isset($model->buying_rate))
        ? $model->buying_rate : ' -'; ?></p>
                                                        <p><b>Selling Rate :</b><?php echo (isset($model->selling_rate))
                                                                            ? $model->selling_rate
                                                                            : ' -'; ?></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p><b>Origin Handling Charges :</b> <?php echo (!empty($model->origin_handling_charges))
                                                                            ? $model->origin_handling_charges
                                                                            : '-' ?></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p><b>Destination Handling Charges :</b> <?php echo (!empty($model->destination_handling_charges))
                                                                            ? $model->destination_handling_charges
                                                                            : '-'; ?></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p><b>Trip Type :</b> <?= Yii::$app->params['tripType'][$model->trip_type] ?></p>
                                                    </div>

                                    <?php if ($model->trip_type
                                        == 2) { ?>
                                                        <div class="col-md-12">
                                                            <!--p><b>Stops :</b> </p-->
                                                            <div class="table-responsive">
                                                                <table class="table color-table muted-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Stop Name</th>
                                                                            <th>Type</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                        <?php foreach ($stopsModel as $stop) { ?>
                                                                            <tr>
                                                                                <td><?= $stop['point'] ?></td>
                                                                                <td><?= $stop['stop_type'] ?></td>
                                                                            </tr>
                                        <?php }
                                        ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <?php /* DetailView::widget([
                                      'model' => $model,
                                      'attributes' => [
                                      'id',
                                      'number',
                                      'vendor_id',
                                      'company_id',
                                      'contact_person_id',
                                      'vehicle_type_id',
                                      'targated_rate',
                                      'buying_rate',
                                      'selling_rate',
                                      'origin_handling_charges',
                                      'destination_handling_charges',
                                      'trip_type',
                                      'origin',
                                      'origin_lat',
                                      'origin_lon',
                                      'destination',
                                      'destination_lat',
                                      'destination_lon',
                                      'service_type',
                                      'trip_date',
                                      'status',
                                      ],
                                      ]) */ ?>

                                    </div>

                                    <?php
                                    $this->registerJs("
// To send sms to vendor
$('.js-send-sms').click(function(){
  var mob = $(this).data('mob');
  var vid = $(this).data('vid');
  var msg = $(this).data('msg');
  var confirmation = confirm('Do you want to send this sms? '+ msg);
  if (confirmation == true) {
    $.post('/trip/manage/sendquotationsms', {mobile:mob, vendor_id:vid, message:msg},function(data) {
      //Set message
      $('body').append('<div  class=\"alert-success myadmin-alert alert myadmin-alert-top-right block fade in\" style=\"display:block\"><button type=\"button\" class=\"m-l-10 close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button><h4>Message Sent Successfully.</h4></div>');
      // Auto close alert
      window.setTimeout(function() {
        $('.myadmin-alert').fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
        });
      }, 4000);
    });
  }
});

$('.vendor_id').click(function(){
	$('#trip-vendor_id').val($(this).val());
});
$('#btnAddVendor').click(function(){
	$('#trip-vendor_id').val($('#new_vendor').val());
});

$('#newVendor').click(function(){
	$('.vendor_id').prop('checked', false);
	$('#trip-vendor_id').val('');
	$('#divNewVendor').show();
});

$('#new_vendor_name').keyup(function(){
	$('#trip-vendor_id').val($(this).val());
});

$('.bidSubmitBtn').click(function(){
  var e = $(this);
  var vendor_id = e.data('vendor_id');
  var bid_amount = e.parents('.form-group').find('input').val();
  if (e.hasClass('mdi-floppy')) {
    return true;
  }
   e.parents('.form-group').find('input').attr('disabled','true');
  $.post('vendor-bids', {id:".$model->id.",vendor_id:vendor_id,bid_amount:bid_amount},function(result) {
          console.log(result);
  });
});

");
                                    ?>

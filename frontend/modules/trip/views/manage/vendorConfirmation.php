<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\widgets\Alert;
use frontend\modules\trip\models\Trip;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<?= Alert::widget() ?>
<div class="trip-view">

<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			<h3><b>VENDOR CONFIRMATION</b> <span class="pull-right"></span></h3>
			<hr>
			<?php $vendorDetails = $model->vendorDetails;  ?>
			<div class="row">
                <div class="col-md-6">
                    <p><b>Vendor Name :</b> <?= !empty($vendorDetails)? $vendorDetails->name:"" ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Mobile :</b> <?= !empty($vendorDetails)? $vendorDetails->mobile :'' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Email :</b> <?= !empty($vendorDetails)? $vendorDetails->email :'' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Category :</b> <?= !empty($vendorDetails)? $vendorDetails->vendorCategory->name :'' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Type :</b> <?= !empty($vendorDetails)? $vendorDetails->vendorType->name:'' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Buying Rate  :</b> <?php echo (isset($model->buying_rate))?$model->buying_rate:' -'; ?>&nbsp;&nbsp;<span>(<?= ($model->buying_rate_incl_handling_charges)?'Including':'Excluding' ?> Handling Charges)</span></p>
                </div>
			</div>
			<hr>
			<?php $form = ActiveForm::begin(); ?>
			<div class="row">
                <div class="col-md-6">
											<?= $form->field($newModel, 'vehicle_id')->widget(Select2::classname(), [
												'data' => \frontend\modules\vendor\models\Vehicle::vendorVehicleList($model->vendor_id),
												'options' => ['placeholder' => 'Select a Vehicle ...','value'=>$model->vehicle_id],
												'pluginOptions' => [
													'tags' => true,
												],
											]);
											?>
                </div>

				<div class="col-md-6">
                <?= $form->field($newModel, 'advance_buying_charges')->textInput(['value'=>$model->advance_buying_charges]) ?>
        </div>
    </div>


			<fieldset class="b-r b-l b-t b-b p-10 m-b-10">
				<legend class="inline b-0 text-muted" style="width: auto;"><small><small>To enable driver login</small></small> </legend>
				<div class="row">
						<div class="col-md-6">

								<script type="text/javascript">
									var driverStaffListWithDetails = <?= json_encode(\frontend\modules\trip\models\TripDriver::driverStaffListWithDetails(),true); ?>;
								</script>
							<?= $form->field($newModel, 'driver_id')->widget(Select2::classname(), [
								'data' => \frontend\modules\trip\models\TripDriver::driverStaffList(),
								'options' => ['placeholder' => 'Select a Driver ...','value'=>$model->driver_id],
								'pluginOptions' => ['tags' => true,'allowClear' => true],
								'pluginEvents' => [
										'select2:select' => 'function(e) {
											console.log(e.params.data.id);
											var driver_name = $("#tripdriver-name");
											var driver_mobile = $("#tripdriver-mobile");
											console.log();
											if(parseInt(e.params.data.id)){
												try {
														driver_name.val(driverStaffListWithDetails[e.params.data.id].username);
														driver_mobile.val(driverStaffListWithDetails[e.params.data.id].mobile);
												} catch (error) {}
											}else{
												 driver_name.val("");
												 driver_mobile.val("");
											 }
										}',
									],
							])->label('Driver Email');
							?>
						</div>
						<div class="col-md-6"><?= $form->field($newModel, 'password')->textInput(['value'=>$model->password]) ?></div>
				</div>
			</fieldset>

				<div class="row">
	          <div class="col-md-6"> <?= $form->field($driverModel, 'name')->textInput() ?></div>
						<div class="col-md-6"><?= $form->field($driverModel, 'mobile')->textInput() ?></div>
				</div>
        <?= $form->field($driverModel, 'trip_id')->hiddenInput(['value'=>$model->id])->label(false); ?>
				<div class="form-group">
					<?= Html::submitButton(($model->status == Trip::STATUS_ORDER_CONFIRMED) ? 'Save' : 'Confirm Order', ['class' => $newModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					<?= Html::a('Cancel', ['view','id'=>$model->id],['class' => 'btn btn-default']) ?>
				</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<?php /*
<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<h3><b>TRIP DETAILS</b>
			<span class="pull-right">
			<?php if($model->status < Trip::STATUS_ORDER_CONFIRMED){ ?>
				<?php if(Yii::$app->commonFunction->accessToElement('createTrip')){ ?>
				<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0']) ?>
				<?php } ?>
				&nbsp;
				<?php if(Yii::$app->commonFunction->accessToElement('deleteTrip')){ ?>
				<?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], ['title'=>'Delete','aria-label'=>'Delete','data-pjax'=>'0','data-confirm'=>'Are you sure you want to delete this item?','data-method'=>'post']) ?>
				<?php } ?>
			<?php } ?>
			</span></h3>
			<hr>
			<div class="row">
                <div class="col-md-6">
                    <p><b>Date :</b> <?= date('d-m-Y',strtotime($model->trip_date)); ?></p>
					<p><b>Vehicle Type :</b> <?= $model->vehicleType->name ?></p>
                </div>

                <div class="col-md-6">
                  <p><b>Customer :</b> <?= $model->companyName->name ?></p>
                  <p><b>Contact Person : <?= (!empty($model->contactPersonName->name)) ? ($model->contactPersonName->name) : ''; ?> </b></p>
                  <p class="text-muted m-l-5">
          				<?= (!empty($model->contactPersonName->designation)) ? $model->contactPersonName->designation : ''; ?>
          				</p>
                  <p class="text-muted m-l-5">
          				<?= (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile : ''; ?>
          				<?= (!empty($model->contactPersonName->email)) ? ' / '.$model->contactPersonName->email : ''; ?>
          				</p>
                </div>

                <div class="col-md-6">
                    <p><b>Origin :</b> <?= $model->originDetails->name ?></p>
                    <p><b>Loading Point :</b> <?php echo (!empty($model->loading_point))?$model->loading_point:'-'; ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Destination :</b> <?= $model->destinationDetails->name ?></p>
                    <p><b>Unloading Point :</b> <?php echo (!empty($model->unloading_point))?$model->unloading_point:'-'; ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Service Type :</b> <?= Yii::$app->params['serviceType'][$model->service_type] ?></p>
                </div>

                <div class="col-md-6">
                    <p><b>Targeted Rate :</b><?php echo (isset($model->targated_rate))?$model->targated_rate:' -'; ?></p>
                    <p><b>Buying Rate :</b><?php echo (isset($model->buying_rate))?$model->buying_rate:' -'; ?></p>
                    <p><b>Selling Rate :</b><?php echo (isset($model->selling_rate))?$model->selling_rate:' -'; ?></p>
                </div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Origin Handling Charges :</b> <?php echo (!empty($model->origin_handling_charges))?$model->origin_handling_charges:'-' ?></p>
                </div>
				<div class="col-md-6">
                    <p><b>Destination Handling Charges :</b> <?php echo (!empty($model->destination_handling_charges))?$model->destination_handling_charges:'-';  ?></p>
                </div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <p><b>Trip Type :</b> <?= Yii::$app->params['tripType'][$model->trip_type] ?></p>
                </div>

				<?php if($model->trip_type == 2){ ?>
                <div class="col-md-12">
                    <!--p><b>Stops :</b> </p-->
					<div class="table-responsive">
						<table class="table color-table muted-table">
							<thead>
								<tr>
									<th>Stop Name</th>
									<th>Handling Charges</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach($stopsModel as $stop){ ?>
								<tr>
									<td><?= $stop['name'] ?></td>
									<td><?= $stop['handling_charges'] ?></td>
								</tr>
							<?php }
							?>
							</tbody>
						</table>
					</div>
                </div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

*/ ?>
    <?php /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'number',
            'vendor_id',
            'company_id',
            'contact_person_id',
            'vehicle_type_id',
            'targated_rate',
            'buying_rate',
            'selling_rate',
            'origin_handling_charges',
            'destination_handling_charges',
            'trip_type',
            'origin',
            'origin_lat',
            'origin_lon',
            'destination',
            'destination_lat',
            'destination_lon',
            'service_type',
            'trip_date',
            'status',
        ],
    ])*/ ?>

</div>



<?php
$this->registerJs("
$('.vendor_id').click(function(){
	$('#trip-vendor_id').val($(this).val());
});

");


?>

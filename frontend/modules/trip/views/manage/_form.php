<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\View;
use frontend\modules\trip\models\Trip;
use yii\helpers\Url;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('@web/plugins/bower_components/nestable/nestable.css',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/nestable/jquery.nestable.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCss("
#trip-trip_date {
    color: #565656;
    height: 38px;
    width: 100%;
    padding: 7px 12px;
    transition: all 300ms linear 0s;
    background-color: #ffffff;
    border: 1px solid #e4e7ea;
    border-radius: 0px
}
.hcharges {	float:right; }
.hcharges input { height: 30px;margin: 10px 0px; padding: 0px 5px;}
.dd-item { border:1px solid rgba(120, 130, 140, 0.13) !important; min-height: 50px; margin: 5px 0px;}
.dd-handle {border:0px solid rgba(120, 130, 140, 0.13) !important; float:left;}
.dd-close { padding: 5px; cursor: pointer; }
");

$select2Options = [
    'multiple' => false,
    'theme' => 'krajee',
    'placeholder' => 'Select a Person ...',
    'language' => 'en-US',
    'width' => '100%',
    'tags' => true,
    'allowClear' => true,
];
?>

<script type="text/javascript">
    var customerNotificationDuration = <?=
json_encode(\frontend\modules\customer\models\Customer::customerNotificationDuration(),
    true)
?>;
</script>

<div class="row">
    <div class="col-md-12">
        <div class="white-box printableArea">

            <div class="trip-form">

                <?php $form           = ActiveForm::begin(); ?>
<?php if ($model->status < Trip::STATUS_ORDER_CONFIRMED) { ?>


                    <div class="row">
                        <div class="col-md-1">
    <?= $form->field($model, 'number')->textInput([
        'maxlength' => true]) ?>
                        </div>
                        <div class="col-md-5">
                            <?=
                            $form->field($model, 'company_id')->widget(Select2::classname(),
                                [
                                'data' => \frontend\modules\customer\models\Customer::CustomerList(),
                                'options' => ['placeholder' => 'Select a Customer ...',
                                ],
                                'pluginOptions' => [
                                    'tags' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    'select2:select' => 'function(e) { populateCustPersons(e.params.data.id); }',
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-6" id="person">
                            <?php //echo $form->field($model, 'contact_person_id')->dropDownList(\frontend\modules\customer\models\Contact::ContactList($model->company_id),['value'=>$model->contact_person_id,'prompt'=>"Select Customer"]) ?>
                            <?php
                            if ($model->isNewRecord) {
                                $contactPersonsList = [];
                            } else {
                                $contactPersonsList = \frontend\modules\customer\models\Contact::ContactList($model->company_id);
                            }
                            ?>
                            <?=
                            $form->field($model, 'contact_person_id')->widget(Select2::classname(),
                                [
                                'data' => $contactPersonsList,
                                'options' => ['placeholder' => 'Select a Person ...',],
                                'pluginOptions' => ['tags' => true, 'allowClear' => true,],
                            ]);
                            ?>
                        </div>
                    </div>

                    <hr />

                    <div class="row">
                        <div class="col-md-6">
                                <?= $form->field($model,
                                    'loading_point')->textInput()->label("Loading point") ?>
                            <div class="hidden" hidden>
    <?= $form->field($model, 'origin_lat')->hiddenInput()
    ?>
                            <?= $form->field($model,
                                'origin_lon')->hiddenInput() ?>
                            </div>
                        </div>
                        <div class="col-md-6">
    <?= $form->field($model, 'unloading_point')->textInput()->label("Unloading point"); ?>
                            <div class="hidden" hidden>
    <?= $form->field($model, 'destination_lat')->hiddenInput() ?>
    <?= $form->field($model, 'destination_lon')->hiddenInput() ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="text-center">
                            <span  id="Totalkm" style="display:none;">Total distance <span class="label label-info"></span></span>
                            <?= $form->field($model, 'approx_distance')->hiddenInput()->label(false); ?>
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-outline-info" onclick="$('#intermittent-stop-holder').show(500);" ><i class="fa-fw mdi mdi-plus"></i> Add Intermittent stop's</button>
                        </p>
                    </div>

                    <div  id="intermittent-stop-holder" style="border:1px solid #ccc;margin:0 15px;display: none;">
                        <div class="row p-20"  >
                            <div class="col-sm-12 col-md-12">
                                <div class="col-md-6">
                                    <input type="text" class="form-control mb-2" id="intermittent-stop-input" placeholder="Enter a location">
                                    <input type="hidden" id="intermittent-lat">
                                    <input type="hidden" id="intermittent-lon">
                                </div>
                                <div class="col-md-4">
                                    <select  id="stop_type" class="form-control">
                                        <option value="loading">Loading</option>
                                        <option value="unloading">Unloading</option>
                                        <option value="loading and unloading" >Loading and Unloading</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success" onclick="saveIntermittentStop();">Save</button>
                                </div>
                            </div>
                        </div>

                        <div class="p-t-10 p-b-10" id="intermittent-stop-list">
                            <!-- Intermittent stop list -->
                        </div>

                    </div>

                    <hr />
                    <?php /*
                      <div class="row">
                      <?php
                      $model->origin_handling_charges      = (!empty($model->origin_handling_charges))
                      ? $model->origin_handling_charges : 0;
                      $model->destination_handling_charges = (!empty($model->destination_handling_charges))
                      ? $model->destination_handling_charges : 0;
                      ?>
                      <div class="col-md-4">
                      <?php // $form->field($model, 'origin')->textInput(['maxlength' => true]) ?>
                      <?php
                      echo $form->field($model, 'origin')->widget(Select2::classname(),
                      [
                      'data' => \frontend\modules\master\models\City::cityList(),
                      'options' => ['placeholder' => 'Select a Location ...'],
                      'pluginOptions' => ['tags' => true, 'allowClear' => true],
                      ]);
                      ?>
                      </div>
                      <div class="col-md-2">
                      <?= $form->field($model, 'origin_handling_charges')->textInput()->label('Handling Charges'); ?>
                      </div>

                      <div class="col-md-4">
                      <?php //$form->field($model, 'destination')->textInput(['maxlength' => true])  ?>
                      <?=
                      $form->field($model, 'destination')->widget(Select2::classname(),
                      [
                      'data' => \frontend\modules\master\models\City::cityList(),
                      'options' => ['placeholder' => 'Select a Location ...'],
                      'pluginOptions' => ['tags' => true, 'allowClear' => true],
                      ]);
                      ?>
                      </div>
                      <div class="col-md-2">
                      <?= $form->field($model,
                      'destination_handling_charges')->textInput()->label('Handling Charges') ?>
                      </div>
                      </div>
                     */ ?>

                    <div class="row">
                        <div class="col-md-6">
                            <!-- <?= $form->field($model, 'vehicle_type_id')->textInput() ?> -->
                            <?=
                            $form->field($model, 'vehicle_type_id')->widget(Select2::classname(),
                                [
                                'data' => \frontend\modules\master\models\VehicleType::vehicletypelist(),
                                'options' => ['placeholder' => 'Select a Vehicle Type ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label" for="trip-trip_date">Vehicle Placement Date</label>
                            <?php
                            $model->trip_date    = $model->isNewRecord ? date('d-m-Y H:i')
                                    : date('d-m-Y H:i',
                                    strtotime($model->trip_date));
                            ?>
    <?=
    $form->field($model, 'trip_date')->textInput([
        'class' => 'datetimepicker'])->label(false)
    ?>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6">
    <?php
    $service_types       = Yii::$app->params['serviceType'];
    krsort($service_types);
    ?>
                        <?= $form->field($model,
                            'service_type')->dropDownList($service_types); ?>
                        </div>
                        <div class="col-md-6"><?= $form->field($model, 'load_type')->dropDownList(Yii::$app->params['load_type'],
                        ['prompt' => 'Select Load Type']) ?></div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?=
                            $form->field($model, 'assigned_to')->widget(Select2::classname(),
                                [
                                'data' => \frontend\modules\staff\models\Staff::allStaffList(2),
                                'options' => ['placeholder' => 'Select team member'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'tags' => true,
                                    'multiple' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-6">
    <?= $form->field($model, 'targated_buying')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                    <?= $form->field($model,
                        'special_instructions')->textarea(); ?>
                        </div>

                    </div>



                    <!--                    <div class="row">
                                            <div class="col-md-6">
    <?= $form->field($model, 'targated_rate')->textInput()
    ?>
                                            </div>
                                            <div class="col-md-6">
                    <?=
                    $form->field($model, 'notification_duration')->textInput()
                    ?>
                                            </div>
                                        </div>-->




                    <?php
                    if (empty($model->status)) {
                        $model->status = Trip::STATUS_VENDOR_ASSIGN_PENDING;
                    }
                    ?>
                    <?= $form->field($model,
                        'status')->hiddenInput()->label(false)
                    ?>

    <?php /* <?= $form->field($model, 'vendor_id')->textInput() ?>
      <?= $form->field($model, 'origin_lat')->textInput() ?>
      <?= $form->field($model, 'origin_lon')->textInput() ?>
      <?= $form->field($model, 'destination_lat')->textInput() ?>
      <?= $form->field($model, 'destination_lon')->textInput() ?>
      <?= $form->field($model, 'buying_rate')->textInput() ?>
      <?= $form->field($model, 'selling_rate')->textInput() ?>
     */ ?>
<?php } else { ?>
                    <label for="">Order Id </label><br> <?= $model->number; ?> <br><br>

                    <div class="row">
                        <div class="col-md-6">
    <?php if (empty($model->selling_rate)) { ?>
        <?= $form->field($model, 'selling_rate')->textInput() ?>
    <?php } else { ?>
                                <label for="">Selling Rate </label><br> <?= $model->selling_rate; ?>
                    <?php } ?>
                        </div>
                        <div class="col-md-6">
                        <?php $model->trip_date = date('d-m-Y',
                            strtotime($model->trip_date));
                        ?>
                        <?= $form->field($model, 'trip_date')->textInput(['class' => 'datepicker form-control']) ?>
                        </div>
                    </div>


<?php } ?>
                <div class="form-group">
<?=
Html::submitButton($model->isNewRecord ? 'Save' : 'Update',
    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
?>
<?=
Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-default'])
?>
                </div>
<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs("
        var updateOutput = function(e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
                try {
                        if (window.JSON) {
                            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                        } else {
                            output.val('JSON browser support required for this demo.');
                        }
                    } catch (err) {

                    }
        };
        
        $('#nestable').nestable({ group: 1 }).on('change', updateOutput);

        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $(document).on('click','.dd-close',function(){
          $(this).parent().parent().remove();
        });

		// Conditional visibility of multistop inputs
		$('#trip-trip_type').change(function(){
			var elem = $(this);
			$('#stops').hide();
			if(elem.val() == '2'){
				$('#stops').show();
			}
		});

		$(document).ready(function(){
			var elem = $('#trip-trip_type');
			$('#stops').hide();
			if(elem.val() == '2'){
				$('#stops').show();
			}

      // customerNotificationDuration
      $('#trip-company_id').change(function(){
        $('#trip-notification_duration').val(customerNotificationDuration[this.value]);
      });
});

// validation for duplicate entry
$('#trip-destination').change(function(){
        var company = $('#trip-company_id').val();
        var vehicle_type = $('#trip-vehicle_type_id').val();
        var trip_date = $('#trip-trip_date').val();
        var origin = $('#trip-origin').val();
        var destination = $(this).val();
        $.post( '/trip/manage/checkduplicate',{company_id:company,vehicle_type_id:vehicle_type,trip_date:trip_date,origin:origin,destination:destination}).done(function( data ) {
                if(data==1){
                    var resp = confirm('Trip request already exist for entered data. Do you want to continue ?');
                    if(!resp){
                        location.reload();
                    }
                }
        });
        

       //  $('#trip-notification_duration').val(customerNotificationDuration[this.value]);
});


 $(document).on('beforeValidate', 'form', function(event, messages, deferreds) {
        $(this).find(':submit').attr('disabled', true);
        console.log('BEFORE VALIDATE TEST');
    }).on('afterValidate', 'form', function(event, messages, errorAttributes) {
        console.log('AFTER VALIDATE TEST');
        if (errorAttributes.length > 0) {
            $(this).find(':submit').attr('disabled', false);
        }
    });

$(document).on('click', '.remove-stops', function(){
    $(this).closest('.row').remove();
        distanceMAtrix();
});



    ", View::POS_END);
?>

<?php ob_start(); // output buffer the javascript to register later    ?>
<script>
    function populateCustPersons(person_id) {
        $('.preloader').show();
        var url = '<?= Url::to(['/customer/contact/list?id=']) ?>' + person_id;
        var $select = $('#trip-contact_person_id');
        $select.find('option').remove().end();
        $.ajax({
            url: url,
            success: function (data) {
                var select2Options = <?= Json::encode($select2Options) ?>;
                select2Options.data = data.data;
                $select.select2(select2Options);
                $select.val(data.selected).trigger('change');
                if (select2Options.data.length == 2) {
                    $("#trip-contact_person_id").val(select2Options.data[1].id).trigger('change.select2');
                }
                $('.preloader').hide();
            },
            error: function (data) {
                console.log(data);
                $('.preloader').hide();
            }
        });
    }
</script>
<?php
$this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean()),
    View::POS_END);
?>

<script>

    // save intermittent stops
    function saveIntermittentStop(){
        var stop = $('#intermittent-stop-input');
        var lat = $('#intermittent-lat');
        var lon = $('#intermittent-lon');
        var type = $('#stop_type');
        var newRow = '<div class="row p-l-30 p-r-20 p-t-10 stopRow" ><div class="col-md-6" >'+
                                        '<input type="text" class="form-control mb-2 stop" value="'+stop.val()+'" style="" readonly name="intermittentStop[stop][]"  >'+
                                        '<input type="hidden" class="lat" name="intermittentStop[lat][]" value="'+lat.val()+'"  />'+
                                        '<input type="hidden" class="lon" name="intermittentStop[lon][]" value="'+lon.val()+'"  />'+
                                    '</div><div class="col-md-4 p-t-5">'+
                                        '<label>' + type.val() + '</label><input type="hidden" name="intermittentStop[stop_type][]" value="' + type.val() + '" />'+
                                    '<a href="#"  class="m-l-20 active remove-stops"><i class="fa-fw mdi mdi-delete"></i></a>'+
                                    ''+
                                    '</div>';
                $('#intermittent-stop-list').append(newRow);
                stop.val("");
                lat.val("");
                lon.val("");
                distanceMAtrix();
    }

// https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=Museum%20of%20Contemporary%20Art%20Australia&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=AIzaSyD_sgQPLL2Epz7Eb8gcrvEwoKkBhGqnQG0
    try {
        function initAutocomplete() {
            // Create the search box and link it to the UI element.
            var loading_point = document.getElementById('trip-loading_point');
            var unloading_point = document.getElementById('trip-unloading_point');
            var intermittentStopInput = document.getElementById('intermittent-stop-input');

            // For loading point
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('trip-loading_point'), {types: ['geocode'],componentRestrictions:{'country': 'in'}});
            autocomplete.setFields(['geometry.location']);
            autocomplete.addListener('place_changed', function(){
                var place = autocomplete.getPlace().geometry.location;
                document.getElementById('trip-origin_lat').value = place.lat();
                document.getElementById('trip-origin_lon').value = place.lng();
                distanceMAtrix();
            });
            
            // For unloading_point
            autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('trip-unloading_point'), {types: ['geocode'],componentRestrictions:{'country': 'in'}});
            autocomplete2.setFields(['geometry.location']);
            autocomplete2.addListener('place_changed', function(){
                var place = autocomplete2.getPlace().geometry.location;
                document.getElementById('trip-destination_lat').value = place.lat();
                document.getElementById('trip-destination_lon').value = place.lng();
                distanceMAtrix()
            });

            // For intermittent point
            autocomplete3 = new google.maps.places.Autocomplete(document.getElementById('intermittent-stop-input'), {types: ['geocode'],componentRestrictions:{'country': 'in'}});
            autocomplete3.setFields(['geometry.location']);
            autocomplete3.addListener('place_changed', function(){
                var place = autocomplete3.getPlace().geometry.location;
                document.getElementById('intermittent-lat').value = place.lat();
                document.getElementById('intermittent-lon').value = place.lng();
                distanceMAtrix()
            });

            distanceMAtrix();
        }


        // Distance matrix
        function distanceMAtrix() {

            if (document.getElementById('trip-origin_lat').value == "" || document.getElementById('trip-destination_lat').value == "") {
                console.log("empty");
                return false;
            }

            var originLatLon = [{lat: parseFloat(document.getElementById('trip-origin_lat').value), lng: parseFloat(document.getElementById('trip-origin_lon').value)}];
            var destinationLatLon = new Array();
            // Intermittent stops listing
            var oi = 1;
            var di = 0;

            $('.stopRow').each(function(e,v){
                var latit = parseFloat($(v).find('.lat').val());
                var longit = parseFloat($(v).find('.lon').val());
                originLatLon[oi] = {lat:latit,lng:longit};
                destinationLatLon[di] = {lat:latit,lng:longit};
                oi++;
                di++;
            });

            destinationLatLon[di] = {lat: parseFloat(document.getElementById('trip-destination_lat').value), lng: parseFloat(document.getElementById('trip-destination_lon').value)};


            console.log(originLatLon);
            console.log(destinationLatLon);

            var service = new google.maps.DistanceMatrixService;
            service.getDistanceMatrix({
                origins: originLatLon,
                destinations: destinationLatLon,
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                console.log(response);
                if (status !== 'OK') {
                    alert('Error was: ' + status);
                } else {
                    var distanceArray = response.rows;
                    var arrayLen = distanceArray.length;
                    var totalKm = 0;
                    for(i=0;i<arrayLen;i++){
                        totalKm = totalKm + (distanceArray[i].elements[i].distance.value / 1000)
                    }
                    totalKm = Math.round(totalKm);
                    //var totalKm = parseInt(distanceArray[arrayLen - 1].distance.value / 1000);
                    $('#Totalkm').show().find('span').text(totalKm + ' Km');
                    $('#trip-approx_distance').val(totalKm);
                }
            });
        }

    } catch (e) {
    }



</script>

<script src="https://maps.googleapis.com/maps/api/js?fields=geometry&key=<?= Yii::$app->params['gmapKey']; ?>&libraries=places&callback=initAutocomplete"
async defer></script>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('@web/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerCss("
tbody {color: #000 !important; }
.table > tbody > tr > td {border-top: 0px; }
");
$customePrice = number_format((float)$model->selling_rate, 2, '.', '');
?>
<?= Alert::widget() ?>
<div class="trip-view">
<div class="row">
	<div class="col-md-12">
		
		<div class="white-box printableArea">
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-invoice">
							<tr><td style="text-align:center;padding:0px;"><h3><b>Tax Invoice</b></h3></td></tr>
							<tr><td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
									<td rowspan="3" style="border:1px solid #000;width:50%;padding:0px;">
										<table class="table table-responsive" style="">
											<tr><td style=""><img src="/images/invoice-logo.jpg" style="width:100px;"/></td>
											<td style="">
											<p><b>Middle Mile Pro LLP</b><br>
											 B-47, Pravasi Industrial Estate,<br>
											Goregaon East Mumbai-400063<br>
											<span style="font-size:12px;">Reg. 103,Rock Garden,Dahisar West Mumbai-400068</span><br>
											GSTIN/UIN: 27ABEFM5858P1ZF<br>
											Contact : 9892593578<br>
											E-Mail : asagore@middlemilepro.com<br>
											www.middlemilepro.com</p>
											</td></tr>
										</table>
									</td>
									<td style="border:1px solid #000;">
										Invoice No.<br><b><?= $model->number ?></b>
									</td>
									<td style="border:1px solid #000;">
										Dated<br><b><?= date('d-M-Y') ?></b>
									</td>
								</tr>
								<tr>
									<td style="border:1px solid #000;">Delivery Note<br><b></b></td>
									<td style="border:1px solid #000;">Mode/Terms of Payment<br><b></b></td>
								</tr>
								<tr>
									<td style="border:1px solid #000;">Supplier's Ref.<br><b></b></td>
									<td style="border:1px solid #000;">Other Reference(s)<br><b style="font-size:10px;"></b></td>
								</tr>
								<tr>
								<td rowspan="5" style="border:1px solid #000;padding:0px;vertical-align: top;">
								Buyer<br>
								<?= $model->companyName->name ?>
								<?php $address = explode(",",$model->companyName->address) ?>
								<?php foreach($address as $line){ ?>
								<?= $line ?>,<br>
								<?php } ?>
								<!--Future Chain Supply Solution Ltd. (BOM)<br>
								701-705,7TH FLOR,349 BUSINES POINT,<br>
								WESTEREN EXPRESS HIGHWAY,<br>
								ANDHERI( EAST)-MUMBAI-400069<br>
								State Name :Maharashtra, Code : 27<br>
									GSTIN/UIN:27AAACF9650N1Z4<br>
									PAN/IT No : <br>
									Place of Supply :Maharashtra--></td>
								<td style="border:1px solid #000;">Buyer's Order No.<br><b>&nbsp;</b></td>
								<td style="border:1px solid #000;">Dated<br><b>&nbsp;</b></td>
								</tr>
								<tr>
								<td style="border:1px solid #000;">Despatch Document No.<br><b>&nbsp;</b></td>
								<td style="border:1px solid #000;">Delivery Note Date<br><b>&nbsp;</b></td>
								</tr>
								<tr>
								<td style="border:1px solid #000;">Despatched through<br><b>BY ROAD</b></td>
								<td style="border:1px solid #000;">Destination<br><b><?= $model->originDetails->name ?> To <?= $model->destinationDetails->name ?></b></td>
								</tr>
								<tr>
								<td style="border:1px solid #000;">Bill of Lading/LR-RR No.<br><b style="font-size:9px;">VH/BWDH/17_18/03407 dt. 23-Sep-2017</b></td>
								<td style="border:1px solid #000;">Motor Vehicle No.<br><b><?= $model->vehicleDetails->registration_number ?></b></td>
								</tr>
								<tr><td colspan="2" style="border:1px solid #000;">Terms of Delivery<br><br><br><br><br><br></td></tr>
							</table>
							</td></tr>
							
							<tr><td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
								<td style="border:1px solid #000;">Sl No.</td>
								<td style="border:1px solid #000;">Description of Goods</td>
								<td style="border:1px solid #000;">HSN/SAC</td>
								<td style="border:1px solid #000;">GST Rate</td>
								<td style="border:1px solid #000;">Part No.</td>
								<td style="border:1px solid #000;">Quantity</td>
								<td style="border:1px solid #000;">Rate</td>
								<td style="border:1px solid #000;">per</td>
								<td style="border:1px solid #000;">Amount</td>
								</tr>
								
								<tr>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">1</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><b>Vehicle Hire Charges</b> Vehicle Type  <?= $model->vehicleType->name ?></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">9973</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">0 %</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><b>1 Nos</b></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><?= $customePrice ?></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">Nos</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"><b><?= $customePrice ?></b></td>
								</tr>
								
								<tr>
								<td style="border-left:1px solid #000;border-right:1px solid #000;">&nbsp;</td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								<td style="border-left:1px solid #000;border-right:1px solid #000;"></td>
								</tr>
								
								<tr>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;">Total</td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"><b>1 Nos</b></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"></td>
								<td style="border:1px solid #000;"><b>₹ <?= $customePrice ?></b></td>
								</tr>
							</table>
							</td></tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
							<tr><td style="border-right:none;border-top:1px solid #000;border-left:1px solid #000;">Amount Chargeable (in words)</td><td style="border-top:1px solid #000;border-right:1px solid #000;text-align:right;">E. & O.E</td></tr>
							<tr><td colspan="2" style="border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000;"><b><?= ucwords(Yii::$app->commonFunction->getIndianCurrency($customePrice))." Only"; ?></b></td></tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
								<td style="width:80%;text-align:center;border:1px solid #000;">HSN/SAC</td>
								<td style="border:1px solid #000;">Taxable Value</td>
								</tr>
								
								<tr>
								<td style="border:1px solid #000;">9973</td>
								<td style="border:1px solid #000;"><?= $customePrice ?></td>
								</tr>
								
								<tr>
								<td style="border:1px solid #000;text-align:right;"><b>Total</b></td>
								<td style="border:1px solid #000;"><b><?= $customePrice ?></b></td>
								</tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;border-left:1px solid #000;border-right:1px solid #000;">
							<tr><td style="" style="">Tax Amount (in words)  : NIL<br><br>&nbsp;</td></tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;border:1px solid #000;">
								<tr><td style="width:50%;padding:0px;border-left:1px solid #000;">
								<table class="table table-responsive">
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>Remarks:</td></tr>
									<tr><td>23-Sep  VH/BWDH/17_18/03407</td></tr>
								</table>
								</td>
								<td style="width:50%;padding:0px;border-right:1px solid #000;">
								<table class="table table-responsive">
									<tr><td colspan="2">Company's Bank Details</td></tr>
									<tr><td>Bank Name</td><td>: <b style="font-size:12px;">Kotak Mahindra Bank 0812171004</b></td></tr>
									<tr><td>A/c No.</td><td>: <b style="font-size:12px;">0812171004</b></td></tr>
									<tr><td>Branch & IFS Code</td><td>: <b style="font-size:12px;">Santacruz & KKBK0000652</b></td></tr>
								</table>
								</td></tr>
							</table>
							</td>
							</tr>
							
							<tr>
							<td style="padding:0px;">
							<table class="table table-responsive" style="margin:0px;">
								<tr>
								<td style="width:50%;border-left:1px solid #000;border-bottom:1px solid #000;"><table class="table table-responsive"><tr><td>Company's PAN</td><td>:<b> ABEFM5858P</b></td></tr>
									<tr><td colspan="2">Declaration</td></tr>
									<tr><td colspan="2">We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</td>
									</tr></table>
								</td>
								<td style="width:50%;border:1px solid #000;padding:0px;"><table class="table table-responsive"><tr><td>for Middle Mile Pro LLP</td></tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr><td>Authorised Signatory</td>
									</tr></table>
								</td>
								</tr>
							</table>
							</td>
							</tr>
							
							<tr><td style="text-align:center;">SUBJECT TO MUMBAI JURISDICTION</td></tr>
							<tr><td style="text-align:center;">This is a Computer Generated Invoice</td></tr>

							

							</tbody>
						</table> <!-- TABLE COLSE -->
					</div>


				</div>
			</div>			
						<div class="row">
				<div class="col-md-12">
					<div class="clearfix"></div>
					<hr>
					<div class="text-right">
					<a class="popup-with-form btn btn-success" href="#Trip">Send Invoice</a>
					<?php
					echo Html::a('Download', ['/trip/manage/viewtaxinvoice?id='.md5($model->id)], [
						'class'=>'btn btn-danger', 
						'target'=>'_blank', 
						'data-toggle'=>'tooltip', 
						'title'=>'Will open the generated PDF file in a new window'
					]);
					?>
						

					</div>
				</div>			
			</div>
		
			
		</div>		
		
		
	</div>
</div>




		<!-- form itself -->
		<?php $form = ActiveForm::begin([
			'id' => $sendInvoiceModel->formName(),			
			'action' =>['/trip/manage/sendtaxinvoice'],
			'enableAjaxValidation' => true,
			'validateOnBlur'=>false,
			'validateOnChange'=>false,
			'options'=>['onsubmit'=>'return false;','class' => 'mfp-hide white-popup-block',]
		  ]); ?>
		  <h1>Email Addresses</h1>
		  <fieldset style="border:0;">
		  <p>Please use comma (,) to separate multiple email id.</p>
		  <input type="hidden" name="tripid" value="<?= md5($model->id); ?>" />
				<div class="container-fluid clearfix margin-top-40">
					<div class="row text-center">
					<div class="col-md-8 text-left" style="float:none; margin:0 auto;">
						<?= $form->field($sendInvoiceModel, 'recipients')->textArea(['row'=>4])->label('Email Address'); ?>
					</div>
					</div>
					<div class="text-center" id="ack-msg"></div>
					<div class="text-center">
						<?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-lg margin-top-20','id'=>'send-button']) ?>
					</div>
				</div>
			</fieldset>
		<?php ActiveForm::end(); ?>






</div>

<?php
$this->registerJs("

	$('#Trip').on('ajaxBeforeSend', function (event, jqXHR, settings) {
		$('#send-button').attr('disabled',true);
	}).on('ajaxComplete', function (event, jqXHR, textStatus) {
		if(typeof jqXHR.responseJSON != 'object' && jqXHR.responseJSON=='1'){
			$('#ack-msg').html('Tax Invoice Sent to Recipients');
			location.reload();
		}else{
			$('#ack-msg').html('Error!!! Please try again.');
		}
		$('#send-button').attr('disabled',null);
	})
	
",View::POS_END);
?>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('@web/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',['depends'=>[\yii\web\JqueryAsset::className()]]);
?>
<?= Alert::widget() ?>
<div class="trip-view">
<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">
			<h3><b>INVOICE</b><img src="/images/invoice-logo.jpg" /> <span class="pull-right">#<?= $model->number ?></span></h3>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="pull-left">
						<address>
							<h3> &nbsp;<b class="text-danger">MIDDLEMILE PRO</b></h3>
							<p class="text-muted m-l-5">E 104, Dharti-2,
								<br> Nr' Viswakarma Temple,
								<br> Talaja Road,
								<br> Bhavnagar - 364002</p>
						</address>
					</div>
					<div class="pull-right text-right">
						<address>
							<h3>To,</h3>
							<h4 class="font-bold"><?= $model->companyName->name ?>,</h4>
							<p class="text-muted m-l-5">
							<?= ($model->contactPersonName->name) ?> / <?= $model->contactPersonName->designation ?>,
							<br><?= $model->contactPersonName->mobile ?> / <?= $model->contactPersonName->email ?>
							</p>
							<?php $address = explode(",",$model->companyName->address) ?>
							<p class="text-muted m-l-30">
							<?php foreach($address as $line){ ?>
							<?= $line ?>,<br>
							<?php } ?>
							</p>
							<p class="m-t-30"><b>Invoice Date :</b> <i class="fa fa-calendar"></i> <?= date('d-M-Y') ?></p>
						</address>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
				<h3><b>Trip Details</b> <span class="pull-right"></span></h3><hr>
				</div>
				<div class="col-md-6">
					<p><b>Date :</b> <?= date('d-m-Y',strtotime($model->trip_date)); ?></p>
				</div>

				<div class="col-md-6">
					<p><b>Vehicle Type :</b> <?= $model->vehicleType->name ?></p>
				</div>

				<div class="col-md-6">
					<p><b>Loading Location :</b> <?= $model->origin ?></p>
					<p class="text-muted m-l-5">Handling Charges : <?= $model->origin_handling_charges ?></p>
				</div>

				<div class="col-md-6">
					<p><b>Unloading Location :</b> <?= $model->destination ?></p>
					<p class="text-muted m-l-5">Handling Charges : <?= $model->destination_handling_charges ?></p>
				</div>

				<div class="col-md-6">
					<p><b>Service Type :</b> <?= Yii::$app->params['serviceType'][$model->service_type] ?></p>
				</div>

				<div class="col-md-6">
					<p><b>Rate :</b> <?= $model->selling_rate ?>/-</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<p><b>Trip Type :</b> <?= Yii::$app->params['tripType'][$model->trip_type] ?></p>
				</div>
			</div>
			<div class="row">
				<?php if($model->trip_type == 2){ ?>
				<div class="col-md-6">
					<!--p><b>Stops :</b> </p-->
					<div class="table-responsive">
						<table class="table color-table muted-table">
							<thead>
								<tr>
									<th>Stop Name</th>
									<th>Handling Charges</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach($stopsModel as $stop){ ?>
								<tr>
									<td><?= $stop['name'] ?></td>
									<td><?= $stop['handling_charges'] ?></td>
								</tr>
							<?php }
							?>
							</tbody>
						</table>
					</div>
				</div>
				<?php } ?>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h3><b>Vendor Details</b> <span class="pull-right"></span></h3><hr>
				</div>
				<div class="col-md-6">
					<p><b>Name :</b> <?= $model->vendorDetails->name ?></p>
				</div>
				<div class="col-md-6">
					<p><b>Mobile :</b> <?= $model->vendorDetails->mobile ?></p>
				</div>
				<div class="col-md-6">
					<p><b>Email :</b> <?= $model->vendorDetails->email ?></p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h3><b>Vehicle Details</b> <span class="pull-right"></span></h3><hr>
				</div>
				<div class="col-md-6">
					<p><b>Registration Number :</b> <?= $model->vehicleDetails->registration_number ?></p>
					<p><b>Chassi Number :</b> <?= $model->vehicleDetails->chassi_number ?></p>
					<p><b>Body Number :</b> <?= $model->vehicleDetails->body_number ?></p>
				</div>
				<div class="col-md-6">
					<p><b>Vehicle Type :</b> <?= $model->vehicleType->name ?></p>
				</div>
				<div class="col-md-6">
					<p><b>Driver :</b></p>
					<?php foreach($driverList as $driver){ ?>
					<p class="text-muted m-l-15"><?= $driver['name'] ?> / <?= $driver['mobile'] ?></p>
					<?php } ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="clearfix"></div>
					<hr>
					<div class="text-right">
					<a class="popup-with-form btn btn-success" href="#sendinvoice-form">Send Invoice</a>
					<?php
					echo Html::a('Download', ['/trip/manage/report?id='.md5($model->id)], [
						'class'=>'btn btn-danger',
						'target'=>'_blank',
						'data-toggle'=>'tooltip',
						'title'=>'Will open the generated PDF file in a new window'
					]);
					?>


					</div>
				</div>
			</div>

		</div>




		<div class="white-box printableArea">
					<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-invoice">
							<tbody>
							<tr style="border-bottom:1px solid #ccc;">
								<td style="width:50%;border-top:none;"><img src="/images/invoice-logo.jpg" /></td>
								<td style="text-align:right;border-top:none;padding-top:30px;">
									<div class="pull-right text-right">
										<address>
											<p><b>Middle Mile Pro LLP</b></p>
											<p>B -47, Pravasi Industrial Estate,</p>
											<p>Vishveshwar Nagar Rd, Goregaon East,</p>
											<p>Mumbai, Maharashtra -400 063.</p>
										</address>
									</div>
								</td>
							</tr>

							<tr>
								<td></td>
								<td></td>
							</tr>

							<tr>
								<td colspan="2" style="background-color:#ccc;height:12px;padding:5px;"><b>Loading Advance Slip / Memo / Broker Slip</b></td>
							</tr>

							<tr style="border-bottom:1px solid #ccc;">
								<td style="border:none;padding:0px;"><div class="pull-left">
										<b><?= $model->number ?></b>
									</div>
								</td>
								<td style="text-align:right;border: none;padding:0px;">
									<div class="pull-right text-right">
										<p class="m-t-30">Date : <?= date('d-M-Y') ?></p>
									</div>
								</td>
							</tr>


							<tr>
								<td style="border:none;padding:15px 0px;">
								<table>
								<tr><td style="padding:0px;border:none;">Intended For</td><td style="padding:0px;border:none;">:</td><td style="padding:0px;border:none;"><?= ($model->contactPersonName->name) ?></td></tr>
								<tr><td style="border:none;padding:0px;">Vendor Code</td><td style="border:none;padding:0px;">:</td><td style="border:none;padding:0px;"><?= $model->vendorDetails->code ?></td></tr>
								<tr><td style="border:none;padding:0px;">Company Name</td><td style="border:none;padding:0px;">:</td><td style="border:none;padding:0px;"><?= $model->companyName->name ?></td></tr>
								</table>
								</td>
								<td style="border:none;"></td>
							</tr>

							<tr>
								<td colspan="2" style="border:none;padding:0px;">
								<table class="table table-responsive table-striped" style="border:1px solid #ccc;">
								<tr style=""><td style="padding:2px;background-color: #eee;">Placement Date and Time</td><td style="padding:2px;background-color: #eee;"><?= date('d-m-Y',strtotime($model->trip_date)); ?></td></tr>
								<tr><td style="padding:2px;">Loading Location</td><td style="padding:2px;"><?= $model->origin ?></td></tr>
								<tr><td style="padding:2px;background-color: #eee;">Unloading Location</td><td style="padding:2px;background-color: #eee;"><?= $model->destination ?></td></tr>
								<tr><td style="padding:2px;">Truck No</td><td style="padding:2px;"><?= $model->vehicleDetails->registration_number ?></td></tr>
								<tr><td style="padding:2px;background-color: #eee;">Truck Type</td><td style="padding:2px;background-color: #eee;"><?= $model->vehicleType->name ?></td></tr>
								<tr><td style="padding:2px;">Other Details</td><td style="padding:2px;"></td></tr>
								</table>
								</td>
							</tr>

							<tr>
								<td style="border:none;padding:0px;">
								<table>
								<tr><td style="border:none;padding:0px;"><b>Payment Mode </b></td><td style="border:none;padding:0px;">: Advance </td></tr>
								<tr><td style="border:none;padding:0px;"><b>Total Rate</b></td><td style="border:none;padding:0px;">: Rs.25000 </td></tr>
								<tr><td style="border:none;padding:0px;"><b>Advance</b></td><td style="border:none;padding:0px;">: Rs.20000 </td></tr>
								<tr><td style="border:none;padding:0px;"><b>Balance</b></td><td style="border:none;padding:0px;">: Rs.5000</td></tr>
								</table>
								</td>
								<td style="border:none;"></td>
							</tr>
							<?php /*
							<tr>
								<td colspan="2" style="border:none;">**No TDS Deduction - TDS Declaration will be provided.</td>
							</tr>
							*/ ?>
							<tr>
								<td colspan="2" style="border:none;padding:10px 0px;">
									<table>
									<tr>
										<td style="border:none;padding:0px;"><img src="/images/pancard.jpg" style="width:250px;" /></td>
										<td style="border:none;padding:0px;">

<table>
	<tr><td style="border:none;padding:3px 0px;"><b>Pan No. of MiddleMile Pro LLP : </b>ABEFM5858P</td></tr>
	<tr><td style="border:none;padding:3px 0px;"><b>Acc Name :</b>MIDDLEMILE PRO LLP</td></tr>
	<tr><td style="border:none;padding:3px 0px;"><b>Acc No. :</b>0812171004</td></tr>
	<tr><td style="border:none;padding:3px 0px;"><b>IFSC Code :</b>KKBK0000652</td></tr>
	<tr><td style="border:none;padding:3px 0px;"><b>Bank Name :</b>Kotak Mahindra bank Ltd.</td></tr>
</table>


										</td>
									</tr>
									</table>
								</td>
							</tr>


							<tr>
								<td colspan="2" style="border:none;padding:0px;">
								<h3>Terms & Conditions </h3>
								<ul>
								<li>This is an electronically generated copy and does not require any signature.</li>
								<li>In case of corrections, kindly reply within 5 days of receipt. In the absence of any such intimation, it will be deemed correct. </li>
								<li>GST is not the responsibility of Middle Mile Pro LLP, it is the responsibility of the consignee/ consignor.</li>
								<li>Freight is without GST charges. </li>
								<li>Loading/Unloading with complete paperwork should happen in a timely manner and vehicle should be released within 24 hours of vehicle arrival. Detention charge of Rs 2500/day will be levied for any delays in releasing the vehicle beyond 24 hours of arrival.</li>
								</ul>
								</td>
							</tr>







							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>




	</div>
</div>




		<!-- form itself -->
		<form id="sendinvoice-form" class="mfp-hide white-popup-block" action="/trip/manage/sendinvoice" onsubmit="return false">
			<h1>Email Addresses</h1>
			<fieldset style="border:0;">
				<p>Please use comma (,) to separate multiple email id.</p>
				<div class="form-group">
					<label class="control-label" for="email">Email Address</label>
					<br>
					<textarea class="form-control" id="email"></textarea>
				</div>
				<div class="form-group">
					<input type="submit" value="Send" class="btn btn-success" id="sendinvoice" />
				</div>
			</fieldset>
		</form>

		<?php $form = ActiveForm::begin([
			'id' => $sendInvoiceModel->formName(),
			'class' => 'mfp-hide white-popup-block',
			'action' =>['/trip/manage/sendinvoice'],
			'enableAjaxValidation' => true,
			'options'=>['onsubmit'=>'return false;']
		  ]); ?>
		  <input type="hidden" name="tripid" value="<?= md5($model->id); ?>" />
				<div class="container-fluid clearfix margin-top-40">
					<div class="row text-center">
					<div class="col-md-8 text-left" style="float:none; margin:0 auto;">
						<?= $form->field($sendInvoiceModel, 'recipients')->textArea(['row'=>4]); ?>
					</div>
					</div>
					<div class="text-center" id="reset-password-msg"></div>
					<div class="text-center">
						<?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-lg margin-top-20','id'=>'send-button']) ?>
					</div>
				</div>
		<?php ActiveForm::end(); ?>






</div>

<?php
$this->registerJs("
	$('#print').click(function() {
		var mode = 'iframe'; //popup
		var close = mode == 'popup';
		var options = {
			mode: mode,
			popClose: close
		};
		$('div.printableArea').printArea(options);
	});


	var emailReg = new RegExp(/^([A-Z0-9.%+-]+@@[A-Z0-9.-]+.[A-Z]{2,6})*([,;][\s]*([A-Z0-9.%+-]+@@[A-Z0-9.-]+.[A-Z]{2,6}))*$/i);
      var emailText = $('#email').val();
      if (!emailReg.test(emailText)) {
          alert('Wrong Email Address\Addresses format! Please reEnter correct format');
            return false;
        }

	$('#Trip').on('ajaxBeforeSend', function (event, jqXHR, settings) {
		$('#send-button').attr('disabled',true);
	}).on('ajaxComplete', function (event, jqXHR, textStatus) {
		if(typeof jqXHR.responseJSON != 'object' && jqXHR.responseJSON=='1'){
			location.reload();
		}
		$('#send-button').attr('disabled',null);
	})

",View::POS_END);
?>

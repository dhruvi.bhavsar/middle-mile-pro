<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

$userList = \common\models\User::find()->select(['id','username'])->asArray()->all();
$userList = ArrayHelper::map($userList, 'id', 'username');
/* @var $this yii\web\View */
/* @var $model frontend\modules\timesheet\models\ResumePlacements */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
  var userList = <?= json_encode($userList,true); ?>;
  var eventStatus = <?= json_encode(\Yii::$app->params['event_status'],true); ?>;
</script>
<div class="modal fade" id="modaladdComments" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content"  style="height: 90vh;">
        <div class="modal-header"><h4 class="modal-title">Add Tracking</h4></div>
        <div class="modal-body" style="height:calc(90vh - 270px);overflow: auto;"><div class="addCommentsListing"><!-- List of all Add Remark via js --></div></div>
        <div class="modal-footer">
          <input type="hidden" class="tripId"/>
          <div class="row">
            <div class="col-md-4">
              <select id="tripEvent" class="form-control" >
                <option value="">Select Event Status</option>
                <?php foreach (\Yii::$app->params['event_status'] as $key => $text) { echo '<option value="'.$key.'">'.$text.'</option>'; } ?>
              </select>
              <div class="text-danger help-block pull-left" style="margin-top:0px !important"></div>
            </div>
            <div class="col-md-4">
              <input type="text" id="tripLocation" placeholder="Location" required class="form-control" aria-invalid="false">
            </div>
            <div class="col-md-4">
              <input type="text" id="tripDateTime" placeholder="Date & Time" required class="form-control datetimepicker" aria-invalid="false">
            </div>
            <div class="clearfix"> <br><br><br> </div>
            <div class="col-md-12">
              <div class="">
                    <textarea class="form-control addRemarksBox" rows="2" style="margin-top:0px !important;margin-left:0px !important; margin-right:0px !important;" placeholder="Please Enter Remark"></textarea>
              </div>
            </div>
          </div>
          <div class="clearfix"> <br> </div>
          <div class="row">
            <div class="text-danger help-block pull-left addCommentModelErrorMessage" style="margin-top:0px !important"></div>
            <div class="col-md-6 text-left">
                <div class="jsSectionPod" style="display:none">
                    <label for="control-label">Upload POD</label>
                    <input type="file" name="tripPodUpload" id="tripPodUpload" onchange="validate_fileupload(this.value);" >
                    <div class="help-block text-danger jsTripPodUploadError"></div>
                    <script type="text/javascript">
                    function validate_fileupload(fileName)
                    {
                      var allowed_extensions = ["png","jpg","gif","docx","doc","pdf"];
                      var file_extension = fileName.split('.').pop().toLowerCase();
                      for(var i = 0; i <= allowed_extensions.length; i++)
                      {
                        if(allowed_extensions[i]==file_extension)
                        {
                          $(".jsTripPodUploadError").text("");
                          return true;
                        }
                      }
                      $(".jsTripPodUploadError").text("Only PNG/JPG/GIF/DOC/PDF allowed.");
                      return false;
                    }
                    </script>
                </div>
            </div>
            <div class="col-md-6 pull-right">
              <a class="btn btn-info m-r-10 pull-right jsaddTripRemarksBtn" >Save</a>
              <a class="btn btn-inverse m-r-10 pull-right jsaddCommentsCancelBtn" data-dismiss="modal">Cancel</a>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<p class="jsReader" hidden></p>
<?php $this->registerJs("
  $(document).ready(function(){
    $('#tripEvent').change(function(){
      //console.log(this.value);
      if (this.value == 8) {
        $('.jsSectionPod').show();
      } else {
        $('.jsSectionPod').hide();
      }
    });
    function getBase64(file) {
       var reader = new FileReader();
       reader.readAsDataURL(file);
       reader.onload = function () {
         $('.jsReader').text(reader.result);
       };
       reader.onerror = function (error) {
         console.log('Error: ', error);
       };

       return $('.jsReader').text();
    }
    processAddComments();
    $('.jsaddTripRemarksBtn').click(function(){
      var saveBtn = $(this);
      if (typeof saveBtn.attr('disabled') !== typeof undefined && saveBtn.attr('disabled') !== false) {
        return false;
      }
      saveBtn.attr('disabled','disabled');
      // getting data from model
      // getting data from model
      var remark = $('.addRemarksBox').val();
      var location = $('#tripLocation').val();
      var datetime = $('#tripDateTime').val();
      var trip_event = $('#tripEvent').val();
      var tripPodUpload = document.querySelector('#tripPodUpload[type=file]').files[0];
      if (tripPodUpload !== undefined) {
        var file_base64 = getBase64(tripPodUpload);
        file_base64 = $('.jsReader').text();
        // console.log(file_base64);
        var file_name = tripPodUpload.name;
      } else {
        file_base64 = '0';
        file_name = '0';
      }

      // $('#tripPodUpload').val();
      if (trip_event == '') {
        $('#tripEvent').next('.help-block').text('Please select a event');
        saveBtn.removeAttr('disabled');
        return false;
      } else {
        $('#tripEvent').next('.help-block').text('');
      }
      if (remark == '') {
        $('.addCommentModelErrorMessage').text('Please enter remark');
        saveBtn.removeAttr('disabled');
        return false;
      } else {
        $('.addCommentModelErrorMessage').text('');
      }
      var id = $('.tripId').val();
      // sending data to save
      setTimeout(function() {
        $.post('/trip/manage/add-remarks',{id:id,remark:remark,location:location,datetime:datetime,trip_event:trip_event,file_base64:$('.jsReader').text(),file_name:file_name},function(result) {
            var user ='';
            try {user = ' By :'+user+userList[".\Yii::$app->user->identity->id."];} catch (err) {user ='';}
            // eventStatus '+user+'
            var list_content = '<div class=\"row  m-t-10\"><div class=\"col-sm-12\">'+
                                '<p class=\"m-b-0\">'+remark+'</p><small>Event :'+eventStatus[trip_event]+' | Location :'+location+' | Date & Time :'+datetime+'</small></div></div><hr>';
            // Appending new comment in model and listing page of emp muster
            $('.addCommentsListing').prepend(list_content);
            // Reloading pjax container except emp muster. This will throw error on emp muster
            try { $.pjax.reload({container: '#trip-index', timeout: 1200}); } catch (err) {}
            // Showing success toast to user
            console.log(result.remark);
            if (result.remark == remark) {
              alertSnackbar('REMARK ADDED.','success');
              $('.addRemarksBox,#tripLocation,#tripLocation,#tripDateTime,#tripEvent').val('');
              $('#modaladdComments').modal('hide');
            }
            saveBtn.removeAttr('disabled');
            return false;
        });
      }, 300);
    });
  });
  $(document).on('ready pjax:end',function () {
    processAddComments();
  });
  function processAddComments()
  {
    // On click of addComments. To show Comments data
    $('.addComments').click(function(){
      var elem = $(this);
      var addCommentsListing = $('.addCommentsListing');
      // Clear old data
      addCommentsListing.html('');
      $('.tripId').val(elem.data('id'));
      var json_data = elem.find('.data-remarks').text();

      if (json_data != '') {
        // Get and display Comments data
        var agent_comments = JSON.parse(json_data);
        console.log(agent_comments);
        agent_comments.reverse();
        $(agent_comments).each(function(i,obj){
          var user = '';
          var dateAndTime = getDateTimePhpStyle(obj.on,1);
          try {
            if (obj.user == 0) {
              user ='';
            } else {
              user = ' By :'+user+userList[obj.user];
            }
          } catch (err) {user ='';}
          // var list_content = '<div class=\"row  m-t-10\"><div class=\"col-sm-12\">'+
          //                     '<p class=\"m-b-0\">'+obj.remark+'</p><small>'+user+'</small></div></div><hr>';
          // eventStatus
          var list_content = '<div class=\"row  m-t-10\"><div class=\"col-sm-12\">'+
                              '<p class=\"m-b-0\">'+obj.remark+'</p><small class=\"pull-right\">'+getDateTimePhpStyle(obj.on,1)+'</small><small>Event :'+eventStatus[obj.trip_event]+' | Location :'+obj.location+' | Date & Time :'+obj.datetime+' <br>'+user+'</small></div></div><hr>';
          addCommentsListing.append(list_content);
        });
      } else {
          addCommentsListing.html('<div class=\"row  m-t-10\"><div class=\"col-sm-12\"><p class=\" jsRemoveCommentsNoRecord text-center \">No Records</p></div></div>');
      }
      // Opening model
      $('#modaladdComments').modal();
    });
  }
"); ?>

<?php $this->registerJs("
// To send sms to vendor
$('.js-sms-origin-destination').click(function(){
	console.log('working');
  var mob = $(this).data('mob');
  var msg = $(this).data('msg');
  var confirmation = confirm('Do you want to send this sms? '+ msg);
  if (confirmation == true) {
    $.post('/trip/manage/sendquotationsms', {mobile:mob, message:msg},function(data) {
      //Set message
      $('body').append('<div  class=\"alert-success myadmin-alert alert myadmin-alert-top-right block fade in\" style=\"display:block\"><button type=\"button\" class=\"m-l-10 close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button><h4>Message Sent Successfully.</h4></div>');
      // Auto close alert
      window.setTimeout(function() {
        $('.myadmin-alert').fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
        });
      }, 4000);
    });
  }
});
"); ?>
<script>
	 function initAutocomplete() {
			// Create the search box and link it to the UI element.
			var trip_location = document.getElementById('tripLocation');
			var searchBox = new google.maps.places.Autocomplete(
                                                                                               document.getElementById('tripLocation'),
                                                                                                {types: ['geocode'],componentRestrictions:{'country': 'in'}});
                                                                         searchBox.setFields(['address_component']);
                                                                  
	 }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['gmapKey']; ?>&libraries=places&callback=initAutocomplete"
async defer></script>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
//yii\web\View::registerMetaTag(['http-equiv'=>'X-FRAME-OPTIONS','content'=>'sameorigin']);
$this->title = 'Invoice-'.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('@web/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerCss("
tbody {color: #000 !important; }
");
?>
<?= Alert::widget() ?>
<div class="trip-view">
<div class="row">
	<div class="col-md-12">

		<div class="white-box printableArea">
			<div class="row">
				<div class="col-sm-12">

					<embed
						style="height: 600px;width: 100%;display: block;"
						allowfullscreen
						src="/invoice/<?= $model->name; ?>"
						type="application/pdf">


				</div>
			</div>
						<div class="row">
				<div class="col-md-12">
					<div class="clearfix"></div>
					<hr>
					<div class="text-right">
					<a class="popup-with-form btn btn-success" href="#Trip">Send Invoice</a>
					<?php
						echo Html::a('Download', ['/invoice/'.$model->name], [
							'class'=>'btn btn-danger',
							'target'=>'_blank',
							'data-toggle'=>'tooltip',
							'title'=>'Will open the generated PDF file in a new window'
						]);
					?>


					</div>
				</div>
			</div>


		</div>


	</div>
</div>








</div>

<?php
$this->registerJs("
	$('#print').click(function() {
		var mode = 'iframe'; //popup
		var close = mode == 'popup';
		var options = {
			mode: mode,
			popClose: close
		};
		$('div.printableArea').printArea(options);
	});



	$('#Trip').on('ajaxBeforeSend', function (event, jqXHR, settings) {
		$('#send-button').attr('disabled',true);
	}).on('ajaxComplete', function (event, jqXHR, textStatus) {
		if(typeof jqXHR.responseJSON != 'object' && jqXHR.responseJSON=='1'){
			$('#ack-msg').html('Order Slip Sent to Recipients');
			location.reload();
		}else{
			$('#ack-msg').html('Error!!! Please try again.');
		}
		$('#send-button').attr('disabled',null);
	})

",View::POS_END);
?>

<!-- form itself -->
<?php $form = ActiveForm::begin([
	'id' => $sendInvoiceModel->formName(),
	'action' =>['/trip/manage/viewinvoice','billing'=>$model->id],
	'enableAjaxValidation' => true,
	'validateOnBlur'=>false,
	'validateOnChange'=>false,
	'options'=>['onsubmit'=>'return false;','class' => 'mfp-hide white-popup-block',]
	]); ?>
	<h1>Email Addresses</h1>
	<fieldset style="border:0;">
	<p>Please use comma (,) to separate multiple email id.</p>
	<input type="hidden" name="tripid" value="<?= $model->id ?>" />
		<div class="container-fluid clearfix margin-top-40">
			<div class="row text-center">
			<div class="col-md-8 text-left" style="float:none; margin:0 auto;">
				<?= $form->field($sendInvoiceModel, 'recipients')->textArea(['row'=>4])->label('Email Address'); ?>
			</div>
			</div>
			<div class="text-center" id="ack-msg"></div>
			<div class="text-center">
				<?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-lg margin-top-20','id'=>'send-button']) ?>
			</div>
		</div>
	</fieldset>
<?php ActiveForm::end(); ?>

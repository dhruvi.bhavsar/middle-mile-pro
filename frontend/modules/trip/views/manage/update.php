<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = 'Update Trip';
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trip-update">

    <?= $this->render('_form', [
        'model' => $model,
        'stopsModel' => $stopsModel,
    ]) ?>

</div>

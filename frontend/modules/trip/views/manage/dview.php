<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->number;
//$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$colseOrders ='';
?>
<div class="trip-view">
	<div class="row">
	  <div class="col-md-6 col-sm-12">
	    <section class="white-box">
	      <div class="user-btm-box p-t-0">
					<!-- <div class="row  m-t-10"><div class="col-sm-12"><strong>TRIP DETAILS</strong><br/><br/></div></div> -->
	        <div class="row  m-t-10">
                                    <div class="col-xs-6 b-r"><strong>Date</strong><p><?= date('d-m-Y',strtotime($model->trip_date)); ?></p></div>
                                    <div class="col-xs-6"><strong>Order Id</strong><p><?= $model->number ?></p></div>
	        </div>
	        <hr>
<!--                              <div class="row  m-t-10">
						<div class="col-sm-6"><strong>Customer</strong><p><?= $model->companyName->name ?></p></div>
	          <div class="col-sm-6 b-l"><strong>Contact Person</strong>
							<p class="m-0"><?= (!empty($model->contactPersonName->name)) ? ($model->contactPersonName->name) : ''; ?></p>
							<p class="font-12 m-0"><?= (!empty($model->contactPersonName->designation)) ? $model->contactPersonName->designation : ''; ?></p>
							<p class="font-12 m-0">
								<?= (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile : ''; ?>
								<?= (!empty($model->contactPersonName->email)) ? ' / '.$model->contactPersonName->email : ''; ?>
							</p>
						</div>
	        </div>
	        <hr>-->
					<div class="row  m-t-10">
	          <div class="col-xs-6 b-r"><strong>Origin</strong><p><?= ($model->originDetails->name) ?></p></div>
	          <div class="col-xs-6"><strong>Destination</strong><p><?= ($model->destinationDetails->name) ?></p></div>
	        </div>
	        <hr>
					<div class="row  m-t-10">
	          <div class="col-xs-6 b-r"><strong>Loading Point</strong><p><?= (!empty($model->loading_point))?$model->loading_point:'-'; ?></p></div>
	          <div class="col-xs-6"><strong>Unloading Point</strong><p><?= (!empty($model->unloading_point))?$model->unloading_point:'-'; ?></p></div>
	        </div>
                <!--
	        <hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Service Type</strong><p><?= Yii::$app->params['serviceType'][$model->service_type] ?></p></div>
						<div class="col-sm-6"><strong>Trip Type</strong><p><?= Yii::$app->params['tripType'][$model->trip_type] ?></p></div>
					</div>
					<hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Targeted  Freight</strong><p><?= (isset($model->targated_rate))?$model->targated_rate:' -'; ?></p></div>
						<div class="col-sm-6"><strong>Targeted  Buying</strong><p><?= isset($model->targated_buying)?$model->targated_buying:' -'; ?></p></div>
					</div>
					<hr>-->
<!--					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Buying Rate</strong><p>
							<?= (isset($model->buying_rate))?$model->buying_rate:' -'; ?>
							<?php if($model->status < Trip::STATUS_ORDER_CONFIRMED && $model->status >= Trip::STATUS_VENDOR_RE_ASSIGN){ ?>
							&nbsp;&nbsp;<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>',['/trip/manage/assignvendor','id'=>$model->id]); ?>
							<?php }	?>
						</p></div>
						<div class="col-sm-6"><strong>Selling Rate</strong><p>
							<?= (isset($model->selling_rate))?$model->selling_rate:' -'; ?>
							<?php if($model->status === Trip::STATUS_VENDOR_CONFIRM_PENDING){ ?>
							&nbsp;&nbsp;<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>',['/trip/manage/customerconfirm','id'=>$model->id]); ?>
							<?php } ?>
						</p></div>
					</div>
					<hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Origin Handling Charges</strong><p><?= (!empty($model->origin_handling_charges))?$model->origin_handling_charges:'-' ?></p></div>
						<div class="col-sm-6"><strong>Destination Handling Charges</strong><p><?= (!empty($model->destination_handling_charges))?$model->destination_handling_charges:'-';  ?></p></div>
					</div>
					<hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Notification Duration</strong><p><?= (!empty($model->notification_duration))?'Every '.$model->notification_duration .' hours':'-' ?></p></div>
	          <div class="col-sm-6"><strong>Vehicle Type</strong><p><?= $model->vehicleType->name ?></p></div>
					</div>

					<?php
                                                                                                                            if($model->status == Trip::STATUS_DELETED){ ?>
						<hr>
						<div class="row  m-t-10"><div class="col-sm-12"><strong>Cancellation Reason</strong><p><?= $model->cancellation ?></p></div></div>
					<?php } ?>
					<?php
						$colseOrder = '';
						if(Yii::$app->commonFunction->accessToElement('deleteTrip')){
							$colseOrders = Html::a('Drop order', ['closeorder','id'=>$model->id],['class' => 'btn btn-warning btn-block waves-effect waves-light']);
						}
					 ?>
					<?php // if($model->status < Trip::STATUS_ORDER_CONFIRMED){ ?>
						<hr>
						<div class="row text-center m-t-10">
							<div class="col-xs-6 b-r">
								<?php if(Yii::$app->commonFunction->accessToElement('createTrip')){ ?>
								<?= Html::a('Update', ['update', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0','class'=>'btn btn-primary btn-block waves-effect waves-light']) ?>
								<?php } ?>
							</div>
							<div class="col-xs-6"><?= $colseOrders; ?></div>
						</div>
					<?php // } ?>

					<?php if($model->status == Trip::STATUS_ORDER_CONFIRMED){ ?>
						<hr>
						<div class="row text-center m-t-10">
							<div class="col-xs-6 b-r">
										<?= Html::a('Booking slip',['/trip/manage/bookingslip', 'id' => $model->id], ['title'=>'Booking slip','aria-label'=>'Booking slip','data-pjax'=>'0','class'=>'btn btn-primary btn-block waves-effect waves-light']) ?>
							</div>
							<div class="col-xs-6">
										<?= Html::a('Order slip', ['/trip/manage/orderslip', 'id' => $model->id], ['title'=>'Order slip','aria-label'=>'Order slip','data-pjax'=>'0','class'=>'btn btn-primary btn-block waves-effect waves-light']) ?>
							</div>
						</div>
						<hr>
						<div class="row text-center m-t-10">
							<div class="col-xs-6 b-r">
										<?= Html::a('Tax Invoice',['/trip/manage/taxinvoice', 'id' => $model->id], ['title'=>'Tax Invoice','aria-label'=>'Tax Invoice','data-pjax'=>'0','class'=>'btn btn-primary btn-block waves-effect waves-light']) ?>
							</div>
							<div class="col-xs-6">
								<?php
									if (!empty($model->billing)) {
										echo Html::a('View Billing',
																	['viewinvoice','billing'=>$model->billing_id],
																	[
																		'title'=>'View Billing',
																		'aria-label'=>'View Billing',
																		'data-pjax'=>'0',
																		'class'=>'btn btn-primary btn-block waves-effect waves-light',
																		'target'=>'_blank'
																	]);
									}
								 ?>
								<?php //= $colseOrders; ?>
							</div>
						</div>
					<?php } ?>
					<?php if(Yii::$app->commonFunction->accessToElement('customerConfirm')){ ?>
						<hr>
						<div class="row text-center m-t-10">
							<div class="col-xs-6 b-r">

								<?php if($model->status < Trip::STATUS_ORDER_CONFIRMED){ ?>
									<?= Html::a('Edit Vendor', ['/trip/manage/assignvendor', 'id' => $model->id], ['title'=>'Edit','aria-label'=>'Edit','data-pjax'=>'0','class'=>'btn btn-primary btn-block waves-effect waves-light']) ?>
									&nbsp;
								<?php } else if ($model->status >= Trip::STATUS_CUSTOMER_CONFIRM_PENDING){
									echo Html::a('Re-Assign Vendor','#frmReAssignVendor', ['title'=>'Confirm Customer','aria-label'=>'Re-Assign Vendor','data-pjax'=>'0','data-toggle'=>'modal','class'=>'btn btn-primary btn-block waves-effect waves-light']) ;
								} ?>
							</div>
							<div class="col-xs-6">
								<?php if ($model->status == Trip::STATUS_CUSTOMER_CONFIRM_PENDING){
									echo Html::a('Confirm Customer',['/trip/manage/customerconfirm', 'id' => $model->id], ['title'=>'Confirm Customer','aria-label'=>'Confirm Customer','data-pjax'=>'0','class'=>'btn btn-primary btn-block waves-effect waves-light']);
								} ?>
							</div>
						</div>
					<?php } ?>

                                                    -->
	      </div>
	    </section>
	  </div>

		<?php /*
		<?php if($model->status == Trip::STATUS_CUSTOMER_CONFIRM_PENDING && Yii::$app->commonFunction->accessToElement('customerConfirm')){ ?>
			<li class=""><a data-toggle="tab" href="#customer-confirmation" aria-expanded="false">Customer Confirmation</a></li>
		<?php } ?>

		 if($model->status == Trip::STATUS_CUSTOMER_CONFIRM_PENDING && Yii::$app->commonFunction->accessToElement('customerConfirm')){ ?>
			<div id="customer-confirmation" class="tab-pane fade">
				<div class="row">
					<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<p>Customer confirmation is pending. Click here to <a href="/trip/manage/customerconfirm?id=<?= $model->id ?>" class="btn btn-info btn-rounded">Confirm customer</a></p>
									<p style=""><span style="text-align:center">&nbsp;&nbsp;OR&nbsp;&nbsp;</span></p>
									<p>Customer not convinced. Click here to <a class="btn btn-info btn-rounded" data-toggle="modal" href="#frmReAssignVendor">Re-Assign Vendor</a></p>
								</div>
							</div>
					</div>
				</div>
			</div>
		<?php } */
		?>

	  <div class="col-md-6 col-sm-12 ">
	    <section class="well p-t-10 mob-p-t-0">
	      <nav class="">
	        <ul class="nav nav-tabs customtab" role="tablist">
                              <?php if($model->trip_type == 2){ ?>
		          <li class="active"><a data-toggle="tab" href="#trip-multistop" aria-expanded="false">
								<span class="visible-xs"><i class="mdi mdi-stop-circle"></i></span>
            		<span class="hidden-xs"> Multistop Details</span>
							</a></li>
                                <?php } ?>

						<?php if($model->status >= Trip::STATUS_ORDER_CONFIRMED){ ?>
							<li class=""><a data-toggle="tab" href="#trip-tracking" aria-expanded="false">
								<span class="visible-xs"><i class="mdi mdi-map"></i></span>
								<span class="hidden-xs"> Tracking</span>
							</a></li>
							<li class=""><a data-toggle="tab" href="#trip-documents" aria-expanded="false">
								<span class="visible-xs"><i class="mdi mdi-receipt"></i></span>
								<span class="hidden-xs"> Documents</span>
							</a></li>

						<?php } ?>
	        </ul>
	      </nav>

	      <div class="tab-content">
					<?php if($model->trip_type == 2){ ?>
		        <div id="trip-multistop" class="tab-pane fade in active">
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive table-custom mobile-table">
										<table class="table color-table table-hover"> <!--  muted-table -->
											<thead>
												<tr>
													<th>Stop Name</th>
													<th>Handling Charges</th>
												</tr>
											</thead>
											<tbody>
											<?php
											foreach($stopsModel as $stop){ ?>
												<tr>
													<td><?= $stop['name'] ?></td>
													<td><?= $stop['handling_charges'] ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		        </div>
					<?php } ?>

					<?php if($model->status >= Trip::STATUS_ORDER_CONFIRMED){ ?>
						<div id="trip-tracking" class="tab-pane fade">
									<div style="max-height:80vh;overflow:auto;">
										<?php Pjax::begin(['id'=>'remarks-index']); ?>
										<?php
										if (!empty($model->remarks)) {
											$quickNote = json_decode($model->remarks,true);
											krsort($quickNote);
											foreach ($quickNote as $id => $log) { ?>
												<p class="m-b-0">
													<?= $log['remark']; ?>
													<span class="btn btn-circle btn-flat waves-effect pull-right editTrackingDetails" data-id = "<?= $id ?>" title="Update"> <span class="hidden"><?= json_encode($log,true) ?></span> <i class="mdi mdi-pencil" style="line-height: 0.8;"></i></span>
												</p>
												<?php $user = \common\models\User::findOne(['id'=>$log['user']]) ?>
												<small>
												<span class="pull-right"><?= date('d-m-Y h:i A',strtotime($log['on'])); ?></span>
												<?= ' Event :'.\Yii::$app->params['event_status'][$log['trip_event']]; ?> |
												<?= ' Location :'.$log['location']; ?>
												<?= ' | Date & Time :'.date('d-m-Y h:i A',strtotime($log['datetime'])); ?>
												<br>
												<?= !empty($user)? ('By:'.$user->username):'' ?>
												</small>
												<hr>
										<?php  }
									} else {echo '<center> No Records </center>';}?>
									</div>
									<?php $this->registerJs("
										$('.editTrackingDetails').click(function(){
											var e = $(this);
											var details = JSON.parse(e.find('.hidden').text());
											$('#tripEvent').val(details.trip_event).trigger('change');
											$('#tripLocation').val(details.location);
											$('#tripDateTime').val(details.datetime);
											$('.addRemarksBox').val(details.remark);
											$('.submitFormTitle').text('Edit Tracking :');
											var tracking_id = e.data('id');
											$('.tracking-id').val(tracking_id);
										});
									"); ?>
									<?php Pjax::end(); ?>
									<!-- comment box secion -->
									<form id="formSubmitRemarks" class="formSubmitRemarks" onsubmit="return false;" action='/trip/manage/add-remarks' method="post" enctype="multipart/form-data">
									<div class="">
											<div class=""><label class="submitFormTitle"></label></div>
											<input type="hidden" name="id" class="tripId" value="<?= $model->id; ?>" />
											<input type="hidden" name="tracking-id" class="tracking-id" value="" />
											<div class="row">
												<div class="col-md-4">
													<select id="tripEvent" name="trip_event" class="form-control" >
														<option value="">Select Event Status</option>
														<?php foreach (\Yii::$app->params['event_status'] as $key => $text) {
															echo '<option value="'.$key.'">'.$text.'</option>';
														} ?>
													</select>
													<div class="text-danger help-block pull-left" style="margin-top:0px !important"></div>
												</div>
												<div class="col-md-4">
													<input type="text" id="tripLocation" name="location" placeholder="Location" required class="form-control" aria-invalid="false">
												</div>
												<div class="col-md-4">
													<input type="text" id="tripDateTime" name="datetime" placeholder="Date & Time" required class="form-control datetimepicker" autocomplete="off" aria-invalid="false">
												</div>
												<div class="clearfix"> <br><br><br> </div>
												<div class="col-md-12">
													<div class="">
																<textarea name="remark" class="form-control addRemarksBox" rows="2" style="margin-top:0px !important;margin-left:0px !important; margin-right:0px !important;" placeholder="Please Enter Remark"></textarea>
																<div class="text-danger help-block pull-left" style="margin-top:0px !important"></div>
													</div>
												</div>
												<div class="col-md-6">
														<div class="jsSectionPod" style="display:none">
																<label for="control-label">Upload POD</label>
																<input type="file" name="tripPodUpload" id="tripPodUpload" onchange="validate_fileupload(this.value);" >
																<div class="help-block text-danger jsTripPodUploadError"></div>
																<script type="text/javascript">
																function validate_fileupload(fileName)
																{
																	var allowed_extensions = ["png","jpg","gif","docx","doc","pdf"];
																	var file_extension = fileName.split('.').pop().toLowerCase();
																	for(var i = 0; i <= allowed_extensions.length; i++)
																	{
																		if(allowed_extensions[i]==file_extension)
																		{
																			$(".jsTripPodUploadError").text("");
																			return true;
																		}
																	}
																	$(".jsTripPodUploadError").text("Only PNG/JPG/GIF/DOC/PDF allowed.");
																	return false;
																}
																</script>
														</div>
												</div>
											</div>
											<div class="row">
												<a class="btn btn-info m-r-10 pull-right jsaddTripRemarksBtn" >Save</a>
												<a class="btn btn-default m-r-10 pull-right jsResetTripRemarksBtn" >Reset</a>
											</div>
										<!-- end comment box secion -->
									</div>
								</form>

								<?php $this->registerJs("
									$(document).ready(function(){

										$('#tripEvent').change(function(){
											//console.log(this.value);
											if (this.value == 8) {
												$('.jsSectionPod').show();
											} else {
												$('.jsSectionPod').hide();
											}
										});

										$('.jsResetTripRemarksBtn').click(function(){
												$('.addRemarksBox,#tripLocation,#tripLocation,#tripDateTime,#tripEvent,.tracking_id').val('');
												$('.submitFormTitle').text('');
												try { $.pjax.reload({container: '#remarks-index', timeout: 1200}); } catch (err) {}
										});

										function getBase64(file) {
										   var reader = new FileReader();
											 reader.readAsDataURL(file);
										   reader.onload = function () {
												 $('.jsReader').text(reader.result);
										   };
										   reader.onerror = function (error) {
										     console.log('Error: ', error);
										   };

											 return $('.jsReader').text();
										}
										$('.jsaddTripRemarksBtn').click(function(){
											if ($('.jsTripPodUploadError').text() != '') {
												return false;
											}
											var saveBtn = $(this);
											if (typeof saveBtn.attr('disabled') !== typeof undefined && saveBtn.attr('disabled') !== false) {
												return false;
											}
											saveBtn.attr('disabled','disabled');
											// getting data from model
											var remark = $('.addRemarksBox').val();
								      var location = $('#tripLocation').val();
								      var datetime = $('#tripDateTime').val();
											var trip_event = $('#tripEvent').val();
											var tracking_id = $('.tracking-id').val();
											var tripPodUpload = document.querySelector('#tripPodUpload[type=file]').files[0];
											if (tripPodUpload !== undefined) {
												var file_base64 = getBase64(tripPodUpload);
												file_base64 = $('.jsReader').text();
												// console.log(file_base64);
												var file_name = tripPodUpload.name;
											} else {
												file_base64 = '0';
												file_name = '0';
											}

											// $('#tripPodUpload').val();
											if (trip_event == '') {
												$('#tripEvent').next('.help-block').text('Please select a event');
												saveBtn.removeAttr('disabled');
												return false;
											} else {
												$('#tripEvent').next('.help-block').text('');
											}
								      if (remark == '') {
								        $('.addRemarksBox').next('.help-block').text('Please enter remark');
								        saveBtn.removeAttr('disabled');
								        return false;
								      } else {
								        $('.addRemarksBox').next('.help-block').text('');
								      }
								      var id = $('.tripId').val();
								      // sending data to save
											setTimeout(function() {
										      $.post('/trip/manage/add-remarks', {id:id,tracking_id:tracking_id,remark:remark,location:location,datetime:datetime,trip_event:trip_event,file_base64:$('.jsReader').text(),file_name:file_name},function(result) {
										          // Reloading pjax container except emp muster. This will throw error on emp muster
										          try { $.pjax.reload({container: '#remarks-index', timeout: 1200}); } catch (err) {}
										          // Showing success toast to user
										          console.log(result.remark);
										          if (result.remark == remark) {
										            alertSnackbar('REMARK ADDED.','success');
																$('.addRemarksBox,#tripLocation,#tripLocation,#tripDateTime,#tripEvent,.tracking_id').val('');
										          }
										          saveBtn.removeAttr('disabled');
										          return false;
													});
											}, 300);
										});
									});
								"); ?>


						</div>
						<div id="trip-documents" class="tab-pane fade">
							<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
							<div class="row">
							  <div class="col-md-12">
									<?= $form->field($modelDocumentUpload, 'document')->fileInput() ?>
								</div>
								<div class="col-md-6">
									<?= $form->field($modelDocumentUpload, 'reference_number')->textInput(['class'=>'form-control','autocomplete'=>'off']) ?>
								</div>
								<div class="col-md-6">
									<?= $form->field($modelDocumentUpload, 'type')->dropdownList(\Yii::$app->params['documents'],['prompt'=>'Select Document Type']) ?>
								</div>
								<div class="col-md-6">
									<?php $modelDocumentUpload->date = date('d-m-Y') ?>
									<?= $form->field($modelDocumentUpload, 'date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?>
								</div>
								<div class="col-md-6">
									<?= $form->field($modelDocumentUpload, 'expire_date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?>
								</div>
							</div>
							<?= Html::submitButton('Upload', ['class' => 'btn btn-success','id'=>'send-button']) ?>
							<?php $form = ActiveForm::end(); ?>
							<br><br>
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr style="line-height:0">
											<th>Document</th>
											<th>Date</th>
											<th>Expire Date</th>
											<th>Details</th>
											<th> </th>
										</tr>
									</thead>
									<tbody>
									<?php if (!empty($modelDocumentUpload->data_document)) {
												$uploadedDocuments = json_decode($modelDocumentUpload->data_document,true);
												foreach ($uploadedDocuments as $document) {
													$user = User::findOne($document['by']);
													?>
													<tr style="line-height:0.2">
														<td>
															<?= isset($document['reference_number'])?$document['reference_number']:'' ?>
															<br>
															<?= \Yii::$app->params['documents'][$document['type']]; ?>
														</td>
														<td ><?= '<label class="hidden-lg hidden-md text-muted">Date : </label>'.(isset($document['date'])?$document['date']:''); ?></td>
														<td ><?= '<label class="hidden-lg hidden-md text-muted">Expire Date : </label>'.(isset($document['expire_date'])?$document['expire_date']:''); ?></td>
														<td>
															<?= '<label class=" text-muted">On : </label>'.date('d-m-Y h:i A',strtotime($document['on'])); ?>
															<br>
															<?= '<label class=" text-muted">By : </label>'.$user->username; ?>
														</td>
														<td class="grid-action-column text-center">
															<a class="btn btn-circle btn-flat waves-effect" target="_blank" href="<?= '/uploads/'.$document['document']; ?>" title="View Document"><i class="mdi mdi-eye"></i></a>
														</td>
													</tr>
									<?php	}
									 	} else{ ?>
										<tr>
											<td colspan="10" class="text-center">No Records...</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					<?php } ?>
	      </div>
	    </section>

	  </div>
	</div>

</div>

<?php
// $this->registerJs("
// 	$('#btn-re-assign').click(function(){
// 		$('#frmReAssignVendor').modal();
// 	});
// ",View::POS_END);

// if($model->status != Trip::STATUS_ORDER_CONFIRMED){
// $this->registerJs("
// 	$(function(){
// 		try {
// 			$(window).load(function() {
// 				$('html, body').animate({ scrollTop: $(document).height() }, 1000);
// 			});
// 		} catch (err) {}
// 	})
// ",View::POS_END);
// }


?>
<p class="jsReader" hidden></p>
<?php $this->registerJs("
// To send sms to vendor
$('.js-sms-origin-destination').click(function(){
  var mob = $(this).data('mob');
  var msg = $(this).data('msg');
  var confirmation = confirm('Do you want to send this sms? '+ msg);
  if (confirmation == true) {
    $.post('/trip/manage/sendquotationsms', {mobile:mob, message:msg},function(data) {
      //Set message
      $('body').append('<div  class=\"alert-success myadmin-alert alert myadmin-alert-top-right block fade in\" style=\"display:block\"><button type=\"button\" class=\"m-l-10 close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button><h4>Message Sent Successfully.</h4></div>');
      // Auto close alert
      window.setTimeout(function() {
        $('.myadmin-alert').fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
        });
      }, 4000);
    });
  }
});

"); ?>
<script>
	 function initAutocomplete() {
			// Create the search box and link it to the UI element.
			var trip_location = document.getElementById('tripLocation');
			var searchBox = new google.maps.places.Autocomplete(
                                                                                               document.getElementById('tripLocation'),
                                                                                                {types: ['geocode'],componentRestrictions:{'country': 'in'}});
                                                                         searchBox.setFields(['address_component']);
	 }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['gmapKey']; ?>&libraries=places&callback=initAutocomplete"
async defer></script>



<!-- Modal Reassign Vendor -->
<?php $form = ActiveForm::begin([
'action' =>['/trip/manage/vendor-re-assign','id'=>$model->id],
]); ?>
<div id="frmReAssignVendor" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Re-Assign Vendor</h4>
      </div>
      <div class="modal-body">
				<?= $form->field($reAssignModel, 'negotiation_remark')->textArea(['row'=>4]); ?>
      </div>
      <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'send-button']) ?>
      </div>
    </div>
  </div>
</div>
<?php $form = ActiveForm::end(); ?>
<?= $this->render('_modalCreateEditTransaction',['modelTransactions'=>$modelTransactions]) ?>
<?php $this->registerJs('
$(document).on("pjax:complete","#trip-transaction-table", function(event) {
		createEditTransaction();
});
'); ?>

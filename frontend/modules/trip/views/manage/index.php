<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\modules\trip\models\Trip;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Trips';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
    tbody tr {cursor: pointer;}
    tbody tr:hover {cursor: pointer; color:#000;}
	.divFilter label {cursor:pointer;color:#337ab7;}
	.p-t-8{
		padding-top:8px !important
	}
	/* .input-group-addon {display:inline !important;} */
	.inline-block{
		display:inline-block !important
	}
	@media (max-width:767px) {
		#date-range{ display:inline !important;}
		#apply { margin-top: 10px;display: inline-block;width: 100%;padding:10px; }
		.pull-right { padding-top:10px !important; }
		.mob-m-t-10 { margin-top:10px !important; }
	}

");
?>
<div class="trip-index">
    <p class="divFilter">
        <?php if (Yii::$app->commonFunction->accessToElement('createTrip')) { ?>
            <?= Html::a('Add New Enquiry', ['create'],
                ['class' => 'btn btn-success']) ?>
<?php } ?>

        <a href="#" class="btn btn-success" onclick="$('#search-form').toggle();">Search Trip</a>

        <span class="pull-right">
            <?php $params = json_encode(Yii::$app->request->queryParams,
                true); ?>
<?= Html::a('CSV',
    ['download-trip', 'params' => $params],
    ['class' => 'btn btn-primary', 'data-method' => "post"]) ?>  
<?= Html::a('PDF',
    ['download-trip', 'params' => $params, 'pdf' => 1],
    ['class' => 'btn btn-primary', 'data-method' => "post"]) ?>
        </span>

    </p>

    <div class="white-box m-b-5" id="search-form" style="display:<?php if (empty(Yii::$app->request->queryParams)) {
            echo "none";
        } else {
            echo "block";
        } ?>;">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php Pjax::begin(['id' => 'trip-index']); ?>
    <?php
    $dataProvider->setSort([
        'attributes' => [
            'number' => [
                'asc' => ['number' => SORT_ASC], 'desc' => ['number' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'Date' => [
                'asc' => ['trip_date' => SORT_ASC], 'desc' => ['trip_date' => SORT_DESC],
                'default' => SORT_DESC
            ],
            'Company' => [
                'asc' => ['custName' => SORT_ASC], 'desc' => ['custName' => SORT_DESC],
                'default' => SORT_DESC
            ],
            'Loading Point' => [
                'asc' => ['origin_name' => SORT_ASC], 'desc' => ['origin_name' => SORT_DESC],
                'default' => SORT_DESC
            ],
            'Unloading Point' => [
                'asc' => ['destination_name' => SORT_ASC], 'desc' => ['destination_name' => SORT_DESC],
                'default' => SORT_DESC
            ],
            'profit' => [
                'asc' => ['profit' => SORT_ASC], 'desc' => ['profit' => SORT_DESC],
                'default' => SORT_DESC
            ],
            'Created on' => [
                'asc' => ['created_at' => SORT_ASC], 'desc' => ['created_at' => SORT_DESC],
                'default' => SORT_DESC
            ],
            'Status' => [
                'asc' => ['status' => SORT_ASC], 'desc' => ['status' => SORT_DESC],
                'default' => SORT_DESC
            ],
        ],
        'defaultOrder' => ['Status' => SORT_ASC, 'Created on' => SORT_DESC]
    ]);
    ?>
    <?php
    $company     = $searchModel->company_id;
    $firstColumn = ['class' => 'yii\grid\SerialColumn'];
    if (!empty($company)) {
        $firstColumn = [
            'attribute' => 'Order Id',
            'label' => '',
            'format' => 'raw',
            'value' => function($model) {
                if ($model->status == 10) {
                    return '<div class="checkbox checkbox-info m-t-0" style="width: 30px">
                                              <input type="checkbox" class="sendInvoiceCheckBox" data-trip_id="'.md5($model->id).'" id="send_invoice_'.$model->id.'">
                                              <label class=" " for="send_invoice_'.$model->id.'"></label></div>';
                }
                return ' <span class="checkbox" hidden></span> ';
            }
        ];
    }
    ?>
    <?=
    GridView::widget([
        'options' => ['class' => 'white-box table-responsive'],
        'tableOptions' => ['class' => 'table'],
        'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid) {
            $style = '';
            if ($model->profit < 0) {
                $style = "background:rgba(255, 148, 169, 0.42);";
            }
            // if ($model->profit > 0) {
            //   $style ="background:rgba(148, 255, 178, 0.42)";
            // }
            return [
                'style' => $style,
                'data-id' => $model->id,
                'class' => 'tripContainer',
                // 'onclick' => 'location.href="/trip/manage/view?id="+$(this).data("id");',
            ];
        },
        'layout' => "{items}\n<div align='center'>{pager}</div>",
        'pager' => [
            'linkOptions' => ['class' => 'btn-circle m-r-10'],
            'disabledListItemSubTagOptions' => ['class' => 'btn btn-circle disable  m-r-10']
        ],
        'columns' => [
            $firstColumn,
            [
                'attribute' => 'Order Id',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->number;
                }
            ],
            [
                'attribute' => 'Company',
                'value' => function($model) {
                    if (empty($model->custName)) {
                        return '(not set)';
                    } else {
                        return $model->custName.(!empty($model->companyName->address)
                                ? " (".$model->companyName->address.")" : "");
                    }
                }
            ],
            [
                'attribute' => 'Date',
                'format' => 'raw',
                'value' => function($model) {
                    return '<label class="hidden-lg hidden-md text-muted">Trip Date : </label>'.date('d-m-Y',
                            strtotime($model->trip_date));
                }
            ],
            [
                'attribute' => 'Loading point',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->loading_point.' / '.ucwords($model->origin_name);
                }
            ],
            [
                'attribute' => 'Unloading point',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->unloading_point.' / '.ucwords($model->destination_name);
                }
            ],
            [
                'attribute' => 'profit',
                'format' => 'raw',
                'value' => function($model) {
                    return '<label class="hidden-lg hidden-md text-muted">Operating Margin : </label>'.(!empty($model->profit)
                            ? $model->profit : '-');
                }
            ],
            [
                'attribute' => 'Created on',
                'format' => 'raw',
                'value' => function($model) {
                    return '<label class="hidden-lg hidden-md text-muted">Created on : </label>'.date('d-m-Y',
                            strtotime($model->created_at));
                }
            ],
            [
                'attribute' => 'Status',
                'format' => 'html',
                'value' => function($model) {
                    return Yii::$app->params['tripStatusLabel'][$model->status];
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {update} ',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $url = Url::to(['view', 'id' => $model->id]);
                        if (Yii::$app->commonFunction->accessToElement('viewTrip')) {
                            return Html::a('<i class="mdi mdi-eye" style="line-height: 0.8;"></i>',
                                    $url,
                                    ['class' => 'btn btn-circle btn-flat waves-effect',
                                    'title' => 'view']);
                        }
                    },
                    'delete' => function ($url, $model) {
                        $url = Url::to(['closeorder', 'id' => $model->id]);
                        if (Yii::$app->commonFunction->accessToElement('deleteTrip')) {
                            return Html::a('<i class="mdi mdi-delete text-danger" style="line-height: 0.8;"></i>',
                                    $url,
                                    ['class' => 'btn btn-circle btn-flat waves-effect',
                                    'title' => 'Drop Order']);
                        }
                    },
                    'update' => function ($url, $model) {
                        $url = Url::to(['update', 'id' => $model->id]);
                        if (Yii::$app->commonFunction->accessToElement('updateTrip')
                            && $model->status < Trip::STATUS_ORDER_CONFIRMED) {
                            return Html::a('<i class="mdi mdi-pencil" style="line-height: 0.8;"></i>',
                                    $url,
                                    ['title' => 'Update', 'class' => 'btn btn-circle btn-flat waves-effect']);
                        } else {
                            return Html::a('<i class="mdi mdi-clipboard-text" style="line-height: 0.8;"></i><span class="data-remarks hidden" hidden>'.$model->remarks.'</span>',
                                    '#modaladdComments',
                                    [
                                    'title' => 'Tracking',
                                    'class' => 'addComments btn btn-circle btn-flat waves-effect',
                                    'onclick' => 'event.stopPropagation();',
                                    'data' => [
                                        'id' => $model->id,
                                    ],
                            ]);
                        }
                    }]
            ],
        ],
    ]);
    ?>
<?php $this->registerJs("
      $(document).ready(function(){
        // Assign trip Ids
        var selectedTrips = [];
        $('.sendInvoiceCheckBox').click(function(){
          selectedTrips = [];
          $('.sendInvoiceCheckBox:checked').each(function(){
            selectedTrips.push($(this).data('trip_id'));
          });
        });
        // Toggle Modal
        $('.btnSendInvoice').click(function(){
          console.log(selectedTrips.length > 0 == true);
          if (selectedTrips.length < 0) {
            alert('Please select atleast 1 trip');
          } else {
            window.location = '/trip/manage/viewinvoice?id='+selectedTrips.join('%2C');
          }
        });
        // Redirect
        $('td:not(.grid-action-column)').click(function(){
          var hasCheckbox = $(this).find('.checkbox').length;
          if (hasCheckbox == 0) {
            var tripId = $(this).parents('.tripContainer').data('id');
            location.href='/trip/manage/view?id='+tripId;
          } else {
            return true;
          }
        });
      });
      ") ?>
<?php Pjax::end(); ?>
</div>



<?= $this->render('_modalAddComments'); ?>

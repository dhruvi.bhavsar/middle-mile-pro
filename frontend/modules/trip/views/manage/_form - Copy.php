<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\View;
use frontend\modules\trip\models\Trip;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
/* @var $form yii\widgets\ActiveForm */

$this->registerCssFile('@web/plugins/bower_components/nestable/nestable.css', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/plugins/bower_components/nestable/jquery.nestable.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCss("
#trip-trip_date {
    color: #565656;
    height: 38px;
    width: 100%;
    padding: 7px 12px;
    transition: all 300ms linear 0s;
    background-color: #ffffff;
    border: 1px solid #e4e7ea;
    border-radius: 0px
}
.hcharges {	float:right; }
.hcharges input { height: 30px;margin: 10px 0px; padding: 0px 5px;}
.dd-item { border:1px solid rgba(120, 130, 140, 0.13) !important; min-height: 50px; margin: 5px 0px;}
.dd-handle {border:0px solid rgba(120, 130, 140, 0.13) !important; float:left;}
.dd-close { padding: 5px; cursor: pointer; }
");
?>


<div class="row">
	<div class="col-md-12">
		<div class="white-box printableArea">

<div class="trip-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

<div class="row">
    <div class="col-md-6">
        <?php //$form->field($model, 'company_id')->textInput() ?>
        <?= $form->field($model, 'company_id')->widget(Select2::classname(), [
            'data' => \frontend\modules\customer\models\Customer::CustomerList(),
            'options' => ['placeholder' => 'Select a Customer ...',
                    'onChange' => "$('#trip-contact_person_id').load('/customer/contact/list',{cust_id:$('#trip-company_id').val()});",
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
        ?>
    </div>
    <div class="col-md-6">
        <!-- <?= $form->field($model, 'contact_person_id')->textInput() ?> -->
        <?= $form->field($model, 'contact_person_id')->dropDownList(\frontend\modules\customer\models\Contact::ContactList($model->company_id),['value'=>$model->contact_person_id,'prompt'=>"Select Customer"]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'vehicle_type_id')->widget(Select2::classname(), [
            'data' => \frontend\modules\master\models\VehicleType::vehicletypelist(),
            'options' => ['placeholder' => 'Select a Vehicle Type ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
    <div class="col-md-6">
        <label class="control-label" for="trip-trip_date">Date</label>
		<?php $model->trip_date = $model->isNewRecord ?date('d-m-Y H:i'):date('d-m-Y H:i',strtotime($model->trip_date)); ?>
        <?= $form->field($model, 'trip_date')->textInput(['class'=>'datetimepicker'])->label(false) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?php //$form->field($model, 'origin')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'origin')->widget(Select2::classname(), [
            'data' => \frontend\modules\master\models\City::cityList(),
            'options' => ['placeholder' => 'Select a Location ...'],
            'pluginOptions' => ['allowClear' => true],
			]);
		?>
    </div>
    <div class="col-md-6">
        <?php //$form->field($model, 'destination')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'destination')->widget(Select2::classname(),[
			'data' => \frontend\modules\master\models\City::cityList(),
            'options' => ['placeholder' => 'Select a Location ...'],
            'pluginOptions' => ['allowClear' => true],
		]);

		?>
    </div>
</div>
<div class="row">
				<?php
					$model->origin_handling_charges = (!empty($model->origin_handling_charges))?$model->origin_handling_charges:0;
					$model->destination_handling_charges = (!empty($model->destination_handling_charges))?$model->destination_handling_charges:0;
				?>
    <div class="col-md-6">
        <?= $form->field($model, 'origin_handling_charges')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'destination_handling_charges')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'loading_point')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'unloading_point')->textInput() ?>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'trip_type')->dropDownList(Yii::$app->params['tripType']) ?>
    </div>

</div>




<div class="row" id="stops" style="display:none;">
    <div class="col-md-12">
        <label class="control-label" for="trip-trip_date">Stops</label>
    <?php
    $tripStopList = \frontend\modules\trip\models\TripStops::tripStopList($model->id,['stop_id','handling_charges']);
    $stopsModel->stop_id = array_column($tripStopList,'stop_id');
    /* echo "<pre>"; print_r(array_column($tripStopList,'handling_charges','stop_id')); */
    $allStopsList = \frontend\modules\master\models\MasterStop::stopList();
    /* print_r(array_column($tripStopList,'stop_id')); */
    ?>
        <?=
            Select2::widget([
                'model' => $stopsModel,
                'attribute' => 'stop_id',
                'data' => $allStopsList,
                'maintainOrder' => true,
                'options' => ['placeholder' => 'Select a Stops ...',
                    // 'onChange' => "console.log('selected stop');",
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    // 'multiple' => true,
                    'tags' => true,
                ],
                'pluginEvents' => [
                    "select2:select" => "function() {
                        var data = jQuery.parseJSON('".json_encode($allStopsList)."');
                        var id = $(this).val();
						var item,dataid,addr;
                        if($.isNumeric(id)){ item = data[id]; dataid=id; addr = data[id];
						}else{ item = id; dataid=id.replace(/(\w+).*/,'$1'); addr = id.replace(/\s+/g, '__');}
                        $('.dd-list').append('<li class=\'dd-item\' id=\'dd-item-'+dataid+'\' data-id='+dataid+'><div class=\'dd-handle\'>'+item+'</div><div class=\'hcharges\'><input type=\'text\' name=TripStops[stops]['+dataid+'][charges] placeholder=\'Handling Charges\' /><input type=\'hidden\' name=\'TripStops[stops]['+dataid+'][addr]\' value='+addr+'><span class=\'dd-close\'>X</span></div></li>');
						$(this).val('').trigger('change');;
                    }",
                ],
            ]);
         ?>
        <div class="white-box" style="border:1px solid rgba(120, 130, 140, 0.13) !important">
            <div class="myadmin-dd" id="nestable">
                <ol class="dd-list">
                <?php if(!empty($tripStopList)){
                $stopId = array_column($tripStopList,'handling_charges','stop_id');
                    foreach($stopId as $k=>$v){ ?>
						<li class="dd-item" id="dd-item-<?= $k ?>" data-id="<?= $k ?>"><div class="dd-handle"><?= $allStopsList[$k] ?></div><div class="hcharges"><input type="text" name="TripStops[stops][<?= $k ?>][charges]" placeholder="Handling Charges" value="<?= $v ?>"><input type="hidden" name="TripStops[stops][<?= $k ?>][addr]" value="<?= $allStopsList[$k] ?>"><span class="dd-close">X</span></div></li>
                <?php } } ?>
                </ol>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'service_type')->dropDownList(Yii::$app->params['serviceType']) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'targated_rate')->textInput() ?>
    </div>
</div>


		<?php if(empty($model->status)){$model->status = Trip::STATUS_VENDOR_ASSIGN_PENDING;} ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>

    <!-- <?= $form->field($model, 'vendor_id')->textInput() ?>
    <?= $form->field($model, 'origin_lat')->textInput() ?>
    <?= $form->field($model, 'origin_lon')->textInput() ?>
    <?= $form->field($model, 'destination_lat')->textInput() ?>
    <?= $form->field($model, 'destination_lon')->textInput() ?>
    <?= $form->field($model, 'buying_rate')->textInput() ?>
    <?= $form->field($model, 'selling_rate')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
		</div>
	</div>
</div>
<?php
$this->registerJs("
        var updateOutput = function(e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        $('#nestable').nestable({ group: 1 }).on('change', updateOutput);

        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $(document).on('click','.dd-close',function(){
          $(this).parent().parent().remove();
        });

		// Conditional visibility of multistop inputs
		$('#trip-trip_type').change(function(){
			var elem = $(this);
			$('#stops').hide();
			if(elem.val() == '2'){
				$('#stops').show();
			}
		});

		$(document).ready(function(){
			var elem = $('#trip-trip_type');
			$('#stops').hide();
			if(elem.val() == '2'){
				$('#stops').show();
			}
		});

    ",View::POS_END);
?>

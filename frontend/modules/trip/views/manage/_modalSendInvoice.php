<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

// $userList = \common\models\User::find()->select(['id','username'])->asArray()->all();
// $userList = ArrayHelper::map($userList, 'id', 'username');
/* @var $this yii\web\View */
/* @var $model frontend\modules\timesheet\models\ResumePlacements */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id'=>$model->formName(), 'enableAjaxValidation'=>true, 'enableClientScript'=>true]); ?>
<div class="modal fade" id="modalSendInvoice" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"><h4 class="modalTransactionsTitle">Send Invoice</h4></div>
        <div class="modal-body" >
          <?= $form->field($model, 'trip_id')->hiddenInput()->label(false) ?>
          <?= $form->field($model, 'email_id')->textInput() ?>
        </div>
          <div class="modal-footer">
            <?= Html::resetButton('Cancel',['class' => 'btn btn-default','data'=>['dismiss'=>'modal']]); ?>
            <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
          </div>
        </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
<?php $this->registerJs("
  $(document).ready(function(){
    // Assign trip Ids
    var selectedTrips = [];
    $('.sendInvoiceCheckBox').click(function(){
      selectedTrips = [];
      $('.sendInvoiceCheckBox:checked').each(function(){
        selectedTrips.push($(this).data('trip_id'));
      });
    });
    // Toggle Modal
    $('.btnSendInvoice').click(function(){
      console.log(selectedTrips.length > 0 == true);
      if (selectedTrips.length < 0) {
        alert('Please select atleast 1 trip');
      } else {
        window.location = 'viewcombainedinvoice?id='+selectedTrips.join('%2C')
        // viewcombainedinvoice
      }
    });
  });
"); ?>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use yii\widgets\Pjax;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
/* @var $form yii\widgets\ActiveForm */
if (!isset($forDriver)) {
  $forDriver = 0;
}

?>
<?php if (!empty($forDriver)): ?>
  <a class="btn btn-primary" onclick="$('#addTrackingModal').modal();">Add Tracking</a>
  <br><br>
<?php endif; ?>

<div style="max-height:80vh;overflow:auto;">
  <?php Pjax::begin(['id'=>'remarks-index']); ?>
  <?php
  if (!empty($model->remarks)) {
    $quickNote = json_decode($model->remarks,true);
    krsort($quickNote);
    foreach ($quickNote as $id => $log) { ?>
      <p class="m-b-0">
        <?= $log['remark']; ?>
        <?php if (empty($forDriver)) { ?>
          <span class="btn btn-circle btn-flat waves-effect pull-right editTrackingDetails" data-id = "<?= $id ?>" title="Update"> <span class="hidden"><?= json_encode($log,true) ?></span> <i class="mdi mdi-pencil" style="line-height: 0.8;"></i></span>
        <?php } ?>
      </p>
      <?php $user = \common\models\User::findOne(['id'=>$log['user']]) ?>
      <small>
      <span class="pull-right"><?= date('d-m-Y h:i A',strtotime($log['on'])); ?></span>
      <?= ' Event :<strong>'.\Yii::$app->params['event_status'][$log['trip_event']] . "</strong>"; ?> |
      <?= ' Location :'.$log['location']; ?>
      <?= ' | Date & Time :'.date('d-m-Y h:i A',strtotime($log['datetime'])); ?>
      <br>
      <?= !empty($user)? ('By:'.$user->username):'' ?>
      <?= !empty($log['km_reading'])? (' | KM Reading:'.$log['km_reading']):'' ?>
      </small>
      <hr>
  <?php  }
} else {echo '<center> No Records </center>';}?>
</div>
<?php $this->registerJs("
  $('.editTrackingDetails').click(function(){
    var e = $(this);
    var details = JSON.parse(e.find('.hidden').text());
    $('#tripEvent').val(details.trip_event).trigger('change');
    $('#tripLocation').val(details.location);
    $('#tripDateTime').val(details.datetime);
    if (details.km_reading != undefined) {
      $('#tripKmReading').val(details.km_reading);
    } else {
      $('#tripKmReading').val('');
    }
    $('.addRemarksBox').val(details.remark);
    $('.submitFormTitle').text('Edit Tracking :');
    var tracking_id = e.data('id');
    $('.tracking-id').val(tracking_id);
  });
"); ?>
<?php Pjax::end(); ?>
<!-- comment box secion -->
<?php if (!empty($forDriver)) { ?>
<div class="modal fade" id="addTrackingModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> <h4>Add Tracking</h4> </div>
      <div class="modal-body">
<?php } ?>

<form id="formSubmitRemarks" class="formSubmitRemarks" onsubmit="return false;" action='/trip/manage/add-remarks' method="post" enctype="multipart/form-data">
<div class="">
    <div class=""><label class="submitFormTitle"></label></div>
    <input type="hidden" name="id" class="tripId" value="<?= $model->id; ?>" />
    <input type="hidden" name="vid" class="vid" value="<?= $model->vehicle_id; ?>" />
    <input type="hidden" name="tracking-id" class="tracking-id" value="" />
    <div class="row">
      <div class="col-md-4">
        <select id="tripEvent" name="trip_event" class="form-control" >
          <option value="">Select Event Status</option>
          <?php foreach (\Yii::$app->params['event_status'] as $key => $text) {
            echo '<option value="'.$key.'">'.$text.'</option>';
          } ?>
        </select>
        <div class="text-danger help-block pull-left" style="margin-top:0px !important"></div>
        <br>
      </div>
      <div class="col-md-4">
        <input type="text" id="tripLocation" name="location" placeholder="Location" required class="form-control" aria-invalid="false">
        <br>
      </div>
      <?php if (empty($forDriver)) { ?>
        <div class="col-md-4">
          <input type="text" id="tripDateTime" name="datetime" placeholder="Date & Time" required class="form-control datetimepicker" autocomplete="off" aria-invalid="false">
          <br>
        </div>
      <?php } else { ?>
        <div class="col-md-4" hidden>
          <input type="hidden" id="tripDateTime" value="<?= date('d-m-Y H:i A') ?>" name="datetime" placeholder="Date & Time" required class="form-control" autocomplete="off" aria-invalid="false">
        </div>
      <?php } ?>
      <div class="col-md-4">
        <input type="text" id="tripKmReading" name="km_reading" placeholder="KM Reading" required class="form-control kmreading" autocomplete="off" aria-invalid="false">
        <div class="text-danger help-block pull-left" style="margin-top:0px !important"></div>
        <br>
      </div>
      <div class="clearfix"> <br><br> </div>
      <div class="col-md-12">
        <div class="">
              <textarea name="remark" class="form-control addRemarksBox" rows="2" style="margin-top:0px !important;margin-left:0px !important; margin-right:0px !important;" placeholder="Please Enter Remark"></textarea>
              <div class="text-danger help-block pull-left" style="margin-top:0px !important"></div>
        </div>
      </div>
      <div class="col-md-6">
          <div class="jsSectionPod" style="display:none">
              <label for="control-label">Upload POD</label>
              <input type="file" name="tripPodUpload" id="tripPodUpload" onchange="validate_fileupload(this.value);" >
              <div class="help-block text-danger jsTripPodUploadError"></div>
              <script type="text/javascript">
              function validate_fileupload(fileName)
              {
                var allowed_extensions = ["png","jpg","gif","docx","doc","pdf"];
                var file_extension = fileName.split('.').pop().toLowerCase();
                for(var i = 0; i <= allowed_extensions.length; i++)
                {
                  if(allowed_extensions[i]==file_extension)
                  {
                    $(".jsTripPodUploadError").text("");
                    return true;
                  }
                }
                $(".jsTripPodUploadError").text("Only PNG/JPG/GIF/DOC/PDF allowed.");
                return false;
              }
              </script>
          </div>
      </div>
    </div>
<?php if (!empty($forDriver)) { ?>
  </div>
  <div class="modal-footer">
<?php } ?>
    <div class="row">
      <a class="btn btn-info m-r-10 pull-right jsaddTripRemarksBtn" >Save</a>
      <a class="btn btn-default m-r-10 pull-right jsResetTripRemarksBtn" onclick="$('.addTransactionModalCloseBtn').click();"> <?= !empty($forDriver)?'Close':'Reset'?> </a>
    </div>

<?php if (!empty($forDriver)) { ?>
    <div class="hidden" hidden>
      <button type="button" class="btn btn-default addTransactionModalCloseBtn" data-dismiss="modal" hidden>Close</button>
    </div>
  </div>
<?php } ?>

  <!-- end comment box secion -->
</div>
</form>
<?php if (!empty($forDriver)) { ?>
    </div>
  </div>
</div>
<?php } ?>



<?php $this->registerJs("
$(document).ready(function(){

  $('#tripEvent').change(function(){
    //console.log(this.value);
    if (this.value == 8) {
      $('.jsSectionPod').show();
    } else {
      $('.jsSectionPod').hide();
    }
  });

  $('.jsResetTripRemarksBtn').click(function(){
      $('.addRemarksBox,#tripLocation,#tripLocation,#tripDateTime,#tripEvent,.tracking_id,#tripKmReading').val('');
      $('.submitFormTitle').text('');
      try { $.pjax.reload({container: '#remarks-index', timeout: 1200}); } catch (err) {}
  });

  function getBase64(file) {
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function () {
       $('.jsReader').text(reader.result);
     };
     reader.onerror = function (error) {
       console.log('Error: ', error);
     };

     return $('.jsReader').text();
  }
  $('.jsaddTripRemarksBtn').click(function(){
    if ($('.jsTripPodUploadError').text() != '') {
      return false;
    }
    var saveBtn = $(this);
    if (typeof saveBtn.attr('disabled') !== typeof undefined && saveBtn.attr('disabled') !== false) {
      return false;
    }
    saveBtn.attr('disabled','disabled');
    // getting data from model
    var remark = $('.addRemarksBox').val();
    var location = $('#tripLocation').val();
    var datetime = $('#tripDateTime').val();
    var km_reading = $('#tripKmReading').val();
    var trip_event = $('#tripEvent').val();
    var tracking_id = $('.tracking-id').val();
    var vid = $('.vid').val();
    var tripPodUpload = document.querySelector('#tripPodUpload[type=file]').files[0];
    if (tripPodUpload !== undefined) {
      var file_base64 = getBase64(tripPodUpload);
      file_base64 = $('.jsReader').text();
      // console.log(file_base64);
      var file_name = tripPodUpload.name;
    } else {
      file_base64 = '0';
      file_name = '0';
    }

    // $('#tripPodUpload').val();
    if (trip_event == '') {
      $('#tripEvent').next('.help-block').text('Please select a event');
      saveBtn.removeAttr('disabled');
      return false;
    } else {
      $('#tripEvent').next('.help-block').text('');
    }

    if (remark == '') {
      $('.addRemarksBox').next('.help-block').text('Please enter remark');
      saveBtn.removeAttr('disabled');
      return false;
    } else {
      $('.addRemarksBox').next('.help-block').text('');
    }

    // Km reading validation
    if(km_reading != ''){
        $.get('/vendor/vehicle/validatekmreading', {vehicle_id:vid,kmreading:km_reading},function(data){
            if(data==0){
                $('#tripKmReading').next('.help-block').text('Please enter valid km reading');
                saveBtn.removeAttr('disabled');
                return false;
            }else{
                $('#tripKmReading').next('.help-block').text('');
            }
        });
    }

    var id = $('.tripId').val();
    // sending data to save
    setTimeout(function() {
        $.post('/trip/manage/add-remarks', {id:id,tracking_id:tracking_id,remark:remark,km_reading:km_reading,location:location,datetime:datetime,trip_event:trip_event,file_base64:$('.jsReader').text(),file_name:file_name},function(result) {
            // Reloading pjax container except emp muster. This will throw error on emp muster
            try {
              $.pjax.reload({container: '#remarks-index', timeout: 1200});
              $.pjax.reload({container: '#transaction-listing', timeout: 1200});
              $('.tracking-id').val('');
              $('.addTransactionModalCloseBtn').click();
            } catch (err) {}
            // Showing success toast to user
            console.log(result.remark);
            if (result.remark == remark) {
              alertSnackbar('REMARK ADDED.','success');
              $('.addRemarksBox,#tripLocation,#tripLocation,#tripDateTime,#tripEvent,.tracking_id,#tripKmReading').val('');
            }
            saveBtn.removeAttr('disabled');
            return false;
        });
    }, 300);
  });
});
"); ?>

<?php

namespace frontend\modules\trip\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\trip\models\Trip;
use frontend\modules\master\models\VehicleType;
use frontend\modules\customer\models\Customer;
use frontend\modules\master\models\City;
use frontend\modules\vendor\models\Vehicle;
/**
 * TripSearch represents the model behind the search form of `frontend\modules\trip\models\Trip`.
 */
class TripSearch extends Trip
{
    /**
     * {@inheritdoc}
     */

     public $origin_state;
     public $origin_city;
     public $destination_state;
     public $destination_city;
     public $vehicle_number;

    public function rules()
    {
        return [
            [['id', 'vendor_id', 'company_id', 'contact_person_id', 'vehicle_id', 'vehicle_type_id', 'buying_rate_incl_handling_charges', 'trip_type', 'service_type', 'status','from_date','to_date'], 'safe'],
            [['number', 'origin', 'loading_point', 'destination', 'unloading_point', 'trip_date', 'recipients', 'created_at', 'updated_at', 'negotiation_remark', 'cancellation'], 'safe'],
            [['targated_rate', 'buying_rate', 'advance_buying_charges', 'selling_rate', 'advance_selling_charges', 'origin_handling_charges', 'destination_handling_charges', 'origin_lat', 'origin_lon', 'destination_lat', 'destination_lon','targated_buying'], 'safe'],
            [['origin_state','origin_city','destination_state','destination_city','vendor_bids','duration','vehicle_number'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

     public function search($params,$onlyQuery = NULL)
     {
       $this->load($params);

       $query = Trip::find()->from([Trip::tableName().' T'])
                            ->select(['T.*','C.name custName','Org.name origin_name','Dest.name destination_name',
                                      '(IF(T.buying_rate_incl_handling_charges IS NULL,(T.selling_rate - T.buying_rate - origin_handling_charges - destination_handling_charges),(T.selling_rate - T.buying_rate))) profit'
                                    ])
                            ->leftJoin(Customer::tableName().' C','C.id = T.company_id ')
                            ->leftJoin(City::tableName().' Org','Org.id = T.origin')
                            ->leftJoin(City::tableName().' Dest','Dest.id = T.destination')
                            ->leftJoin(Vehicle::tableName().' V','V.id = T.vehicle_id');
       if(!empty($params['status']) && isset($params['status']) && $params['status'] == '2'){
          // $where['status']=$params['s'];
          $query = $query->where(['T.status'=>$params['status']]);
       }else{
           // $where = ['NOT',['status'=>Trip::STATUS_DELETED]];
           if (!empty($this->status)) {
             $query = $query->where(['T.status'=>$this->status]);
           } else {
             $query = $query->where(['NOT',['T.status'=>Trip::STATUS_DELETED]]);
           }

       }
      if(!empty($this->duration))
      {
       switch($this->duration){
           case 'currentmonth':
               $sdate = date('Y-m-1 00:00:00');
               $edate = date('Y-m-d 23:59:59');
               $query = $query->andWhere(['>','T.trip_date',$sdate]);
               $query = $query->andWhere(['<','T.trip_date',$edate]);
               break;
           case 'currentweek':
               $sdate = date('Y-m-d 00:00:00',strtotime('-7 Day'));
               $edate = date('Y-m-d 23:59:59');
               $query = $query->andWhere(['>','T.trip_date',$sdate]);
               $query = $query->andWhere(['<','T.trip_date',$edate]);
               break;
           case 'custom':
                $sdate = date('Y-m-d 00:00:00',strtotime((!empty($this->from_date)?$this->from_date:'1-Jan-1980 00:00:00')));
                $edate = date('Y-m-d 23:59:59',strtotime((!empty($this->to_date)?$this->to_date:'1-Jan-2900 23:59:59')));
                $query = $query->andWhere(['>=','T.trip_date',$sdate]);
                $query = $query->andWhere(['<=','T.trip_date',$edate]);
               break;
       }
      }
      //
      // var_dump($query->createCommand()->sql);
      // die();
       // echo date('Y-m-d H:i:s',strtotime('+1 Month'));
       // $query = $query->orderBy(['status'=>SORT_ASC,'created_at'=>SORT_ASC]);
       // echo $query->createCommand()->sql; die();
       // echo "<pre>";print_r($where); die();

       $query->andFilterWhere([
           'Org.id'=> $this->origin_city,
           'Org.state_id'=>$this->origin_state,
           'Dest.id'=>$this->destination_city,
           'Dest.state_id'=>$this->destination_state,
           'T.company_id'=>$this->company_id
       ]);

       if (!empty($this->trip_date)) {
         $query->andFilterWhere(['like', 'T.trip_date',date('Y-m-d',strtotime($this->trip_date))]);
       }

       $query->andFilterWhere(['like', 'T.number', $this->number])
             ->andFilterWhere(['like', 'T.vendor_id', $this->vendor_id])
             ->andFilterWhere(['like', 'V.registration_number',$this->vehicle_number,])
             ->andFilterWhere(['like', 'T.vehicle_type_id', $this->vehicle_type_id])
             ->groupBy(['T.id']);


      if (!empty($onlyQuery)) {return $query;}

       $dataProvider = new ActiveDataProvider([
           'query' => $query,
           'sort' => ['defaultOrder'=>'id asc'],
           'pagination' => [ 'pageSize' => 10, ],
       ]);

       return $dataProvider;
     }

    public function searchFindTasks($params)
    {
      $team = Yii::$app->user->identity->team;
      if(Yii::$app->user->identity->role == 10){ $team = 0; }

      $this->load($params);

      $query = self::find()->from([self::tableName().' T'])
          ->select([
            'T.id','T.number','T.origin','T.destination','T.trip_date','T.created_at','T.updated_at','T.status','T.loading_point','T.unloading_point','T.buying_rate','T.targated_buying','T.vendor_bids',
            'cust.name as custName','vend.name as vendor','orgn.name as origin_location','dest.name as destination_location','vtype.name as vehicle_type'
          ])
          ->where(['NOT IN','T.status',[self::STATUS_DELETED,self::STATUS_ORDER_CONFIRMED,self::STATUS_ORDER_DELIVERED]]);
      if($team == 1){
          $query = $query->where(['T.status'=>self::STATUS_CUSTOMER_CONFIRM_PENDING]);
      }else if($team == 2){
          $query = $query->andWhere(['IN', 'T.status', [self::STATUS_VENDOR_ASSIGN_PENDING,self::STATUS_VENDOR_CONFIRM_PENDING,self::STATUS_VENDOR_RE_ASSIGN]]);
      }
      $query = $query->innerJoin('customer as cust','cust.id= T.company_id');
      $query = $query->innerJoin('master_city as orgn','orgn.id= T.origin');
      $query = $query->innerJoin('master_city as dest','dest.id= T.destination');
      $query = $query->innerJoin(VehicleType::tableName().' as vtype','vtype.id= T.vehicle_type_id');
      // vehicle_type_id VehicleType
      $query = $query->leftJoin('vendor as vend','vend.id= T.vendor_id');
      // $query = $query->orderBy(['T.status'=>SORT_ASC]);

      // 'origin_state','origin_city','destination_state','destination_city'
      $query->andFilterWhere([
          'orgn.id'=> $this->origin_city,
          'orgn.state_id'=>$this->origin_state,
          'dest.id'=>$this->destination_city,
          'dest.state_id'=>$this->destination_state,
          'T.company_id'=>$this->company_id,
      ]);

      if (!empty($this->trip_date)) {
        $query->andFilterWhere(['like', 'T.trip_date',date('Y-m-d',strtotime($this->trip_date))]);
      }


      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => [ 'pageSize' => 10, ],
      ]);

      $query->andFilterWhere(['like', 'T.number', $this->number])
            ->andFilterWhere(['like', 'T.vendor_id', $this->vendor_id])
            ->andFilterWhere(['like', 'T.vehicle_type_id', $this->vehicle_type_id]);

      return $dataProvider;
    }
}

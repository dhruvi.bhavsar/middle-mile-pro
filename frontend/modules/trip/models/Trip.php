<?php

namespace frontend\modules\trip\models;

use Yii;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\customer\models\Customer;
use frontend\modules\customer\models\Contact;
use frontend\modules\master\models\VehicleType;
use frontend\modules\master\models\City;
use frontend\modules\trip\models\TripDriver;
use frontend\modules\master\models\MasterStop;
use frontend\modules\trip\models\Billing;

/**
 * This is the model class for table "trip".
 */
class Trip extends \yii\db\ActiveRecord
{
    const STATUS_VENDOR_ASSIGN_PENDING = 1;
    const STATUS_DELETED = 2;
    const STATUS_VENDOR_RE_ASSIGN = 3;
    const STATUS_CUSTOMER_CONFIRM_PENDING = 5;
    const STATUS_VENDOR_CONFIRM_PENDING = 8;
    const STATUS_ORDER_CONFIRMED = 10;
    const STATUS_ORDER_DELIVERED = 20;

    public $origin_location;
    public $destination_location;
    public $custName;
    public $vendor;
    public $vehicle_type;
    public $origin_name;
    public $destination_name;
    public $profit;
    public $duration;
    public $from_date;
    public $to_date;
    public $trip_event;
    public $document;
    public $driver_id;
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'vehicle_id', 'vehicle_type_id', 'trip_type', 'service_type', 'status','load_type'], 'integer','on'=>'createTrip'],

            [['company_id', 'contact_person_id', 'vehicle_type_id', 'loading_point', 'unloading_point','trip_date'], 'required','on'=>'createTrip'],

            ['destination','compare','compareAttribute' => 'origin','operator' => '!==','on'=>'createTrip'],
            [['targated_rate', 'buying_rate', 'selling_rate', 'origin_handling_charges', 'destination_handling_charges', 'origin_lat', 'origin_lon', 'destination_lat', 'destination_lon'], 'number','on'=>'createTrip'],
            [['trip_date','assigned_to','approx_distance'], 'safe','on'=>'createTrip'],
            [['loading_point','unloading_point','driver_id'], 'string', 'max' => 255,'on'=>'createTrip'],
            [['number'], 'string', 'max' => 50,'on'=>'createTrip'],
            [['targated_buying','notification_duration'],'number'],
            // [[], 'string', 'max' => 100,'on'=>'createTrip'],

            /* Vendor Assignment */
            [['vendor_id'], 'required','on'=>'vendorAssignment','message'=>'Please select {attribute}'],
            [['buying_rate','buying_rate_incl_handling_charges'], 'required','on'=>'vendorAssignment'],
            // ['vendor_id','integer','on'=>'vendorAssignment'],
            ['buying_rate','number','on'=>'vendorAssignment'],

            /* Customer Confirmation */
            [['selling_rate','origin_handling_charges','destination_handling_charges','advance_selling_charges'], 'required','on'=>'customerConfirmation'],
            [['selling_rate','origin_handling_charges','destination_handling_charges','advance_selling_charges'],'number','on'=>'customerConfirmation'],

            /* Vendor Confirmation */
            [['vehicle_id','advance_buying_charges'], 'required','on'=>'vendorConfirmation'],
            // ['vehicle_id','integer','on'=>'vendorConfirmation'],
            ['advance_buying_charges','number','on'=>'vendorConfirmation'],

            /* Send Invoice */
            ['recipients', 'required','on'=>'sendInvoice'],
            ['recipients', 'multipleemails','on'=>'sendInvoice'],

            /* Vendor Re-assignment Request */
            ['negotiation_remark', 'required','on'=>'reassignRequest'],
            ['negotiation_remark', 'string','on'=>'reassignRequest'],

            /* Order Cancellation */
            ['cancellation', 'required','on'=>'cancelOrder'],
            ['cancellation', 'string','on'=>'cancelOrder'],
            ['vendor_bids', 'string'],
            [['billing_id'],'integer'],
            [['remarks','special_instructions','last_notified_at','trip_event','pod_receival_date','unloaded_datetime','final_bill_date'],'safe'],
            [['document'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,gif,docx,doc,pdf'],
            [['tracking_status'],'safe'],
            // ['document','required','when'=>function($model){ return $model->trip_event == 8; }, 'whenClient' => "function (attribute, value) { return $('#trip-trip_event').val() == 8; }",'message'=>'','on'=>'addRemarks'],

            [['vendor_bids','cancellation','negotiation_remark','recipients','advance_buying_charges','vehicle_id','selling_rate','origin_handling_charges','destination_handling_charges','advance_selling_charges','targated_buying','company_id', 'contact_person_id', 'vehicle_type_id', 'origin', 'destination','trip_date','id', 'trip_type', 'service_type', 'status','remarks','notification_duration','last_notified_at','trip_event','pod_receival_date','unloaded_datetime','final_bill_date','load_type'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    public static function tripNotificationText($tripId){
        $trip = Trip::findOne($tripId);
        return $trip->vehicleType->name . ", " . $trip->loading_point . " - " . $trip->unloading_point . ", " . date("d-m-y h:i a",strtotime($trip->trip_date)) . ". ";
    }


    public function multipleemails($attribute, $params)
    {
      if (!$this->hasErrors()) {

            $emailList = explode(",",$this->$attribute);
            $errorFlag = false;
            $errorMessage = '';
            foreach($emailList as $eml){
                if (1 !== preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $eml)) {
                    $errorFlag = true;
                    $errorMessage =  $errorMessage.'('.$eml.')';
                }
            }

            if($errorFlag){
                $this->addError($attribute, 'Invalid emails '.$errorMessage);
                    return;
            }
      }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Order ID',
            'vendor_id' => 'Vendor',
            'company_id' => 'Company',
            'contact_person_id' => 'Contact Person',
            'vehicle_id' => 'Vehicle',
            'vehicle_type_id' => 'Vehicle Type',
            'targated_rate' => 'Target Freight',
            'buying_rate' => 'Buying Rate',
            'advance_buying_charges' => 'Advance to Vendor',
            'selling_rate' => 'Selling Rate',
            'advance_selling_charges' => 'Advance From Customer',
            'origin_handling_charges' => 'Origin Handling Charges',
            'destination_handling_charges' => 'Destination Handling Charges',
            'buying_rate_incl_handling_charges' => 'Rate Including Handling Charges',
            'trip_type' => 'Trip Type',
            'origin' => 'Origin',
            'origin_lat' => 'Loading Location Lat',
            'origin_lon' => 'Loading Location Lon',
            'loading_point' => 'Loading Point',
            'destination' => 'Destination',
            'destination_lat' => 'Unloading Locations Lat',
            'destination_lon' => 'Unloading Locations Lon',
            'unloading_point' => 'Unloading Point',
            'service_type' => 'Service Type',
            'load_type' => 'Load Type',
            'trip_date' => 'Trip Date',
            'targated_buying'=>'Targeted Buying',
            'vendor_bids'=>'Vendor Bids',
            'recipients' => 'Recipients',
            'status' => 'Status',
            'created_at' => 'Created Date',
            'negotiation_remark' => 'Reason',
            'cancellation' => 'Cancellation Reason',
            'unloaded_datetime' => 'Unloaded Date & time',
            'pod_receival_date' => 'POD Receival Date',
            'final_bill_date' => 'Final Bill Date',
            'notification_duration' => 'Notification Duration (in hrs.)',
        ];
    }

    public function getVendorDetails()
    {
        return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
    }

    public function getCompanyName()
    {
        return $this->hasOne(Customer::className(), ['id' => 'company_id']);
    }

    public function getBilling()
    {
        return $this->hasOne(Billing::className(), ['id' => 'billing_id']);
    }

    public function getContactPersonName()
    {
        return $this->hasOne(Contact::className(), ['id' => 'contact_person_id']);
    }

    public function getVehicleType()
    {
        return $this->hasOne(VehicleType::className(), ['id' => 'vehicle_type_id']);
    }

    public function getVehicleDetails(){
        return $this->hasOne(Vehicle::className(), ['id' => 'vehicle_id']);
    }

    public function getOriginDetails(){
        return $this->hasOne(City::className(), ['id' => 'origin']);
    }

    public function getDestinationDetails(){
        return $this->hasOne(City::className(), ['id' => 'destination']);
    }
    public function getDriver(){
        return $this->hasOne(TripDriver::className(), ['trip_id' => 'id']);
    }

    public static function findTasks(){
        $team = Yii::$app->user->identity->team;
        if(Yii::$app->user->identity->role == 10){ $team = 0; }

        $data = self::find()->from([self::tableName().' T'])
            ->select([
                'T.id','T.number','T.origin','T.destination','T.trip_date',
                'T.created_at','T.updated_at','T.status','T.loading_point',
                'T.unloading_point','T.target_buying','cust.name as custName',
                'vend.name as vendor','orgn.name as origin_location','dest.name as destination_location',
            ])
            ->where(['NOT IN','T.status',[self::STATUS_DELETED,self::STATUS_ORDER_CONFIRMED]]);
        if($team == 1){
            $data = $data->where(['T.status'=>self::STATUS_CUSTOMER_CONFIRM_PENDING]);
        }else if($team == 2){
            $data = $data->andWhere(['IN', 'T.status', [self::STATUS_VENDOR_ASSIGN_PENDING,self::STATUS_VENDOR_CONFIRM_PENDING,self::STATUS_VENDOR_RE_ASSIGN]]);
        }
        $data = $data->innerJoin('customer as cust','cust.id= T.company_id');
        $data = $data->innerJoin('master_city as orgn','orgn.id= T.origin');
        $data = $data->innerJoin('master_city as dest','dest.id= T.destination');
        $data = $data->leftJoin('vendor as vend','vend.id= T.vendor_id');
        $data = $data->orderBy(['T.status'=>SORT_ASC]);
        return $data;
    }

    public static function tripCounts(){
        $total = self::find()->select('count(id) count')->asArray()->all();
        $delivered = self::find()->select('count(id) count')->where(['status'=>self::STATUS_ORDER_DELIVERED])->asArray()->one();
        $active = self::find()->select('count(id) count')->where(['status'=>self::STATUS_ORDER_CONFIRMED])->asArray()->all();
        $closed = self::find()->select('count(id) count')->where(['status'=>self::STATUS_DELETED])->asArray()->all();
        $pending = self::find()->select('count(id) count')->where(['IN', 'status', [self::STATUS_VENDOR_ASSIGN_PENDING,self::STATUS_VENDOR_CONFIRM_PENDING,self::STATUS_VENDOR_RE_ASSIGN,self::STATUS_CUSTOMER_CONFIRM_PENDING]])->asArray()->all();
        $data = [
            'total'=>$total[0]['count'],
            'delivered'=>$delivered['count'],
            'active'=>$active[0]['count'],
            'closed'=>$closed[0]['count'],
            'pending'=>$pending[0]['count'],
        ];
        return $data;
    }

    public static function closedOrders(){
        $data = self::find()->from([self::tableName().' T'])
            ->select([
                'T.id','T.number','T.origin','T.destination','T.trip_date','T.created_at','T.updated_at','T.status',
                'cust.name as custName',
                'vend.name as vendor',
                'orgn.name as origin_location',
                'dest.name as destination_location',
            ])
            ->where(['T.status'=>self::STATUS_DELETED]);
        $data = $data->innerJoin('customer as cust','cust.id=T.company_id');
        $data = $data->innerJoin('master_city as orgn','orgn.id=T.origin');
        $data = $data->innerJoin('master_city as dest','dest.id=T.destination');
        $data = $data->leftJoin('vendor as vend','vend.id=T.vendor_id');
        $data = $data->orderBy(['T.status'=>SORT_ASC]);
        return $data;
    }



    // public static function findTasks(){
    //     $team = Yii::$app->user->identity->team;
    //     if(Yii::$app->user->identity->role == 10){ $team = 0; }
    //     $tbl = self::tableName();
    //     $data = self::find()
    //         ->select([
    //             $tbl.'.id',
    //             $tbl.'.number',
    //             $tbl.'.origin',
    //             $tbl.'.destination',
    //             $tbl.'.trip_date',
    //             $tbl.'.created_at',
    //             $tbl.'.updated_at',
    //             $tbl.'.status',
    //             'cust.name',
    //             'vend.name as vendor',
    //             'orgn.name as origin_location',
    //             'dest.name as destination_location',
    //         ])
    //         ->where(['NOT IN',$tbl.'.status',[self::STATUS_DELETED,self::STATUS_ORDER_CONFIRMED]]);
    //     if($team == 1){
    //         $data = $data->where([$tbl.'.status'=>self::STATUS_CUSTOMER_CONFIRM_PENDING]);
    //     }else if($team == 2){
    //         $data = $data->andWhere(['IN', $tbl.'.status', [self::STATUS_VENDOR_ASSIGN_PENDING,self::STATUS_VENDOR_CONFIRM_PENDING,self::STATUS_VENDOR_RE_ASSIGN]]);
    //     }
    //     $data = $data->innerJoin('customer as cust','cust.id='.$tbl.'.company_id');
    //     $data = $data->innerJoin('master_city as orgn','orgn.id='.$tbl.'.origin');
    //     $data = $data->innerJoin('master_city as dest','dest.id='.$tbl.'.destination');
    //     $data = $data->leftJoin('vendor as vend','vend.id='.$tbl.'.vendor_id');
    //     $data = $data->orderBy([$tbl.'.status'=>SORT_ASC]);
    //     $data = $data->asArray()->all();

    //     return $data;
    // }

    public function getContactId(){
      $model = Contact::findOne(['id'=>$this->contact_person_id]);
      if(empty($model)){
        $model = Contact::findOne(['name'=>$this->contact_person_id]);
        if(empty($model)){
          $model = new Contact();
          $model->customer_id = $this->company_id;
          $model->name = $this->contact_person_id;
          $model->status = 1;
          if($model->save(false)){
            Yii::$app->activity->add($model->id,'frontend\modules\customer\models\Contact','New Customer Contact Person Created');
          }
        }
        $this->contact_person_id = $model->id;
      }
      return $model;
    }

    public function getCompanyId($obj){
      $custModel = Customer::findOne(['id'=>$this->company_id]);
      if(empty($custModel)){
          $custModel = new Customer();
          $custModel->name = $this->company_id;
          $custModel->type = 1;
          $custModel->status = 1;
          if($custModel->save(false)){
          Yii::$app->activity->add($custModel->id,$obj->customer_model_path,'New Customer Created');
            //$this->getContactId();
          }
      }
      $this->company_id = $custModel->id;
      return $custModel;
    }

    public function getOriginId()
    {
      $originModel = City::findOne(['id'=>$this->origin]);
      if(empty($originModel)){
        $originModel = new City();
        $originModel->name = $this->origin;
        $originModel->state_id = 0;
        $originModel->status = 1;
        $originModel->save(false);
      }
      $this->origin = $originModel->id;
    }

    public function getDestinationId()
    {
      $destinationModel = City::findOne(['id'=>$this->destination]);
      if(empty($destinationModel)){
        $destinationModel = new City();
        $destinationModel->name = $this->destination;
        $destinationModel->state_id = 0;
        $destinationModel->status = 1;
        $destinationModel->save(false);
      }
      $this->destination = $destinationModel->id;
    }

    public function saveTripStops($trip_id,$postData)
    {
        $arrData = [];
        $intermittentStopsArray = $postData['intermittentStop'];
          foreach($intermittentStopsArray['stop'] as $i=>$v){
//                  $newStop = new MasterStop();
//                  $newStop->name = (str_replace('__',' ',$v['addr']));
//                  $newStop->status = Trip::STATUS_VENDOR_ASSIGN_PENDING;
//                  if($newStop->validate() && $newStop->save()){
//                    //Yii::$app->activity->add($newStop->id,$obj->stop_model_path,'Master Stop Created');
//                  }
                  $arrData[] = [$trip_id,$v,$intermittentStopsArray['lat'][$i],$intermittentStopsArray['lon'][$i],$intermittentStopsArray['stop_type'][$i]];
          }
          
          $stops = new TripStops();
          Yii::$app->db->createCommand()->batchInsert($stops->tableName(), ['trip_id','point','lat','lon','stop_type'], $arrData)->execute();
    }

    public static function vehicleStatus($vehicleId)
    {
      // $vehicleId = 1 ;
      $newBookings = self::find()
                      ->andWhere(['vehicle_id'=>$vehicleId,'status'=>[self::STATUS_CUSTOMER_CONFIRM_PENDING,self::STATUS_VENDOR_CONFIRM_PENDING]])
                      ->orderBy(['status'=>SORT_DESC,'trip_date'=>SORT_DESC,'pod_receival_date'=>SORT_DESC,'updated_at'=>SORT_DESC])
                      ->asArray()->all();

      $lastTrips = self::find()
                      ->andWhere(['vehicle_id'=>$vehicleId,'status'=>self::STATUS_ORDER_CONFIRMED])
                      ->andWhere(['between','trip_date',date('Y-m-d 00:00:00',strtotime('3 week ago')),date('Y-m-d 23:59:59',strtotime('1 day ago'))])
                      ->orderBy(['trip_date'=>SORT_DESC,'pod_receival_date'=>SORT_ASC,'updated_at'=>SORT_DESC])
                      ->asArray()->all();

      $todayTrips = self::find()
                      ->andWhere(['vehicle_id'=>$vehicleId,'status'=>self::STATUS_ORDER_CONFIRMED])
                      ->andWhere(['between','trip_date',date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')])
                      ->orderBy(['trip_date'=>SORT_DESC,'pod_receival_date'=>SORT_DESC,'updated_at'=>SORT_DESC])
                      ->asArray()->all();

      $upcommingTrips = self::find()
                      ->andWhere(['vehicle_id'=>$vehicleId,'status'=>self::STATUS_ORDER_CONFIRMED])
                      ->andWhere(['>=','trip_date',date('Y-m-d 23:59:59')])
                      ->orderBy(['trip_date'=>SORT_DESC,'pod_receival_date'=>SORT_DESC,'updated_at'=>SORT_DESC])
                      ->asArray()->all();

      $currentVehicleStatus = ['details'=>[],'type'=>''];
      if (!empty($todayTrips)) {
        // Todays Trip. Highest priorty
        $currentVehicleStatus['details'] = $todayTrips[0];
        $currentVehicleStatus['type'] = 'Today\'s Trip';
      } elseif (!empty($upcommingTrips) && (empty($lastTrips) || !empty($lastTrips[0]['pod_receival_date']))) {
        // If last trip over. Then 2nd priorty Else no priorty
        $currentVehicleStatus['details'] = $upcommingTrips[0];
        $currentVehicleStatus['type'] = 'Upcomming Trip';
      } elseif (!empty($lastTrips)){
        $currentVehicleStatus['details'] = $lastTrips[0];
        // Is Running or over
        if (empty($lastTrips[0]['pod_receival_date'])) {
          $currentVehicleStatus['type'] = 'Running';
        } else {
          $currentVehicleStatus['type'] = 'Last Trip';
        }
      } elseif (!empty($newBookings)){
        // If no record then show booking whos confirmation is pending
        $currentVehicleStatus['details'] = $newBookings[0];
        if (empty($newBookings[0]['status'] == self::STATUS_CUSTOMER_CONFIRM_PENDING)) {
          $currentVehicleStatus['type'] = 'Customer Confirmation Pending';
        } else {
          $currentVehicleStatus['type'] = 'Vendor Confirmation Pending';
        }
      }

      return [
        'newBookings'=>$newBookings,
        'lastTrips'=>$lastTrips,
        'todayTrips'=>$todayTrips,
        'upcommingTrips'=>$upcommingTrips,
        'currentVehicleStatus'=>$currentVehicleStatus,
      ];
    }

    public static function driverCurrentTrip($driverId)
    {
        $query = self::find()->from([self::tableName().' T'])
                            ->leftJoin(TripDriver::tableName().' TD','TD.trip_id = T.id')
                            ->andWhere([
                                          'TD.driver_id'=>$driverId,
                                          'T.status'=>self::STATUS_ORDER_CONFIRMED,
                                          'T.unloaded_datetime'=>NULL
                                        ])
                            ->orderBy(['T.trip_date'=>SORT_ASC])
                            ->one();

        return $query;
    }
}

<?php

namespace frontend\modules\trip\models;

use Yii;
use yii\web\UploadedFile;
use frontend\modules\trip\models\Trip;
/**
 * This is the model class for table "trip_documents".
 *
 * @property int $id
 * @property int $trip_id
 * @property string $data_document
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 */
class TripDocuments extends \yii\db\ActiveRecord
{
    public $document;
    public $type;
    public $date;
    public $expire_date;
    public $reference_number;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trip_documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trip_id','type','date'], 'required'],
            [['trip_id', 'status'], 'integer'],
            [['data_document'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['expire_date', 'reference_number'], 'safe'],
            [['document'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png,jpg,gif,docx,doc,pdf'],
            [['type','date'],'safe'],

            // HTMLPurifier
            [['id','trip_id','data_document','created_at','updated_at','status','type','expire_date', 'reference_number','date'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'data_document' => 'Data Document',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    public function uploadDocument($fileName=NULL,$fileContent=NULL)
    {
        $dataDocument= empty($this->data_document)?[]:json_decode($this->data_document,true);
        // data_document
        if (empty($fileName)) {
          $document = strtotime('now').'_'.$this->document->baseName . '.' .$this->document->extension;
          $this->document->saveAs('uploads/' .$document);
        } else {
          $document = strtotime('now').'_'.$fileName;
          $data = explode('base64,', $fileContent);
          $content = base64_decode($data['1'],true);
          file_put_contents('uploads/'.$document, $content);
        }

        $dataDocument[]=[
          'reference_number'=>$this->reference_number,
          'document'=>$document,
          'type'=>$this->type,
          'on'=>date('Y-m-d H:i:s'),
          'date'=>empty($this->date)?date('d-m-Y'):$this->date,
          'expire_date'=>empty($this->expire_date)?'':$this->expire_date,
          'by'=>\Yii::$app->user->identity->id,
        ];

        if ($this->type == 6) {
          // if POD then mark pod date
          $modelTrip = Trip::findOne(['id'=>$this->trip_id]);
          $modelTrip->pod_receival_date = date('Y-m-d',strtotime($this->date));
          $modelTrip->save(false);
        }
        if ($this->type == 7) {
          // if Final bill then mark final_bill_date
          $modelTrip = Trip::findOne(['id'=>$this->trip_id]);
          $modelTrip->final_bill_date = date('Y-m-d',strtotime($this->date));
          $modelTrip->save(false);
        }
        $this->data_document = json_encode($dataDocument,true);
    }
}

<?php

namespace frontend\modules\trip\models;

use Yii;
use common\models\User;
use frontend\modules\staff\models\Staff;
/**
 * This is the model class for table "trip_driver".
 *
 * @property integer $id
 * @property integer $trip_id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property integer $status
 */
class TripDriver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $password;

    public static function tableName()
    {
        return 'trip_driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trip_id', 'name', 'mobile'], 'required'],
            [['trip_id', 'status'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
            [['mobile'], 'string', 'max' => 20],
            ['mobile', 'match', 'pattern' =>'/^[\+\ \(\)\-0-9]{6,18}$/'],
            [['mobile'], 'string','min' => 10,'max'=>10,'tooShort'=>'{attribute} should be minimum 10 digits'],
            [['status'],'default','value'=>1],
            [['password','driver_id'],'safe'],

            [['id','trip_id','name','mobile','status','password','driver_id'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'name' => 'Driver Name',
            'email' => 'Email',
            'mobile' => 'Driver Mobile',
            'status' => 'Status',
        ];
    }

    public static function tripDriverList($trip_id){
        $data= self::find()->where(['status'=>1,'trip_id'=>$trip_id]);
        $data = $data->asArray()->all();

        return $data;
        // return \yii\helpers\ArrayHelper::map($data,'id','registration_number');
    }

    public static function tripDriverListforReport($trip_id){
        $data= self::find()->where(['status'=>1,'md5(trip_id)'=>$trip_id]);
        $data = $data->asArray()->all();

        return $data;
        // return \yii\helpers\ArrayHelper::map($data,'id','registration_number');
    }

    public function processData($post)
    {
      // Add driver to user account
      $this->password = $post['Trip']['password'];
      $this->driver_id = $post['Trip']['driver_id'];
      if (!empty($this->driver_id) && !empty($this->password)  && empty((int)$this->driver_id)) {
          $user = new Staff();
          $user->username = $this->name;
          $user->email = $this->driver_id;
          $user->mobile = $this->mobile;
          $user->role = 1;
          $user->team = 3;
          $user->password = $this->password;
          $user->addStaff();
          $user = Staff::findOne(['username'=>$this->name,'email'=>$this->driver_id,'role'=>1]);
          $this->driver_id = $user->id;
      }
      $this->save(false);
      $this->refresh();
      return true;
    }

    public static function driverStaffList()
    {
        $query = User::find()->select(['id','email'])->where(['role'=>1])->asArray()->all();

        return \yii\helpers\ArrayHelper::map($query,'id','email');
    }

    public static function driverStaffListWithDetails()
    {
        $query = User::find()->select(['id','username','mobile'])->where(['role'=>1])->asArray()->all();
        return \yii\helpers\ArrayHelper::index($query,'id');
    }

}

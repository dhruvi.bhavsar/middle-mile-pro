<?php

namespace frontend\modules\trip\models;

use Yii;
use frontend\modules\master\models\MasterStop;

/**
 * This is the model class for table "trip_stops".
 *
 * @property integer $id
 * @property integer $trip_id
 * @property integer $stop_id
 * @property double $handling_charges
 */
class TripStops extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trip_stops';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trip_id', 'stop_id'], 'required'],
            [['trip_id', 'stop_id'], 'integer'],
            [['handling_charges'], 'number'],
            [['handling_charges'], 'default','value'=>0],

            [['id','trip_id','stop_id','handling_charges'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'stop_id' => 'Stop ID',
            'handling_charges' => 'Handling Charges',
        ];
    }

    public function getStop(){
        return $this->hasOne(MasterStop::className(),['id'=>'stop_id']);
    }

    public static function tripStopList($trip_id,$columns=NULL){
        $query = self::find()->where(['trip_id'=>$trip_id]);
        if($columns){
           $query = $query->select($columns)->asArray();
        }
        return $query->all();
    }

    public static function tripStopListByName($trip_id){
        $tbl = self::tableName();
        $query = self::find()->select([$tbl.'.*'])->where(['trip_id'=>$trip_id])->asArray();
        return $query->all();
    }

        public static function tripStopListByNameforReport($trip_id){
        $tbl = self::tableName();
        $query = self::find()->select([$tbl.'.*','sm.name'])
            ->innerJoin('stop_master as sm','sm.id='.$tbl.'.stop_id')
            ->where(['md5(trip_id)'=>$trip_id])->asArray();
        return $query->all();
    }


}

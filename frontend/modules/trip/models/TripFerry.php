<?php

namespace frontend\modules\trip\models;

use Yii;

/**
 * This is the model class for table "trip_ferry".
 *
 * @property int $id
 * @property int $driver_id
 * @property int $vendor_vehicle_id
 * @property string $from_location
 * @property int $from_km_reading
 * @property string $to_location
 * @property int $to_km_reading
 * @property string $start_date_time
 * @property string $end_date_time
 * @property string $remark
 * @property int $status
 */
class TripFerry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trip_ferry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['driver_id', 'vendor_vehicle_id', 'from_location', 'from_km_reading', 'to_location', 'to_km_reading', 'start_date_time', 'end_date_time', 'remark'], 'required'],
            [['driver_id', 'vendor_vehicle_id', 'from_km_reading', 'to_km_reading', 'status'], 'integer'],
            [['start_date_time', 'end_date_time'], 'safe'],
            [['from_location', 'to_location', 'remark'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Driver ID',
            'vendor_vehicle_id' => 'Vendor Vehicle ID',
            'from_location' => 'From Location',
            'from_km_reading' => 'From Km Reading',
            'to_location' => 'To Location',
            'to_km_reading' => 'To Km Reading',
            'start_date_time' => 'Start Date Time',
            'end_date_time' => 'End Date Time',
            'remark' => 'Remark',
            'status' => 'Status',
        ];
    }
}

<?php

namespace frontend\modules\trip\models;

use Yii;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vehicle;
/**
 * This is the model class for table "transactions".
 *
 * @property int $id
 * @property int $trip_id
 * @property string $transaction_id
 * @property double $amount
 * @property int $transaction_type
 * @property string $note
 * @property int $status
 */
class Transactions extends \yii\db\ActiveRecord
{
    // Transaction type
    const PAID = 2;
    const DEBIT = 3;

    const ADVANCE_RECEIVED = 33;
    const RECEIVED = 4;
    const WRITE_OFF = 5;

    const TRANSACTION_TYPE_RECEIVABLE = 0;

    // status
    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_REJECTED = 2;
    const STATUS_DELETED = 5;

    const APPROVED = 1;
    const REJECTED = 2;


    // Payment_type
    const ORIGIN_DETENTION_CHARGES = 6;
    const DESTINATION_DETENTION_CHARGES = 7;
    const TOLL = 8;
    const PERMIT = 9;
    const ADBLUE = 10;
    const PUNCTURES = 11;
    const REPAIR_AND_MAINTENANCE = 12;
    const DIESEL = 13;
    const CHALLAN = 14;

    const COMMISSION = 15;
    const MAMUL = 16;
    const TDS = 17;

    //const TOLL_CHARGES = 3;
    const OTHER = 9;

    public $send_sms_to_vendor;
    
    public $receivable_remark;
    public $receivable_amount;
    public $payable_remark;
    public $payable_amount;

    public $is_payable;
    public $is_receivable;

    /**
     * {@inheritdoc}
     */
    
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_type'], 'required'],
            [['trip_id', 'transaction_type', 'status'], 'integer'],
            [['amount','mamul','mamul_type','detention_days','payable_amount','receivable_amount'], 'number'],
            [['note'], 'string'],
            [['transaction_id'], 'string', 'max' => 100],
            [['datetime','created_at','updated_at','send_sms_to_vendor'],'safe'],
            [['payment_type','payable_remark','payable_amount','receivable_remark','receivable_amount','is_payable','is_receivable'],'safe'],
            ['detention_days','required','when'=>function($model){ return ($model->payment_type == 1 || $model->payment_type == 2); }, 'whenClient' => "function (attribute, value) { return ($('#transactions-payment_type').val() == 1 || $('#transactions-payment_type').val() == 2); }"],

            [['id','trip_id','transaction_type','amount','note','datetime','created_at','updated_at','status','mamul','mamul_type','send_sms_to_vendor','payment_type','detention_days'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'transaction_id' => 'Transaction ID',
            'amount' => 'Amount',
            'transaction_type' => 'Transaction Type',
            'payment_type' => 'Payment Type',
            'detention_days' => 'Detention Days',
            'note' => 'Note',
            'datetime' => 'Date & Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    public function processData($model)
    {
      // Setting data for first entry
      if ($this->isNewRecord) {
        $this->status = 1;
        $this->created_at = date('Y-m-d H:i:s');
      }
      $this->trip_id = $model->id;
      $this->updated_at = date('Y-m-d H:i:s');
      $this->datetime = date('Y-m-d H:i:s',strtotime($this->datetime));

      // Is mamul applicable? What type?
      // if ($this->transaction_type == self::TRANSACTION_TYPE_PAID) {
      //     $transactionsCount = self::find()->where(['trip_id'=>$model->id,'transaction_type'=>self::TRANSACTION_TYPE_PAID])->count();
      //     if (empty($transactionsCount)) {
      //       // Advance mamul
      //       $this->mamul = \Yii::$app->commonFunction->getMamul($model->buying_rate,'advance_mamul');
      //       $this->mamul_type = 1;
      //     } else {
      //       $totalTransaction = self::tripTransactions($model->id);
      //       $balance = (float)$model->buying_rate - (float)$totalTransaction['paid'];
      //       if ($balance <= $this->amount) {
      //         // If final transaction
      //         $this->mamul = \Yii::$app->commonFunction->getMamul($balance,'balance_mamul');
      //         $this->mamul_type = 2;
      //       }
      //     }
      // }

      return $this->validate();
    }

    public function transactionEntry($postData,$tripId){
        $postData = $postData['Transactions'];

         if(in_array($postData['transaction_type'],[2,3,4,5,33])){
                    $model = new Transactions();
                    $model->transaction_type = $postData['transaction_type'];
                    $model->amount = $postData['amount'];
                    $model->note = $postData['note'];
//                    $model->mamul = $postData['mamul'];

                    if(in_array($postData['transaction_type'],[2,3])){
                        $model->applicable_for = 1;
                        if(Yii::$app->user->identity->team == 4){
                            $model->status = 1;
                        }else{
                            $model->status = 0;
                        }
                    }else if(in_array($postData['transaction_type'],[4,5,33])){
                        $model->applicable_for = 2;
                    }
                    
                    $model->datetime = date('Y-m-d H:i:s');
                    $model->created_at = date('Y-m-d H:i:s');
                    $model->trip_id = $tripId;
                    $model->updated_at = date('Y-m-d H:i:s');
                    
                    if(empty($model->status)){
                        $model->status = 0;
                    }

                    $model->approval = 1;
                    $model->save(false);
            }else{
                $detension = "";
                if(!empty($postData['detention'])){
                        $detension = json_encode($postData['detention']);
                }
                if(!empty($postData['is_payable']) && !empty($postData['payable_amount'])){


                    $payableModel = new Transactions();
                    $payableModel->transaction_type = $postData['transaction_type'];
                    $payableModel->amount = $postData['payable_amount'];
                    $payableModel->note = $postData['payable_remark'];
                    $payableModel->receipt = $postData['receipt'];
                    $payableModel->applicable_for = 1;
                    $payableModel->datetime = date('Y-m-d H:i:s');
                    $payableModel->created_at = date('Y-m-d H:i:s');
                    $payableModel->trip_id = $tripId;
                    $payableModel->updated_at = date('Y-m-d H:i:s');
                    $payableModel->status = 0;
                    $payableModel->approval = 0;
                    $payableModel->detention = $detension;

                    // Approval pending on
                    $trip = Trip::findOne(['id'=>$tripId]);
                    if($trip->vendor_id==1){
                        $payableModel->assigned_to = 3;
                    }else{
                        $payableModel->assigned_to = 4;
                    }


                    $payableModel->save(false);
                }
                
                if(!empty($postData['is_receivable']) && !empty($postData['receivable_amount'])){
                    $receivableModel = new Transactions();
                    $receivableModel->transaction_type = $postData['transaction_type'];
                    $receivableModel->amount = $postData['receivable_amount'];
                    $receivableModel->note = $postData['receivable_remark'];
                    $receivableModel->applicable_for = 2;
                    $receivableModel->datetime = date('Y-m-d H:i:s');
                    $receivableModel->created_at = date('Y-m-d H:i:s');
                    $receivableModel->trip_id = $tripId;
                    $receivableModel->updated_at = date('Y-m-d H:i:s');
                    $receivableModel->status = 0;
                    $receivableModel->approval = 1;
                    $receivableModel->detention = $detension;

                    $receivableModel->save(false);
                }
            }
    }

    public function sendSmsToVendor($model)
    {
      if (!empty($this->send_sms_to_vendor) && !empty($model->vendor_id)) {
        $vendor = $model->vendorDetails;
        $vehicle = $model->vehicleDetails;
        $origin = $model->originDetails;
        $destination = $model->destinationDetails;

        $message = 'Hi Partner, '
            . 'amount of Rs '.$this->amount.' has been paid for '.$vehicle->registration_number.'  from '.$origin->name.' to '.$destination->name.' '
            . 'via txn Id:'.$this->transaction_id . " Thank you Middle Mile Pro";

        // $message ='For trip '.$model->number.', Rs.'.$this->amount.' has been paid on '.date('d-M-Y h:i A',strtotime($this->datetime)).'.';
        // If mamul dedution
        if (!empty($this->mamul_type) && !empty($this->mamul)) {
          $message .= \Yii::$app->params['mamul_type'][$this->mamul_type].' Rs.'.$this->mamul.' detected from the amount.';
        }
        $message .= ' Contact Partner Support at 7676955555 for any further clarification.';
        $mobile = $vendor->mobile;
        if (Yii::$app->commonFunction->sendSMS($mobile,$message)) {
          // var_dump($message);
          // die('working');
          return true;
        }
        // Yii::$app->session->setFlash('danger', 'Unable to send SMS.');
        return false;
      }
    }

    public static function tripTransactions($tripId)
    {
      $totalPayment = self::find()
                      ->select([
                        // Vendor details
                        'SUM(IF(transaction_type = '.self::PAID.' AND status='.self::STATUS_ACTIVE.' ,amount,0)) AS paid',
                        'SUM(IF(transaction_type = '.self::DEBIT.' AND status='.self::STATUS_ACTIVE.' ,amount,0)) AS debit',
                        'SUM(if(applicable_for = 1 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (6,7,8,9,10,11,12,13,14) ,amount,0))  AS vendor_ancillary_charges',
                          
                        'SUM(if(applicable_for = 1 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (15) ,amount,0))  AS vendor_commission',
                        'SUM(if(applicable_for = 1 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (16) ,amount,0))  AS vendor_mamul',
                        'SUM(if(applicable_for = 1 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (17) ,amount,0))  AS vendor_tds',
                        'SUM(if(applicable_for = 1 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (18) ,amount,0))  AS vendor_debit',
                        

                        // Customer details
                        'SUM(IF(transaction_type in ('.self::RECEIVED.',' . self::ADVANCE_RECEIVED . ') AND status='.self::STATUS_ACTIVE.',amount,0)) AS received',
                        'SUM(IF(transaction_type = '.self::WRITE_OFF.' AND status='.self::STATUS_ACTIVE.' ,amount,0)) AS writeOff',
                        'SUM(if(applicable_for = 2 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (6,7,8,9,10,11,12,13,14) ,amount,0))  AS customer_ancillary_charges',
                          
                        'SUM(if(applicable_for = 2 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (15) ,amount,0))  AS customer_commission',
                        'SUM(if(applicable_for = 2 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (16) ,amount,0))  AS customer_mamul',
                        'SUM(if(applicable_for = 2 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (17) ,amount,0))  AS customer_tds',
                        'SUM(if(applicable_for = 2 AND status='.self::STATUS_ACTIVE.' AND transaction_type in (18) ,amount,0))  AS customer_debit',

                     //   'SUM(IF(transaction_type = '.self::TRANSACTION_TYPE_RECEIVABLE.',amount,0)) AS receivable',
                      ])
                      ->where(['trip_id'=>$tripId,'status'=>self::STATUS_ACTIVE])
                      ->asArray()->one();

      //        var_dump($totalPayment);
      return $totalPayment;
    }

    public static function tripTransactionDetails($tripId,$transactionType = NULL,$params=[])
    {
      $query = self::find()->where(['trip_id'=>$tripId]);

      if (!empty($transactionType)) {
        $query = $query->andWhere(['transaction_type'=>$transactionType]);
      }
      
      if(isset($params['status'])){
                  $query = $query->andWhere(['status'=>$params['status']]);
      }

      if(isset($params['applicable_for'])){
                  $query = $query->andWhere(['applicable_for'=>$params['applicable_for']]);
      }

      if(isset($params['receipt'])){
                  $query = $query->andWhere(['receipt'=>$params['receipt']]);
      }


      return $query->orderBy('datetime DESC');
    }

    public function getTrip()
    {
        return $this->hasOne(Trip::className(), ['id' => 'trip_id']);
    }

}

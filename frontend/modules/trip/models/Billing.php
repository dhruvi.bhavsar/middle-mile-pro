<?php

namespace frontend\modules\trip\models;

use Yii;

/**
 * This is the model class for table "billing".
 *
 * @property int $id
 * @property string $name
 * @property int $company_id
 * @property string $date
 * @property int $status
 */
class Billing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'billing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'company_id', 'date'], 'required'],
            [['company_id', 'status'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],

            [['id','name', 'company_id', 'date','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'company_id' => 'Company ID',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }
}

<?php

namespace frontend\modules\trip\controllers;

use Yii;
use frontend\modules\trip\models\Trip;
use frontend\modules\trip\models\TripStops;
use frontend\modules\trip\models\TripDriver;
use frontend\modules\master\models\MasterStop;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\master\models\VehicleType;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use kartik\mpdf\Pdf;
use frontend\modules\customer\models\Customer;
use frontend\modules\customer\models\Contact;
use frontend\modules\master\models\City;
use frontend\modules\trip\models\TripSearch;
use frontend\modules\trip\models\TripDocuments;
use yii\web\UploadedFile;
use frontend\modules\trip\models\Transactions;
use frontend\modules\trip\models\Billing;
/**
 * ManageController implements the CRUD actions for Trip model.
 */
class ManageController extends Controller
{
    public $model_path = 'frontend\modules\trip\models\Trip';
    public $customer_model_path = 'frontend\modules\customer\models\Customer';
    public $contact_model_path = 'frontend\modules\customer\models\Contact';
    public $stop_model_path = 'frontend\modules\master\models\MasterStop';
    public $vendor_model_path = 'frontend\modules\vendor\models\Vendor';
    public $vehicle_model_path = 'frontend\modules\vendor\models\Vehicle';
    public $trip_driver_model_path = 'frontend\modules\trip\models\TripDriver';

    public function beforeAction($action)
    {
        if($action->id == 'sendquotationsms'){$this->enableCsrfValidation = false;}
        if($action->id == 'sendtaxinvoice'){$this->enableCsrfValidation = false;}
        if($action->id == 'vendor-bids'){$this->enableCsrfValidation = false;}
        if($action->id == 'sendinvoice'){$this->enableCsrfValidation = false;}
        if($action->id == 'index'){Yii::$app->commonFunction->accessToAction('tripRequest');}
        if($action->id == 'view'){Yii::$app->commonFunction->accessToAction('tripRequest');}
        if($action->id == 'create'){Yii::$app->commonFunction->accessToAction('createTrip');}
        if($action->id == 'update'){Yii::$app->commonFunction->accessToAction('updateTrip');}
        if($action->id == 'delete'){Yii::$app->commonFunction->accessToAction('deleteTrip');}
        if($action->id == 'assignvendor'){Yii::$app->commonFunction->accessToAction('assignVendor');}
        if($action->id == 'vendorconfirm'){Yii::$app->commonFunction->accessToAction('vendorConfirm');}
        if($action->id == 'customerconfirm'){Yii::$app->commonFunction->accessToAction('customerConfirm');}
        if($action->id == 'download-trip'){$this->enableCsrfValidation = false;}
        if($action->id == 'add-remarks'){$this->enableCsrfValidation = false;}

        return parent::beforeAction($action);
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => ['viewbookingslip'],
                        'allow' => true,
                        'roles' => ['?','@'],
                    ],
                     [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-transaction' => ['POST'],
                    'sendquotationsms'=>['POST'],
                    'vendor-bids'=>['POST'],
                    'download-trip'=>['POST'],
                    'add-remarks'=>['POST'],
                    'checkduplicate'=>['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TripSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $model = new SendInvoice();
        //
        // if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        //       Yii::$app->response->format = Response::FORMAT_JSON;
        //       return ActiveForm::validate($model);
        // }
        // if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        //       $model->sendInvoice();
        //       return $this->refresh();
        // }

        return $this->render('index', [
            'searchModel'=>$searchModel,
            'dataProvider' => $dataProvider,
            //'model' => $model,
        ]);
    }

    /**
     * Displays a single Trip model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $team = Yii::$app->user->identity->team;
        $model = $this->findModel($id);
        $this->view->params['backUrl']='index';
        $status = $model->status;
        $driverList = TripDriver::tripDriverList($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $reAssignModel = new Trip(['scenario'=>'reassignRequest']);
        $addRemarks = new Trip(['scenario'=>'addRemarks']);
        // if($team == 2 && $status == 1){
        //     return $this->redirect(['/trip/manage/assignvendor','id'=>$id]);
        // }else if($team == 1 && $status == 3){
        //     return $this->redirect(['/trip/manage/customerconfirm','id'=>$id]);
        // }else if($team == 2 && $status == 4){
        //     return $this->redirect(['/trip/manage/vendorconfirm','id'=>$id]);
        // }
        // echo "<pre>"; print_r($model); die();
        $modelDocumentUpload = TripDocuments::findOne(['trip_id'=>$model->id]);
        if (empty($modelDocumentUpload)) {
          $modelDocumentUpload = new TripDocuments();
          $modelDocumentUpload->trip_id = $model->id;
          $modelDocumentUpload->created_at = date('Y-m-d H:i:s');
          $modelDocumentUpload->updated_at = date('Y-m-d H:i:s');
          $modelDocumentUpload->status = 1;
        }
        if ($modelDocumentUpload->load(Yii::$app->request->post())) {
          $modelDocumentUpload->document = UploadedFile::getInstance($modelDocumentUpload, 'document');
          if ($modelDocumentUpload->validate()) {
            $modelDocumentUpload->uploadDocument();
            $modelDocumentUpload->save(false);
            $this->refresh();
          }
        }

        $modelTransactions = new Transactions();
        if ($modelTransactions->load(Yii::$app->request->post()) && !empty($modelTransactions->id)) {
          $modelTransactions = Transactions::findOne($modelTransactions->id);
          $modelTransactions->load(Yii::$app->request->post());
          $modelTransactions->save(false);
        }else if($modelTransactions->load(Yii::$app->request->post())){
                $modelTransactions->transactionEntry(Yii::$app->request->post(),$model->id);
                $this->refresh();
        }
        
        $totalTransaction = Transactions::tripTransactions($model->id);
        $dataTransactions = new ActiveDataProvider([
            'query' => Transactions::tripTransactionDetails($model->id),
            'pagination' => [ 'pageSize' => 25, ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'stopsModel' => $stopsModel,
            'driverList' => $driverList,
            'reAssignModel' => $reAssignModel,
            'addRemarks' => $addRemarks,
            'modelDocumentUpload'=>$modelDocumentUpload,
            'modelTransactions'=>$modelTransactions,
            'dataTransactions'=>$dataTransactions,
            'totalTransaction'=>$totalTransaction,
        ]);
    }

    /**
     * Creates a new Trip model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trip(['scenario'=>'createTrip']);
        $model->trip_type = 1;
        $stopsModel = new TripStops();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $postData = Yii::$app->request->post();

            $company = $model->getCompanyId($this);
            $contact = $model->getContactId();

            // Assigned to members
            $model->assigned_to = json_encode($model->assigned_to,true);

            // Validate Company' id and contact's company id
            if ($company->id == $contact->customer_id) {
              $model->trip_date = date('Y-m-d H:i:s',strtotime($model->trip_date));
              $model->created_at = date('Y-m-d H:i:s');
              $model->getOriginId();
              $model->getDestinationId();
              if($model->save()){
                  Yii::$app->activity->add($model->id,$this->model_path,'New Enquiry Created');
              }

              // Check for multistop trip
              if(isset($postData['intermittentStop']) && !empty($postData['intermittentStop'])){
                  $model->trip_type = 2;
                  $model->saveTripStops($model->id,$postData);
              }
              
              if (empty($model->number)) {
                $model->number = "RN::".date('dmY::Hi::').str_pad($model->id,4,"0",STR_PAD_LEFT);
              }

              $model->save(false);

              // notification entries
              $assignedTo = json_decode($model->assigned_to,true);
            if(!empty($assignedTo)){
                  foreach ($assignedTo as $assignee){
                      $message = Trip::tripNotificationText($model->id) . " Please confirm vehicle availability and rates";
                      $url = "/trip/manage/view?id=" . $model->id;
                      $title = "New Indent Received";
                      \frontend\modules\notification\models\Notifications::newEntry($assignee,$title,$message, $url);
                  }
              }

              return $this->redirect(['/trip/manage/view','id'=>$model->id]);
            } else {
              $model->contact_person_id = NULL;
              $model->validate();
            }
        }
        return $this->render('create', [
            'model' => $model,
            'stopsModel' => $stopsModel,
        ]);
    }

    /**
     * Updates an existing Trip model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'createTrip';
        $stopsModel = new TripStops(['scenario'=>'createTrip']);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $postData = Yii::$app->request->post();
            $model->trip_date = date('Y-m-d H:i:s',strtotime($model->trip_date));
            $company = $model->getCompanyId($this);
            $contact = $model->getContactId();
            // Validate Company' id and contact's company id
            if ($company->id == $contact->customer_id) {
              $model->getOriginId();
              $model->getDestinationId();
              if($model->save(false)){
                Yii::$app->activity->add($model->id,$this->model_path,'Enquiry Updated');
              }
              TripStops::deleteAll(['trip_id'=>$model->id]);
              $model->saveTripStops($this,$postData);
              return $this->redirect(['/trip/manage/view','id'=>$model->id]);
            } else {
             $model->contact_person_id = NULL;
             $model->validate();
           }
        }
        $model->trip_date = date('d-m-Y',strtotime($model->trip_date));
        return $this->render('update', [
            'model' => $model,
            'stopsModel' => $stopsModel,
        ]);
    }

    /**
     * Deletes an existing Trip model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Trip::STATUS_DELETED;
        if($model->save(false)){
            Yii::$app->activity->add($id,$this->model_path,'Enquiry Closed');
        }
        return $this->redirect(['index']);
    }

    public function actionCloseorder($id){
        // $model = $this->findModel($id);
        // $driverList = TripDriver::tripDriverList($id);
        // $stopsModel = TripStops::tripStopListByName($id);
        // if(!empty($model)){
        //     return $this->render('close_order',[
        //     'model' => $model,
        //     'stopsModel' => $stopsModel,
        //     'driverList' => $driverList,
        //     ]);
        // }else{

        // }

        $this->view->params['backUrl']='view?id='.$id;
        $team = Yii::$app->user->identity->team;
        $newModel = new Trip(['scenario'=>'cancelOrder']);
        $model = $this->findModel($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $vendors = Vendor::vendorSuggestions($model);
        $driverList = TripDriver::tripDriverList($id);
        $vehicle = new Vehicle();

        if ($newModel->load(Yii::$app->request->post())){
            $model->cancellation = $newModel->cancellation;
            $model->status = Trip::STATUS_DELETED;
            if($model->update(false)){
            Yii::$app->activity->add($model->id,$this->model_path,'Enquiry Closed');
            }
            // echo "<pre>";
            // print_r($model); die();
            Yii::$app->session->setFlash('success', 'Order Closed successfully.');
            return $this->redirect(['/trip/manage/index',]);
        }else{
            return $this->render('close_order', [
                'newModel' => $newModel,
                'model' => $model,
                'stopsModel' => $stopsModel,
                'vendors' => $vendors,
                'driverList' => $driverList,
                'vehicle' => $vehicle,
            ]);
        }

    }

    /**
     * Finds the Trip model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trip the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /* vendor assignment */
    public function actionAssignvendor($id){
        $team = Yii::$app->user->identity->team;
        $newModel = new Trip(['scenario'=>'vendorAssignment']);
        $this->view->params['backUrl']='view?id='.$id;
        $model = $this->findModel($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $vendors = Vendor::vendorSuggestions($model);

        if ($newModel->load(Yii::$app->request->post())){
            $vendorModel = Vendor::findOne(['id'=>$newModel->vendor_id]);
            if(empty($vendorModel)){
                $vendorModel = new Vendor();
                $vendorModel->name = $newModel->vendor_id;
                $vendorModel->intended_name = $newModel->vendor_id;
                $vendorModel->type = 1;
                $vendorModel->category = 1;
                $vendorModel->status = 1;
                if($vendorModel->save(false)){
            Yii::$app->activity->add($vendorModel->id,$this->vendor_model_path,'Vendor Created');
                }
            }
            $model->vendor_id = $vendorModel->id;
            $model->buying_rate = $newModel->buying_rate;
            $model->buying_rate_incl_handling_charges = $newModel->buying_rate_incl_handling_charges;
            if($model->status <= Trip::STATUS_CUSTOMER_CONFIRM_PENDING){
                $model->status = Trip::STATUS_CUSTOMER_CONFIRM_PENDING;
            }
            if($model->validate()){
                if($model->update(false)){
                Yii::$app->activity->add($model->id,$this->model_path,'Vendor Assigned');
                Yii::$app->session->setFlash('success', 'Vendor assigned successfully.');
                }
                return $this->redirect(['/trip/manage/view','id'=>$id]);
            }
            // else{
            //     \Yii::$app->response->format = Response::FORMAT_JSON;
            //     return ActiveForm::validate($model);
            //     die();
            // }
        }else{
            // echo "<pre>";
            // print_r($newModel); die();
            return $this->render('vendorAssignment', [
                'newModel' => $newModel,
                'model' => $model,
                'stopsModel' => $stopsModel,
                'vendors' => $vendors,
            ]);
        }
    }

    public function actionCustomerconfirm($id){
        $team = Yii::$app->user->identity->team;
        $newModel = new Trip(['scenario'=>'customerConfirmation']);
        $model = $this->findModel($id);
        $this->view->params['backUrl']='view?id='.$id;
        $stopsModel = TripStops::tripStopListByName($id);

        if ($newModel->load(Yii::$app->request->post())){
            $model->selling_rate = $newModel->selling_rate;
            $model->advance_selling_charges = $newModel->advance_selling_charges;
            $model->origin_handling_charges = $newModel->origin_handling_charges;
            $model->destination_handling_charges = $newModel->destination_handling_charges;
            $model->status = Trip::STATUS_VENDOR_CONFIRM_PENDING;

            // Add Advance Amount Transactions
//            $transactions = new Transactions();
//            $transactions->trip_id = $model->id;
//            $transactions->amount = $newModel->advance_selling_charges;
//            $transactions->transaction_type = Transactions::TRANSACTION_TYPE_RECEIVED;
//            $transactions->datetime = date('Y-m-d H:i:s');
//            $transactions->created_at = date('Y-m-d H:i:s');
//            $transactions->updated_at = date('Y-m-d H:i:s');
//            $transactions->status = 1;
//            $transactions->save(false);
//

            if($model->validate()){
                if($model->update(false)){
                Yii::$app->activity->add($model->id,$this->model_path,'Customer Confirmed');
                Yii::$app->session->setFlash('success', 'Customer confirmation successfully done.');
                }
                return $this->redirect(['/trip/manage/view','id'=>$id]);
            }
        }else{
            return $this->render('customerConfirmation', [
                'newModel' => $newModel,
                'model' => $model,
                'stopsModel' => $stopsModel,
            ]);
        }
    }

    public function actionVendorconfirm($id){
        $team = Yii::$app->user->identity->team;
        $newModel = new Trip(['scenario'=>'vendorConfirmation']);
        $this->view->params['backUrl']='view?id='.$id;
        $model = $this->findModel($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $vendors = Vendor::vendorSuggestions($model);
        // $driverModel = new TripDriver();
        $driverModel = TripDriver::findOne(['trip_id'=>$id,'status'=>1]);
        if(empty($driverModel)){
            $driverModel = new TripDriver();
        }
        $vehicle = new Vehicle();

        if ($newModel->load(Yii::$app->request->post()) && $driverModel->load(Yii::$app->request->post())){

            $vehicleModel = Vehicle::findOne(['id'=>$newModel->vehicle_id]);
            if(empty($vehicleModel)){
                $vehicleModel = new Vehicle();
                $vehicleModel->registration_number = $newModel->vehicle_id;
                $vehicleModel->type_id = $model['vehicle_type_id'];
                $vehicleModel->vendor_id = $model['vendor_id'];
                if($vehicleModel->save(false)){
                  Yii::$app->activity->add($vehicleModel->id,$this->vehicle_model_path,'Vehicle Added');
                }
            }
            $model->vehicle_id = $vehicleModel->id;
            $model->advance_buying_charges = $newModel->advance_buying_charges;
            $model->status = Trip::STATUS_ORDER_CONFIRMED;
            

            // Update vehicle assigned event for tracking
            $model->tracking_status = 0;
            $data[] = [
                            'remark'=>'Assigned',
                            'location'=>"",
                            'datetime'=>$model->trip_date,
                            'trip_event'=>0,
                            'km_reading'=>"",
                            'user'=>\Yii::$app->user->identity->id,
                            'on'=>date('Y-m-d H:i:s'),
                          ];
            $model->remarks = json_encode($data,true);


            $driverModel->trip_id = $id;
            if($model->validate() && $driverModel->validate()){

//                if (!empty($newModel->advance_buying_charges)) {
//                  $transactions = new Transactions();
//                  $transactions->trip_id = $model->id;
//                  $transactions->amount = $newModel->advance_buying_charges;
//                  $transactions->transaction_type = Transactions::TRANSACTION_TYPE_PAID;
//                  $transactions->datetime = date('Y-m-d H:i:s');
//                  $transactions->created_at = date('Y-m-d H:i:s');
//                  $transactions->updated_at = date('Y-m-d H:i:s');
//                  $transactions->status = 1;
//                  $transactions->save(false);
//
//                  $vendor = $model->vendorDetails;
//                  if (!empty($vendor->mobile)) {
//                    $vendorName = empty($vendor->intended_name)?$vendor->name:$vendor->intended_name;
//                    $vehicle = $model->vehicleDetails;
//                    // Ref No. - 1198974012920999936 Nov 25,2019 18:30 Trip start Time -  '.$model->trip_date.' RTA - Required time of arrival - Nov 30,2019 18:30     RTA - Required time of arrival - Nov 30,2019 18:30
//                    $message = 'Partner Name - '.$vendorName.'  ADVANCE payment info  Amount - '.$transactions->amount.'   Trip Details  Vehicle No - '.$vehicle->registration_number.'  From : '.$model->loading_point.' to '.$model->unloading_point.'  Trip Date -  '.date('M d Y'.strtotime($model->trip_date)).' Please reach as per RTA to avoid deductions. Team MMP LLP 🙏 🙏';
//                    $vendor->mobile;
//                    Yii::$app->commonFunction->sendSMS($vendor->mobile,$message);
//                  }
//                }

                if($model->update(false)){
                    Yii::$app->activity->add($model->id,$this->model_path,'Vendor Confirmed');
                }
                if($driverModel->processData(Yii::$app->request->post())){
                    Yii::$app->session->setFlash('success', 'Vendor confirmation successfully done.');
                    return $this->redirect(['/trip/manage/bookingslip','id'=>$id]);
                }
            }
        }else{
            return $this->render('vendorConfirmation', [
                'newModel' => $newModel,
                'model' => $model,
                'stopsModel' => $stopsModel,
                'vendors' => $vendors,
                'driverModel' => $driverModel,
                'vehicle' => $vehicle,
            ]);
        }
    }

    public function actionBookingslip($id){
        $team = Yii::$app->user->identity->team;
        $model = $this->findModel($id);
        $this->view->params['backUrl']='view?id='.$id;
        $status = $model->status;
        $driverList = TripDriver::tripDriverList($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $sendInvoiceModel = new Trip(['scenario'=>'sendInvoice']);

        return $this->render('payslip', [
            'model' => $model,
            'stopsModel' => $stopsModel,
            'driverList' => $driverList,
            'sendInvoiceModel' => $sendInvoiceModel,
        ]);
    }

    public function actionSendbookingslip(){
        $postData = Yii::$app->request->post();

        $model = Trip::find()->where(['md5(id)'=>$postData['tripid']])->one();
        $sendModel = new Trip(['scenario'=>'sendInvoice']);
        $sendModel->recipients = $postData['Trip']['recipients'];

        if($sendModel->validate()){
            $arr = [];
            $emailList = explode(",",$sendModel->recipients);;
            foreach($emailList as $eml){ array_push($arr,$eml);}
            $path = \Yii::$app->mailer->setViewPath("@common/mail");
            $sendInvoice = \Yii::$app->mailer->compose(['html' => $path.'booking-slip-html'], ['id' => $postData['tripid']])
                ->setFrom(\Yii::$app->params['adminEmail'])
                ->setTo($arr)
                ->setSubject('Booking Slip');
            if($sendInvoice->send()){
                Yii::$app->activity->add($postData['tripid'],$this->model_path,'Booking Slip Sent');
                return 1; }
            else{ return 0; }
        }else{
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($sendModel);
            die();
        }
    }

    public function actionViewbookingslip($id) {
            // $model = $this->findModel($id);
            $model = Trip::find()->where(['md5(id)'=>$id])->one();
            if ( $model == null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $this->view->params['backUrl']='view?id='.$id;
            // var_dump($model->id); die();

            $driverList = TripDriver::tripDriverListforReport($id);
            $stopsModel = TripStops::tripStopListByNameforReport($id);
        $content = $this->renderPartial('booking_slip_pdf_view', [
                'model' => $model,
                'stopsModel' => $stopsModel,
                'driverList' => $driverList,
            ]);

        $filename = Yii::getAlias('@frontend/web/invoice/'.$model->number.'.pdf');

        // if (file_exists($filename)) {
        //    return false;
        // }

        // $pdf = new Pdf([
        //     'mode' => Pdf::MODE_CORE,
        //     'format' => Pdf::FORMAT_A4,
        //     'orientation' => Pdf::ORIENT_PORTRAIT,
        //     'filename' => $filename,
        //     'destination' => Pdf::DEST_FILE,
        //     'content' => $content,
        //     'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        //     'cssInline' => '.kv-heading-1{font-size:18px}',
        //         'options' => ['title' => 'MiddleMilePro Invoice'],
        //     'methods' => [
        //         'SetHeader'=>['MiddleMilePro'],
        //         'SetFooter'=>['{PAGENO}'],
        //     ]
        // ]);
        // $pdf->render();


        $pdfView = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:12px}}',
            'options' => ['title' => 'MiddleMilePro Invoice'],
            'methods' => [
                // 'SetHeader'=>['MiddleMilePro'],
                // 'SetFooter'=>['{PAGENO}'],
            ],
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        return $pdfView->render();

    }

    public function actionTaxinvoice($id){
        $team = Yii::$app->user->identity->team;
        $model = $this->findModel($id);
        $status = $model->status;
        $driverList = TripDriver::tripDriverList($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $sendInvoiceModel = new Trip(['scenario'=>'sendInvoice']);

        return $this->render('taxinvoice', [
            'model' => $model,
            'stopsModel' => $stopsModel,
            'driverList' => $driverList,
            'sendInvoiceModel' => $sendInvoiceModel,
        ]);
    }

    public function actionSendtaxinvoice(){
        $postData = Yii::$app->request->post();

        $model = Trip::find()->where(['md5(id)'=>$postData['tripid']])->one();
        $sendModel = new Trip(['scenario'=>'sendInvoice']);
        $sendModel->recipients = $postData['Trip']['recipients'];

        if($sendModel->validate()){
            $arr = [];
            $emailList = explode(",",$sendModel->recipients);;
            foreach($emailList as $eml){ array_push($arr,$eml);}
            $path = \Yii::$app->mailer->setViewPath("@common/mail");
            $sendInvoice = \Yii::$app->mailer->compose(['html' => $path.'customer-tax-invoice-html'], ['id' => $postData['tripid']])
                ->setFrom(\Yii::$app->params['adminEmail'])
                ->setTo($arr)
                ->setSubject('Tax Invoice');
              //  var_dump($sendInvoice->send()); die();
            if($sendInvoice->send()){
            Yii::$app->activity->add($postData['tripid'],$this->model_path,'Tax Invoice Sent');
                return 1; }
            else{ return 0; }
        }else{
            \Yii::$app->response->format = Response::FORMAT_JSON;
            var_dump($sendModel->errors);
            return ActiveForm::validate($sendModel);
            die();
        }
    }

    public function actionViewtaxinvoice($id) {
        $model = Trip::find()->where(['md5(id)'=>$id])->one();
        if ( $model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $driverList = TripDriver::tripDriverListforReport($id);
        $stopsModel = TripStops::tripStopListByNameforReport($id);
        // return $this->render('tax_invoice_pdf_view', ['model' => $model,'stopsModel' => $stopsModel,'driverList' => $driverList,]);

        $content = $this->renderPartial('tax_invoice_pdf_view', [
                'model' => $model,
                'stopsModel' => $stopsModel,
                'driverList' => $driverList,
            ]);

        $filename = Yii::getAlias('@frontend/web/invoice/'.$model->number.'.pdf');

        $pdfView = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:12px}}',
            'options' => ['title' => 'MiddleMilePro Invoice'],
            'marginTop' => 5,
            'marginBottom' => 0,
        ]);
        return $pdfView->render();

    }

    public function actionViewinvoice($id=[],$billing=NULL) {

        if (empty($billing)) {
          if (!is_array($id)) {$id = explode(',', $id);}
          $model = Trip::find()->where(['md5(id)'=>$id])->all();
          if (empty($model)) {
              throw new NotFoundHttpException('The requested page does not exist.');
          }
          $this->view->params['backUrl']= Yii::$app->request->referrer;
          $isNewBill = Trip::find()->andWhere(['md5(id)'=>$id])->andWhere(['billing_id'=>NULL])->count();
          if (!empty($isNewBill)) {
            $content = $this->renderPartial('invoice_pdf_view', [
                    'model' => $model,
                ]);
            $file = strtotime('now').'.pdf';
            $filename = Yii::getAlias('@frontend/web/invoice/'.$file);

            $pdfView = new Pdf([
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'destination' => Pdf::DEST_BROWSER,
                'content' => $content,
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                'cssInline' => '
                                    .kv-heading-1{font-size:12px}}
                                    @media all{
                                        .font_next{font-family:DoodlePen}table{border-collapse:collapse;width:100%}td{border:1px solid #000}.page-break {display: none;}
                                    }
                                    @media print{
                                        .page-break{display: block;page-break-before: always;}
                                    }
                              ',
                'options' => ['title' => 'MiddleMilePro Invoice'],
                'marginTop' => 5,
                'marginBottom' => 0,
                'destination'=>'F',
                'filename'=>$filename,
            ]);
            $pdfView->render();
            // Save Bill
            $modelBilling = new Billing();
            $modelBilling->name = $file;
            $modelBilling->company_id = $model[0]->company_id;
            $modelBilling->date = date('Y-m-d');
            $modelBilling->status = 1;
            $modelBilling->save();

            foreach ($model as $trip) {
              $modelTrip = Trip::findOne($trip->id);
              $modelTrip->billing_id = $modelBilling->id;
              $modelTrip->save();
            }
          } else {
            $modelBilling = Billing::findOne($model[0]->billing_id);
          }
        } else {
          $modelBilling = Billing::findOne($billing);
        }
        $modelCompany = Customer::findOne($modelBilling->company_id);

        $sendModel = new Trip(['scenario'=>'sendInvoice']);

        if ($sendModel->load(Yii::$app->request->post()) && $sendModel->validate()) {
              $arr = [];
              $emailList = explode(",",$sendModel->recipients);;
              foreach($emailList as $eml){ array_push($arr,$eml);}
              $path = \Yii::$app->mailer->setViewPath("@common/mail");
              $sendInvoice = \Yii::$app->mailer->compose(['html' => $path.'multiple-trip-invoice-html'], ['invoice' => $modelBilling->name])
                  ->setFrom(\Yii::$app->params['adminEmail'])
                  ->setTo($arr)
                  ->setSubject('Invoice');
              if($sendInvoice->send()){
                  //Yii::$app->session->setFlash('success', 'Invoice sent successfully.');
                  //Yii::$app->activity->add($postData['tripid'],$this->model_path,'Order Slip Sent');
                  return 1;
                }
              else{ return 0; }
        }

        return $this->render('view_billing', [
            'model' => $modelBilling,
            'modelCompany' => $modelCompany,
            'sendInvoiceModel'=>$sendModel,
        ]);
    }

    public function actionOrderslip($id){
        $team = Yii::$app->user->identity->team;
        $model = $this->findModel($id);
        $this->view->params['backUrl']='view?id='.$id;
        $status = $model->status;
        $driverList = TripDriver::tripDriverList($id);
        $stopsModel = TripStops::tripStopListByName($id);
        $sendInvoiceModel = new Trip(['scenario'=>'sendInvoice']);

        return $this->render('orderslip', [
            'model' => $model,
            'stopsModel' => $stopsModel,
            'driverList' => $driverList,
            'sendInvoiceModel' => $sendInvoiceModel,
        ]);
    }

    public function actionSendorderslip(){
        $postData = Yii::$app->request->post();
        $this->view->params['backUrl']='view?id='.$id;
        $model = Trip::find()->where(['md5(id)'=>$postData['tripid']])->one();
        $sendModel = new Trip(['scenario'=>'sendInvoice']);
        $sendModel->recipients = $postData['Trip']['recipients'];

        if($sendModel->validate()){
            $arr = array();
            $emailList = explode(",",$sendModel->recipients);;
            foreach($emailList as $eml){ array_push($arr,$eml);}
            $path = \Yii::$app->mailer->setViewPath("@common/mail");
            $sendInvoice = \Yii::$app->mailer->compose(['html' => $path.'order-slip-html'], ['id' => $postData['tripid']])
                ->setFrom(\Yii::$app->params['adminEmail'])
                ->setTo($arr)
                ->setSubject('Order Slip');
            if($sendInvoice->send()){
                Yii::$app->activity->add($postData['tripid'],$this->model_path,'Order Slip Sent');
                return 1; }
            else{ return 0; }
        }else{
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($sendModel);
            die();
        }
    }

    public function actionVieworderslip($id) {
            $model= Trip::find()->where(['md5(id)'=>$id])->one();
            if ( $model == null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $this->view->params['backUrl']='view?id='.$id;
            $driverList = TripDriver::tripDriverListforReport($id);
            $stopsModel = TripStops::tripStopListByNameforReport($id);
        $content = $this->renderPartial('order_slip_pdf_view', [
                'model' => $model,
                'stopsModel' => $stopsModel,
                'driverList' => $driverList,
            ]);

        $filename = Yii::getAlias('@frontend/web/invoice/'.$model->number.'.pdf');
        $pdfView = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:12px}}',
            'options' => ['title' => 'MiddleMilePro Invoice'],
            'methods' => [
                // 'SetHeader'=>['MiddleMilePro'],
                // 'SetFooter'=>['{PAGENO}'],
            ],
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        return $pdfView->render();
    }

    public function actionVendorReAssign($id){
        $model = $this->findModel($id);
        $postData = Yii::$app->request->post();
        $reAssignModel = new Trip(['scenario'=>'reassignRequest']);

        if($reAssignModel->load($postData) && $reAssignModel->validate()){
            $model->negotiation_remark = $reAssignModel->negotiation_remark;
            $model->status = Trip::STATUS_VENDOR_RE_ASSIGN;
            // Drop vehicle and driver vehicle_id
            $model->vehicle_id = NULL;
            $modelTripDriver = TripDriver::findOne(['trip_id'=>$model->id]);
            if (!empty($modelTripDriver)) {
              $modelTripDriver->status=2;
              $modelTripDriver->save();
            }
            if($model->update(false)){
                Yii::$app->activity->add($model->id,$this->model_path,'Vendor Re Assigned');
                $this->redirect(['view','id'=>$id]);
            }
        }
    }
    public function actionSendquotationsms(){
      $postData = Yii::$app->request->post();
      if (Yii::$app->commonFunction->sendSMS($postData["mobile"],$postData["message"])) {
        return true;
      } else {
        return false;
      }
    }

    public function actionVendorBids()
    {
        $postData = Yii::$app->request->post();
        if (!isset($postData['id'])) { return false; }
        $model = Trip::findOne($postData['id']);
        $bids = json_decode($model->vendor_bids,true);
        if (empty($bids)) {
          $bids = [];
        }
        $bids[$postData['vendor_id']]=$postData['bid_amount'];
        $model->vendor_bids = json_encode($bids,true);
        $model->save();
    }

    public function actionDownloadTrip($params = NULL,$pdf = NULL)
    {
      $header = ['Month','Sales Person Name','Enquiry / Indent','Enquiry / Indent No','Enquiry / Indent Date','Customer Name','Zone','Origin location','Destination location','Loading Point 1','Loading Point 2','Unloading point 1','Unloading point 2','Intended Trip Date','Vehicle Type','Load Chassie Type','Container Height','Other Requitments','Traffic Executive Name','Vendor Code','Vedor Name','Truck Reg No','Buying Price','Vendor Colelction memo Amount','Driver Mob No','Placement Status','Placement remark','Origin Reporting Date & Time','Loading Entry Date & Time','Departure Date & Time','LR /THC/VHC No','e-way bill number','eway Bill Validity Date','Origin Detention Days','Consignee contact name & number','Selling Freight','Advance Mamul deducted','Advance Payable','Advance  Paymnt Request Date','Advance Paid Amount','Advance  Payment Date','Destination  Arrival date & Time','Unloading Completion Date & Time','Destination  Detention Days','POD Receipt Date','Forwarded for Invoicing Date','Finance confirmation POD receipt','Invoices forwarded to billing not billed','Remarks','Balance Freight To be Paid to Vendor','Loading Charges','Remarks- Loading Charges','Unloading Charges','Remarks- Unloading Charges','Origin Detention Charges','Remarks- Origin Detention Charges','Destination Detention Charges','Remarks- Destination Detention Charges','Toll Charges','Remarks- Toll Charges','Extra Distance Charges','Remarks- Extra Distance Charges','Overlaod Charges','Remarks- Overlaod Charges','Penalty Charges','Remarks- Penalty Charges','Other Charges','Remarks - Other Charges','Other Charges 2','Remarks - Other Charges 2','Other Charges 3','Remarks - Other Charges 3','Payment 2','Payment 2 Date','TDS','TDS deducted Month','Balance Mamul','Balance Payable to Vendor','Balance Paid','Balance Payment Date','Remarks - Balance Payment','Vendor Outstanding','Trip Exp Total','LR /THC/VHC Amount','Selling Rate variance','Discount','Transportation Charges','Total Detention Days','Per Day Detention Rate','Loading Charges','Unloading Charges','Detention Charges','Toll Charges','Extra Distance Charge','Overload Charge','Penalty charge','Other Charge -1','Other Charge -2','Other Charge -3','Total Invoice Value (without GST)','CGST','SGST','IGST','Invoice Amt','Advance Received','Invoice No','Invoice Date','Cust Invoice Min Value','Invoice amount higher than Customer invoice min value','Remarks','Inv Dispatch Date','Courier Awb No','Courier Name','Courier Delivery Date','Courier Received by','Inv Booking Date','Booking Reference  No','Booking Amount','Booking Date','Credit Period','Payment Due Date','Customer Payment - 1','Details ( Bank/Driver/Diesl)','TDS','Date','Customer Payment - 2','Details ( Bank/Driver/Diesl)','TDS','Date','Customer Payment - 3','Details ( Bank/Driver/Diesl)','TDS','Date','Deduction','Remark - Deduction 1','Deduction 2','Remark - Deduction 2','Payment Remarks','Customer Outstanding','Provision for bad debt','Bad Debts','Collection Executive Name'];
      if (!empty($params))
      {
        $params = json_decode($params,true);
      }

      $searchModel = new TripSearch();
      $data = $searchModel->search($params,1)->orderBy(['T.id'=>SORT_ASC])->all();
      $content= self::generateTripCsvContent($data);
      if (empty($pdf)) {
        $file = \Yii::$app->commonFunction->exportCSV($header, $content,NULL,'InvSupp_Master.csv');
        return Yii::$app->response->sendFile($file);
      } else {
        $contentHtml = $this->renderPartial('@frontend/views/layouts/generate_html_report_pdf', [
                      'header' => $header,
                      'body'=> $content,
                      'footer'=> NULL,
                  ]);

        $filename = Yii::getAlias('@frontend/web/invoice/'.date("Ymd").'_InvSupp_Master'.'.pdf');

        $pdfView = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LETTER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $contentHtml,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:12px}}',
            'options' => ['title' => 'MiddleMilePro Invoice'],
            'methods' => [
                // 'SetHeader'=>['MiddleMilePro'],
                // 'SetFooter'=>['{PAGENO}'],
            ],
            'marginTop' => 5,
            'marginBottom' => 5,
        ]);
        return $pdfView->render();
      }

    }

    private static function generateTripCsvContent($data)
    {
      $content = [];
      foreach ($data as $key => $model)
      {
        $count = 0;
        $vendor = Vendor::findOne($model->vendor_id);
        /*
        use frontend\modules\vendor\models\Vehicle;
        use frontend\modules\master\models\VehicleType;
        */
        $vehicle = Vehicle::findOne($model->vehicle_id);
        $vehicleType = VehicleType::findOne($model->vehicle_type_id);
        $vehicleDetails=[''];
        if (!empty($vehicleType)) {
          $vehicleDetails = explode('FT', $vehicleType->name);
        }

        $content []=[
          date('M/y',strtotime($model->created_at)),/*'Month',*/
          '',/*'Sales Person Name',*/
          '',/*'Enquiry / Indent',*/
          $model->number,/*'Enquiry / Indent No',*/
          date('d/M/y',strtotime($model->created_at)),/*'Enquiry / Indent Date',*/
          $model->custName, //'Customer Name',
          '', // 'Zone',
          $model->origin_name, //'Origin location',
          $model->destination_name, //'Destination location',
          $model->loading_point, //'Loading Point 1',
          '', //'Loading Point 2',
          $model->unloading_point, //'Unloading point 1',
          '', //'Unloading point 2',
          date('d/M/y',strtotime($model->trip_date)), //'Intended Trip Date',
          $vehicleDetails[0]."'", //'Vehicle Type',
          (isset($vehicleDetails[1])?$vehicleDetails[1]:''), //'Load Chassie Type',
          '', //'Container Height',
          '', //'Other Requitments',
          '', //'Traffic Executive Name',
          (!empty($vendor)?$vendor->code:''), //'Vendor Code',
          (!empty($vendor)?(!empty($vendor->intended_name)?$vendor->intended_name:$vendor->name):''), //'Vedor Name',
          (!empty($vehicle)?$vehicle->registration_number:''), //'Truck Reg No',
          $model->buying_rate, //'Buying Price',
          '', //'Vendor Colelction memo Amount',
          '', //'Driver Mob No',
          '', //'Placement Status',
          '', //'PLACEMENT REMARK'
          '', //'Origin Reporting Date & Time'
          '', //,'Loading Entry Date & Time'
          '', //,'Departure Date & Time'
          '', //,'LR /THC/VHC No'
          '', //,'e-way bill number'
          '', //,'eway Bill Validity Date'
          '', //,'Origin Detention Days'
          '', //,'CONSIGNEE CONTACT NAME & NUMBER'
          $model->selling_rate, //,'Selling Freight'
          '', //,'Advance Mamul deducted'
          '', //,'Advance Payable'
          '', //,'Advance  Paymnt Request Date'
          '', //,'Advance Paid Amount'
          '', //,'Advance  Payment Date'
          '', //,'Destination  Arrival date & Time'
          '', //,'Unloading Completion Date & Time'
          '', //,'Destination  Detention Days'
          '', //,'POD Receipt Date'
          '', //,'Forwarded for Invoicing Date'
          '', //,'Finance confirmation POD receipt'
          '', //,'Invoices forwarded to billing not billed'
          '', //'Remarks'
          '', //,'Balance Freight To be Paid to Vendor'
          $model->origin_handling_charges, //,'Loading Charges'
          '', //,'Remarks- Loading Charges'
          $model->destination_handling_charges, //,'Unloading Charges'
          '', //,'Remarks- Unloading Charges',
          '',//'Origin Detention Charges',
          '',//'Remarks- Origin Detention Charges',
          '',//'Destination Detention Charges',
          '',//'Remarks- Destination Detention Charges',
          '',//'Toll Charges',
          '',//'Remarks- Toll Charges',
          '',//'Extra Distance Charges',
          '',//'Remarks- Extra Distance Charges',
          '',//'Overlaod Charges',
          '',//'Remarks- Overlaod Charges',
          '',//'Penalty Charges',
          '',//'Remarks- Penalty Charges',
          '',//'Other Charges',
          '',//'Remarks - Other Charges',
          '',//'Other Charges 2',
          '',//'Remarks - Other Charges 2',
          '',//'Other Charges 3',
          '',//'Remarks - Other Charges 3',
          '',//'Payment 2'
          '',//,'Payment 2 Date',
          '',//'TDS'
          '',//,'TDS deducted Month',
          '',//'Balance Mamul',
          '',//'Balance Payable to Vendor',
          '',//'Balance Paid',
          '',//'Balance Payment Date',
          '',//'Remarks - Balance Payment',
          '',//'Vendor Outstanding',
          '',//'Trip Exp Total',
          '',//'LR /THC/VHC Amount',
          '',//'Selling Rate variance',
          '',//'Discount',
          '',//'Transportation Charges',
          '',//'Total Detention Days',
          '',//'Per Day Detention Rate',
          '',//'Loading Charges',
          '',//'Unloading Charges',
          '',//'Detention Charges',
          '',//'Toll Charges',
          '',//'Extra Distance Charge',
          '',//'Overload Charge',
          '',//'Penalty charge',
          '',//'Other Charge -1',
          '',//'Other Charge -2',
          '',//'Other Charge -3',
          '',//'Total Invoice Value (without GST)',
          '',//'CGST',
          '',//'SGST',
          '',//'IGST',
          '',//'Invoice Amt',
          '',//'Advance Received',
          '',//'Invoice No',
          '',//'Invoice Date',
          '',//'Cust Invoice Min Value',
          '',//'Invoice amount higher than Customer invoice min value',
          '',//'Remarks'
          '',//,'Inv Dispatch Date',
          '',//'Courier Awb No'
          '',//,'Courier Name',
          '',//'Courier Delivery Date',
          '',//'Courier Received by',
          '',//'Inv Booking Date',
          '',//'Booking Reference  No',
          '',//'Booking Amount',
          '',//'Booking Date',
          '',//'Credit Period',
          '',//'Payment Due Date',
          '',//'Customer Payment - 1',
          '',//'Details ( Bank/Driver/Diesl)',
          '',//'TDS',
          '',//'Date',
          '',//'Customer Payment - 2',
          '',//'Details ( Bank/Driver/Diesl)',
          '',//'TDS'
          '',//,'Date',
          '',//'Customer Payment - 3',
          '',//'Details ( Bank/Driver/Diesl)',
          '',//'TDS',
          '',//'Date',
          '',//'Deduction',
          '',//'Remark - Deduction 1',
          '',//'Deduction 2',
          '',//'Remark - Deduction 2',
          '',//'Payment Remarks',
          '',//'Customer Outstanding',
          '',//'Provision for bad debt',
          '',//'Bad Debts',
          '',//'Collection Executive Name'
      ];
      }
      return $content;
    }

    public function actionAddRemarks()
    {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $postData = Yii::$app->request->post();
      $model = $this->findModel($postData['id']);
      //$postData = json_decode(\yii\helpers\HtmlPurifier::process(json_encode($postData,true)),true);
      if (isset($postData['file_name']) && !empty($postData['file_name'])) {
        // Document Upload
        $modelDocumentUpload = TripDocuments::findOne(['trip_id'=>$postData['id']]);
        if (empty($modelDocumentUpload)) {
          $modelDocumentUpload = new TripDocuments();
          $modelDocumentUpload->trip_id = $postData['id'];
          $modelDocumentUpload->created_at = date('Y-m-d H:i:s');
          $modelDocumentUpload->updated_at = date('Y-m-d H:i:s');
          $modelDocumentUpload->status = 1;
        }
        $modelDocumentUpload->date = (empty($postData['datetime'])?date('d-m-Y'):date('d-m-Y',strtotime($postData['datetime'])));
        $modelDocumentUpload->type = 6;
        //var_dump($postData['tripPodUpload']); die();
        $modelDocumentUpload->uploadDocument($postData['file_name'],$postData['file_base64']);
        $modelDocumentUpload->save(false);
      }

      $data = [
        'remark'=>$postData['remark'],
        'location'=>$postData['location'],
        'datetime'=>(empty($postData['datetime'])?date('d-m-Y h:i A'):date('d-m-Y  h:i A',strtotime($postData['datetime']))),
        'trip_event'=>$postData['trip_event'],
        'km_reading'=>$postData['km_reading'],
        'user'=>\Yii::$app->user->identity->id,
        'on'=>date('Y-m-d H:i:s'),
      ];

      if ($postData['trip_event'] == 7) {
        $model->unloaded_datetime = date('Y-m-d H:i:s',strtotime($data['datetime']));
        $model->status = Trip::STATUS_ORDER_DELIVERED;
      }

      // On Vehicle depart make entry in transactions table for advance pay to vendor
        if ($postData['trip_event'] == 3 && !empty($model->advance_buying_charges)) {
            $transaction = new Transactions();
            $transaction->amount = $model->advance_buying_charges;
            $transaction->applicable_for = 1;
            $transaction->transaction_type = 2;
            $transaction->datetime = date('Y-m-d H:i:s');
            $transaction->created_at = date('Y-m-d H:i:s');
            $transaction->trip_id = $model->id;
            $transaction->updated_at = date('Y-m-d H:i:s');
            $transaction->approval = 1;
            $transaction->status = 0;
            $transaction->save(false);
        }

      $model->tracking_status=$postData['trip_event'];
      if (empty($model->remarks)) {
        $model->remarks = json_encode([$data],true);
      } else {
        $remarks = json_decode($model->remarks,true);
        if ($postData['tracking_id'] == NULL) {
          array_push($remarks,$data);
        } else {
          $remarks[$postData['tracking_id']] = $data;
        }
        $model->remarks = json_encode($remarks,true);
      }

      // Update km reading to vehicle table
      $vehicle = Vehicle::findOne(['id'=>$model->vehicle_id]);
      $vehicle->meter_reading = $postData['km_reading'];
      $vehicle->update(false);

      $model->save();
      return $data;
    }

    public function actionDeleteTransaction($id)
    {
      $model = Transactions::findOne(['id'=>$id]);
      $model->status = Transactions::STATUS_DELETED ;
      $model->save();
      return $this->redirect(Yii::$app->request->referrer);
      // Transactions
    }

    public function actionApproveTransaction($id)
    {
      $model = Transactions::findOne(['id'=>$id]);
      $model->approval = Transactions::APPROVED ;
      $model->save();
      return $this->redirect(Yii::$app->request->referrer);
      // Transactions
    }

    public function actionRejectTransaction($id)
    {
      $model = Transactions::findOne(['id'=>$id]);
      $model->approval = Transactions::REJECTED ;
      $model->save();
      return $this->redirect(Yii::$app->request->referrer);
      // Transactions
    }

    public function actionConfirmTransaction($id)
    {
      $model = Transactions::findOne(['id'=>$id]);
      $model->status = Transactions::APPROVED ;
      $model->save();
      return $this->redirect(Yii::$app->request->referrer);
      // Transactions
    }

    
    public function actionPodReceived($trip_id,$status,$remark='-')
    {
        $trips = explode(",", $trip_id);

        foreach ($trips as $trip_id){
            $trip = Trip::findOne($trip_id);
            $data = [
              'remark'=>$remark,
              'location'=>'-',
              'datetime'=>date('d-m-Y h:i A'),
              'trip_event'=>$status,
              'km_reading'=>'',
              'user'=>\Yii::$app->user->identity->id,
              'on'=>date('Y-m-d H:i:s'),
            ];

            $trip->tracking_status=$status;
            if (empty($trip->remarks)) {
              $trip->remarks = json_encode([$data],true);
            } else {
                  $remarks = json_decode($trip->remarks,true);
                  array_push($remarks,$data);
              }
            $trip->remarks = json_encode($remarks,true);


            // make entry in trip pod's collection
            $model = new \frontend\modules\pod\models\TripPodCollection();
            $model->trip_id = $trip->id;
            $model->collected_by = \Yii::$app->user->identity->id;
            $model->created_on = date("Y-m-d H:i:s");
            $model->save(false);

            $trip->update(false);
        }

      Yii::$app->session->setFlash('success', Yii::$app->params['event_status'][$status]);
      return $this->redirect(Yii::$app->request->referrer);
      // Transactions
    }

    public function actionReceiptReceived($transaction_id,$remark=null)
    {

            $transaction = Transactions::findOne($transaction_id);
            
            $transaction->receipt = 1;
            if (empty($transaction->note)) {
              $transaction->note = $remark;
            } else {
                  $transaction->note = $transaction->note . " - " . $remark;
              }
            $transaction->update(false);
        
        Yii::$app->session->setFlash('success', 'Receipt marked as received successfull');
        return $this->redirect(Yii::$app->request->referrer);
      // Transactions
    }

    public function actionCheckduplicate(){
        $postData = Yii::$app->request->post();

        $tripCount = Trip::find()->where(['company_id'=>$postData['company_id'],
            'vehicle_type_id'=>$postData['vehicle_type_id'],
            'origin'=>$postData['origin'],
            'destination'=>$postData['destination'],
            'DATE_FORMAT(trip_date, "%d-%m-%Y")'=>date("d-m-Y", strtotime($postData['trip_date']))])->count();
        return $tripCount;
    }

}

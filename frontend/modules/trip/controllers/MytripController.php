<?php

namespace frontend\modules\trip\controllers;

use Yii;
use frontend\modules\trip\models\Trip;
use frontend\modules\trip\models\TripFerry;
use frontend\modules\trip\models\TripDriver;
use frontend\modules\master\models\City;

class MytripController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $driver = Yii::$app->user->id;

        $searchModel = new \frontend\modules\staff\models\StaffSearch();
        $searchModel->load(Yii::$app->request->queryParams);

        // Load trips data
        $query = Trip::find()->from([Trip::tableName().' T'])
                            ->select(['T.trip_date','Org.name from','Dest.name to','T.remarks','s.username','s.mobile','s.email','registration_number','vt.load_rate','vt.ferry_rate'])
                            ->leftJoin(\frontend\modules\vendor\models\Vehicle::tableName().' vv','T.vehicle_id = vv.id')
                            ->leftJoin(TripDriver::tableName().' td','T.id = td.trip_id')
                            ->leftJoin(\frontend\modules\master\models\VehicleType::tableName().' vt','vv.type_id = vt.id')
                            ->leftJoin(\frontend\modules\staff\models\Staff::tableName().' s','s.id = td.driver_id and s.role = 1')
                            ->leftJoin(City::tableName().' Org','Org.id = T.origin')
                            ->leftJoin(City::tableName().' Dest','Dest.id = T.destination');
       $query = $query->andFilterWhere(['like', 'username',$searchModel->username ]);
       $query = $query->andFilterWhere(['like', 's.mobile',$searchModel->mobile ]);
       $query = $query->andFilterWhere(['like', 's.email',$searchModel->email ]);
       $query = $query->andWhere(['tracking_status'=>7])->orderBy('T.id DESC')->asArray()->all();
        $loadTrips = new \yii\data\ArrayDataProvider([
            'allModels' => $query,
        ]);

        // Ferry trips data
        $ferryQuery = TripFerry::find()->from([TripFerry::tableName().' tf'])
                                ->select(['*'])
                                ->leftJoin(\frontend\modules\staff\models\Staff::tableName().' s','s.id = tf.driver_id')
                                ->leftJoin(\frontend\modules\vendor\models\Vehicle::tableName().' vv','tf.vendor_vehicle_id = vv.id')
                                ->leftJoin(\frontend\modules\master\models\VehicleType::tableName().' vt','vv.type_id = vt.id');
        $ferryQuery = $ferryQuery->andFilterWhere(['like', 'username',$searchModel->username ]);
        $ferryQuery = $ferryQuery->andFilterWhere(['like', 's.mobile',$searchModel->mobile ]);
        $ferryQuery = $ferryQuery->andFilterWhere(['like', 's.email',$searchModel->email ]);

        $ferryQuery = $ferryQuery->orderBy('tf.id DESC')->asArray()->all();
        $ferrytrips = new \yii\data\ArrayDataProvider([
            'allModels' => $ferryQuery,
        ]);


        return $this->render('index',['loadtrips'=>$loadTrips,'ferrytrips'=>$ferrytrips,'searchModel'=>$searchModel]);
    }

}

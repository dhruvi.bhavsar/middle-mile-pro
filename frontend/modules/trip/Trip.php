<?php

namespace frontend\modules\trip;
use Yii;

/**
 * trip module definition class
 */
class Trip extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\trip\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        // Yii::$app->commonFunction->accessToAction('tripRequest');
        parent::init();

        // custom initialization code goes here
    }
}

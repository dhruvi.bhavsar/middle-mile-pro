<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\staff\models\Staff */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="staff-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
		<label class="control-label" style="font-weight:bold !important;">Username : </label><?= $model->username; ?>
        </div>
        <div class="col-md-6">
		<label class="control-label" style="font-weight:bold !important;">Email : </label><?= $model->email; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true])->label('Full Name'); ?>
        </div>
        <div class="col-md-6">
        <?= $form->field($model, 'mobile')->textInput() ?>
        </div>
    </div>

    <div class="row">
	<?php if($model->role != '10'){ ?>
        <div class="col-md-6">
        <?= $form->field($model, 'role')->dropDownList(Yii::$app->params['staffRole'],['prompt'=>'Select Role','value'=>$model->role]) ?>
        </div>
	<?php }	?>
        <div class="col-md-6">
        <?= $form->field($model, 'team')->dropDownList(Yii::$app->params['team'],['prompt'=>'Select Department','value'=>$model->team])->label('Department') ?>
        </div>
        <div class="col-md-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['userStatus'],['prompt'=>'Select Status','value'=>$model->status]) ?>
        </div>
    </div>

    <div class="row">
      <div class="col-md-6">
          <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-6">
          <?= $form->field($model, 'work_address')->textArea(['class'=>'form-control ']) ?>
      </div>
    </div>


    <div class="row"><div class="col-md-12"><br><label>Personal Details:</label><br><br></div></div>

    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'contact_number_personal')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'dob')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'blood_group')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'emergency_number')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'address')->textArea(['class'=>'form-control ']) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'spouse_name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'wedding_anniversary')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
    </div>
    <div class="row">
      <div class="col-md-6"><?= $form->field($model, 'child_1_name')->textInput(['class'=>'form-control']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'child_2_name')->textInput(['maxlength' => true]) ?></div>
    </div>

    <div class="row"><div class="col-md-12"><br><label>Employee Profile:</label><br><br></div></div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'pan')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'adhaar')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
      <div class="col-md-6"><?= $form->field($model, 'joining_date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
      <div class="col-md-6"><?= $form->field($model, 'end_date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'bank_account_details')->textArea(['rows'=>3,'class'=>'form-control ']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'training_record')->textArea(['rows'=>3,'class'=>'form-control ']) ?></div>
    </div>
    <div class="row">
      <div class="col-md-6"><?= $form->field($model, 'performance_review_history')->textArea(['rows'=>3,'class'=>'form-control ']) ?></div>
      <div class="col-md-6"><?= $form->field($model, 'employment_histroy')->textArea(['rows'=>3,'class'=>'form-control ']) ?></div>
    </div>
    <div class="row">
      <div class="col-md-6"><?= $form->field($model, 'salary_details')->textArea(['rows'=>3,'class'=>'form-control ']) ?></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>

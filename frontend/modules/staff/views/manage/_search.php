<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\staff\models\StaffSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staff-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-2">
      <?= $form->field($model, 'username') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'email') ?>
    </div>
    <div class="col-md-2">
      <?= $form->field($model, 'mobile') ?>
    </div>
    <div class="col-md-2">
      <?= $form->field($model, 'role')->widget(Select2::classname(), [
        'data' => \Yii::$app->params['role'],
        'options' => [
          'placeholder' => 'Select a Role ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'team')->widget(Select2::classname(), [
        'data' => \Yii::$app->params['team'],
        'options' => [
          'placeholder' => 'Select a department',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ])->label('Department');
      ?>
    </div>
    <div class="form-group m-0 m-b-0 pull-right">
      <?php $params = json_encode(Yii::$app->request->queryParams,true); ?>
      <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
      <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/staff/manage/index"']) ?>
      <?= Html::a('CSV', ['download-staff','params'=>$params], ['class' => 'btn btn-primary','data-method'=>"post"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

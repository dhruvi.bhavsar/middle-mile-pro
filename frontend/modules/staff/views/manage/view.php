<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\staff\models\Staff */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-view">
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			<h3><?= (Html::encode($this->title)) ?>
			<span class="pull-right">
				<?= Html::a('<i class="mdi mdi-pencil" style="line-height:0.8"></i>', ['update', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0','class'=>'btn btn-flat btn-outline btn-circle waves-effect m-r-15']) ?>
				<?php if ($model->role != 10): ?>
					<?= Html::a('<i class="mdi mdi-delete text-danger" style="line-height:0.8"></i>', ['delete', 'id' => $model->id], ['title'=>'Delete','aria-label'=>'Delete','data-pjax'=>'0','data-confirm'=>'Are you sure you want to delete this item?','data-method'=>'post','class'=>'btn btn-flat btn-outline btn-circle waves-effect']) ?>
				<?php endif; ?>
			</span></h3>
			<hr>
			<div class="row">
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Username : </label><?= $model->username; ?>
				</div>
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Email : </label><?= $model->email; ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Full Name : </label><?= $model->full_name; ?>
				</div>
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Contatct Number(Official) : </label><?= $model->mobile; ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Role : </label><?= Yii::$app->params['role'][$model->role]; ?>
				</div>
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Team : </label><?= Yii::$app->params['team'][$model->team]; ?>
				</div>
			</div>
      <div class="row">
        <div class="col-md-6">
        <label class="control-label" style="font-weight:bold !important;">Designation : </label><?= $model->designation; ?>
        </div>
        <div class="col-md-6">
        <label class="control-label" style="font-weight:bold !important;">Work Address : </label><?= $model->work_address; ?>
        </div>
      </div>

			<div class="row">
				<div class="col-md-6">
				<label class="control-label" style="font-weight:bold !important;">Status : </label><?= Yii::$app->params['userStatus'][$model->status]; ?>
				</div>
			</div>


      <div class="row"><div class="col-md-12"><br><label>Personal Details:</label><br><br></div></div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Contact Number(Personal) : </label><?= $model->contact_number_personal; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">DOB : </label><?= !empty($model->dob)?date('d-M-Y',strtotime($model->dob)):''; ?>
        </div>
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Blood Group : </label><?= $model->blood_group; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Emergency Number : </label><?= $model->emergency_number; ?>
        </div>
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Home Address : </label><?= $model->address; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Spouse Name : </label><?= $model->spouse_name; ?>
        </div>
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Wedding Anniversary : </label><?= !empty($model->wedding_anniversary)?date('d-M-Y',strtotime($model->wedding_anniversary)):''; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Child 1 Name : </label><?= $model->child_1_name; ?>
        </div>
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Child 2 Name : </label><?= $model->child_2_name; ?>
        </div>
      </div>

      <div class="row"><div class="col-md-12"><br><label>Employee Profile:</label><br><br></div></div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">PAN Card # : </label><?= $model->pan; ?>
        </div>
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Adhaar Card # : </label><?= $model->adhaar; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">Joining Date : </label><?= !empty($model->joining_date)?date('d-M-Y',strtotime($model->joining_date)):''; ?>
        </div>
        <div class="col-md-6">
          <label class="control-label" style="font-weight:bold !important;">End Date : </label><?= !empty($model->end_date)?date('d-M-Y',strtotime($model->end_date)):''; ?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <label class="control-label" style="font-weight:bold !important;">Bank Account Details : </label><?= $model->bank_account_details; ?>
        </div>
        <div class="col-md-12">
          <label class="control-label" style="font-weight:bold !important;">Training Record : </label><?= $model->training_record; ?>
        </div>
        <div class="col-md-12">
          <label class="control-label" style="font-weight:bold !important;">Performance Review : </label><?= $model->performance_review_history; ?>
        </div>
        <div class="col-md-12">
          <label class="control-label" style="font-weight:bold !important;">Employment Histroy : </label><?= $model->employment_histroy; ?>
        </div>
        <div class="col-md-12">
          <label class="control-label" style="font-weight:bold !important;">Salary Details : </label><?= $model->salary_details; ?>
        </div>
      </div>
	</div>
</div>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Staff';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
    tbody tr {cursor: pointer;}
    tbody tr:hover {cursor: pointer; color:#000;}
    ");
?>
<div class="staff-index">



    <p>
        <?= Html::a('Add Staff', ['create'],
            ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $dataProvider->setSort([
        'attributes' => [
            'username' => ['asc' => ['username' => SORT_ASC], 'desc' => ['username' => SORT_DESC],
                'default' => SORT_ASC],
            'email' => ['asc' => ['email' => SORT_ASC], 'desc' => ['email' => SORT_DESC],
                'default' => SORT_ASC],
            'mobile' => ['asc' => ['mobile' => SORT_ASC], 'desc' => ['mobile' => SORT_DESC],
                'default' => SORT_ASC],
            'Role' => ['asc' => ['role' => SORT_ASC], 'desc' => ['role' => SORT_DESC],
                'default' => SORT_ASC],
            'Department' => ['asc' => ['team' => SORT_ASC], 'desc' => ['team' => SORT_DESC],
                'default' => SORT_ASC],
            'Status' => ['asc' => ['status' => SORT_DESC], 'desc' => ['status' => SORT_ASC],
                'default' => SORT_ASC],
        ]
    ]);
    ?>
    <?=
    GridView::widget([
        'options' => ['class' => 'white-box table-responsive'],
        'tableOptions' => ['class' => 'table'],
        'layout' => "{items}\n<div align='center'>{pager}</div>",
        'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid) {
            return [
                'data-id' => $model->id,
                'onclick' => 'location.href="/staff/manage/view?id="+$(this).data("id");',
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'username',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            'email:email',
            // 'full_name',
            'mobile',
            // 'role',
            [
                'attribute' => 'Role',
                'value' => function($model) {
                    return Yii::$app->params['role'][$model->role];
                }
            ],
            // 'team',
            [
                'attribute' => 'Department',
                'value' => function($model) {
                    return Yii::$app->params['team'][$model->team];
                }
            ],
            // 'status',
            [
                'attribute' => 'Status',
                'value' => function($model) {
                    return Yii::$app->params['userStatus'][$model->status];
                }
            ],
            // 'created_at',
            // 'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'
            ],
        ],
    ]);
    ?>
</div>

<?php

namespace frontend\modules\staff\controllers;

use Yii;
use frontend\modules\staff\models\Staff;
use frontend\modules\staff\models\StaffSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\SignupForm;
use yii\web\Response;
use yii\bootstrap\ActiveForm;

/**
 * ManageController implements the CRUD actions for Staff model.
 */
class ManageController extends Controller
{
    public $model_path = 'frontend\modules\staff\models\Staff';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if($action->id == 'download-staff'){$this->enableCsrfValidation = false;}

        return parent::beforeAction($action);
    }
    /**
     * Lists all Staff models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StaffSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Staff model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->view->params['backUrl']='index';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Staff model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Staff(['scenario'=>'create']);

        // if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        //     if ($user = $signupModel->addStaff()) {
        //         if (Yii::$app->getUser()->login($user)) {
        //             return $this->redirect('/site/index');
        //         }
        //     }
        // }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($user = $model->addStaff()) {
            Yii::$app->activity->add($user->id,$this->model_path,'Staff Member Created');
                return $this->redirect(['view', 'id' => $user->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Staff model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->dob = date('d-m-Y',strtotime($model->dob));
        $model->wedding_anniversary = date('d-m-Y',strtotime($model->wedding_anniversary));
        $model->joining_date = date('d-m-Y',strtotime($model->joining_date));
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // $model->updated_at = date('Y-m-d H:i:s');
            $model->dob = date('Y-m-d',strtotime($model->dob));
            $model->wedding_anniversary = date('Y-m-d',strtotime($model->wedding_anniversary));
            $model->joining_date = date('Y-m-d',strtotime($model->joining_date));
            $model->end_date = date('Y-m-d',strtotime($model->end_date));

            unset($model->password_reset_token);

            if($model->update(false)){
              Yii::$app->activity->add($id,$this->model_path,'Staff Member Updated');
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Staff model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = 2;
        $model->username = $model->username . "-Deleted-" . time();
        $model->email = $model->email . "-Deleted-" . time();
        if($model->update(false)){
            Yii::$app->activity->add($id,$this->model_path,'Staff Member Deleted');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Staff model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Staff the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Staff::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionSignup()
    {
        $this->layout = 'guest';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
            Yii::$app->activity->add($user->id,$this->model_path,'Staff Member Created');
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect('/site/index');
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionDownloadStaff($params = [])
    {
      $header = ['ID','Username','Full Name','Designation','Mobile','Email','Contact Number Personal','Role','Team','Blood Group','Address','Work Address','Emergency Number','DOB','Wedding Anniversary','Spouse Name','Child Name 1','Child Name 2','Bank Account Details','PAN','Adhaar','Joining Date','End Date','Employment Histroy','Performance Review History','Salary Details','Training Record'];
      $searchModel = new StaffSearch();
      if (!is_array($params) && !empty($params)) {
        $params = json_decode($params,true);
      }
      $data = $searchModel->search($params,1);
      $content = [];
      foreach ($data as $model) {
        $content[]=[
          $model['id'],
          $model['username'],
          $model['full_name'],
          $model['designation'],
          $model['mobile'],
          $model['email'],
          $model['contact_number_personal'],
          \Yii::$app->params['role'][$model['role']],
          \Yii::$app->params['team'][$model['team']],
          $model['blood_group'],
          $model['address'],
          $model['work_address'],
          $model['emergency_number'],
          !empty($model['dob'])?date('d-M-Y',strtotime($model['dob'])):'',
          !empty($model['wedding_anniversary'])?date('d-M-Y',strtotime($model['wedding_anniversary'])):'',
          $model['spouse_name'],
          $model['child_1_name'],
          $model['child_2_name'],
          $model['bank_account_details'],
          $model['pan'],
          $model['adhaar'],
          !empty($model['joining_date'])?date('d-M-Y',strtotime($model['joining_date'])):'',
          !empty($model['end_date'])?date('d-M-Y',strtotime($model['end_date'])):'',
          $model['employment_histroy'],
          $model['performance_review_history'],
          $model['salary_details'],
          $model['training_record']
        ];
      }
      $fileName = 'staff_details_'.strtotime('now').'.csv';
      $file = \Yii::$app->commonFunction->exportCSV($header, $content,NULL,$fileName);
      return Yii::$app->response->sendFile($file);
    }

}

<?php

namespace frontend\modules\staff\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\staff\models\Staff;

/**
 * StaffSearch represents the model behind the search form about `frontend\modules\staff\models\Staff`.
 */
class StaffSearch extends Staff
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role', 'team', 'status', 'created_at', 'updated_at'], 'safe'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'full_name', 'mobile', 'blood_group', 'address', 'emergency_number', 'dob', 'wedding_anniversary', 'spouse_name', 'child_1_name', 'child_2_name', 'bank_account_details', 'joining_date', 'employment_histroy', 'performance_review_history'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$asArray = NULL)
    {
        $query = Staff::find()->where(['NOT',['status'=>2]]);

        // add conditions that should always apply here
        if (empty($asArray)) {
          $dataProvider = new ActiveDataProvider([
              'query' => $query,
          ]);
        }


        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'team' => $this->team,
            'dob' => $this->dob,
            'wedding_anniversary' => $this->wedding_anniversary,
            'joining_date' => $this->joining_date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'blood_group', $this->blood_group])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'emergency_number', $this->emergency_number])
            ->andFilterWhere(['like', 'spouse_name', $this->spouse_name])
            ->andFilterWhere(['like', 'child_1_name', $this->child_1_name])
            ->andFilterWhere(['like', 'child_2_name', $this->child_2_name])
            ->andFilterWhere(['like', 'bank_account_details', $this->bank_account_details])
            ->andFilterWhere(['like', 'employment_histroy', $this->employment_histroy])
            ->andFilterWhere(['like', 'performance_review_history', $this->performance_review_history]);

        if (!empty($asArray)) {
          return $query->asArray()->all();
        }
        return $dataProvider;
    }
}

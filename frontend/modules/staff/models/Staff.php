<?php

namespace frontend\modules\staff\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $full_name
 * @property integer $mobile
 * @property integer $role
 * @property integer $team
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Staff extends \yii\db\ActiveRecord
{
    public $password;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'mobile', 'role', 'team'], 'required'],
            ['password', 'required','on'=>'create'],
            [['role', 'team','password_reset_token','status'], 'integer'],
            ['username', 'trim'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.','on'=>'create'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.','on'=>'create'],
            ['password', 'string', 'min' => 6],
            [['mobile','emergency_number','contact_number_personal'], 'match', 'pattern' =>'/^[\+\ \(\)\-0-9]{6,18}$/'],
            [['mobile','emergency_number','contact_number_personal'], 'string','min' => 10,'max'=>10,'tooShort'=>'{attribute} should be minimum 10 digits'],
            ['mobile', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This mobile number has already been taken.','on'=>'create'],
            ['full_name', 'string', 'min' => 2, 'max' => 255],
            [['spouse_name','child_1_name','child_2_name'], 'string', 'min' => 2, 'max' => 100],
            ['blood_group', 'string', 'max' => 20],
            [['address','work_address','designation'], 'string', 'max' => 255],
            [['pan','adhaar'], 'string', 'max' => 20],
            [['employment_histroy','performance_review_history','salary_details','training_record'], 'string'],
            [['dob','wedding_anniversary','joining_date','end_date'],'safe'],

            // HTMLPurifier
            [['id','username', 'email', 'mobile','contact_number_personal','blood_group','address', 'role','auth_key','password_hash','password_reset_token','full_name','designation', 'team','work_address','emergency_number','wedding_anniversary','child_1_name','child_2_name','bank_account_details','pan','adhaar','updated_at','training_record','salary_details','performance_review_history','employment_histroy','end_date','joining_date','spouse_name','dob','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Name',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'full_name' => 'Full Name',
            'designation'=>'Designation',
            'mobile' => 'Contatct Number(Official)',
            'contact_number_personal' => 'Contact Number(Personal)',
            'role' => 'Role',
            'team' => 'Department',
            'status' => 'Status',
            'blood_group'=>'Blood Group',
            'address'=>'Home Address',
            'work_address'=>'Work Address',
            'emergency_number'=>'Emergency Number',
            'dob'=>'DOB',
            'wedding_anniversary'=>'Wedding Anniversary',
            'spouse_name'=>'Spouse Name',
            'child_1_name'=>'Child 1 Name',
            'child_2_name'=>'Child 2 Name',
            'bank_account_details'=>'Bank Account Details',
            'pan'=>'PAN Card #',
            'adhaar'=>'Adhaar Card #',
            'joining_date'=>'Joining Date',
            'end_date'=>'End Date',
            'employment_histroy' => 'Employment Histroy',
            'performance_review_history' => 'Performance Review',
            'salary_details' => 'Salary Details',
            'training_record' => 'Training Record',
            'updated_at' => 'Updated At',
            'password' => 'Password',
        ];
    }

    public function addStaff()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->mobile = $this->mobile;
        $user->role = $this->role;
        $user->team = $this->team;

        $user->blood_group = $this->blood_group;
        $user->address = $this->address;
        $user->emergency_number = $this->emergency_number;
        if (!empty($this->dob)) {
          $user->dob = date('Y-m-d',strtotime($this->dob));
        }
        if (!empty($this->wedding_anniversary)) {
          $user->wedding_anniversary = date('Y-m-d',strtotime($this->wedding_anniversary));
        }
        if (!empty($this->joining_date)) {
          $user->joining_date = date('Y-m-d',strtotime($this->joining_date));
        }
        if (!empty($this->end_date)) {
          $user->end_date = date('Y-m-d',strtotime($this->end_date));
        }
        $user->spouse_name = $this->spouse_name;
        $user->child_1_name = $this->child_1_name;
        $user->child_2_name = $this->child_2_name;
        $user->bank_account_details = $this->bank_account_details;
        $user->employment_histroy = $this->employment_histroy;
        $user->performance_review_history = $this->performance_review_history;
        // $user->created_at = date('Y-m-d H:i:s');
        $user->setPassword($this->password);
        $user->generateAuthKey();
        return $user->save() ? $user : null;
    }

    public static function allStaffList($team=null)
    {
      $data = User::find();

      if(!empty($team)){
          $data=$data->where(['team'=>$team]);
      }

      $data = $data->asArray()->all();

      return \yii\helpers\ArrayHelper::map($data,'id','username');
    }

}

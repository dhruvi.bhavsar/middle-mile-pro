<?php

namespace frontend\modules\staff;
use Yii;

/**
 * staff module definition class
 */
class Staff extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\staff\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->commonFunction->accessToAction('staff');
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace frontend\modules\pod\Models;

use Yii;

/**
 * This is the model class for table "trip_pod_collection".
 *
 * @property int $id
 * @property int $trip_id
 * @property int $collected_by
 * @property int $menifest_id
 * @property string $created_on
 * @property int $status
 */
class TripPodCollection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trip_pod_collection';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trip_id', 'collected_by', 'created_on'], 'required'],
            [['trip_id', 'collected_by', 'menifest_id', 'status'], 'integer'],
            [['created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'collected_by' => 'Collected By',
            'menifest_id' => 'Menifest ID',
            'created_on' => 'Created On',
            'status' => 'Status',
        ];
    }
}

<?php

namespace frontend\modules\pod\Models;

use Yii;

/**
 * This is the model class for table "trip_pod_menifest".
 *
 * @property int $id
 * @property int $status
 * @property string $courier_details
 * @property int $created_by
 * @property string $created_on
 */
class TripPodMenifest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trip_pod_menifest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by'], 'integer'],
            [['courier_details'], 'string'],
            [['created_by', 'created_on'], 'required'],
            [['created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'courier_details' => 'Courier Details',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
        ];
    }

        public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }

}

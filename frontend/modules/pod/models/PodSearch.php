<?php

namespace frontend\modules\pod\models;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\customer\models\Customer;
use frontend\modules\master\models\State;
use frontend\modules\master\models\City;
use frontend\modules\trip\models\Transactions;
/**
 * PodSearch represents the model behind the search form about `frontend\modules\trip\models\Trip`.
 */
class PodSearch extends Trip
{
    public $zone;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vendor_id', 'company_id', 'contact_person_id', 'vehicle_id', 'vehicle_type_id', 'buying_rate_incl_handling_charges', 'trip_type', 'service_type', 'status','tracking_status'], 'safe'],
            [['number', 'vendor_bids', 'origin', 'loading_point', 'destination', 'unloading_point', 'trip_date', 'recipients', 'created_at', 'updated_at', 'negotiation_remark', 'cancellation', 'remarks', 'pod_receival_date', 'final_bill_date', 'last_notified_at'], 'safe'],
            [['targated_rate', 'targated_buying', 'buying_rate', 'advance_buying_charges', 'selling_rate', 'advance_selling_charges', 'origin_handling_charges', 'destination_handling_charges', 'origin_lat', 'origin_lon', 'destination_lat', 'destination_lon', 'notification_duration','zone'], 'safe'],
            [['from_date','to_date','duration'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trip::find()->from([Trip::tableName().' T'])
                    ->select([
                            'T.id','T.number','T.buying_rate','T.advance_buying_charges','T.selling_rate','T.advance_selling_charges','T.pod_receival_date','T.trip_date',
                            'T.loading_point','T.unloading_point',
                            'T.tracking_status','T.remarks',
                            'VV.registration_number',
                            'IF(V.intended_name IS NULL,V.name,V.intended_name) vendor',
                            'C.name customer',
//                            'SUM(IF(transaction_type = '.Transactions::PAID.' AND TR.status='.Transactions::STATUS_ACTIVE.' ,amount,0)) AS paid',
//                            'SUM(IF(transaction_type = '.Transactions::RECEIVED.' AND TR.status='.Transactions::STATUS_ACTIVE.',amount,0)) AS received',
                            
//                            '(T.buying_rate - (SELECT IF(SUM(amount) IS NULL,0,SUM(amount)) FROM transactions WHERE trip_id = T.id AND status = 1 AND transaction_type = 2)) balance_payable',
//                            '(T.selling_rate - (SELECT IF(SUM(amount) IS NULL,0,SUM(amount)) FROM transactions WHERE trip_id = T.id AND status = 1 AND transaction_type = 1)) balance_receipt',
                           
                          ])
                    ->leftJoin(Vendor::tableName().' V','V.id = T.vendor_id')
                    ->leftJoin(\frontend\modules\vendor\models\Vehicle::tableName().' VV','VV.id = T.vehicle_id')
                    ->leftJoin(Customer::tableName().' C','C.id = T.company_id')
//                    ->leftJoin(City::tableName().' CITY','CITY.id = T.origin')
//                    ->leftJoin(State::tableName().' S','S.id = CITY.state_id')
//                   ->leftJoin(Transactions::tableName() . ' TR','TR.trip_id = T.id')
                    ->andWhere(['T.billing_id'=>NULL])
                    ->andWhere(['NOT',['T.unloaded_datetime'=>NULL]]);
        // add conditions that should always apply here
        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'T.id' => $this->id,
            'T.vendor_id' => $this->vendor_id,
            'T.company_id' => $this->company_id,
            // 'T.contact_person_id' => $this->contact_person_id,
            // 'T.vehicle_id' => $this->vehicle_id,
            // 'T.vehicle_type_id' => $this->vehicle_type_id,
            // 'T.targated_rate' => $this->targated_rate,
            // 'T.targated_buying' => $this->targated_buying,
            // 'T.buying_rate' => $this->buying_rate,
            // 'T.advance_buying_charges' => $this->advance_buying_charges,
            'T.tracking_status' => $this->tracking_status,
            'T.notification_duration' => $this->notification_duration,
            'T.pod_receival_date' => $this->pod_receival_date,
            'T.final_bill_date' => $this->final_bill_date,
            'T.last_notified_at' => $this->last_notified_at,
            'S.zone' => $this->zone,
        ]);

        if(!empty($this->duration))
        {
         switch($this->duration){
             case 'currentmonth':
                 $sdate = date('Y-m-d 00:00:00',strtotime('-30 Day'));
                 $edate = date('Y-m-d 23:59:59');
                 $query = $query->andWhere(['>','T.trip_date',$sdate]);
                 $query = $query->andWhere(['<','T.trip_date',$edate]);
                 break;
             case 'currentweek':
                 $sdate = date('Y-m-d 00:00:00',strtotime('-7 Day'));
                 $edate = date('Y-m-d 23:59:59');
                 $query = $query->andWhere(['>','T.trip_date',$sdate]);
                 $query = $query->andWhere(['<','T.trip_date',$edate]);
                 break;
             case 'custom':
                  $sdate = date('Y-m-d 00:00:00',strtotime((!empty($this->from_date)?$this->from_date:'1-Jan-1980 00:00:00')));
                  $edate = date('Y-m-d 23:59:59',strtotime((!empty($this->to_date)?$this->to_date:'1-Jan-2900 23:59:59')));
                  $query = $query->andWhere(['>=','T.trip_date',$sdate]);
                  $query = $query->andWhere(['<=','T.trip_date',$edate]);
                 break;
         }
        }

        $query->andFilterWhere(['like', 'T.number', $this->number])
            ->andFilterWhere(['like', 'T.vendor_bids', $this->vendor_bids])
            ->andFilterWhere(['like', 'T.origin', $this->origin])
            ->andFilterWhere(['like', 'T.loading_point', $this->loading_point])
            ->andFilterWhere(['like', 'T.destination', $this->destination])
            ->andFilterWhere(['like', 'T.unloading_point', $this->unloading_point])
            ->andFilterWhere(['like', 'T.recipients', $this->recipients])
            ->andFilterWhere(['like', 'T.negotiation_remark', $this->negotiation_remark])
            ->andFilterWhere(['like', 'T.cancellation', $this->cancellation])
            ->andFilterWhere(['like', 'T.remarks', $this->remarks])
            ->andFilterWhere(['like', 'VV.registration_number', $this->vehicle_id]);

            // var_dump($query->createCommand()->sql);
            // die();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            'sort'=> [
                'attributes' => [
                                  'number'=>'number',
                                  'vendor'=>'vendor',
                                  'customer'=>'customer',
                                  'buying_rate'=>'buying_rate',
                                  'advance_buying_charges'=>'advance_buying_charges',
                                  'paid'=>'paid',
                                  'balance_payable'=>'balance_payable',
                                  'selling_rate'=>'selling_rate',
                                  'advance_selling_charges'=>'advance_selling_charges',
                                  'received'=>'received',
                                  'balance_receipt'=>'balance_receipt',
                                  'pod_receival_date'=>'pod_receival_date',
                                ],
                'defaultOrder' => ['pod_receival_date'=>SORT_ASC,]
              ],
        ]);

        return $dataProvider;
    }


}

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\trip\models\Transactions;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\account\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manifest details';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php Pjax::begin(); ?>

<div class="white-box">
    <div class="row">
        <div class="col-md-4">
            <strong>Manifest Id:</strong> <?= $model->unique_id; ?>
        </div>
        <div class="col-md-4">
            <strong>Created On:</strong> <?= date("d M Y h:i A",strtotime($model->created_on)); ?>
        </div>
        <div class="col-md-4">
            <strong>Created By:</strong> <?= $model->user->username; ?>
        </div>
        <div class="col-md-4">
            <strong>Status:</strong>
            <?php
                 switch($model->status){
                      case 0:
                          echo  "Courier details pending";
                      break;

                      case 1:
                          echo "Couriered";
                      break;

                      case 2:
                          echo "Open";
                      break;

                      case 3:
                          echo "Closed";
                      break;

                  };
            ?>
        </div>
        <div class="col-md-8">
            
            <strong>Courier Details:</strong> <?= $model->courier_details; ?>
            <?php if(empty($model->courier_details)){ ?>
            <span id="courier-detils-box" >
                <form method="post" action="/pod/manage/update-manifest-courier?id=<?= $model->id; ?>">
                   <input type="text" id="courier-details" class="form-control" name="courier_details" placeholder="Courier Details">
                   <input type="text" id="courier-details-datetime" name="courier_date" class="form-control datepicker" name="" placeholder="Date time">
                   <button type="submit" class="btn btn-primary pull-right" id="courier-details-save">Save</button>
               </form>
            </span>
            <?php } ?>

        </div>

    </div>
</div>

  <div class="trip-index">
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'summary' => '',
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'label' => '',
                'attribute'=>'number',
                'format'=>'raw',
                'value'=>function($model){
                    $text = "";
//                    if($model['tracking_status']==12){
//                        $text = '<input type="checkbox" onclick="$(\'#courier-pod-box\').show();"  name="vehicle1"  class="trip_id" value="'. $model['id'].'">';
//                    }
                   return $text;
                  //return '<label class="hidden-lg hidden-md text-muted">Order ID : </label>'.($model['number'].'</br><small>'.$model['loading_point']  . '<br/>' . $model['unloading_point'] .'</small>'.'</br><small> Trip date:'.date('d-m-Y',strtotime($model['trip_date'])).'</small>');
                }
              ],
              [
                'label' => 'Order ID',
                'attribute'=>'number',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Order ID : </label>'.($model['number'].'</br><small>'.$model['loading_point']  . '<br/>' . $model['unloading_point'] .'</small>'.'</br><small> Trip date:'.date('d-m-Y',strtotime($model['trip_date'])).'</small>');
                }
              ],
              [
                'attribute'=>'vendor',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Vendor : </label>'.$model['vendor'] . "<br/>" . '<strong> ' . $model['registration_number'] . ' </strong>';
                }
              ],
              [
                'attribute'=>'customer',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Customer : </label>'.$model['customer'];
                }
              ],
              [
                'label' => '',
                'attribute'=>'tracking_status',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md text-muted">Status : </label>' . Yii::$app->params['event_status'][$model['tracking_status']] . "<br />";

                  $remarks = json_decode($model['remarks'],true);
                  $index = count($remarks)-1;

                  if($model['status'] == 1 || $model['status'] == 2){
                        $text.= "<small >By - " .common\models\User::findOne($remarks[$index]['user'])->username . " <br /> " . date("d-m-y h:i A", strtotime($remarks[$index]['on'])) . "</small><br />";
                  }else{
                      $text ="Pod not received";
                  }

                    return $text;
                }
              ],
               
              [
                'label' => '',
                'format'=>'raw',
                'attribute'=>'status',
                'value'=>function($model){
                          $text = "";
                          if($model['status'] == 1 && ( in_array(Yii::$app->user->identity->team,[6,5]) ||  in_array(Yii::$app->user->identity->role,[10,9]))){
                                $text.=' <a tabindex="0" class="btn btn-xs btn-info" href="/pod/manage/receivepod?id='.$model['cid'].'&status=2"  >Received</a>';
                                $text.=' <a tabindex="0" class="btn btn-xs btn-danger" href="/pod/manage/receivepod?id='.$model['cid'].'&status=3"  >Not Received</a>';
                          }
                          return $text;
                }
              ],
                /*
              [
                'label' => 'Received',
                'attribute'=>'received',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md">Received : </label>'.((float)$model['received']);
                  return $text;
                  //return Html::a($text,NULL,['data-title'=>'Received : '.$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
              [
                'label' => 'Balance',
                'format'=>'raw',
                'attribute'=>'balance_receipt',
                'value'=>function($model){return '<label class="hidden-lg hidden-md text-muted">Balance : </label>'.(float)$model['balance_receipt'];}
              ],
               */
              // ['class' => 'yii\grid\ActionColumn'],
          ],
      ]); ?>

      <div class="white-box" style="display: none;" id="courier-pod-box">
          <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-primary" id="courier-pod">Create manifest</button>
              </div>
          </div>
      </div>

  </div>

<?php $this->registerJs("
      $(document).ready(function(){
        var selectedTrips = [];
        $('#courier-pod').click(function(){
          selectedTrips = [];
          if($('.trip_id:checked').length < 1){
                alert('Please select POD');
                return false;
          }

          $('.trip_id:checked').each(function(){
            selectedTrips.push($(this).val());
          });
          window.location = '/pod/manage/create-manifest?trip_id='+selectedTrips.join('%2C');
        });        

      });
      ") ?>

<?php Pjax::end(); ?>
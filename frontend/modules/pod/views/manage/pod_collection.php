<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\trip\models\Transactions;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\account\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'POD\'s Collected';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
  <div class="trip-index">
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'label' => '',
                'attribute'=>'number',
                'format'=>'raw',
                'value'=>function($model){
                    $text = "";
                    if($model['tracking_status']==12){
                        $text = '<input type="checkbox" onclick="$(\'#courier-pod-box\').show();"  name="vehicle1"  class="trip_id" value="'. $model['id'].'">';
                    }
                   return $text;
                  //return '<label class="hidden-lg hidden-md text-muted">Order ID : </label>'.($model['number'].'</br><small>'.$model['loading_point']  . '<br/>' . $model['unloading_point'] .'</small>'.'</br><small> Trip date:'.date('d-m-Y',strtotime($model['trip_date'])).'</small>');
                }
              ],
              [
                'label' => 'Order ID',
                'attribute'=>'number',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Order ID : </label>'.($model['number'].'</br><small>'.$model['loading_point']  . '<br/>' . $model['unloading_point'] .'</small>'.'</br><small> Trip date:'.date('d-m-Y',strtotime($model['trip_date'])).'</small>');
                }
              ],
              [
                'attribute'=>'vendor',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Vendor : </label>'.$model['vendor'] . "<br/>" . '<strong> ' . $model['registration_number'] . ' </strong>';
                }
              ],
              [
                'attribute'=>'customer',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Customer : </label>'.$model['customer'];
                }
              ],
              [
                'label' => '',
                'attribute'=>'tracking_status',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md text-muted">Status : </label>' . Yii::$app->params['event_status'][$model['tracking_status']] . "<br />";

                  $remarks = json_decode($model['remarks'],true);
                  $index = count($remarks)-1;


                 $text.= "<small >By - " .common\models\User::findOne($remarks[$index]['user'])->username . " <br /> " . date("d-m-y h:i A", strtotime($remarks[$index]['on'])) . "</small><br />";

                    
                  // For collect pod button
                  switch($model['tracking_status']){
                      case 12:
                         // $text.=' <a tabindex="0" class="btn btn-xs btn-info" href="/trip/manage/pod-received?trip_id='.$model['id'].'&status=13"  >Courier POD</a><br/><br/>';
                      break;
                      case 13:
                          if(Yii::$app->user->identity->team == 5){
                          $text.=' <a tabindex="0" class="btn btn-xs btn-info" href="/trip/manage/pod-received?trip_id='.$model['id'].'&status=14"  >POD Received</a><br/><br/>';
                          }
                      default :
                          if(!in_array($model['tracking_status'],[12,13,14]))
                          $text.=' <a tabindex="0" class="btn btn-xs btn-info" href="/trip/manage/pod-received?trip_id='.$model['id'].'&status=12"  >Collect POD</a><br/><br/>';
                      break;
                  }


                    $pendingTransactions = Transactions::tripTransactionDetails($model['id'],[],['status'=>0,'receipt'=>[0,1],'applicable_for'=>1])->asArray()->all();

                    if(!empty($pendingTransactions)){
                            foreach($pendingTransactions as $key=>$transaction){
                                $text .= "<div class='receipt-row'><p>" . "<small>" . \Yii::$app->params['transaction_type'][$transaction['transaction_type']] . "</small> - " . (float)$transaction['amount'] .
                                                     " <small>".$transaction['note']."</small>";

                                if($transaction['receipt']==1){ $text.=' <span class="text-success"> <i class="fa-fw mdi success mdi-check-circle"></i><span>';}

                                $text .= "</p>";
                                if($transaction['receipt']==0){
                                    $text .= '<p><input type="text" id="recept-remark-' . $model['id'] . "-" . $key . '" class="receipt-remark form-control" name="" placeholder="Remark"></p>';
                                    $text .= '<p class="text-right" ><a tabindex="0" class="btn btn-xs btn-info receive-recept" data-trid="' . $transaction['id'] . '" data-remark="recept-remark-' . $model['id'] . "-" . $key . '" role="button" >Receive receipt</a></p></div>';
                                }
                                $text.="<hr/>";
                            }
                    }


                    return $text;
                }
              ],
               /*
              [
                'label' => 'Advance',
                'format'=>'raw',
                'attribute'=>'advance_selling_charges',
                'value'=>function($model){return '<label class="hidden-lg hidden-md text-muted">Advance : </label>'.(float)$model['advance_selling_charges'];}
              ],
              [
                'label' => 'Received',
                'attribute'=>'received',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md">Received : </label>'.((float)$model['received']);
                  return $text;
                  //return Html::a($text,NULL,['data-title'=>'Received : '.$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
              [
                'label' => 'Balance',
                'format'=>'raw',
                'attribute'=>'balance_receipt',
                'value'=>function($model){return '<label class="hidden-lg hidden-md text-muted">Balance : </label>'.(float)$model['balance_receipt'];}
              ],
               */
              // ['class' => 'yii\grid\ActionColumn'],
          ],
      ]); ?>

      <div class="white-box" style="display: none;" id="courier-pod-box">
          <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-primary" id="courier-pod">Create manifest</button>
              </div>
          </div>
      </div>

  </div>

<?php $this->registerJs("
      $(document).ready(function(){
        var selectedTrips = [];
        $('#courier-pod').click(function(){
          selectedTrips = [];
          if($('.trip_id:checked').length < 1){
                alert('Please select POD');
                return false;
          }

          $('.trip_id:checked').each(function(){
            selectedTrips.push($(this).val());
          });
          window.location = '/pod/manage/create-manifest?trip_id='+selectedTrips.join('%2C');
        });        

      });
      ") ?>

<?php Pjax::end(); ?>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\trip\models\Transactions;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\account\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manifests';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
  <div class="trip-index">
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'label' => '',
                'attribute'=>'number',
                'format'=>'raw',
                'value'=>function($model){
                    $text = "";
                   return $text;
                  //return '<label class="hidden-lg hidden-md text-muted">Order ID : </label>'.($model['number'].'</br><small>'.$model['loading_point']  . '<br/>' . $model['unloading_point'] .'</small>'.'</br><small> Trip date:'.date('d-m-Y',strtotime($model['trip_date'])).'</small>');
                }
              ],
              [
                'label' => 'Manifest ID',
                'attribute'=>'number',
                'format'=>'raw',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Manifest ID : </label>'.($model['unique_id']);
                }
              ],

               
              [
                'label' => 'Courier details',
                'format'=>'raw',
                'attribute'=>'courier_details',
                'value'=>function($model){return '<label class="hidden-lg hidden-md text-muted">Courier details : </label>'.$model['courier_details'];}
              ],

              [
                'label' => 'Status',
                'format'=>'raw',
                'attribute'=>'status',
                'value'=>function($model){

                  switch($model['status']){
                      case 0:
                          return "Courier details pending";
                      break;

                      case 1:
                          return "Couriered";
                      break;

                      case 2:
                          return "Open";
                      break;

                      case 3:
                          return "Closed";
                      break;

                  }

                }
              ],
              ['class' => 'yii\grid\ActionColumn',
                  'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view' || $action === 'update') {
                    $url ='/pod/manage/view-manifest?id='.$model['id'];
                    return $url;
                }

                if ($action === 'delete') {
                    $url ='/pod/manage/delete-manifest?id='.$model['id'];
                    return $url;
                }

              }
              ],
          ],
      ]); ?>

      <div class="white-box" style="display: none;" id="courier-pod-box">
          <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-primary" id="courier-pod">Create manifest</button>
              </div>
          </div>
      </div>

  </div>

<?php $this->registerJs("
      $(document).ready(function(){
        var selectedTrips = [];
        $('#courier-pod').click(function(){
          selectedTrips = [];
          if($('.trip_id:checked').length < 1){
                alert('Please select POD');
                return false;
          }

          $('.trip_id:checked').each(function(){
            selectedTrips.push($(this).val());
          });
          window.location = '/pod/manage/create-manifest?trip_id='+selectedTrips.join('%2C');
        });        

      });
      ") ?>

<?php Pjax::end(); ?>
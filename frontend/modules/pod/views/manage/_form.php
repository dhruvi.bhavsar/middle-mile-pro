<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'company_id')->textInput() ?>

    <?= $form->field($model, 'contact_person_id')->textInput() ?>

    <?= $form->field($model, 'vehicle_id')->textInput() ?>

    <?= $form->field($model, 'vehicle_type_id')->textInput() ?>

    <?= $form->field($model, 'vendor_bids')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'targated_buying')->textInput() ?>

    <?= $form->field($model, 'advance_buying_charges')->textInput() ?>

    <?= $form->field($model, 'selling_rate')->textInput() ?>

    <?= $form->field($model, 'advance_selling_charges')->textInput() ?>

    <?= $form->field($model, 'origin_handling_charges')->textInput() ?>

    <?= $form->field($model, 'destination_handling_charges')->textInput() ?>

    <?= $form->field($model, 'trip_type')->textInput() ?>

    <?= $form->field($model, 'origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'service_type')->textInput() ?>

    <?= $form->field($model, 'trip_date')->textInput() ?>

    <?= $form->field($model, 'recipients')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'negotiation_remark')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cancellation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'notification_duration')->textInput() ?>

    <?= $form->field($model, 'pod_receival_date')->textInput() ?>

    <?= $form->field($model, 'last_notified_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

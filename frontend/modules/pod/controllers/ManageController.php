<?php

namespace frontend\modules\pod\controllers;

use Yii;
use frontend\modules\trip\models\Trip;
use frontend\modules\pod\models\PodSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use frontend\modules\pod\models\TripPodCollection;

/**
 * ManageController implements the CRUD actions for Trip model.
 */

class ManageController extends Controller
{
    /**
     * @inheritdoc
     */
   public function beforeAction($action){
       Yii::$app->commonFunction->accessToAction('pod');
        if($action->id == 'update-manifest-courier'){$this->enableCsrfValidation = false;}

       return parent::beforeAction($action);
   }
    public function behaviors()
    {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => [],
              'rules' => [
                  [
                      'actions' => [],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['POST'],
              ],
          ],
      ];
    }

    /**
     * Lists all Trip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCollection()
    {
        $where = ['collected_by'=>Yii::$app->user->id,'menifest_id'=>NULL];

        $searchModel = TripPodCollection::find()
            ->select([
                 'T.id','T.number','T.buying_rate','T.advance_buying_charges','T.selling_rate','T.advance_selling_charges','T.pod_receival_date','T.trip_date',
                            'T.loading_point','T.unloading_point',
                            'T.tracking_status','T.remarks',
                            'VV.registration_number',
                            'IF(V.intended_name IS NULL,V.name,V.intended_name) vendor',
                            'C.name customer',
                ])
            ->from([TripPodCollection::tableName().' Tc'])
            ->leftJoin(Trip::tableName() . ' T','Tc.trip_id = T.id')
            ->leftJoin(\frontend\modules\vendor\models\Vendor::tableName().' V','V.id = T.vendor_id')
            ->leftJoin(\frontend\modules\customer\models\Customer::tableName().' C','C.id = T.company_id')

            ->leftJoin(\frontend\modules\vendor\models\Vehicle::tableName().' VV','VV.id = T.vehicle_id')
            ->where($where);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $searchModel->asArray()->all()
        ]);



        return $this->render('pod_collection', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionViewManifest($id)
    {
        $where = ['collected_by'=>Yii::$app->user->id,'menifest_id'=>$id];

        $searchModel = TripPodCollection::find()
            ->select([
                 'T.id','T.number','T.buying_rate','T.advance_buying_charges','T.selling_rate','T.advance_selling_charges','T.pod_receival_date','T.trip_date',
                            'T.loading_point','T.unloading_point',
                            'T.tracking_status','T.remarks',
                            'VV.registration_number',
                            'IF(V.intended_name IS NULL,V.name,V.intended_name) vendor',
                            'Tc.status','Tc.id cid',
                            'C.name customer',
                ])
            ->from([TripPodCollection::tableName().' Tc'])
            ->leftJoin(Trip::tableName() . ' T','Tc.trip_id = T.id')
            ->leftJoin(\frontend\modules\vendor\models\Vendor::tableName().' V','V.id = T.vendor_id')
            ->leftJoin(\frontend\modules\customer\models\Customer::tableName().' C','C.id = T.company_id')

            ->leftJoin(\frontend\modules\vendor\models\Vehicle::tableName().' VV','VV.id = T.vehicle_id')
            ->where($where);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $searchModel->asArray()->all(),
            'pagination' => [
                                            'pageSize' => 100,
                                        ],
            ]);

        $manifest = \frontend\modules\pod\models\TripPodMenifest::findOne($id);
        return $this->render('manifest_view', [
            'dataProvider' => $dataProvider,
            'model'=>$manifest
        ]);
    }

    public function actionDeleteManifest($id){
        TripPodCollection::updateAll(['menifest_id'=>NULL],['menifest_id'=>$id]);
        \frontend\modules\pod\models\TripPodMenifest::deleteAll(['id'=>$id]);
         Yii::$app->session->setFlash('success', 'Manifest deleted successfully.');
        return $this->redirect(Yii::$app->request->referrer);
     }

    public function actionReceivepod($id,$status=null){
        // Update pod status
        $model = \frontend\modules\pod\models\TripPodCollection::findOne($id);
        $model->status = $status;
        $model->update(false);


        if($status == 2){
                    // Update pod status in trip table
                    $trip = Trip::findOne($model->trip_id);
                    $trip->tracking_status=14;

                    $data = [
                        'remark'=>'-',
                        'location'=>'MMP Office',
                        'datetime'=>date('d-m-Y h:i A'),
                        'trip_event'=>14,
                        'km_reading'=>'',
                        'user'=>\Yii::$app->user->identity->id,
                        'on'=>date('Y-m-d H:i:s'),
                      ];

                    if (empty($trip->remarks)) {
                      $trip->remarks = json_encode([$data],true);
                    } else {
                      $remarks = json_decode($trip->remarks,true);
                      array_push($remarks,$data);
                      $trip->remarks = json_encode($remarks,true);
                    }
                    $trip->update(false);
        }

         // Close menifest if all pods are received
        $notReceivedpod = \frontend\modules\pod\models\TripPodCollection::find()->where(['menifest_id'=>$model->menifest_id,'status'=>[1,3]])->count();
        $menifest = \frontend\modules\pod\models\TripPodMenifest::findOne($model->menifest_id);
        if($notReceivedpod==0){
            $menifest->status = 3;
        }else{
            $menifest->status = 2;
        }
        $menifest->update(false);
        
        
        if($status== 3){
            Yii::$app->session->setFlash('success', 'Pod marked as not received');
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            Yii::$app->session->setFlash('success', 'Pod received successfully');
            return $this->redirect(Yii::$app->request->referrer);
        }
     }

     public function actionUpdateManifestCourier($id){
        $manifest = \frontend\modules\pod\models\TripPodMenifest::findOne($id);
        $postData = Yii::$app->request->post();
        $manifest->courier_details = $postData['courier_details'];
        $manifest->courier_date = date("Y-m-d H:i:s", strtotime($postData['courier_date']));
        $manifest->status =1;
        $manifest->update(false);
        Yii::$app->session->setFlash('success', 'Courier details updated successfully.');

        return $this->redirect(Yii::$app->request->referrer);
     }

    public function actionManifests()
    {

        if(!in_array(Yii::$app->user->identity->team,[6,5]) ||  !in_array(Yii::$app->user->identity->role,[10,9])){
            $where = ['created_by'=>Yii::$app->user->id];
        }else{
            $where = [];
        }

        $searchModel = \frontend\modules\pod\models\TripPodMenifest::find()
            ->select(['*'])
            ->where($where);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $searchModel->asArray()->all()
        ]);

        return $this->render('manifest_index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateManifest($trip_id){

        $trips = explode(",", $trip_id);

        $manifest = new \frontend\modules\pod\models\TripPodMenifest();
        $manifest->created_by = Yii::$app->user->id;
        $manifest->created_on = date("Y-m-d H:i:s");
        $manifest->unique_id = 'MMP-'.time();
        $manifest->save(false);

       TripPodCollection::updateAll(['menifest_id'=>$manifest->id],['trip_id'=>$trips]);
        Yii::$app->session->setFlash('success', 'Manifest created successfully.');

       return $this->redirect("/pod/manage/view-manifest?id=" . $manifest->id);

    }

    // /**
    //  * Finds the Trip model based on its primary key value.
    //  * If the model is not found, a 404 HTTP exception will be thrown.
    //  * @param integer $id
    //  * @return Trip the loaded model
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    protected function findModel($id)
    {
        if (($model = Trip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

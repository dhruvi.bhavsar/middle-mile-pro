<?php

namespace frontend\modules\inventory\models;

use Yii;

/**
 * This is the model class for table "tyre".
 *
 * @property int $id
 * @property string $company
 * @property string $serial_number
 * @property string $price
 * @property string $total_repair_charges
 * @property int $current_vehicle_id
 * @property int $total_km_run
 * @property string $purchase_date
 * @property string $scraped_date
 * @property string $data_usage
 * @property int $status
 */
class Tyre extends \yii\db\ActiveRecord
{
    public $date;
    public $action;
    public $vehicle_id;
    public $vehicle_reading;
    public $repair_charges;
    public $at_km;
    public $note;
    public $user;
    public $on_date;

    /**
     * {@inheritdoc}
     */
    const STATUS_STOCK = 1;
    const STATUS_DELETED = 2;
    const STATUS_INSTALLED = 3;
    const STATUS_STEPNEY = 5;
    const STATUS_REPAIR = 8;
    const STATUS_SCRAPED = 10;

    public static function tableName()
    {
        return 'tyre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {

        return [
            // Create update tyre
            [['company', 'serial_number', 'price'], 'required','on'=>'addTyre'],
            [['price', 'total_repair_charges'], 'number','on'=>'addTyre'],
            [['current_vehicle_id', 'total_km_run', 'status','current_vehicle_id'], 'integer'],
            [['purchase_date', 'scraped_date','created_at'], 'safe','on'=>'addTyre'],
            [['data_usage'], 'string','on'=>'addTyre'],
            [['company', 'serial_number'], 'string', 'max' => 255,'on'=>'addTyre'],
            [['status'],'default','value'=>1,'on'=>'addTyre'],
            [['created_at'],'default','value'=>date('Y-m-d H:i:s'),'on'=>'addTyre'],

            // on update tyre status
            [['date','action'],'required','on'=>'updateTyreStatus'],
            [['repair_charges','at_km'],'number','on'=>'updateTyreStatus'],
            [['vehicle_id','action','user'],'integer','on'=>'updateTyreStatus'],
            [['note','vehicle_reading'], 'string', 'max' => 400,'on'=>'updateTyreStatus'],
            [['date','on_date'],'safe','on'=>'updateTyreStatus'],

            // HTMLPurifier
            [['id','company', 'serial_number', 'price','total_repair_charges' ,'current_vehicle_id','total_km_run','purchase_date','scraped_date','data_usage','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

            [['date', 'action', 'vehicle_id','repair_charges','at_km','note','user','vehicle_reading','on_date'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            },'on'=>'updateTyreStatus'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company' => 'Company',
            'serial_number' => 'Serial Number',
            'price' => 'Price',
            'total_repair_charges' => 'Total Repair Charges',
            'current_vehicle_id' => 'Current Vehicle ID',
            'total_km_run' => 'Total KM Run',
            'purchase_date' => 'Purchase Date',
            'scraped_date' => 'Scraped Date',
            'data_usage' => 'Data Usage',
            'status' => 'Status',
        ];
    }

    public function setData()
    {
      if (!empty($this->purchase_date)) {
        $this->purchase_date = date('d-m-Y',strtotime($this->purchase_date));
      }
      if (!empty($this->scraped_date)) {
        $this->scraped_date = date('d-m-Y',strtotime($this->scraped_date));
      }
    }

    public function processData()
    {
        if (!$this->validate()) {
          return $this->validate();
        }
        if (!empty($this->purchase_date)) {
          $this->purchase_date = date('Y-m-d',strtotime($this->purchase_date));
        }
        if (!empty($this->scraped_date)) {
          $this->scraped_date = date('Y-m-d',strxtotime($this->scraped_date));
        }

        // $this->status = !empty($this->current_vehicle_id)?self::STATUS_INSTALLED:self::STATUS_STOCK;
        return $this->save();

    }

    public function addTyreStatus()
    {
      if (!$this->validate()) {
        return $this->validate();
      }

      $model = Tyre::findOne($this->id);
      $model->scenario = 'addTyre';

      $array = [
        'date'=>$this->date,
        'action'=>$this->action,
        'vehicle_id'=>$this->vehicle_id,
        'repair_charges'=>$this->repair_charges,
        'at_km'=>$this->at_km,
        'note'=>$this->note,
        'user'=> \Yii::$app->user->identity->id,
        'on_date'=>date('d-m-Y h:i A'),
      ];
      $model->total_repair_charges += (float)$this->repair_charges;
      $model->current_vehicle_id = $this->vehicle_id;
      $model->total_km_run = ($model->total_km_run > $this->at_km)?$model->total_km_run:$this->at_km;
      $dataUsage = [];
      if (!empty($model->data_usage)) {
        $dataUsage = json_decode($model->data_usage,true);
      }
      $dataUsage[]=$array;
      $model->data_usage = json_encode($dataUsage,true);
      $model->save(false);
      return true;
    }
}

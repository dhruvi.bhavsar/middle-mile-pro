<?php

namespace frontend\modules\inventory\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\inventory\models\Tyre;

/**
 * TyreSearch represents the model behind the search form about `frontend\modules\inventory\models\Tyre`.
 */
class TyreSearch extends Tyre
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'current_vehicle_id', 'total_km_run', 'status'], 'integer'],
            [['company', 'serial_number', 'purchase_date', 'scraped_date', 'data_usage'], 'safe'],
            [['price', 'total_repair_charges'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tyre::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'total_repair_charges' => $this->total_repair_charges,
            'current_vehicle_id' => $this->current_vehicle_id,
            'total_km_run' => $this->total_km_run,
            'purchase_date' => $this->purchase_date,
            'scraped_date' => $this->scraped_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'serial_number', $this->serial_number])
            ->andFilterWhere(['like', 'data_usage', $this->data_usage]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\inventory\controllers;

use Yii;
use frontend\modules\inventory\models\Tyre;
use frontend\modules\inventory\models\TyreSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;

/**
 * TyreController implements the CRUD actions for Tyre model.
 */
class TyreController extends Controller
{
    /**
     * @inheritdoc
     */
    public $model_path = 'frontend\modules\inventory\models\Tyre';

    public function behaviors()
    {

      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => [],
              'rules' => [
                  [
                      'actions' => [],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['POST'],
              ],
          ],
      ];
    }

    public function beforeAction($action)
    {
        // 'inventory'=>['viewInventory','manageInventory','updateInventoryStatus'],
        if(in_array($action->id, ['index','view'])){Yii::$app->commonFunction->accessToAction('viewInventory');}
        if(in_array($action->id, ['create','update','delete'])){Yii::$app->commonFunction->accessToAction('manageInventory');}
        // http://mmp.local/inventory/tyre
        return parent::beforeAction($action);
    }

    /**
     * Lists all Tyre models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TyreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tyre model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->view->params['backUrl']='index';
        $model = $this->findModel($id);
        $modelStatus = $this->findModel($id);
        $modelStatus->scenario = 'updateTyreStatus';
        $modelStatus->vehicle_id = $model->current_vehicle_id;
        $dataHistory = new ArrayDataProvider([
            'allModels' => json_decode($model->data_usage,true),
            'sort'=> [
                'attributes' => [
                                  'date'=>'date',
                                  'action'=>'action',
                                  'vehicle_id'=>'vehicle_id',
                                  'repair_charges'=>'repair_charges',
                                  'note'=>'note',
                                  'user'=>'user',
                                  'on_date'=>'on_date',
                                ],
                'defaultOrder' => ['date'=>SORT_ASC,]
              ],
              'pagination' => [ 'pageSize' => 10, ],
        ]);
        //if (Yii::$app->commonFunction->accessToAction('updateInventoryStatus')) {
          if ($modelStatus->load(Yii::$app->request->post()) && $modelStatus->addTyreStatus()) {
            return $this->refresh();
          }
        //}

        return $this->render('view', [
            'model' => $model,
            'modelStatus'=>$modelStatus,
            'dataHistory'=>$dataHistory,
        ]);
    }

    /**
     * Creates a new Tyre model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tyre(['scenario' => 'addTyre']);

        if ($model->load(Yii::$app->request->post()) && $model->processData()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tyre model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'addTyre';
        $model->setData();
        if ($model->load(Yii::$app->request->post()) && $model->processData()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tyre model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = 2;
        $model->save(false);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Tyre model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tyre the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tyre::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\modules\inventory\models\Tyre */

$this->title = $model->company.' - '.$model->serial_number;
?>
<div class="tyre-view">
    <div class="row">
      <div class="col-md-4 col-sm-12">
          <section class="white-box">
            <div class="user-btm-box p-t-0">
                <div class="row  m-t-10">
                    <div class="col-sm-6 b-r"><strong>Company</strong><p><?= $model->company; ?></p>
                    </div>
                    <div class="col-sm-6 "><strong>Serial Number</strong><p><?= $model->serial_number; ?></p>
                    </div>
                </div>
                <hr/>
                <div class="row  m-t-10">
                    <div class="col-sm-6 b-r"><strong>Price</strong><p><?= $model->price; ?></p>
                    </div>
                    <div class="col-sm-6 "><strong>Total Repair Charges</strong><p><?= $model->total_repair_charges; ?></p>
                    </div>
                </div>
                <hr/>
                <div class="row  m-t-10">
                    <div class="col-sm-6 b-r"><strong>Current Vehicle</strong><p>
                      <?php
                      if (!empty($model->current_vehicle_id)){
                      $vehicles = frontend\modules\vendor\models\Vehicle::vendorVehicleListWithVendorName() ?>
                      <?= $vehicles[$model->current_vehicle_id]; ?>
                      <?php } ?>
                    </p></div>
                    <div class="col-sm-6 "><strong>Total Km Run</strong><p><?= $model->total_km_run; ?></p>
                    </div>
                </div>
                <hr/>
                <div class="row  m-t-10">
                    <div class="col-sm-6 b-r"><strong>Purchase Date</strong><p><?= !empty($model->purchase_date)?date('d-m-Y',strtotime($model->purchase_date)):''; ?></p>
                    </div>
                    <div class="col-sm-6 "><strong>Scraped Date</strong><p><?= !empty($model->scraped_date)?date('d-m-Y',strtotime($model->scraped_date)):''; ?></p>
                    </div>
                </div>
                <hr/>
                <div class="row  m-t-10">
                    <div class="col-sm-6 b-r"><strong>Status</strong><p><?= \Yii::$app->params['tyreStatusLabel'][$model['status']]; ?></p>
                    </div>
                    <div class="col-sm-6"><strong></strong><p></p></div>
                </div>
                <hr/>
                <div class="row text-center m-t-10">
                    <div class="col-xs-6 b-r">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary waves-effect waves-light m-r-10 btn-block ']) ?>
                    </div>
                    <div class="col-xs-6">
                      <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                          'class' => 'btn btn-danger waves-effect waves-light m-r-10 btn-block ',
                          'data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],
                      ]) ?>
                    </div>
                </div>
            </div>
          </section>
      </div>
      <div class="col-md-8 col-sm-12 ">
          <section class="well p-t-10 mob-p-t-0">
            <nav class="">
                <ul class="nav nav-tabs customtab">
                    <li class="active"><a data-toggle="tab" href="#tyre-status-log" aria-expanded="false"><span class="visible-xs"><i class="mdi mdi-history"></i></span><span class="hidden-xs"> History</span></a></li>
                    <?php // if (\Yii::$app->commonFunction->accessToAction('updateInventoryStatus')) { ?>
                        <li class=""><a data-toggle="tab" href="#tyre-status" aria-expanded="false"><span class="visible-xs"><i class="mdi mdi-pencil"></i></span><span class="hidden-xs"> Update Status</span></a></li>
                    <?php // } ?>
                </ul>
            </nav>
            <div class="tab-content">
                <div id="tyre-status-log" class="tab-pane fade in active" style="font-size:12px">

                  <?php Pjax::begin(); ?>

                  <?= GridView::widget([
                      'dataProvider' => $dataHistory,
                      //'filterModel' => $searchModel,
                      'options' => ['class'=>'white-box table-responsive'],
                      'tableOptions' => ['class'=>'table'],
                      'emptyText' => '<center>No records</center>',
                      'columns' => [
                          ['class' => 'yii\grid\SerialColumn'],
                          'date',
                          [
                            'attribute'=>'action',
                            'format'=>'raw',
                            'value'=>function($model){
                              return \Yii::$app->params['tyreStatusLabel'][$model['action']];
                            }
                          ],
                          [
                            'attribute'=>'vehicle_id',
                            'format'=>'raw',
                            'value'=>function($model){
                              if (empty($model['vehicle_id'])) {
                                return '';
                              }
                              return frontend\modules\vendor\models\Vehicle::allVehicles()[$model['vehicle_id']];
                            }
                          ],
                          'repair_charges',
                          'note',
                          [
                            'attribute'=>'user',
                            'format'=>'raw',
                            'value'=>function($model){
                              if (empty($model['user'])) {
                                return '';
                              }
                              return frontend\modules\staff\models\Staff::allStaffList()[$model['user']];
                            }
                          ],
                          'on_date',
                      ],
                  ]); ?>

                  <?php Pjax::end(); ?>
                </div>
                <?php // if (\Yii::$app->commonFunction->accessToAction('updateInventoryStatus')) { ?>
                    <div id="tyre-status" class="tab-pane fade">
                  <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                      <div class="col-md-6"><?= $form->field($modelStatus, 'date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
                      <div class="col-md-6"><?= $form->field($modelStatus, 'action')->dropdownList(
                          \Yii::$app->params['tyreStatusDropdown'],['prompt'=>'Select Action']) ?></div>
                      <div class="clearfix"></div>
                      <div class="col-md-6">
                        <?= $form->field($modelStatus, 'vehicle_id')->widget(Select2::classname(), [
                          'data' =>  \frontend\modules\vendor\models\Vehicle::vendorVehicleListWithVendorName(),
                          'options' => [
                            'placeholder' => 'Select Vehicle ...',
                          ],
                          'pluginOptions' => [
                            'allowClear' => true,
                            //'multiple' => true,
                          ],
                          ])->label("Vehicle");
                          ?>
                        </div>
                        <div class="col-md-6"><?= $form->field($modelStatus, 'vehicle_reading')->textInput(['class'=>'form-control','autocomplete'=>'off']) ?></div>
                        <div class="clearfix"></div>
                        <div class="col-md-6"><?= $form->field($modelStatus, 'at_km')->textInput(['class'=>'form-control','autocomplete'=>'off']) ?></div>
                        <div class="col-md-6"><?= $form->field($modelStatus, 'repair_charges')->textInput(['class'=>'form-control','autocomplete'=>'off']) ?></div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                          <?= $form->field($modelStatus, 'note')->textarea(['class'=>'form-control','autocomplete'=>'off']) ?>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                              <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                          </div>
                        </div>
                    </div>
                  <?php ActiveForm::end(); ?>
                </div>
                <?php // } ?>
            </div>
          </section>
      </div>
    </div>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\inventory\models\Tyre */

$this->title = 'Add Tyre';
$this->params['breadcrumbs'][] = ['label' => 'Tyres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tyre-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

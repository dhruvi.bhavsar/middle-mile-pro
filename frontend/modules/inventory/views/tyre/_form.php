<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\modules\inventory\models\Tyre */
/* @var $form yii\widgets\ActiveForm */
// vendorVehicleListWithVendorName
?>

<div class="tyre-form">
  <div class="white-box">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?></div>
        <div class="clearfix"></div>
        <div class="col-md-6"><?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6">
          <?= $form->field($model, 'current_vehicle_id')->widget(Select2::classname(), [
            'data' =>  \frontend\modules\vendor\models\Vehicle::vendorVehicleListWithVendorName(),
            'options' => [
              'placeholder' => 'Select Vehicle ...',
            ],
            'pluginOptions' => [
              'allowClear' => true,
              //'multiple' => true,
            ],
          ])->label("Vehicle");
          ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6"><?= $form->field($model, 'purchase_date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'status')->dropdownList(
            \Yii::$app->params['tyreStatusDropdown']) ?></div>
        <div class="clearfix"></div>
        <div class="col-md-6"><?= $form->field($model, 'total_repair_charges')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'total_km_run')->textInput(['maxlength' => true]) ?></div>
        <div class="clearfix"></div>
        <div class="col-md-6"><?= $form->field($model, 'scraped_date')->textInput(['class'=>'form-control datepicker','autocomplete'=>'off']) ?></div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
</div>

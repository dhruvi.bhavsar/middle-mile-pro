<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\inventory\models\TyreSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tyre-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="col-md-3">
        <?= $form->field($model, 'company') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'serial_number') ?>
    </div>
    <?php /*
    <div class="col-md-3">
        <?= $form->field($model, 'price') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'total_repair_charges') ?>
    </div>
    */ ?>
    <?php // echo $form->field($model, 'current_vehicle_id') ?>

    <?php // echo $form->field($model, 'total_km_run') ?>

    <?php // echo $form->field($model, 'purchase_date') ?>

    <?php // echo $form->field($model, 'scraped_date') ?>

    <?php // echo $form->field($model, 'data_usage') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group m-t-20 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/vendor/manage/index"']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\inventory\models\TyreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tyres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tyre-index">
  <p><?= Html::a('Add Tyre', ['create'], ['class' => 'btn btn-success']) ?></p>
  <div class="white-box m-b-5">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php Pjax::begin(); ?>
    <?= GridView::widget([
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              //'id',
              'company',
              'serial_number',
              'price',
              'total_repair_charges',
              // 'current_vehicle_id',
              // 'total_km_run',
              [
                'attribute'=>'purchase_date',
                'value'=>function($model){
                  return !empty($model->purchase_date)?date('d-m-Y',strtotime($model->purchase_date)):'';
                }
              ],
              [
                'attribute'=>'scraped_date',
                'value'=>function($model){
                  return !empty($model->scraped_date)?date('d-m-Y',strtotime($model->scraped_date)):'';
                }
              ],
              // 'data_usage:ntext', tyreStatusLabel
              [
                'attribute'=>'status',
                'format'=>'html',
                'value'=>function($model){
                  return \Yii::$app->params['tyreStatusLabel'][$model['status']];
                }
              ],

              ['class' => 'yii\grid\ActionColumn'],
          ],
      ]); ?>
  <?php Pjax::end(); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\inventory\models\Tyre */

$this->title = 'Update Tyre: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tyres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tyre-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

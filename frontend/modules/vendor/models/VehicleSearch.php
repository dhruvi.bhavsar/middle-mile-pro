<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\master\models\MasterVendorType;
use yii\data\ArrayDataProvider;
use frontend\modules\master\models\VehicleType;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\trip\models\Trip;
use frontend\modules\customer\models\Customer;
use frontend\modules\master\models\City;
/**
 * VendorSearch represents the model behind the search form about `frontend\modules\vendor\models\Vendor`.
 */
class VehicleSearch extends Vehicle
{
    /**
     * @inheritdoc
     */
     public $month;
     public $customer;
    public function rules()
    {
        return [
            [['vendor_id','type_id'], 'safe'],
            [['month','customer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
     public function searchVehicle($params)
     {
         $query = Vehicle::find()->from([Vehicle::tableName().' VE'])
                                 ->select([
                                   'VE.*','VT.name vehicle_type','C.name customer_name',
                                   'IF(V.intended_name IS NULL,V.name,V.intended_name) vendor_name','T.number number','O.name origin','D.name destination',
                                   'MAX(T.trip_date) trip_date','T.status','T.loading_point','T.unloading_point',
                                   '(CASE
                                       WHEN T.id IS NULL THEN "Avalible"
                                       WHEN T.status = '.Trip::STATUS_CUSTOMER_CONFIRM_PENDING.' THEN "Customer Confirmation Pending"
                                       WHEN T.status = '.Trip::STATUS_VENDOR_CONFIRM_PENDING.' THEN "Vendor Confirmation Pending"
                                       WHEN T.status = '.Trip::STATUS_ORDER_CONFIRMED.' AND T.trip_date > NOW() THEN "Next Trip"
                                       ELSE "In Progress"
                                    END) AS trip_status'
                                 ])
                                  ->leftJoin(Vendor::tableName().' V','V.id = VE.vendor_id')
                                  ->leftJoin(VehicleType::tableName().' VT','VT.id = VE.type_id')
                                  ->leftJoin('(SELECT max(id) id, vehicle_id FROM '.Trip::tableName().' group by vehicle_id) LT','LT.vehicle_id = VE.id')
                                  ->leftJoin(Trip::tableName().' T','T.id = LT.id AND (
                                          /* IF today record*/
                                         (T.trip_date BETWEEN "'.date('Y-m-d 00:00:00').'" AND "'.date('Y-m-d 23:59:59').'")
                                         /* IF running trip */
                                         OR (T.status = '.Trip::STATUS_ORDER_CONFIRMED.' AND T.pod_receival_date IS NULL)
                                         /* IF New booked */
                                         OR (T.status IN('.Trip::STATUS_CUSTOMER_CONFIRM_PENDING.','.Trip::STATUS_VENDOR_CONFIRM_PENDING.'))
                                         /* IF recently closed trip within 2 days*/
                                  )')
                                  ->leftJoin(Customer::tableName().' C','C.id = T.company_id')
                                  ->leftJoin(City::tableName().' D','D.id = T.destination')
                                  ->leftJoin(City::tableName().' O','O.id = T.origin')
                                  ->andWhere(['VE.status'=>1,'V.status'=>1])
                                  ->groupBy('VE.id');

          //var_dump($query->createCommand()->sql); die();
         // grid filtering conditions
         if ($this->load($params)) {
           $query->andFilterWhere([
             'VE.vendor_id' => $this->vendor_id,
             'VE.type_id' => $this->type_id,
           ]);
         }

         $dataProvider = new ArrayDataProvider([
             'allModels' => $query->asArray()->all(),
             'sort'=> [
                 'attributes' => [
                                   //'registration_number'=>'registration_number',
                                   'trip_date'=>'trip_date',
                                   'vehicle_owner'=>'vehicle_owner',
                                   'origin'=>'origin',
                                   'destination'=>'destination',
                                   'customer_name'=>'customer_name',
                                   'status'=>'status',
                                 ],
                 'defaultOrder' => []// 'registration_number'=>SORT_ASC,
               ],
         ]);

         return $dataProvider;
     }

     public function searchVehicleStatus($params,$id)
     {

        $this->load($params);
        $query = Trip::find()->from([Trip::tableName().' T'])
                             ->select(['C.name customer_name',
                               'T.number order_id','D.name origin','D.name destination',
                               'T.trip_date trip_date','T.status','T.remarks'
                             ])
                             ->leftJoin(Customer::tableName().' C','C.id = T.company_id')
                             ->leftJoin(City::tableName().' D','D.id = T.destination')
                             ->leftJoin(City::tableName().' O','O.id = T.origin')
                             ->andWhere(['T.vehicle_id'=>$id])
                             ->andWhere(['<>','T.remarks','']);

          if (empty($this->month)) {
            $this->month = date('m-Y');
          }
          $query = $query->andFilterWhere([
                                            'OR',
                                            ['between','T.trip_date',date('Y-m-1 00:00:00',strtotime('1-'.$this->month)),date('Y-m-t 23:59:59',strtotime('1-'.$this->month))],
                                            ['between','T.pod_receival_date',date('Y-m-1 00:00:00',strtotime('1-'.$this->month)),date('Y-m-t 23:59:59',strtotime('1-'.$this->month))],
                                          ]);
          /*
            month wise filter for optimization
            via trip date and pod receival date
          */
          $query = $query->asArray()->all();
          $data = [];
          if (!empty($query)) {
            foreach ($query as $trip) {
              if (empty($trip['remarks'])) {
                $data[]=[
                  'trip_date'=>$trip['trip_date'],
                  'datetime'=>$trip['trip_date'],
                  'customer_name'=>$trip['customer_name'],
                  'order_id'=>$trip['order_id'],
                  'origin'=>$trip['origin'],
                  'destination'=>$trip['destination'],
                  'remark'=>'',
                  'trip_event'=>'Assigned',
                  'location'=>'',
                ];
              } else {
                foreach (json_decode($trip['remarks'],true) as $remarks) {
                  $data[]=[
                    'trip_date'=>$trip['trip_date'],
                    'datetime'=>$remarks['datetime'],
                    'customer_name'=>$trip['customer_name'],
                    'order_id'=>$trip['order_id'],
                    'origin'=>$trip['origin'],
                    'destination'=>$trip['destination'],
                    'remark'=>$remarks['remark'],
                    'trip_event'=>$remarks['trip_event'],
                    'location'=>$remarks['location'],
                  ];
                }
              }
            }
          }

          //var_dump($query->createCommand()->sql); die();
         // grid filtering conditions

         $dataProvider = new ArrayDataProvider([
             'allModels' => $data,
             'sort'=> [
                 'attributes' => [
                                   //'registration_number'=>'registration_number',
                                   'trip_date'=>'trip_date',
                                   'datetime'=>'datetime',
                                   'customer_name'=>'customer_name',
                                   'order_id'=>'order_id',
                                   'origin'=>'origin',
                                   'destination'=>'destination',
                                   'trip_status'=>'trip_status',
                                   'trip_event'=>'trip_event',
                                   'location'=>'location',
                                 ],
                 'defaultOrder' => ['datetime'=>SORT_DESC]// 'registration_number'=>SORT_ASC,
               ],
         ]);

         return $dataProvider;
     }
}

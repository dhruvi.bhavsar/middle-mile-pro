<?php

namespace frontend\modules\vendor\models;

use Yii;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\master\models\VehicleType;
/**
 * This is the model class for table "gps_tracking".
 *
 * @property int $id
 * @property string $registration_number
 * @property string $data
 * @property int $ignition
 * @property double $lat
 * @property double $long
 * @property string $created_at
 * @property string $updated_at
 */
class GpsTracking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gps_tracking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registration_number', 'data', 'ignition', 'lat', 'long'], 'required'],
            [['data','api'], 'string'],
            [['ignition','state_code','zone'], 'integer'],
            [['lat', 'long'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['registration_number','reverse_geocoding','address'], 'string', 'max' => 255],
            [['created_at'], 'default','value'=>date('Y-m-d H:i:s')],
            [['api'], 'default','value'=>'AL'],

            // HTMLPurifier
            [['id','registration_number','reverse_geocoding','address','state_code', 'data','ignition', 'lat','long', 'created_at', 'updated_at','zone'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'api' => 'API',
            'registration_number' => 'Registration Number',
            'data' => 'Data',
            'ignition' => 'Ignition',
            'lat' => 'Lat',
            'long' => 'Long',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getCurrentVehicleLocation($regNo = [])
    {
        $query = self::find()->from([self::tableName().' GT'])
                    ->select([
                                'GT.id','GT.api','GT.registration_number','GT.data','GT.ignition','GT.lat','GT.long',
                                'VT.name v_type','GT.address','GT.state_code','GT.zone'
                              ])
                    ->leftJoin(Vehicle::tableName().' V','UPPER(V.registration_number) LIKE UPPER(GT.registration_number)')
                    ->leftJoin(VehicleType::tableName().' VT','VT.id = V.type_id')
                    ->indexBy(['registration_number']);
        if (!empty($regNo)) {
          $query = $query->andWhere(['GT.registration_number'=>$regNo]);
        }
        $query = $query->asArray()->all();
        return $query;
    }

    public static function getTripTracking($tripId)
    {
        $model = Trip::findOne(['id'=>$tripId]);

        $query = self::find()->andWhere(['like','registration_number',$model->vehicleDetails->registration_number])
                            ->andWhere(['>=','created_at',$model->trip_date]);
        if (!empty($model->unloaded_datetime)) {
          $query = $query->andWhere(['<=','created_at',$model->unloaded_datetime]);
        }
        $query = $query->asArray()->all();
        return $query;
    }
}

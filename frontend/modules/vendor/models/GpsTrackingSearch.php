<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\vendor\models\GpsTracking;
use frontend\modules\master\models\MasterVendorType;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\master\models\VehicleType;

/**
 * VendorSearch represents the model behind the search form about `frontend\modules\vendor\models\Vendor`.
 */
class GpsTrackingSearch extends GpsTracking
{
    /**
     * @inheritdoc
     */
    public $v_type_id;

    public function rules()
    {
        return [
            [['id', 'registration_number', 'data', 'ignition', 'lat', 'long','data','api','state_code','created_at', 'updated_at','reverse_geocoding','address','v_type_id','zone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      $this->load($params);
      $query = self::find()->from([self::tableName().' GT'])
                  ->select([
                              'GT.id','GT.api','GT.registration_number','GT.data','GT.ignition','GT.lat','GT.long',
                              'VT.name v_type','GT.address','GT.state_code','GT.zone'
                            ])
                  ->leftJoin(Vehicle::tableName().' V','UPPER(V.registration_number) LIKE UPPER(GT.registration_number)')
                  ->leftJoin(VehicleType::tableName().' VT','VT.id = V.type_id')
                  ->indexBy(['registration_number']);

      $query->andFilterWhere(['like', 'GT.registration_number', $this->registration_number])
          ->andFilterWhere(['like', 'GT.address', $this->address]);

      $query->andFilterWhere([
              'GT.zone' => $this->zone,
              'V.type_id' => $this->v_type_id,
          ]);



      if (!empty($regNo)) {
        $query = $query->andWhere(['GT.registration_number'=>$regNo]);
      }

      $query = $query->asArray()->all();


//        var_dump($query);
//        die();

      return $query;
    }
}

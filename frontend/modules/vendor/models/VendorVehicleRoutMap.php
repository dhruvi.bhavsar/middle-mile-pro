<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "vendor_vehicle_rout_map".
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $vehicle_type
 * @property string $from_loc
 * @property string $to_loc
 * @property int $status
 */
class VendorVehicleRoutMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendor_vehicle_rout_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendor_id', 'vehicle_type_id', 'from_loc', 'to_loc', 'status'], 'required'],
            [['vendor_id', 'status'], 'integer'],
            [['vehicle_type_id','from_loc', 'to_loc'], 'safe'],

            // HTMLPurifier
            [['id','vendor_id', 'vehicle_type_id', 'from_loc', 'to_loc','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendor_id' => 'Vendor ID',
            'vehicle_type_id' => 'Vehicle Type',
            'from_loc' => 'From',
            'to_loc' => 'To',
            'status' => 'Status',
        ];
    }

    public static function vendorLocations($vendor_id){
        $query = self::find()->where(['status'=>1,'vendor_id'=>$vendor_id]);

        $result = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $result;
    }
}

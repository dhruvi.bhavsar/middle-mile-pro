<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\master\models\MasterVendorType;

/**
 * VendorSearch represents the model behind the search form about `frontend\modules\vendor\models\Vendor`.
 */
class VendorSearch extends Vendor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'category', 'status'], 'safe'],
            [['name', 'intended_name', 'code', 'note', 'gst_no', 'pan_no', 'bank_ac', 'bank_ac_name', 'bank_ifsc', 'bank_name', 'email', 'mobile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vendor::find()->from([Vendor::tableName().' V'])
                                ->select(['V.*','VT.name vendor_type'])
                                ->leftJoin(MasterVendorType::tableName().' VT','VT.id = V.type');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'V.id' => $this->id,
            'V.type' => $this->type,
            'V.category' => $this->category,
            'V.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'V.name', $this->name])
            ->andFilterWhere(['like', 'V.intended_name', $this->intended_name])
            ->andFilterWhere(['like', 'V.code', $this->code])
            ->andFilterWhere(['like', 'V.note', $this->note])
            ->andFilterWhere(['like', 'V.gst_no', $this->gst_no])
            ->andFilterWhere(['like', 'V.pan_no', $this->pan_no])
            ->andFilterWhere(['like', 'V.bank_ac', $this->bank_ac])
            ->andFilterWhere(['like', 'V.bank_ac_name', $this->bank_ac_name])
            ->andFilterWhere(['like', 'V.bank_ifsc', $this->bank_ifsc])
            ->andFilterWhere(['like', 'V.bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'V.email', $this->email])
            ->andFilterWhere(['like', 'V.mobile', $this->mobile]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\data\ActiveDataProvider;
use frontend\modules\master\models\City;

/**
 * This is the model class for table "vendor_location".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $vendor_id
 * @property integer $status
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'vendor_id'], 'required'],
            [['vendor_id', 'status'], 'integer'],

            // HTMLPurifier
            [['id','city_id', 'vendor_id','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City',
            'vendor_id' => 'Vendor',
            'status' => 'Status',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public static function locationList($vendor_id,$columns=NULL){
        $query = self::find()->where(['status'=>1,'vendor_id'=>$vendor_id]);
        if($columns){
           $query = $query->select($columns)->asArray();
        }
        return $query->all();
    }

    public static function vendorLocations($vendor_id){
        $query = self::find()->where(['status'=>1,'vendor_id'=>$vendor_id]);

        $result = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $result;
    }
}

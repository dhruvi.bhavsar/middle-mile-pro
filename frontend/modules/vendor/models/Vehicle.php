<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use frontend\modules\master\models\VehicleType;
use frontend\modules\vendor\models\Vendor;
/**
 * This is the model class for table "vendor_vehicle".
 *
 * @property integer $id
 * @property string $registration_number
 * @property string $chassi_number
 * @property string $body_number
 * @property integer $type_id
 * @property integer $vendor_id
 * @property integer $status
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor_vehicle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registration_number', 'chassi_number', 'body_number', 'type_id', 'vendor_id'], 'required'],
            [['type_id', 'vendor_id', 'status'], 'integer'],
            [['registration_number'], 'string', 'max' => 100],
            [['vehicle_owner','vehicle_owner_mobile'], 'string', 'max' => 255],
            [['chassi_number', 'body_number'], 'string', 'max' => 200],

            // HTMLPurifier
            [['id','registration_number', 'chassi_number', 'body_number', 'type_id', 'vendor_id','vehicle_owner','vehicle_owner_mobile','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'registration_number' => 'Registration Number',
            'chassi_number' => 'Chassi Number',
            'body_number' => 'Body Number',
            'type_id' => 'Vehicle Type',
            'vendor_id' => 'Vendor',
            'vehicle_owner' => 'Owner',
            'vehicle_owner_mobile' => 'Owner\'s Mobile',
            'status' => 'Status',
        ];
    }

    public function getVehicleType()
    {
        return $this->hasOne(VehicleType::className(), ['id' => 'type_id']);
    }

    public function getVendor()
    {
        return $this->hasOne(Vendor::className(), ['id' => 'vendor_id']);
    }

    public static function vendorVehicles($vendor_id){
        $query = self::find()->where(['status'=>1,'vendor_id'=>$vendor_id]);

        $result = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
        return $result;
    }

    public static function vendorVehicleList($vendor_id){
        $data= self::find()->select(['id','registration_number'])->where(['status'=>1,'vendor_id'=>$vendor_id])->asArray()->all();

        return \yii\helpers\ArrayHelper::map($data,'id','registration_number');
    }

    public static function allVehicles(){
        $data = self::find()->asArray()->all();
        return \yii\helpers\ArrayHelper::map($data,'id','registration_number');
    }
    public static function vendorVehicleListWithVendorName($showDeleted = NULL){
        $data = self::find()->from([self::tableName().' VE'])
                    ->select(['VE.id','CONCAT(VE.registration_number," (",IF(V.intended_name IS NULL,V.name,V.intended_name),")") vehicle_name'])
                    ->leftJoin(Vendor::tableName().' V','V.id = VE.vendor_id');

        if (empty($showDeleted)) {
          $data = $data->andWhere(['VE.status'=>1,'V.status'=>1]);
        }

        $data = $data->asArray()->all();

        return \yii\helpers\ArrayHelper::map($data,'id','vehicle_name');
    }

}

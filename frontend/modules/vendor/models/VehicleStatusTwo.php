<?php

namespace frontend\modules\vendor\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\master\models\MasterVendorType;
use yii\data\ArrayDataProvider;
use frontend\modules\master\models\VehicleType;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\trip\models\Trip;
use frontend\modules\customer\models\Customer;
use frontend\modules\master\models\City;
/**
 * VendorSearch represents the model behind the search form about `frontend\modules\vendor\models\Vendor`.
 */
class VehicleStatusTwo extends Vehicle
{
    /**
     * @inheritdoc
     */
     public $month;
     public $customer;
     public $from_date;
     public $to_date;

    public function rules()
    {
        return [
            [['vendor_id','type_id'], 'safe'],
            [['month','customer','from_date','to_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

     public function searchVehicleStatusTwo($params,$id = NULL)
     {
        \Yii::$app->db->createCommand('SET SESSION group_concat_max_len = 100000;')->execute();
         $this->load($params);

         if (empty($this->vendor_id)) {
           // MMP ids
           $this->vendor_id = [1];
         }

         if (empty($this->from_date)) {
           $this->from_date = date('Y-m-d 00:00:00',strtotime('this week monday'));
         } else {
           $this->from_date = date('Y-m-d 00:00:00',strtotime($this->from_date));
         }

         if (empty($this->to_date)) {
           $this->to_date = date('Y-m-d 23:59:59',strtotime('this week sunday'));
         } else {
           $this->to_date = date('Y-m-d 23:59:59',strtotime($this->to_date));
         }

         $query = Vehicle::find()->from([Vehicle::tableName().' V'])
                        ->select([
                                    'V.registration_number',
                                    'GROUP_CONCAT(C.name) customer_name',
                                    'GROUP_CONCAT(T.trip_date) trip_dates',
                                    'GROUP_CONCAT(T.id) trip_ids',
                                    'GROUP_CONCAT(T.unloaded_datetime) unloaded_datetimes',
                                    'GROUP_CONCAT(T.remarks) vehicle_status',
                                    'GROUP_CONCAT(DEST.name," | ",T.loading_point) dest',
                                    'GROUP_CONCAT(ORIG.name," | ",T.unloading_point) origin',
                                ])
                        ->leftJoin(Trip::tableName().' T','T.vehicle_id = V.id')
                        ->leftJoin(Customer::tableName().' C','C.id = T.company_id')
                        ->leftJoin(City::tableName().' DEST','DEST.id = T.destination')
                        ->leftJoin(City::tableName().' ORIG','ORIG.id = T.origin')
                        // ->andWhere(['NOT',['AND',['>','T.trip_date',$this->from_date],['>','T.unloaded_datetime',$this->from_date]]])
                        ->andWhere(['OR',
                                    [
                                      'OR',
                                      ['<=','T.trip_date',$this->from_date],
                                      ['BETWEEN','T.trip_date',$this->from_date,$this->to_date]
                                    ],
                                    ['T.trip_date'=>NULL]
                        ])
                        ->andWhere([
                            'OR',
                            [
                                'OR',
                                ['>=','T.unloaded_datetime',$this->to_date],
                                ['BETWEEN','T.unloaded_datetime',$this->from_date,$this->to_date]
                            ],
                            ['T.unloaded_datetime'=>NULL]
                        ])
                        ->andWhere(['V.vendor_id'=>$this->vendor_id])
                        ->groupBy(['V.registration_number'])
                        ->orderBy('vehicle_status DESC');

//                      echo $query->createCommand()->sql;
//                      var_dump($query->createCommand()->params);
////                      die();

                      $query= $query->asArray()->all();

//                      var_dump($query);
//                      die();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query,
            'sort' => [
              // 'attributes' => ['id'=>'id','priority'=>'priority','due_date'=>'due_date','status'=>'status'],
              // 'defaultOrder' => ['due_date'=>SORT_DESC]
            ],
            'pagination' => ['pageSize' => 100],
          ]);
        $this->from_date =  date('d-M-Y',strtotime($this->from_date));
        $this->to_date = date('d-M-Y',strtotime($this->to_date));

        return $dataProvider;
     }
}

<?php

namespace frontend\modules\vendor\models;

use Yii;
use frontend\modules\master\models\MasterVendorType;
use frontend\modules\master\models\VendorCategory;
use frontend\modules\master\models\City;

/**
 * This is the model class for table "vendor".
 *
 * @property integer $id
 * @property string $name
 * @property string $intended_name
 * @property string $code
 * @property integer $type
 * @property string $note
 * @property string $gst_no
 * @property string $pan_no
 * @property string $bank_ac
 * @property string $bank_ac_name
 * @property string $bank_ifsc
 * @property string $bank_name
 * @property string $email
 * @property string $mobile
 */
class Vendor extends \yii\db\ActiveRecord
{
    public $vendor_type;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'intended_name', 'type', 'email', 'mobile', 'category'], 'required'],
            [['type','category','state_code'], 'integer'],
            [['name', 'intended_name'], 'string', 'max' => 100],
            [['code', 'email',], 'string', 'max' => 50],
            ['email', 'email'],
            ['mobile','number'],
            ['mobile','string','length' => [10,15]],
            [['note'], 'string', 'max' => 200],
            [['address'], 'string', 'max' => 255],
            [['gst_no', 'pan_no', 'bank_ac', 'bank_ac_name', 'bank_ifsc', 'bank_name'], 'string', 'max' => 30],
            [['status'],'default','value'=>1],

            // HTMLPurifier
            [['id','name', 'intended_name', 'type', 'email', 'mobile','code','note','gst_no','pan_no', 'category','status','bank_ac','bank_ac_name','bank_ifsc','bank_name','state_code','address'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process($value);
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'intended_name' => 'Intended Name',
            'code' => 'Code',
            'type' => 'Vendor Type',
            'category' => 'Vendor Category',
            'note' => 'Note',
            'state_code' => 'State',
            'address' => 'Address',
            'gst_no' => 'Gst No',
            'pan_no' => 'Pan No',
            'bank_ac' => 'Account Number',
            'bank_ac_name' => 'Account Name',
            'bank_ifsc' => 'Branch (IFSC)',
            'bank_name' => 'Bank Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
        ];
    }


   public static function VendorList($forAutosuggest=false){
        $data= self::find()->where(['status'=>1])->all();

        if(!empty($forAutosuggest) && !empty($data)){
            $returnArray = [];
            foreach($data as $row){
                $returnArray[$row['id']] = ['id'=>$row['id'],'text'=>$row['name'],'code'=>$row['code']];
            }
            return $returnArray;
        }

        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

    public function getVendorType()
    {
        return $this->hasOne(MasterVendorType::className(), ['id' => 'type']);
    }

    public function getVendorCategory()
    {
        return $this->hasOne(VendorCategory::className(), ['id' => 'category']);
    }



    public static function vendorSuggestions($trip){
        // $origin = City::cityId($trip->origin);
        // $destination = City::cityId($trip->destination);

        $tbl = self::tableName();
        $query = self::find()
            ->select([
                $tbl.'.id',
                $tbl.'.name',
                $tbl.'.intended_name',
                $tbl.'.mobile',
                'vehicle.type_id',
                //'loc.city_id',
                'cat.name as category',
                'type.name as type',
                // 'GROUP_CONCAT(DISTINCT(city.id) SEPARATOR \'|\') locations_id',
            ])
            ->innerJoin('vendor_vehicle as vehicle','vehicle.vendor_id='.$tbl.'.id')
            ->innerJoin('vendor_vehicle_rout_map as loc','loc.vendor_id='.$tbl.'.id')
            //->innerJoin('vendor_location as loc','loc.vendor_id='.$tbl.'.id')
            ->innerJoin('vendor_category as cat','cat.id='.$tbl.'.category')
            ->innerJoin('vendor_type as type','type.id='.$tbl.'.type')
            //->innerJoin('master_city as city','city.id=loc.city_id')
            ->where([$tbl.'.status'=>1])
            ->andWhere(['like', 'loc.vehicle_type_id', $trip->vehicle_type_id])
            ->andFilterWhere([ 'AND', ['like', 'loc.from_loc', $trip->origin],['like', 'loc.to_loc', $trip->destination] ])
            // ->groupBy([$tbl.'.id'])
            ->asArray()->all();
        // echo $query->createCommand()->sql; die();

        // echo "<pre>"; print_r($query); die();
        return $query;
    }


    /* old */
    public static function vendorSuggestionsold($trip){
        $tbl = self::tableName();
        // $orderBy[] =  new \yii\db\Expression("vehicle.type_id = '2'");
        // $orderBy[] =  new \yii\db\Expression("id asc, name desc");
        // $expression = new \yii\db\Expression('field(vehicle.type_id,2)');
        $query = self::find()
            ->select([
                $tbl.'.*',
                'GROUP_CONCAT(DISTINCT(vehicle.type_id) SEPARATOR \'|\') vehicletypes',
                'GROUP_CONCAT(DISTINCT(city.id) SEPARATOR \'|\') locations_id',
                'GROUP_CONCAT(DISTINCT(city.name) SEPARATOR \'|\') locations',
            ])
            ->innerJoin('vendor_vehicle as vehicle','vehicle.vendor_id='.$tbl.'.id')
            ->innerJoin('vendor_location as loc','loc.vendor_id='.$tbl.'.id')
            ->innerJoin('master_city as city','city.id=loc.city_id')
            ->where([$tbl.'.status'=>1])
            ->groupBy([$tbl.'.id'])
            ->limit(10)
            // ->orderBy($expression)
            ->asArray()->all();

        // echo "<pre>";
        // print_r($query); die();
        return $query;
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\VendorSearch */
/* @var $form yii\widgets\ActiveForm */
// 'from_date','to_date','customer'
?>

<div class="vendor-search row">

    <?php $form = ActiveForm::begin([
        'action' => [Url::current()],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3">
      <?= $form->field($model, 'month')->textInput(['class'=>'monthpicker']) ?>
    </div>

    <div class="col-md-3">
      <?php // $form->field($model, 'customer') ?>
    </div>

    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary','data-pjax'=>true]) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="'.Url::canonical(['lg'=>null], true).'"']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\Vehicle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row"><div class="col-md-12"><div class="white-box">
<div class="vehicle-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php
		if(!empty(Yii::$app->request->get('vendor'))){
			$model->vendor_id = Yii::$app->request->get('vendor');
		}
	?>
    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'registration_number')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-6">
        <?= $form->field($model, 'chassi_number')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-6">
        <?= $form->field($model, 'body_number')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-6">
        <?= $form->field($model, 'type_id')->widget(Select2::classname(), [
          'data' => \frontend\modules\master\models\VehicleType::vehicletypelist(),
          'options' => ['placeholder' => 'Select a Vehicle Type ...'],
          'pluginOptions' => [
            'allowClear' => true
          ],
        ]);
        ?>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-6">
        <?= $form->field($model, 'vehicle_owner')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-6">
        <?= $form->field($model, 'vehicle_owner_mobile')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="clearfix"></div>
    </div>


	<?php echo $form->field($model, 'vendor_id')->hiddenInput(['value'=>$model->vendor_id])->label(false);	?>
    <?php //= $form->field($model, 'vendor_id')->textInput() ?>

    <?= $form->field($model, 'status')->hiddenInput(['value'=>1])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>

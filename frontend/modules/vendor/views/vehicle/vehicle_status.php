<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\Vehicle */

$this->title = $model->registration_number;
$this->params['breadcrumbs'][] = ['label' => 'Vehicles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-view">

  	<div class="row">
  	  <div class="col-md-4 col-sm-12">
  	    <section class="white-box">
  	      <div class="user-btm-box p-t-0">
  					<!-- <div class="row  m-t-10"><div class="col-sm-12"><strong>TRIP DETAILS</strong><br/><br/></div></div> -->
  	        <div class="row  m-t-10">
  	          <div class="col-sm-6 b-r"><strong>Registration Number</strong><p><?= $model->registration_number; ?></p></div>
  						<div class="col-sm-6"><strong>Chassi Number</strong><p><?= $model->chassi_number ?></p></div>
  	        </div>
  	        <hr>
            <div class="row  m-t-10">
              <div class="col-sm-6 b-r"><strong>Body Number</strong><p><?= $model->body_number; ?></p></div>
              <div class="col-sm-6"><strong>Type</strong><p><?= !empty($model->vehicleType)?$model->vehicleType->name:'' ?></p></div>
            </div>
            <hr>
            <div class="row  m-t-10">
              <div class="col-sm-6 b-r"><strong>Vehicle Owner</strong><p><?= $model->vehicle_owner ?></p></div>
              <div class="col-sm-6"><strong>Vehicle Owner Mobile</strong><p><?= $model->vehicle_owner_mobile; ?></p></div>
            </div>
            <hr>
            <div class="row  m-t-10">
              <div class="col-sm-6 b-r"><strong>Vendor</strong><p>
                <?php
                if (!empty($model->vendor)) {
                  echo !empty($model->vendor->intended_name)?$model->vendor->intended_name:$model->vendor->name;
                } ?>
              </p></div>
              <div class="col-sm-6"></div>
            </div>
            <hr>
  	      </div>
  	    </section>
  	  </div>



  	  <div class="col-md-8 col-sm-12 ">
  	    <section class="well p-t-10 mob-p-t-0">
  	      <nav class="">
  	        <ul class="nav nav-tabs customtab" role="tablist">
  		          <li class="active"><a data-toggle="tab" href="#trip-multistop" aria-expanded="false">
  								<span class="visible-xs"><i class="mdi mdi-stop-circle"></i></span>
              		<span class="hidden-xs"> Vechile Status</span>
  							</a></li>

  	        </ul>
  	      </nav>

  	      <div class="tab-content">
            <div id="trip-multistop" class="tab-pane fade in active">
              <?php Pjax::begin(['id'=>'pjax-vehicledetails']); ?>
                  <?= $this->render('_vehicle_status_search', ['model' => $searchModel]); ?>
                  <?= GridView::widget([
                    'options' => ['class'=>'table-responsive','style'=>'font-size:12px'],
                    'tableOptions' => ['class'=>'table'],
                    'emptyText' => '<center>No records</center>',
                    'dataProvider' => $dataProvider,
                    'rowOptions'   => function ($model, $key, $index, $grid) {
                      //$GLOBALS['vehicleStatus'] = Trip::vehicleStatus($model['id']);
                      // return [
                      //   'data-id' => $model['id'],
                      //   // 'onclick' => 'location.href="/trip/manage/view?id="+$(this).data("id");',
                      // ];
                    },
                    'layout' => "{items}\n<div align='center'>{pager}</div>",
                    'pager'=>[
                    'linkOptions'=>['class'=>'btn-circle m-r-10'],
                    'disabledListItemSubTagOptions' => ['class' => 'btn btn-circle disable  m-r-10']
                    ],
                    'columns' => [
                      ['class' => 'yii\grid\SerialColumn'],
                      [
                        'attribute'=>'datetime',
                        'label'=>'Date & Time',
                        'format'=>'raw',
                        'value'=>function($model){
                          return date('d-m-Y h:i A',strtotime($model['datetime']));
                        }
                      ],
                      'origin',
                      'destination',
                      'customer_name',
                      [
                        'attribute'=>'trip_event',
                        'label'=>'Status',
                        'format'=>'raw',
                        'value'=>function($model){
                          if (intval($model['trip_event'])) {
                            return \Yii::$app->params['event_status'][$model['trip_event']];
                          }
                          return $model['trip_event'];
                        }
                      ],
                      // 'trip_event',
                      // 'location',
                      // 'trip_remark',
                      [
                        'attribute'=>'trip_date',
                        'label'=>'Trip Date',
                        'format'=>'raw',
                        'value'=>function($model){
                          return date('d-m-Y h:i A',strtotime($model['trip_date']));
                        }
                      ],
                    ]]); ?>
              <?php Pjax::end(); ?>
            </div>

  	      </div>
  	    </section>

  	  </div>
  	</div>


</div>

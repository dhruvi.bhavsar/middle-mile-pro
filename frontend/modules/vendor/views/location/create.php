<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\Location */

$this->title = 'Add/Update Location';
$this->params['breadcrumbs'][] = ['label' => 'Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\Location */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php
		if(!empty(Yii::$app->request->get('vendor'))){
			$model->vendor_id = Yii::$app->request->get('vendor');
		}
	?>
    <?php
	$vendorLocations = \frontend\modules\vendor\models\Location::locationList($model->vendor_id,['city_id']);
	$model->city_id = array_column($vendorLocations,'city_id');
	?>
	<?= $form->field($model, 'city_id')->widget(Select2::classname(), [
		'data' => \frontend\modules\master\models\City::citylist(),
		'options' => ['placeholder' => 'Select a City ...'],
		'pluginOptions' => [
			'allowClear' => true,
			'multiple' => true,
		],
	]);
	?>

	<?= $form->field($model, 'vendor_id')->hiddenInput(['value'=>$model->vendor_id])->label(false); ?>

    <?= $form->field($model, 'status')->hiddenInput(['value'=>1])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['/vendor/manage/index'],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\Location */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
	//$vendorLocations = \frontend\modules\vendor\models\Location::locationList($model->vendor_id,['city_id']);
	//$model->city_id = array_column($vendorLocations,'city_id');
	?>
	<?= $form->field($model, 'vehicle_type_id')->widget(Select2::classname(), [
  		'data' => \frontend\modules\master\models\VehicleType::VehicleTypeList(),
  		'options' => ['placeholder' => 'Select a Vehicle Type ...'],
  		'pluginOptions' => [
  			'allowClear' => true,
  			'multiple' => true,
  		],
  	]);
	?>

  <?= $form->field($model, 'from_loc')->widget(Select2::classname(), [
      'data' => \frontend\modules\master\models\City::citylist(),
      'options' => ['placeholder' => 'Select a City ...'],
      'pluginOptions' => [
        'allowClear' => true,
        'multiple' => true,
      ],
      'pluginEvents'=>[
          "select2:select" => "function() {
            var unselected = $(this.options).not( \":selected\" );
            reinitSelect2(unselected);
            console.log('working');
           }",
          "select2:unselect" => "function() {
            var unselected = $(this.options).not( \":selected\" );
            reinitSelect2(unselected);
            console.log('working');
          }"
        ],

    ]);
  ?>
  <div style="position:relative !important">
    <?= $form->field($model, 'to_loc')->widget(Select2::classname(), [
        //'data' => \frontend\modules\master\models\City::citylist(),
        'options' => ['placeholder' => 'Select a City ...','id'=>'to_list'],
        'pluginOptions' => [
          'allowClear' => true,
          'multiple' => true,
        ],
      ]);
    ?>
  </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>
<?php
  $this->registerJs("
      function reinitSelect2(unselected){
        $('#to_list').html('').select2({data: [{id: '', text: ''}]});
        var length = unselected.length;
        var data=[];
        for(i = 0; i < length; i++){
          data [i]= {'id':unselected[i].value, 'text':unselected[i].text};
        }
         $('#to_list').select2({
            data: data,
            tags: 'true',
            theme: 'krajee',
            width:'100%',
            placeholder:'Select a City ..'
          });
      }
  ");
?>

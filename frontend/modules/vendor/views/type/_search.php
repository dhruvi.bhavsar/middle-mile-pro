<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\MasterVendorTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-vendor-type-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="col-md-3">
        <?= $form->field($model, 'name') ?>
    </div>
    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/vendor/type/index"']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\MasterVendorType */

$this->title = 'New Vendor Type';
$this->params['breadcrumbs'][] = ['label' => 'Vendor Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-vendor-type-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

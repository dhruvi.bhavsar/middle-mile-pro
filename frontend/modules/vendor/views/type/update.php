<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\MasterVendorType */

$this->title = 'Update: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Vendor Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-vendor-type-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

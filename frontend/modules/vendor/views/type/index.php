<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendor Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-vendor-type-index">


    <p>
        <?= Html::a('Add Vendor Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive mobile-table'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}','contentOptions'=>['class' => 'text-center','style'=>'width:90px'],],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Vendor */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="vendor-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'intended_name')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <?= $form->field($model, 'type')->dropDownList(frontend\modules\master\models\MasterVendorType::VendorTypeList(),['prompt'=>"Select Vendor Type"]); ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'category')->dropDownList(frontend\modules\master\models\VendorCategory::VendorCategoryList(),['prompt'=>"Select Vendor Type"]); ?>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'gst_no')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'pan_no')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'bank_ac_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'bank_ac')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'bank_account_type')->dropdownList(['1' =>'Savings','2'=>'Current'])->label("") ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'bank_ifsc')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row">
  <div class="col-md-6">
    <?= $form->field($model, 'state_code')->dropdownList(frontend\modules\master\models\State::gstStateList(),['prompt'=>'Select State']) ?>
  </div>
    <div class="col-md-6">
        <?= $form->field($model, 'address')->textArea(['maxlength' => true,'rows'=>4]) ?>
    </div>
</div>

 <div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'note')->textArea(['maxlength' => true,'rows'=>4]) ?>
    </div>
</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>

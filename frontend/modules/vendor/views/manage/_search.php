<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\vendor\models\VendorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vendor-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3">
      <?= $form->field($model, 'name') ?>
    </div>

    <div class="col-md-3">
      <?= $form->field($model, 'intended_name') ?>
    </div>

    <div class="col-md-2">
      <?= $form->field($model, 'code') ?>
    </div>
    <div class="col-md-2">
      <?= $form->field($model, 'type')->dropDownList(frontend\modules\master\models\MasterVendorType::VendorTypeList(),['prompt'=>"Select Vendor Type"]); ?>
    </div>

    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/vendor/manage/index"']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

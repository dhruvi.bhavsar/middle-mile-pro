<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Vendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-view">

    

    <p>
        <?= Html::a('Cancel', ['index'],['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'intended_name',
            'code',
            [
                'label' => 'Vendor type',
                'value' => $model->vendorType->name
            ],
            'email:email',
            'mobile',
            'note',
            'gst_no',
            'pan_no',
            'bank_ac',
            'bank_ac_name',
            'bank_ifsc',
            'bank_name',

        ],
    ]) ?>

</div>

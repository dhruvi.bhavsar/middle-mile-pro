<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendors';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
    tbody tr {cursor: pointer;}
    tbody tr:hover {cursor: pointer; color:#000;}
    ");
?>
<div class="vendor-index">

    <p>
        <?= Html::a('Add Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
      $dataProvider->setSort([
            'attributes' => [
                'name' => ['asc' => ['name' => SORT_ASC],'desc' => ['name' => SORT_DESC],'default' => SORT_ASC],
                'intended_name' => ['asc' => ['intended_name' => SORT_ASC],'desc' => ['intended_name' => SORT_DESC],'default' => SORT_ASC],
                'code' => ['asc' => ['code' => SORT_ASC],'desc' => ['code' => SORT_DESC],'default' => SORT_ASC],
                'vendor_type' => ['asc' => ['vendor_type' => SORT_ASC],'desc' => ['vendor_type' => SORT_DESC],'default' => SORT_ASC],
              ]
        ]);
     ?>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'rowOptions'   => function ($model, $key, $index, $grid) {
            return [
                'data-id' => $model->id,
                'onclick' => 'location.href="/vendor/manage/view?id="+$(this).data("id");',
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'attribute'=>'name',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Name : </label>'.$model->name;
              }
            ],
            [
              'attribute'=>'intended_name',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Intended Name : </label>'.$model->intended_name;
              }
            ],
            [
              'attribute'=>'code',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Code : </label>'.$model->code;
              }
            ],
            [
              'attribute'=>'vendor_type',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Vendor Type : </label>'.$model->vendor_type;
              }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}  {update}',
            ],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\modules\master\models\City;
use frontend\modules\master\models\VehicleType;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Vendor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-view">

<!--div class="col-lg-12 col-sm-12 col-xs-12"-->
	<div class="white-box">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs customtab" role="tablist">
			<li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Details</span></a></li>
			<li role="presentation" class=""><a href="#vehicles" aria-controls="vehicles" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="mdi mdi-truck"></i></span> <span class="hidden-xs">Vehicles</span></a></li>
			<li role="presentation" class=""><a href="#locations" aria-controls="locations" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="mdi mdi-map-marker"></i></span> <span class="hidden-xs"> Locations</span></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="details">
					<p class="text-muted m-b-40">
        <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?></p>

        <div class="row">
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Name</label><br>
		        <?= $model->name ?>
		    </div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Intended Name</label><br>
		        <?= $model->intended_name ?>
		    </div>
		</div>

		<div class="row">
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">code</label><br>
		        <?= $model->code ?>
		    </div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Vendor Type</label><br>
		        <?= $model->vendorType->name ?>
		    </div>
		</div>

		<div class="row">
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Email</label><br>
		        <?= $model->email ?>
		    </div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Mobile</label><br>
		        <?= $model->mobile ?>
		    </div>
		</div>

		<div class="row">
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Gst No</label><br>
		        <?= $model->gst_no ?>
		    </div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Pan No</label><br>
		        <?= $model->pan_no ?>
		    </div>
		</div>

		<div class="row">
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Account Name</label><br>
		        <?= $model->bank_ac_name ?>
		    </div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Account Number</label><br>
		        <?= $model->bank_ac ?>
                                                    <?php if(!empty($model->bank_account_type)){ ?>
                                                            (<?= Yii::$app->params['bankAccountTypes'][$model->bank_account_type] ?>)
                                                    <?php } ?>
		    </div>
		</div>

		<div class="row">
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Bank Name</label><br>
		        <?= $model->bank_name ?>
		    </div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Branch (IFSC)</label><br>
		        <?= $model->bank_ifsc ?>
		    </div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<?php $gstStateList = frontend\modules\master\models\State::gstStateList(); ?>
				<label class="control-label" for="trip-trip_date">State & GST State Code</label><br><?= !empty($model->state_code)? $gstStateList[$model->state_code].' ('.$model->state_code.')':''; ?>
			</div>
		    <div class="col-md-6">
		    	<label class="control-label" for="trip-trip_date">Adress</label><br>
		        <?= $model->address ?>
		    </div>
		</div>

		 <div class="row">
			 <div class="col-md-6">
				 <label class="control-label" for="trip-trip_date">Vendor Category</label><br>
					 <?= $model->vendorCategory->name ?>
			 </div>
		    <div class="col-md-12">
		    	<label class="control-label" for="trip-trip_date">Note</label><br>
		        <?= $model->note ?>
		    </div>
		</div>

				<div class="clearfix"></div>
			</div>
			<div role="tabpanel" class="tab-pane" id="vehicles">
			<p class="text-muted m-b-40"> <?= Html::a('Add Vehicle', ['/vendor/vehicle/create','vendor' => $model->id], ['class' => 'btn btn-success']) ?> </p>

			<?= GridView::widget([
				'options' => ['class'=>''],
				'tableOptions' => ['class'=>'table table-responsive'],
				'emptyText' => '<center>No records</center>',
				'dataProvider' => $vehicleModel,
				'columns' => [
						['class' => 'yii\grid\SerialColumn'],
					  [
							'attribute' => 'registration_number',
							'format'=>'raw',
							'value' => function($model) {
								return '<label class="hidden-lg hidden-md text-muted">Registration No : </label>'.$model->registration_number;
							}
					  ],
					  [
							'attribute' => 'Vehicle type',
							'format'=>'raw',
							'value' => function($model) {
								return '<label class="hidden-lg hidden-md text-muted">Vehicle Type : </label>'.$model->vehicleType->name;
							}
					  ],
						[
							'attribute' => 'chassi_number',
							'format'=>'raw',
							'value' => function($model) {
								return '<label class="hidden-lg hidden-md text-muted">Chassi No. : </label>'.$model->chassi_number;
							}
						],
						[
							'attribute' => 'body_number',
							'format'=>'raw',
							'value' => function($model) {
								return '<label class="hidden-lg hidden-md text-muted">Body No. : </label>'.$model->body_number;
							}
						],
						[
							'attribute' => 'vehicle_owner',
							'format'=>'raw',
							'value' => function($model) {
								return '<label class="hidden-lg hidden-md text-muted">Vehicle Owner : </label>'.$model->vehicle_owner;
							}
						],
						[
							'attribute' => 'vehicle_owner_mobile',
							'format'=>'raw',
							'value' => function($model) {
								return '<label class="hidden-lg hidden-md text-muted">Owner\'s Mobile : </label>'.$model->vehicle_owner_mobile;
							}
						],
						[
						'class'    => 'yii\grid\ActionColumn',
						'template' => '{update} {delete}',
						'buttons'  => [
						    'update' => function ($url, $model) {
						        $url = Url::to(['/vendor/vehicle/update', 'id' => $model->id,'vendor'=>$model->vendor_id]);

						        return Html::a('<i class="mdi mdi-pencil" style="line-height: 0.8;"></i>', $url, ['title' => 'update','class'=>'btn btn-circle btn-flat waves-effect']);
						    },
						    'delete' => function ($url, $model) {
						        $url = Url::to(['/vendor/vehicle/delete', 'id' => $model->id,'vendor'=>$model->vendor_id]);
						        return Html::a('<i class="mdi mdi-delete text-danger" style="line-height: 0.8;"></i>', $url, [
						            'title'=> 'delete',
												'class'=>'btn btn-circle btn-flat waves-effect',
						            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
						            'data-method'  => 'post',
						        ]);
						    },
							]
						],
				],
			]); ?>
			</div>
			<div role="tabpanel" class="tab-pane" id="locations">
			<p class="text-muted m-b-40"> <?= Html::a('Add / Update Location', ['/vendor/location/create','vendor' => $model->id], ['class' => 'btn btn-success']) ?> </p>
			<?= GridView::widget([
				'options' => ['class'=>''],
				'tableOptions' => ['class'=>'table table-responsive'],
				'emptyText' => '<center>No records</center>',
				'dataProvider' => $locationModel,
				'columns' => [
					['class' => 'yii\grid\SerialColumn'],
					[
						'attribute' => 'From',
						'format'=>'raw',
						'value' => function($model) {
							return '<label class="hidden-lg hidden-md text-muted">From : </label>'.City::findCityById(explode(',',implode(",",json_decode($model->from_loc))));
						}
					],
					[
						'attribute' => 'To',
						'format'=>'raw',
						'value' => function($model) {
							return '<label class="hidden-lg hidden-md text-muted">To : </label>'.City::findCityById(explode(',',implode(",",json_decode($model->to_loc))));
						}
					],
					[
						'attribute' => 'Vehicle Type',
						'format'=>'raw',
						'value' => function($model) {
							return '<label class="hidden-lg hidden-md text-muted">Vehicle Type : </label>'.VehicleType::VehicleTypeId(explode(',',implode(",",json_decode($model->vehicle_type_id))));
						}
					],
					[
					'class'    => 'yii\grid\ActionColumn',
					'template' => '{delete}',
					'buttons'  => [
					    'delete' => function ($url, $model) {
					        $url = Url::to(['/vendor/location/delete', 'id' => $model->id,'vendor'=>$model->vendor_id]);
					        return Html::a('<i class="mdi mdi-delete text-danger" style="line-height: 0.8;"></i>', $url, [
					            'title'        => 'delete',
											'class'=>'btn btn-circle btn-flat waves-effect',
					            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
					            'data-method'  => 'post',
					        ]);
					    },
						]
					],
				],
			]); ?>

			<?php

/*     'update' => function ($url, $model) {
        $url = Url::to(['/vendor/location/update', 'id' => $model->id,'vendor'=>$model->vendor_id]);
        return Html::a('<span class="fa fa-pencil"></span>', $url, ['title' => 'update']);
    }, */
			?>

			</div>
		</div>
	</div>
<!--/div-->



</div>

<?php

namespace frontend\modules\vendor\controllers;

use Yii;
use frontend\modules\vendor\models\Location;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\vendor\models\VendorVehicleRoutMap;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class LocationController extends Controller
{
    public $model_path = 'frontend\modules\vendor\models\Location';

    public function beforeAction($action){
        Yii::$app->commonFunction->accessToAction('vendorLocation');
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Location models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->where(['status'=>1]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Location model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Location model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!empty(Yii::$app->request->get('vendor'))){
            $customer = $this->findVendor(Yii::$app->request->get('vendor'));
        }
        $model = new VendorVehicleRoutMap();
        if(!empty(Yii::$app->request->get('vendor'))){
          $model->vendor_id = Yii::$app->request->get('vendor');
        }
        $model->status = 1;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          // Activity log pending. Need to confirm with omkar sir
          $model->vehicle_type_id = json_encode($model->vehicle_type_id);
          $model->from_loc = json_encode($model->from_loc);
          $model->to_loc = json_encode($model->to_loc);
          if ($model->save()) {
            return $this->redirect(['/vendor/manage/view', 'id' => $model->vendor_id]);
          }
          /* Old code kept for reference
            Location::deleteAll(['vendor_id'=>$params['Location']['vendor_id']]);
            $arrData = array();
            foreach($params['Location']['city_id'] as $k=>$v){
                $arrData[] = [$v,$params['Location']['vendor_id']];
            }
            $vendorModel = new Location();
            Yii::$app->db->createCommand()->batchInsert($vendorModel->tableName(), ['city_id','vendor_id'], $arrData)->execute();
            Yii::$app->activity->add($model->id,$this->model_path,'Vendor Locations Created');
            */

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Location model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$vendor)
    {
        if(!empty(Yii::$app->request->get('vendor'))){
            $customer = $this->findVendor(Yii::$app->request->get('vendor'));
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->activity->add($model->id,$this->model_path,'Vendor Locations Updated');
            return $this->redirect(['/vendor/manage/view', 'id' => $vendor]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Location model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$vendor)
    {
        if(!empty(Yii::$app->request->get('vendor'))){
            $customer = $this->findVendor(Yii::$app->request->get('vendor'));
        }
        $model = VendorVehicleRoutMap::findOne(['id'=>$id]);
        $model->status = 2;
        if($model->save(false)){
            Yii::$app->activity->add($model->id,$this->model_path,'Vendor Locations Deleted');
        }
        return $this->redirect(['/vendor/manage/view', 'id' => $vendor]);
    }

    /**
     * Finds the Location model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Location the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Location::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findVendor($vendor){
        $model = Vendor::findOne(['status'=>1,'id'=>$vendor]);
        if($model !== null){
            return $model;
        }else{
            return $this->redirect('\vendor\manage\index');
        }
    }
}

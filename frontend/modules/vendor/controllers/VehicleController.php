<?php

namespace frontend\modules\vendor\controllers;

use Yii;
use frontend\modules\vendor\models\Vehicle;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\vendor\models\VehicleSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\trip\models\Trip;

/**
 * VehicleController implements the CRUD actions for Vehicle model.
 */
class VehicleController extends Controller
{
    public $model_path = 'frontend\modules\vendor\models\Vehicle';

    public function beforeAction($action){
        Yii::$app->commonFunction->accessToAction('vendorVehicle');
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vehicle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Vehicle::find()->where(['status'=>1]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vehicle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionVehiclestatus($id)
    {
      $this->view->params['backUrl']='/site/index';
      $searchModel = new VehicleSearch();
      $dataProvider = $searchModel->searchVehicleStatus(Yii::$app->request->queryParams,$id);

      return $this->render('vehicle_status', [
          'model' => $this->findModel($id),
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
        ]);
    }

    public function actionValidatekmreading($vehicle_id,$kmreading){
        $vehicle = Vehicle::findOne(['id'=>$vehicle_id]);
        //var_dump($vehicle);die();
        if(!empty($vehicle)){
            if(intval($vehicle->meter_reading) > $kmreading){
                return 0;
            }
            //$vehicle->meter_reading = $kmreading;
            //$vehicle->update(false);
            return 1;
        }
        return 0;
    }
    /**
     * Creates a new Vehicle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!empty(Yii::$app->request->get('vendor'))){
            $customer = $this->findVendor(Yii::$app->request->get('vendor'));
        }
        $model = new Vehicle();
        // $vehicleType = VehicleType::vehicletypelist();
        // echo "<pre>";print_r($model->vehicletypelist); die();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->activity->add($model->id,$this->model_path,'Vendor Vehicle Added');
            return $this->redirect(['/vendor/manage/view', 'id' => $model->vendor_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                // 'vehicleTypeModel' => $vehicleType,
            ]);
        }
    }

    /**
     * Updates an existing Vehicle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$vendor)
    {
        if(!empty(Yii::$app->request->get('vendor'))){
            $customer = $this->findVendor(Yii::$app->request->get('vendor'));
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->activity->add($model->id,$this->model_path,'Vendor Vehicle Updated');
            return $this->redirect(['/vendor/manage/view?id='.$vendor.'#vehicles']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Vehicle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$vendor)
    {
        $model = $this->findModel($id);
        $model->status = 2;
        if($model->save(false)){
            Yii::$app->activity->add($model->id,$this->model_path,'Vendor Vehicle Deleted');
        }
        return $this->redirect(['/vendor/manage/view?id='.$vendor.'#vehicles']);
    }

    /**
     * Finds the Vehicle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vehicle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vehicle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findVendor($vendor){
        $model = Vendor::findOne(['status'=>1,'id'=>$vendor]);
        if($model !== null){
            return $model;
        }else{
            return $this->redirect('\vendor\manage\index');
        }
    }

}

<?php

namespace frontend\modules\vendor;
use Yii;

/**
 * vendor module definition class
 */
class Vendor extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\vendor\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->commonFunction->accessToAction('vendors');
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace frontend\modules\master;
use Yii;

/**
 * master module definition class
 */
class Master extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\master\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->commonFunction->accessToAction('masterSettings');
        parent::init();

        // custom initialization code goes here
    }
}

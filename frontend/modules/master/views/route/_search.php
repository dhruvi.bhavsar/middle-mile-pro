<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\RouteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="route-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3">
      <?= $form->field($model, 'from')->dropDownList(frontend\modules\master\models\City::CityList(),['prompt'=>"From"]); ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'to')->dropDownList(frontend\modules\master\models\City::CityList(),['prompt'=>"To"]); ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'via') ?>
    </div>


    <?php // echo $form->field($model, 'status') ?>
    <div class="form-group m-t-20 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/master/route/index"']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

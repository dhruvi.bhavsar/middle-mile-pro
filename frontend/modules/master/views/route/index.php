<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Routes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-index">



    <p>
        <?= Html::a('Add Route', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
      $dataProvider->setSort([
            'attributes' => [
                'From' => ['asc' => ['from_city' => SORT_ASC],'desc' => ['from_city' => SORT_DESC],'default' => SORT_ASC],
                'To' => ['asc' => ['to_city' => SORT_ASC],'desc' => ['to_city' => SORT_DESC],'default' => SORT_ASC],
                'via' => ['asc' => ['via' => SORT_ASC],'desc' => ['via' => SORT_DESC],'default' => SORT_ASC],
              ]
        ]);
     ?>

    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'From',
                'format'=>'raw',
                'value' => function($model) {
                    return '<label class="hidden-lg hidden-md text-muted">From : </label>'.$model->from_city;
                }
            ],
            [
                'attribute' => 'To',
                'format'=>'raw',
                'value' => function($model) {
                    return '<label class="hidden-lg hidden-md text-muted">To : </label>'.$model->to_city;
                }
            ],
            [
                'attribute' => 'via',
                'format'=>'raw',
                'value' => function($model) {
                    return '<label class="hidden-lg hidden-md text-muted">Via : </label>'.$model->via;
                }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Route */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="route-form">

    <?php $form = ActiveForm::begin(); ?>

   <?= $form->field($model, 'from')->dropDownList(frontend\modules\master\models\City::CityList(),['prompt'=>"From"]); ?>

   <?= $form->field($model, 'to')->dropDownList(frontend\modules\master\models\City::CityList(),['prompt'=>"To"]); ?>

    <?= $form->field($model, 'via')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>

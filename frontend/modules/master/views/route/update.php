<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Route */

$this->title = 'Update Route: ';
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="route-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

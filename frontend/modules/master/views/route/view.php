<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Route */

$routeText = $model->fromCity->name . " - " . $model->toCity->name;
$this->title = $routeText;
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-view">

    <p>
        <?= Html::a('Back', ['index'],['class' => 'btn btn-default']) ?>
    </p>

	
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<h3><?= Html::encode($this->title) ?> 
				<span class="pull-right">
					<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0']) ?>
					<?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], ['title'=>'Delete','aria-label'=>'Delete','data-pjax'=>'0','data-confirm'=>'Are you sure you want to delete this item?','data-method'=>'post']) ?>
				</span></h3>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<label class="control-label" style="font-weight:bold !important;">From : </label><?= $model->fromCity->name; ?>
					</div>
					<div class="col-md-6">
						<label class="control-label" style="font-weight:bold !important;">To : </label><?= $model->toCity->name; ?>
					</div>
					<div class="col-md-6">
						<label class="control-label" style="font-weight:bold !important;">Via : </label><?= $model->via; ?>
					</div>
					
				</div>	
			</div>
		</div>
	</div>	

</div>

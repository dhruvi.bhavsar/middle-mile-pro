<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\Route */

$this->title = 'Add Route';
$this->params['breadcrumbs'][] = ['label' => 'Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

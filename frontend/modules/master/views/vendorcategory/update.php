<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\VendorCategory */

$this->title = 'Update: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vendor Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vendor-category-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

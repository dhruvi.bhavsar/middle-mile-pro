<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\VendorCategory */

$this->title = 'New Vendor Category';
$this->params['breadcrumbs'][] = ['label' => 'Vendor Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-category-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendor Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-category-index">



    <p>
        <?= Html::a('Create Vendor Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
              'attribute'=>'order',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Sorting Order : </label>'.$model->order;
              }
            ],
            [
			'class' => 'yii\grid\ActionColumn',
			'template'=>'{update}{delete}',
			],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehicle Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-type-index">



    <p>
        <?= Html::a('Add Vehicle Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            [
                'label' => 'Load trip rate/km',
                'value' => function ($model) {
                    return $model->load_rate;
                }
            ],
            [
                'label' => 'Ferry trip rate/km',
                'value' => function ($model) {
                    return $model->ferry_rate;
                }
            ],
            // 'status',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',],
        ],
    ]); ?>
</div>

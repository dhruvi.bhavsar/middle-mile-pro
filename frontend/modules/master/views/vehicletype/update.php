<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\VehicleType */

$this->title = 'Update Vehicle Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vehicle-type-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\VehicleType */

$this->title = 'Add Vehicle Type';
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-type-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

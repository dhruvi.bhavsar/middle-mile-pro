<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\MasterStop */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Stops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-stop-view">

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<h3><?= Html::encode($this->title) ?>
				<span class="pull-right">
					<?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], ['title'=>'Update','aria-label'=>'Update','data-pjax'=>'0']) ?>
					<?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], ['title'=>'Delete','aria-label'=>'Delete','data-pjax'=>'0','data-confirm'=>'Are you sure you want to delete this item?','data-method'=>'post']) ?>
				</span></h3>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<label class="control-label" style="font-weight:bold !important;">Name : </label><?= $model->name; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label class="control-label" style="font-weight:bold !important;">lat : </label><?= $model->lat; ?>
					</div>
					<div class="col-md-6">
						<label class="control-label" style="font-weight:bold !important;">lon : </label><?= $model->lon; ?>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>

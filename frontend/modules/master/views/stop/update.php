<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\MasterStop */

$this->title = 'Update Master Stop: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Master Stops', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-stop-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

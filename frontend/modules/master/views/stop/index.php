<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Stops';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-stop-index">



    <p>
        <?= Html::a('Add Master Stop', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            [
              'attribute'=>'lat',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Lat : </label>'.$model->lat;
              }
            ],
            [
              'attribute'=>'lon',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Lon : </label>'.$model->lon;
              }
            ],
            // 'status',

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
        ],
    ]); ?>
</div>

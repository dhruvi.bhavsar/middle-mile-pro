<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\MasterStop */

$this->title = 'Add Master Stop';
$this->params['breadcrumbs'][] = ['label' => 'Master Stops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-stop-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

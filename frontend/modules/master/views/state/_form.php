<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\State */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row"><div class="col-md-12"><div class="white-box">
<div class="state-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'gst_state_code')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'zone')->dropdownList(\Yii::$app->params['zone'],['prompt'=>'Select Zone']) ?>
      </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel',Yii::$app->request->referrer,['class' => 'btn btn-default']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
</div></div></div>

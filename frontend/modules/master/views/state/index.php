  <?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'States';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="state-index">



    <p>
        <?= Html::a('Add State', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="white-box m-b-5">
      <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?= GridView::widget([
		'options' => ['class'=>'white-box table-responsive'],
		'tableOptions' => ['class'=>'table'],
		'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
              'attribute'=>'gst_state_code',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">State Code : </label>'.$model->gst_state_code;
              }
            ],
            [
              'attribute'=>'zone',
              'format'=>'raw',
              'value'=>function($model){
                return '<label class="hidden-lg hidden-md text-muted">Zone : </label>'.(empty($model['zone'])?'':\Yii::$app->params['zone'][$model['zone']]);
              }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\City */

$this->title = 'Add City';
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\master\models\CitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="col-md-3">
      <?= $form->field($model, 'name') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'pincode') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'state_id')->dropDownList(frontend\modules\master\models\State::StateList(),['prompt'=>"Select State"]); ?>
    </div>
    <div class="form-group m-t-20 pull-right">
      <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
      <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/master/city/index"']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

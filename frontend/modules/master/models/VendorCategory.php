<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "vendor_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order
 * @property integer $status
 */
class VendorCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'order'], 'required'],
            [['order', 'status'], 'integer'],
            [['name'], 'string', 'max' => 200],

            // HTMLPurifier
            [['id','name','order','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Title',
            'order' => 'Sorting Order',
            'status' => 'Status',
        ];
    }

    public static function VendorCategoryList(){
        $data= self::find()->where(['status'=>1])->orderBy(['order'=>SORT_ASC])->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }
}

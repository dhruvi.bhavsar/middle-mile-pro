<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "route_lane".
 *
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property string $via
 * @property integer $status
 */
class Route extends \yii\db\ActiveRecord
{

    public $from_city;
    public $to_city;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'route_lane';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            ['to', function ($attribute, $params, $validator) {
                if ($this->from == $this->to) {
                    $this->addError($attribute, 'From and To cannot be same.');
                }
            }],

            [['from', 'to', 'status'], 'integer'],
            [['via'], 'string', 'max' => 100],

            // HTMLPurifier
            [['id','from', 'via','to','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'via' => 'Via',
            'status' => 'Status',
        ];
    }

    function updateRouteName(){
        $this->name = $this->fromCity->name . " - " . $this->toCity->name;
        if(!empty($this->via)){
            $this->name = $this->name . " via " . $this->via;
        }
    }

    public static function RouteList(){
        $data= self::find()->where(['status'=>1])->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

    public function getFromCity(){
        return $this->hasOne(City::className(), ['id' => 'from']);
     }

    public function getToCity(){
        return $this->hasOne(City::className(), ['id' => 'to']);
     }


}

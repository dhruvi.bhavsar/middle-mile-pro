<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "stop_master".
 *
 * @property integer $id
 * @property string $name
 * @property double $lat
 * @property double $lon
 * @property integer $status
 */
class MasterStop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stop_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['lat', 'lon'], 'number'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],

            // HTMLPurifier
            [['id','name','lat','lon','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'status' => 'Status',
        ];
    }

    public static function stopList(){
        $data = self::find()->where(['status'=>1])->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }
}

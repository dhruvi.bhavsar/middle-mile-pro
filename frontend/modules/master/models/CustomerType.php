<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "customer_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class CustomerType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 200],

            // HTMLPurifier
            [['id','name','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    public static function CustomerTypeList(){
        $data= self::find()->where(['status'=>1]);
        $data = $data->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }
}

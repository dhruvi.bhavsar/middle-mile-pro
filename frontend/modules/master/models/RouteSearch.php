<?php

namespace frontend\modules\master\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\master\models\Route;
use frontend\modules\master\models\City;
/**
 * RouteSearch represents the model behind the search form about `frontend\modules\master\models\Route`.
 */
class RouteSearch extends Route
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from', 'to', 'status'], 'safe'],
            [['name', 'via'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Route::find()->from([Route::tableName().' R'])
                      ->select(['R.*','F.name from_city','T.name to_city'])
                      ->leftJoin(City::tableName().' F','F.id = R.from')
                      ->leftJoin(City::tableName().' T','T.id = R.to')
                      ->where(['R.status'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'R.id' => $this->id,
            'R.from' => $this->from,
            'R.to' => $this->to,
            'R.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'R.name', $this->name])
            ->andFilterWhere(['like', 'R.via', $this->via]);

        return $dataProvider;
    }
}

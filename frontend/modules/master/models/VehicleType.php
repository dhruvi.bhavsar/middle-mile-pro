<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "vehicle_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class VehicleType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status','load_rate','ferry_rate'], 'integer'],
            [['name'], 'string', 'max' => 200],

            // HTMLPurifier
            [['id','name','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    public static function VehicleTypeList(){
        $data= self::find()->where(['status'=>1])->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }
    public static function VehicleTypeId($ids){
        $data= self::find()
            ->select(['id','name'])
            ->where(['id'=>$ids])
            ->asArray()->all();
        $data = \yii\helpers\ArrayHelper::map($data, 'id', 'name');
        return implode(', ',$data);
    }
}

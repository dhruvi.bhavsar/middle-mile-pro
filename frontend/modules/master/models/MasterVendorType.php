<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "master_vendor_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class MasterVendorType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 200],

            // HTMLPurifier
            [['id','name','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Title',
            'status' => 'Status',
        ];
    }


    public static function VendorTypeList(){
        $data= self::find()->where(['status'=>1])->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

}

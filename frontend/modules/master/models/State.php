<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "states".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status','gst_state_code'], 'integer'],
            [['name','zone'], 'string', 'max' => 20],

            // HTMLPurifier
            [['id','name', 'gst_state_code','zone','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'gst_state_code' => 'State Code(GST)',
            'zone' => 'Zone',
            'status' => 'Status',
        ];
    }

     public static function StateList(){
        $data= self::find()->where(['status'=>1])->asArray()->all();
        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

    public static function gstStateList(){
       $data= self::find()->where(['status'=>1])->asArray()->all();
       return \yii\helpers\ArrayHelper::map($data,'gst_state_code','name');
   }

     public static function getZoneFromStateId(){
        $zone = \Yii::$app->params['zone'];
        $query = self::find()->select(['gst_state_code','zone'])->where(['status'=>1])->asArray()->all();
        $data = [];
        foreach ($query as $state) {
          $data[$state['gst_state_code']] = !empty($state['zone'])?$zone[$state['zone']]: NULL; // $zone[$state['zone']]
        }
        return $data;
    }
}

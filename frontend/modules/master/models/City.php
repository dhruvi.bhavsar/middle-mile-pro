<?php

namespace frontend\modules\master\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property integer $pincode
 * @property integer $state_id
 * @property integer $status
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'state_id','pincode'], 'required'],
            [[ 'state_id', 'status'], 'integer'],
            [['name'], 'string', 'max' => 50],

            // HTMLPurifier
            [['id','name', 'state_id','pincode','status'],
              'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                  $value = json_encode($value,true);
                }
                return \yii\helpers\HtmlPurifier::process(trim($value));
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'pincode' => 'Pincode',
            'state_id' => 'State',
            'status' => 'Status',
        ];
    }


    public function getState(){
        return $this->hasOne(State::className(), ['id' => 'state_id']);
     }


     public static function CityList($params=[]){
        $data= self::find()->where(['status'=>1]);

        if(isset($params['vendor_id'])){
            $vendorLocations = \frontend\modules\vendor\models\Location::locationList($params['vendor_id'],['id']);
            // var_dump(array_column($vendorLocations,'id')); die();
            $data = $data->andWhere(['NOT IN','id',array_column($vendorLocations,'id')]);

        }
        // echo $data->createCommand()->sql; die();
        $data = $data->all();

        return \yii\helpers\ArrayHelper::map($data,'id','name');
    }

    public static function cityId($city){
        $data= self::find()
            ->select(['id'])
            ->where(['status'=>1])
            ->andFilterWhere(['like', 'name', $city])->asArray()->all();
        return $data[0]['id'];
    }

    public static function findCityById($city){
        $data= self::find()
            ->select(['id','name'])
            ->where(['id'=>$city])
            ->asArray()->all();
        $data = \yii\helpers\ArrayHelper::map($data, 'id', 'name');
        return implode(', ',$data);
    }

}

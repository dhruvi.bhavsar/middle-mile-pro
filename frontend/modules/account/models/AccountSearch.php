<?php

namespace frontend\modules\account\models;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use frontend\modules\trip\models\Trip;
use frontend\modules\vendor\models\Vendor;
use frontend\modules\customer\models\Customer;
use frontend\modules\trip\models\Transactions;
/**
 * AccountSearch represents the model behind the search form about `frontend\modules\trip\models\Trip`.
 */
class AccountSearch extends Trip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vendor_id', 'company_id', 'contact_person_id', 'vehicle_id', 'vehicle_type_id', 'buying_rate_incl_handling_charges', 'trip_type', 'service_type', 'status'], 'integer'],
            [['number', 'vendor_bids', 'origin', 'loading_point', 'destination', 'unloading_point', 'trip_date', 'recipients', 'created_at', 'updated_at', 'negotiation_remark', 'cancellation', 'remarks', 'pod_receival_date', 'final_bill_date', 'last_notified_at'], 'safe'],
            [['targated_rate', 'targated_buying', 'buying_rate', 'advance_buying_charges', 'selling_rate', 'advance_selling_charges', 'origin_handling_charges', 'destination_handling_charges', 'origin_lat', 'origin_lon', 'destination_lat', 'destination_lon', 'notification_duration'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function searchBalanceReceivablesPayables($params)
    {
        $query = Trip::find()->from([Trip::tableName().' T'])
                    ->select([
                            'T.number','T.id',
                            'T.buying_rate','T.advance_buying_charges',
                            'T.selling_rate','T.advance_selling_charges',
                            'T.buying_rate_incl_handling_charges',
                            'T.origin_handling_charges','T.destination_handling_charges',
                            'T.pod_receival_date',
                            'C.name customer',
                            'IF(V.intended_name IS NULL,V.name,V.intended_name) vendor',
                            'DATE_FORMAT((T.pod_receival_date + INTERVAL C.credit_period day),"%d-%m-%Y") due_date',
                            '(SELECT IF(SUM(amount) IS NULL,0,SUM(amount)) FROM transactions WHERE trip_id = T.id AND status = 1 AND transaction_type = 2) paid_amount',
                          ])
                    ->innerJoin(Vendor::tableName().' V','V.id = T.vendor_id')
                    ->innerJoin(Customer::tableName().' C','C.id = T.company_id')
                    ->andWhere(['NOT',['T.pod_receival_date'=>NULL]])
                    ->andWhere('T.pod_receival_date <= NOW() - INTERVAL C.credit_period day');
        // add conditions that should always apply here
        // var_dump($query->createCommand()->sql); die();

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'T.id' => $this->id,
            'T.vendor_id' => $this->vendor_id,
            'T.company_id' => $this->company_id,
            'T.contact_person_id' => $this->contact_person_id,
            'T.vehicle_id' => $this->vehicle_id,
            'T.vehicle_type_id' => $this->vehicle_type_id,
            'T.targated_rate' => $this->targated_rate,
            'T.targated_buying' => $this->targated_buying,
            'T.buying_rate' => $this->buying_rate,
            'T.advance_buying_charges' => $this->advance_buying_charges,
            'T.selling_rate' => $this->selling_rate,
            'T.advance_selling_charges' => $this->advance_selling_charges,
            'T.origin_handling_charges' => $this->origin_handling_charges,
            'T.destination_handling_charges' => $this->destination_handling_charges,
            'T.buying_rate_incl_handling_charges' => $this->buying_rate_incl_handling_charges,
            'T.trip_type' => $this->trip_type,
            'T.origin_lat' => $this->origin_lat,
            'T.origin_lon' => $this->origin_lon,
            'T.destination_lat' => $this->destination_lat,
            'T.destination_lon' => $this->destination_lon,
            'T.service_type' => $this->service_type,
            'T.trip_date' => $this->trip_date,
            'T.status' => $this->status,
            'T.created_at' => $this->created_at,
            'T.updated_at' => $this->updated_at,
            'T.notification_duration' => $this->notification_duration,
            'T.pod_receival_date' => $this->pod_receival_date,
            'T.final_bill_date' => $this->final_bill_date,
            'T.last_notified_at' => $this->last_notified_at,
        ]);

        $query->andFilterWhere(['like', 'T.number', $this->number])
            ->andFilterWhere(['like', 'T.vendor_bids', $this->vendor_bids])
            ->andFilterWhere(['like', 'T.origin', $this->origin])
            ->andFilterWhere(['like', 'T.loading_point', $this->loading_point])
            ->andFilterWhere(['like', 'T.destination', $this->destination])
            ->andFilterWhere(['like', 'T.unloading_point', $this->unloading_point])
            ->andFilterWhere(['like', 'T.recipients', $this->recipients])
            ->andFilterWhere(['like', 'T.negotiation_remark', $this->negotiation_remark])
            ->andFilterWhere(['like', 'T.cancellation', $this->cancellation])
            ->andFilterWhere(['like', 'T.remarks', $this->remarks]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            'sort'=> [
                'attributes' => [
                                  'number'=>'number',
                                  'vendor'=>'vendor',
                                  'customer'=>'customer',
                                  'selling_rate'=>'selling_rate',
                                  'advance_selling_charges'=>'advance_selling_charges',
                                  'buying_rate'=>'buying_rate',
                                  'advance_buying_charges'=>'advance_buying_charges',
                                  'origin_handling_charges'=>'origin_handling_charges',
                                  'destination_handling_charges'=>'destination_handling_charges',
                                  'paid_amount'=>'paid_amount',
                                  //'due_amount'=>'due_amount',
                                  'pod_receival_date'=>'pod_receival_date',
                                  'due_date'=>'due_date',
                                ],
                'defaultOrder' => ['pod_receival_date'=>SORT_ASC,]
              ],
        ]);

        return $dataProvider;
    }


    public function searchAdvanceReceivablesPayables($params,$payable = NULL)
    {
        $query = Trip::find()->from([Trip::tableName().' T'])
                    ->select([
                            'T.number','T.id',
                            'T.buying_rate',
                            'T.advance_buying_charges',
                            'T.selling_rate',
                            'T.advance_selling_charges',
                            'C.name customer',
                            'trip_date',
                            'IF(V.intended_name IS NULL,V.name,V.intended_name) vendor',
                          ])
                    ->innerJoin(Vendor::tableName().' V','V.id = T.vendor_id')
                    ->innerJoin(Customer::tableName().' C','C.id = T.company_id')
                    ->andWhere(['NOT',['V.id'=>NULL]]);
                    // ->andWhere(['T.pod_receival_date'=>NULL])
        if (!empty($payable)) {

           $query = $query->leftJoin(Transactions::tableName() . ' TR','TR.trip_id = T.id')
                          ->addSelect([  'SUM(IF(transaction_type = '.Transactions::PAID.' AND TR.status='.Transactions::STATUS_ACTIVE.' ,amount,0)) AS paid_amount',
                                                    'SUM(IF(transaction_type = '.Transactions::DEBIT.' AND TR.status='.Transactions::STATUS_ACTIVE.' ,amount,0)) AS debit',
                                                    'SUM(IF(applicable_for = 1 AND transaction_type = '.Transactions::MAMUL.' AND TR.status='.Transactions::STATUS_ACTIVE.' ,amount,0)) AS mamu',

                                                    'SUM(IF(applicable_for = 1 AND TR.approval = '.Transactions::APPROVED.' AND  TR.status='.Transactions::STATUS_PENDING.' ,amount,0)) AS account_status_pending',
                                                    'SUM(if(applicable_for = 1 AND TR.approval = '.Transactions::APPROVED.' AND TR.status='.Transactions::STATUS_ACTIVE.' AND transaction_type in (6,7,8,9,10,11,12,13,14) ,amount,0))  AS vendor_ancillary_charges']);
           $query = $query->groupBy("TR.trip_id");

        } else{
            $query = $query->leftJoin(Transactions::tableName() . ' TR','TR.trip_id = T.id')
                            ->addSelect([ 'SUM(IF(transaction_type in  ('.Transactions::RECEIVED.','.Transactions::ADVANCE_RECEIVED.') AND TR.status='.Transactions::STATUS_ACTIVE.',amount,0)) AS received_amount',
                                                    'SUM(IF(transaction_type = '.Transactions::ADVANCE_RECEIVED.' AND TR.status='.Transactions::STATUS_ACTIVE.' ,amount,0)) AS adv_received',
                                                    'SUM(IF(transaction_type = '.Transactions::WRITE_OFF.' AND TR.status='.Transactions::STATUS_ACTIVE.' ,amount,0)) AS write_off',
                                                    'SUM(IF(applicable_for = 2 AND  TR.status='.Transactions::STATUS_PENDING.' ,amount,0)) AS account_status_pending',
                                                    'SUM(if(applicable_for = 2 AND TR.status='.Transactions::STATUS_ACTIVE.' AND transaction_type in (6,7,8,9,10,11,12,13,14) ,amount,0))  AS receivable_amount',]);
            
            $query = $query->groupBy("TR.trip_id");
        }

        // add conditions that should always apply here
        // var_dump($query->createCommand()->sql); die();
        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'T.id' => $this->id,
            'T.vendor_id' => $this->vendor_id,
            'T.company_id' => $this->company_id,
            'T.contact_person_id' => $this->contact_person_id,
            'T.vehicle_id' => $this->vehicle_id,
            'T.vehicle_type_id' => $this->vehicle_type_id,
            'T.targated_rate' => $this->targated_rate,
            'T.targated_buying' => $this->targated_buying,
            'T.buying_rate' => $this->buying_rate,
            'T.advance_buying_charges' => $this->advance_buying_charges,
            'T.selling_rate' => $this->selling_rate,
            'T.advance_selling_charges' => $this->advance_selling_charges,
            'T.origin_handling_charges' => $this->origin_handling_charges,
            'T.destination_handling_charges' => $this->destination_handling_charges,
            'T.buying_rate_incl_handling_charges' => $this->buying_rate_incl_handling_charges,
            'T.trip_type' => $this->trip_type,
            'T.origin_lat' => $this->origin_lat,
            'T.origin_lon' => $this->origin_lon,
            'T.destination_lat' => $this->destination_lat,
            'T.destination_lon' => $this->destination_lon,
            'T.service_type' => $this->service_type,
            'T.trip_date' => $this->trip_date,
            'T.status' => $this->status,
            'T.created_at' => $this->created_at,
            'T.updated_at' => $this->updated_at,
            'T.notification_duration' => $this->notification_duration,
            'T.pod_receival_date' => $this->pod_receival_date,
            'T.final_bill_date' => $this->final_bill_date,
            'T.last_notified_at' => $this->last_notified_at,
        ]);

        $query->andFilterWhere(['like', 'T.number', $this->number])
            ->andFilterWhere(['like', 'T.vendor_bids', $this->vendor_bids])
            ->andFilterWhere(['like', 'T.origin', $this->origin])
            ->andFilterWhere(['like', 'T.loading_point', $this->loading_point])
            ->andFilterWhere(['like', 'T.destination', $this->destination])
            ->andFilterWhere(['like', 'T.unloading_point', $this->unloading_point])
            ->andFilterWhere(['like', 'T.recipients', $this->recipients])
            ->andFilterWhere(['like', 'T.negotiation_remark', $this->negotiation_remark])
            ->andFilterWhere(['like', 'T.cancellation', $this->cancellation])
            ->andFilterWhere(['like', 'T.remarks', $this->remarks]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            'sort'=> [
                'attributes' => [
                                  'trip_date'=>'trip_date',
                                  'number'=>'number',
                                  'vendor'=>'vendor',
                                  'customer'=>'customer',
                                  'selling_rate'=>'selling_rate',
                                  'advance_selling_charges'=>'advance_selling_charges',
                                  'paid_amount'=>'paid_amount',
                                  'received_amount'=>'received_amount',
                                  'buying_rate'=>'buying_rate',
                                  'advance_buying_charges'=>'advance_buying_charges',
                                  'origin_handling_charges'=>'origin_handling_charges',
                                  'destination_handling_charges'=>'destination_handling_charges',
                                  //'due_amount'=>'due_amount',
                                  'pod_receival_date'=>'pod_receival_date',
                                  'due_date'=>'due_date',
                                ],
                'defaultOrder' => ['pod_receival_date'=>SORT_ASC,]
              ],
        ]);

        return $dataProvider;
    }

}

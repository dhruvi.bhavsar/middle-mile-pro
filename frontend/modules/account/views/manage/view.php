<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Back', ['index'],['class' => 'btn btn-default']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'number',
            'vendor_id',
            'company_id',
            'contact_person_id',
            'vehicle_id',
            'vehicle_type_id',
            'vendor_bids:ntext',
            'targated_rate',
            'targated_buying',
            'buying_rate',
            'advance_buying_charges',
            'selling_rate',
            'advance_selling_charges',
            'origin_handling_charges',
            'destination_handling_charges',
            'buying_rate_incl_handling_charges',
            'trip_type',
            'origin',
            'origin_lat',
            'origin_lon',
            'loading_point',
            'destination',
            'destination_lat',
            'destination_lon',
            'unloading_point',
            'service_type',
            'trip_date',
            'recipients',
            'status',
            'created_at',
            'updated_at',
            'negotiation_remark:ntext',
            'cancellation:ntext',
            'remarks:ntext',
            'notification_duration',
            'pod_receival_date',
            'final_bill_date',
            'last_notified_at',
        ],
    ]) ?>

</div>

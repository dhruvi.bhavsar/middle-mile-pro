<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\trip\models\Transactions;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\account\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receivable';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
  <div class="trip-index">
      <div class="white-box">
        <?= $this->render('_receivable_search', ['model' => $searchModel]); ?>
      </div>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'attribute' => 'trip_date',
                'format'=>'raw',
                'value' =>function($model) {
                  return '<label class="hidden-lg hidden-md text-muted">Trip Date : </label>'.date('d-m-Y',strtotime($model['trip_date']));
                }
              ],
              [
                'label' => 'Order ID',
                'attribute'=>'number',
                'format'=>'raw',
                'value' =>function($model) {
                  return '<label class="hidden-lg hidden-md text-muted">Order ID : </label><a href="/trip/manage/view?id=' . $model['id'] . '">'.$model['number'] . '</a>';
                }
              ],
              [
                'attribute'=>'customer',
                'format'=>'raw',
                'value' =>function($model) {
                  return '<label class="hidden-lg hidden-md text-muted">Customer : </label>'.$model['customer'];
                }
              ],
              // 'vendor', '<label class="hidden-lg hidden-md">Received : </label>'.
              [
                'label' => 'Selling Rate',
                'format'=>'raw',
                'attribute'=>'selling_rate',
                'value'=>function($model){return '<label class="hidden-lg hidden-md text-muted">Selling Rate : </label>'.((float)$model['selling_rate']);}
              ],
              [
                'label' => 'Advance Receivable',
                'format'=>'raw',
                'attribute'=>'advance_selling_charges',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Advance receivable: </label>'.
                      ((float)$model['advance_selling_charges']);
                }
              ],
              [
                'label' => 'Received',
                'attribute'=>'received_amount',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md">Received : </label>'.((float)$model['received_amount']);
                  return $text;
                 // return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
              [
                'label' => 'Advance Pending',
                'attribute'=>'adv_received',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md">Advance Pending : </label>'. ((float)$model['advance_selling_charges'] - (float)$model['adv_received']);
                  return $text;
                 // return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
              [
                'label' => 'Account status pending',
                'attribute'=>'account_status_pending',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md">Account status pending : </label>'.((float)$model['account_status_pending']);

                  if(Yii::$app->user->identity->team == 5 || Yii::$app->user->identity->role >=9) {
                    $pendingTransactions = Transactions::tripTransactionDetails($model['id'],[],['status'=>0,'applicable_for'=>2])->asArray()->all();
                    if(!empty($pendingTransactions)){
                        $content = "";
                        foreach($pendingTransactions as $transaction){
                            $content .= "<p><small>" . \Yii::$app->params['transaction_type'][$transaction['transaction_type']] . "</small> - " . (float)$transaction['amount'] . "  " .
                                                  Html::a('Confirm','/trip/manage/confirm-transaction?id='.$transaction['id'],
                                                                  ['title' => 'Confirm','class'=>'btn btn-success btn-xs waves-effect','data-method'=>'POST',
                                                                  'data-confirm'=>'Are you sure you checked transaction in bank record ?']) .
                                                 "<br /><small>".$transaction['note']."</small></p><hr />";
                        }
                        $text.=' <a tabindex="0" class="btn btn-xs btn-info" role="button" data-toggle="popover" data-trigger="focus" data-html="true" title="Update transaction" data-placement="left" data-content=\'' . $content . '\'>Update</a>';
                    }
                  }

                  return $text;
//                  return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
//              [
//                'label' => 'Ancillary charges',
//                'attribute'=>'receivable_amount',
//                'format'=>'raw',
//                'value'=>function($model){
//                  $text = '<label class="hidden-lg hidden-md">Ancillary charges  : </label>'.((float)$model['receivable_amount']);
//                  return $text; // Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
//                }
//              ],
//              [
//                'label' => 'Write Off',
//                'attribute'=>'write_off',
//                'format'=>'raw',
//                'value'=>function($model){
//                  $transations = Transactions::tripTransactionDetails($model['id'],Transactions::WRITE_OFF)->asArray()->all();
//                  $text = '<label class="hidden-lg hidden-md">Write Off : </label>'.((float)$model['write_off'].'<span class="jsonData hidden" hidden>'.json_encode($transations,true).'</span>');
//                  return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
//                }
//              ],
//              [
//                'attribute' => 'Balance',
//                'format'=>'raw',
//                'value' =>function($model) {
//                  $amount = $model['selling_rate'] + $model['receivable_amount'] - $model['received_amount'] - $model['write_off'];
//                  return '<label class="hidden-lg hidden-md text-muted">Balance : </label>'.(($amount > 0)?$amount:0);
//                }
//              ],
          ],
      ]); ?>
  </div>
<?php Pjax::end(); ?>
<?= $this->render('@frontend/modules/trip/views/manage/_modalTransactions'); ?>

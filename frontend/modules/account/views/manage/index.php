<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\trip\models\Transactions;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\account\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Balance Payable';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
  <div class="trip-index">
      <div class="white-box">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
      </div>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              'due_date',
              [
                'label' => 'Order ID',
                'attribute'=>'number',
              ],
              // 'customer',
              'vendor',
              [
                 'attribute' => 'buying_rate',
                 'format' => 'html',
                 'value' => function($model) {
                    return ((float)$model['buying_rate']);
                 }
              ],
              [
                'label' => 'Advance',
                'attribute'=>'advance_buying_charges',
                'format'=>'html',
                'value'=>function($model){
                  return (float)$model['advance_buying_charges'].(((float)$model['advance_buying_charges']<=(float)$model['paid_amount'])?'<i class="mdi mdi-check text-success"></i>':'');
                }
              ],
              [
                'attribute'=>'origin_handling_charges',
                'value'=>function($model){return (float)$model['origin_handling_charges'];}
              ],
              [
                'attribute'=>'destination_handling_charges',
                'value'=>function($model){return (float)$model['destination_handling_charges'];}
              ],
              [
                'label' => 'Paid',
                'attribute'=>'paid_amount',
                'format'=>'raw',
                'value'=>function($model){
                  $transations = Transactions::tripTransactionDetails($model['id'],Transactions::TRANSACTION_TYPE_PAID)->asArray()->all();
                  $text = (float)$model['paid_amount'].'<span class="jsonData hidden" hidden>'.json_encode($transations,true).'</span>';
                  return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
              [
                 'attribute' => 'Balance',
                 'value' => function($model) {
                    $totalTransation = frontend\modules\trip\models\Transactions::tripTransactions($model['id']);
                    $payable = $model['buying_rate'] - $totalTransation['paid'];
                    // if (!empty($model['buying_rate_incl_handling_charges'])) {
                    //   $payable += (float)$model['origin_handling_charges'] + (float)$model['destination_handling_charges'];
                    // }
                    return (float)$payable;
                 }
              ],
              // ['class' => 'yii\grid\ActionColumn'],
          ],
      ]); ?>
  </div>
<?php Pjax::end(); ?>
<?= $this->render('@frontend/modules/trip/views/manage/_modalTransactions'); ?>

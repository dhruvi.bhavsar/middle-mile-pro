<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\modules\account\models\AccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['receivable'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3">
        <?= $form->field($model, 'number') ?>
    </div>
    <div class="col-md-3">
      <?= $form->field($model, 'company_id')->widget(Select2::classname(), [
        'data' =>  \frontend\modules\customer\models\Customer::CustomerList(),
        'options' => [
          'placeholder' => 'Select Customer ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ])->label("Customer");
      ?>
    </div>
    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/account/manage/receivable"']) ?>
        &nbsp; | &nbsp;

        <?php if(empty(Yii::$app->request->queryParams)){
            echo Html::resetButton('CSV', ['class' => 'btn btn-primary','onclick'=>'window.location =window.location.href+"?export=true"']);
        }else{
            echo Html::resetButton('CSV', ['class' => 'btn btn-primary','onclick'=>'window.location =window.location.href+"&export=true"']);
        } ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>

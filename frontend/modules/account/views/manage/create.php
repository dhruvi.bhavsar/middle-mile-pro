<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = 'Add Trip';
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

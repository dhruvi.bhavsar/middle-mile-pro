<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\trip\models\Transactions;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\account\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payable';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
  <div class="trip-index">
      <div class="white-box">
        <?= $this->render('_payable_search', ['model' => $searchModel]); ?>
      </div>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          //'filterModel' => $searchModel,
          'options' => ['class'=>'white-box table-responsive'],
          'tableOptions' => ['class'=>'table'],
          'emptyText' => '<center>No records</center>',
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'attribute' => 'trip_date',
                'format'=>'raw',
                'value' =>function($model) {
                  return '<label class="hidden-lg hidden-md text-muted">Trip Date : </label>'.date('d-m-Y',strtotime($model['trip_date']));
                }
              ],
              [
                'label' => 'Order ID',
                'format'=>'raw',
                'attribute'=>'number',
                'value' =>function($model) {
                  return '<label class="hidden-lg hidden-md text-muted">Order ID : </label>'.$model['number'];
                }
              ],
              // 'customer',
              [
                'format'=>'raw',
                'attribute'=>'vendor',
                'value' =>function($model) {
                  return '<label class="hidden-lg hidden-md text-muted">Vendor : </label>'.$model['vendor'];
                }
              ],
              [
                'label' => 'Buying Rate',
                'format'=>'raw',
                'attribute'=>'buying_rate',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Buying Rate : </label>'.(float)$model['buying_rate'];
                }
              ],
              [
                'label' => 'Advance payable',
                'format'=>'raw',
                'attribute'=>'advance_buying_charges',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Advance : </label>'.((float)$model['advance_buying_charges'].(((float)$model['advance_buying_charges']<=(float)$model['paid_amount'])?'<i class="mdi mdi-check text-success"></i>':''));
                }
              ],
              [
                'label' => 'Paid',
                'attribute'=>'paid_amount',
                'format'=>'raw',
                'value'=>function($model){
                 return '<label class="hidden-lg hidden-md">Paid : </label>'.((float)$model['paid_amount']);
//                  return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],
              [
                'label' => 'Account status pending',
                'attribute'=>'account_status_pending',
                'format'=>'raw',
                'value'=>function($model){
                  $text = '<label class="hidden-lg hidden-md">Account status pending : </label>'.((float)$model['account_status_pending']);
                  return $text;
//                  return Html::a($text,NULL,['data-title'=>$model['number'],'class'=>'jsShowTransationDetails','style'=>'cursor: pointer;']);
                }
              ],

              [
                'label' => 'Ancillary charges',
                'format'=>'raw',
                'attribute'=>'vendor_ancillary_charges',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Ancillary charges : </label>'.(float)$model['vendor_ancillary_charges'];
                }
              ],

              [
                'label' => 'Mamul',
                'format'=>'raw',
                'attribute'=>'amount_mamul',
                'value'=>function($model){
                  return '<label class="hidden-lg hidden-md text-muted">Mamul : </label>'.(float)$model['amount_mamul'];
                }
              ],
                  
              [
                'attribute' => 'Balance',
                'format'=>'raw',
                'value' =>function($model) {
                  $amount = $model['buying_rate'] - ($model['paid_amount'] + (float)$model['amount_mamul'] + (float)$model['debit']);
                  return '<label class="hidden-lg hidden-md text-muted">Balance : </label>'.(($amount > 0)?$amount:0);
                }
              ],
          ],
      ]); ?>
  </div>
<?php Pjax::end(); ?>

<?= $this->render('@frontend/modules/trip/views/manage/_modalTransactions'); ?>

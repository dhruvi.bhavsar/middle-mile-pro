<?php

namespace frontend\modules\account\controllers;

use Yii;
use frontend\modules\trip\models\Trip;
use frontend\modules\account\models\AccountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * ManageController implements the CRUD actions for Trip model.
 */
class ManageController extends Controller
{
    public function beforeAction($action){
        Yii::$app->commonFunction->accessToAction('account');
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => [],
              'rules' => [
                  [
                      'actions' => [],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['POST'],
              ],
          ],
      ];
    }
    /**
     * Lists all Trip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->redirect('payable');
        $searchModel = new AccountSearch();
        $dataProvider = $searchModel->searchBalanceReceivablesPayables(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayable()
    {
        $searchModel = new AccountSearch();
        $dataProvider = $searchModel->searchAdvanceReceivablesPayables(Yii::$app->request->queryParams,1);

        return $this->render('payable', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionReceivable()
    {
        $get = Yii::$app->request->queryParams;

        $searchModel = new AccountSearch();
        $dataProvider = $searchModel->searchAdvanceReceivablesPayables(Yii::$app->request->queryParams);

        if(isset($get['export'])){
            $header= ['Trip Date','Order Id','Customer','Selling Rate','Advance Receivable','Received','Advance Pending','Account status pending'];
            if(!empty($dataProvider->allModels)){
                $data = $dataProvider->allModels;
                $csvData = [];
                foreach ($data as $model){
                    $csvData[] = [$model['trip_date'],$model['number'],$model['customer'],$model['selling_rate'],$model['advance_selling_charges'],$model['received_amount'],
                                            ($model['selling_rate']-$model['adv_received']),$model['account_status_pending']];
                };
                // Export data to CSV file
                $file = \Yii::$app->commonFunction->exportCSV($header, $csvData,NULL,'receivables-' . time() . '.csv');
                return Yii::$app->response->sendFile($file);
            }

        }

        return $this->render('receivable', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    // /**
    //  * Displays a single Trip model.
    //  * @param integer $id
    //  * @return mixed
    //  */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }
    //
    // /**
    //  * Creates a new Trip model.
    //  * If creation is successful, the browser will be redirected to the 'view' page.
    //  * @return mixed
    //  */
    // public function actionCreate()
    // {
    //     $model = new Trip();
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }
    //
    // /**
    //  * Updates an existing Trip model.
    //  * If update is successful, the browser will be redirected to the 'view' page.
    //  * @param integer $id
    //  * @return mixed
    //  */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }
    //
    // /**
    //  * Deletes an existing Trip model.
    //  * If deletion is successful, the browser will be redirected to the 'index' page.
    //  * @param integer $id
    //  * @return mixed
    //  */
    // public function actionDelete($id)
    // {
    //     $model = $this->findModel($id);
    //     $model->status = 2;
    //     $model->save(false);
    //     return $this->redirect(['index']);
    // }
    //
    // /**
    //  * Finds the Trip model based on its primary key value.
    //  * If the model is not found, a 404 HTTP exception will be thrown.
    //  * @param integer $id
    //  * @return Trip the loaded model
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    protected function findModel($id)
    {
        if (($model = Trip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

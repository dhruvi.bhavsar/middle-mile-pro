<?php

namespace frontend\modules\tracking\controllers;

use Yii;
use frontend\modules\trip\models\Trip;
use common\models\User;
use frontend\modules\tracking\models\TrackingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ManageController implements the CRUD actions for Trip model.
 */
class ManageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
      return [
          'access' => [
              'class' => AccessControl::className(),
              'only' => [],
              'rules' => [
                  [
                      'actions' => ['view'],
                      'allow' => true,
                      'roles' => ['?'],
                  ],
                  [
                      'actions' => ['index','view','share','download-tracking'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['POST'],
                  'share' => ['POST'],
                  'download-tracking' => ['POST'],
              ],
          ],
      ];
    }

    public function beforeAction($action){
        if($action->id == 'share'){$this->enableCsrfValidation = false;}
        if($action->id == 'download-tracking'){$this->enableCsrfValidation = false;}
        return parent::beforeAction($action);
    }
    /**
     * Lists all Trip models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrackingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trip model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->view->params['backUrl']='index';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionShare()
    {
      $postData = Yii::$app->request->post("data");
      // var_dump($postData);
      return Yii::$app->mailer
          ->compose(
              ['html' => 'trip_tracking_report'],
              ['id' => $postData['id']]
          )
          ->setFrom(\Yii::$app->params['adminEmail'])
          ->setTo(explode(',',$postData['email']))
          ->setSubject($postData['subject'])
          ->send();
    }

    /**
     * Finds the Trip model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trip the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    { // MD5()
        $model = Trip::find()->where(['MD5(id)'=>$id])->one();
        if (!empty($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownloadTracking($params = NULL)
    {
      $content = [];
      $exportData = new TrackingSearch();
      if (!empty($params))
      {
        $params = json_decode($params,true);
      }
      $header =['Order Id','Customer','Trip Date','POD Receival','Loading Point','Unloading Point','Service Type','Trip Type'];
      // ,'Contact Person','Email',
      // 'Origin Handlilng Charges','Destination Handlilng Charges','Profit'
      $data = $exportData->search($params,1)->getModels();
      $content = self::generateTracking($data);
      $file = \Yii::$app->commonFunction->exportCSV($header, $content, NULL,'Tracking.csv');
      return Yii::$app->response->sendFile($file);
    }

    private static function generateTracking($data)
    {
      $content = [];
      foreach ($data as $key => $model)
      {
        // Trip Data
        $content []=[
                        $model['number'],
                        $model['custName'],
                        date('d-m-Y',strtotime($model['trip_date'])),
                        (empty($model['pod_receival_date'])?'':date('d-m-Y',strtotime($model['pod_receival_date']))),
                        $model['loading_point'],
                        $model['unloading_point'],
                        \Yii::$app->params['serviceType'][$model['service_type']],
                        \Yii::$app->params['tripType'][$model['trip_type']],
                    ];

        if (!empty($model['remarks'])) {
          $remarks = json_decode($model['remarks'],true);
          // Tracking Details
          $content [] = [''];
          if (\Yii::$app->user->identity->role != 0) {
            $content []=['','Date & Time','Event','Location','Remark','By','Marked On'];
          } else {
            $content []=['','Date & Time','Event','Location','Remark'];
          }

          foreach ($remarks as $remark) {
            $temp = [];
            $temp = ['',
              date('d-m-Y h:i A',strtotime($remark['datetime'])),
              \Yii::$app->params['event_status'][$remark['trip_event']],
              $remark['location'],
              $remark['remark']
            ];
            if (\Yii::$app->user->identity->role != 0) {
              $user = User::findOne($remark['user']);
              $temp[]= $user['username'];
              $temp[]= date('d-m-Y h:i A',strtotime($remark['on']));
            }
            $content [] = $temp;
          }
          $content [] = [''];
        }

      }
      return $content;
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\trip\models\Trip;
// This is a open page which should not be indexed by search engine
\Yii::$app->view->registerMetaTag(['name' => 'robots','content' => 'noindex,nofollow']);
/* @var $this yii\web\View */
/* @var $model frontend\modules\trip\models\Trip */

$this->title = 'Tracking : '.$model->number;
$this->params['breadcrumbs'][] = ['label' => 'Trips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-view">

  <div class="row">
    <div class="col-md-4 col-sm-12">
	    <section class="white-box">
	      <div class="user-btm-box p-t-0">
					<!-- <div class="row  m-t-10"><div class="col-sm-12"><strong>TRIP DETAILS</strong><br/><br/></div></div> -->
	        <div class="row  m-t-10">
	          <div class="col-sm-6 b-r"><strong>Date</strong><p><?= date('d-m-Y',strtotime($model->trip_date)); ?></p></div>
						<div class="col-sm-6"><strong>Order Id</strong><p><?= $model->number ?></p></div>
	        </div>
	        <hr>
					<div class="row  m-t-10">
						<div class="col-sm-6"><strong>Customer</strong><p><?= $model->companyName->name ?></p></div>
	          <div class="col-sm-6 b-l"><strong>Contact Person</strong>
							<p class="m-0"><?= (!empty($model->contactPersonName->name)) ? ($model->contactPersonName->name) : ''; ?></p>
							<p class="font-12 m-0"><?= (!empty($model->contactPersonName->designation)) ? $model->contactPersonName->designation : ''; ?></p>
              <?php /*
							<p class="font-12 m-0">
								<?= (!empty($model->contactPersonName->mobile)) ? $model->contactPersonName->mobile : ''; ?>
								<?= (!empty($model->contactPersonName->email)) ? ' / '.$model->contactPersonName->email : ''; ?>
							</p>
              */ ?>
						</div>
	        </div>
	        <hr>
          <?php /*
					<div class="row  m-t-10">
	          <div class="col-sm-6 b-r"><strong>Origin</strong><p><?= ($model->originDetails->name) ?></p></div>
	          <div class="col-sm-6"><strong>Destination</strong><p><?= ($model->destinationDetails->name) ?></p></div>
	        </div>
	        <hr>
          */ ?>
					<div class="row  m-t-10">
	          <div class="col-sm-6 b-r"><strong>Loading Point</strong><p><?= (!empty($model->loading_point))?$model->loading_point:'-'; ?></p></div>
	          <div class="col-sm-6"><strong>Unloading Point</strong><p><?= (!empty($model->unloading_point))?$model->unloading_point:'-'; ?></p></div>
	        </div>
	        <hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Service Type</strong><p><?= Yii::$app->params['serviceType'][$model->service_type] ?></p></div>
						<div class="col-sm-6"><strong>Trip Type</strong><p><?= Yii::$app->params['tripType'][$model->trip_type] ?></p></div>
					</div>
					<hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Origin Handling Charges</strong><p><?= (!empty($model->origin_handling_charges))?$model->origin_handling_charges:'-' ?></p></div>
						<div class="col-sm-6"><strong>Destination Handling Charges</strong><p><?= (!empty($model->destination_handling_charges))?$model->destination_handling_charges:'-';  ?></p></div>
					</div>
					<hr>
					<div class="row  m-t-10">
						<div class="col-sm-6 b-r"><strong>Notification Duration</strong><p><?= (!empty($model->notification_duration))?'Every '.$model->notification_duration .' hours':'-' ?></p></div>
	          <div class="col-sm-6"><strong>Vehicle Type</strong><p><?= $model->vehicleType->name ?></p></div>
					</div>
	      </div>
	    </section>
	  </div>
    <div class="col-md-8 col-sm-12 ">
      <section class="well p-t-10 mob-p-t-0">
        <nav class="">
          <ul class="nav nav-tabs customtab" role="tablist">
            <li class="active"><a data-toggle="tab" href="#trip-tracking" >Tracking</a></li>
            <li class=""><a data-toggle="tab" href="#vehicle-on-map">Vehicle On Map</a></li>
          </ul>
        </nav>
        <div class="tab-content">
          <div id="trip-tracking" class="tab-pane fade in active">
            <div class="row">
              <div class="col-md-12">
                <div style="max-height:80vh;overflow:auto;">
                  <?php
                  if (!empty($model->remarks)) {
                    $tracking = json_decode($model->remarks,true);
                    krsort($tracking);
                    foreach ($tracking as $log) { ?>
                      <p class="m-b-0"><?= $log['remark']; ?></p>
                      <?php $user = \common\models\User::findOne(['id'=>$log['user']]) ?>
                      <small>
                      <?php if(!\Yii::$app->user->isGuest && \Yii::$app->user->identity->role != 0){?>
                      <span class="pull-right"><?= date('d-m-Y h:i A',strtotime($log['on'])); ?></span>
                      <?php } ?>
                      <?= ' Event :'.\Yii::$app->params['event_status'][$log['trip_event']]; ?> |
                      <?= ' Location :'.$log['location']; ?>
                      <?= ' | Date & Time :'.date('d-m-Y h:i A',strtotime($log['datetime'])); ?>
                      <br>
                      <?php if(!\Yii::$app->user->isGuest && \Yii::$app->user->identity->role != 0){?>
                      <?= !empty($user)? ('By:'.$user->username):'' ?>
                      <?php } ?>
                      </small>
                      <hr>
                  <?php  }
                } else {echo '<center> No Records </center>';}?>
                </div>
              </div>
            </div>
          </div>
          <div id="vehicle-on-map" class="tab-pane fade">
            <p class="jsMessageNoRecord text-danger"></p>
            <div id="map-container" style="width:100%; height:400px">Loading</div>
           <?php
           $this->registerCss("#map_canvas { height: 100% }");
           $this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyD_sgQPLL2Epz7Eb8gcrvEwoKkBhGqnQG0',['depends' => [\yii\web\JqueryAsset::className()]]);

           $dataGps = frontend\modules\vendor\models\GpsTracking::getTripTracking($model->id);
           $location = [];
           if (!empty($dataGps)) {
             foreach ($dataGps as $data) {
               $location[] = array_merge(
                 json_decode($data['data'],true),
                 [
                   'location'=>$data['address'],
                 ]);
             }
           }
           $this->registerJs("

            var locations = ".json_encode($location,true).";

            //console.log(locations);
            $(function(){
              if (locations != undefined && locations != '') {
                initialize(locations);
              }
            });

             function initialize(locations) {

               var myOptions = {
                 center: new google.maps.LatLng(21.1458, 79.0882),
                 zoom: 4,
                 mapTypeId: google.maps.MapTypeId.ROADMAP

               };
               var map = new google.maps.Map(document.getElementById('map-container'),
                   myOptions);

               setMarkers(map,locations)
             }


             function setMarkers(map,locations){

                 var marker, i

           for (i = 0; i < locations.length; i++)
            {

                var vehicle = locations[i]['vehicleregnumber'];
                var lat = locations[i]['latitude'];
                var long = locations[i]['longitude'];
                var location = locations[i]['location'];
                var datentime =  locations[i]['datetime'];
                var ignition =  locations[i]['ignition'];
                var speed =  locations[i]['speed'];
                var altitude =  locations[i]['altitude'];


                latlngset = new google.maps.LatLng(lat, long);
                var icon = 'marker.png';
                if (i + 1 == locations.length) {
                  var icon = 'marker_current.png';
                }
                 var marker = new google.maps.Marker({
                         map: map, title: vehicle , position: latlngset,icon:'https://stage.middlemilepro.com/images/'+icon
                       });
                       map.setCenter(marker.getPosition())


                       var content = '<h3>Vehicle Number: ' + vehicle +  '</h3>'
                                   + 'Location : ' + location + '<br />'
                                   + 'Last tracking : ' + datentime + '<br />'
                                   + 'Ignition : ' + ignition + '<br />'
                                   + 'Speed : ' + speed + '<br />'
                                   + 'Altitude : ' + altitude + '<br />';


                 var infowindow = new google.maps.InfoWindow()

               google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
                       return function() {
                                          infowindow.setContent(content);
                                          infowindow.open(map,marker);
                                       };
                                   })(marker,content,infowindow));

               }
           }

     initialize();
     ", yii\web\View::POS_END);

       ?>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>

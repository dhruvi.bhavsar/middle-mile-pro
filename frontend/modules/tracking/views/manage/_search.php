<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\modules\tracking\models\TrackingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trip-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
    <?php if (\Yii::$app->user->identity->role != 0) { ?>
      <div class="col-md-3">
        <?= $form->field($model, 'company_id')->widget(Select2::classname(), [
          'data' =>  \frontend\modules\customer\models\Customer::CustomerList(),
          'options' => [
            'placeholder' => 'Select Customer ...',
          ],
          'pluginOptions' => [
            'allowClear' => true,
            //'multiple' => true,
          ],
        ])->label("Customer");
        ?>
      </div>
    <?php } ?>

    <div class="col-md-3">
      <?= $form->field($model, 'number')->label("Order Id") ?>
    </div>
    <?php /*
    <div class="col-md-3">
      <?= $form->field($model, 'vendor_id')->widget(Select2::classname(), [
        'data' =>  \frontend\modules\vendor\models\Vendor::VendorList(),
        'options' => [
          'placeholder' => 'Select Vendor ...',
        ],
        'pluginOptions' => [
          'allowClear' => true,
          //'multiple' => true,
        ],
      ]);
      ?>
    </div>
    */ ?>
    <div class="col-md-3">
      <?= $form->field($model, 'trip_date')->textInput(['class'=>'datepicker form-control','autocomplete'=>'off']) ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
      <?= $form->field($model, 'duration')->radioList(['currentmonth'=>'This Month','currentweek'=>'This Week','custom'=>'Custom'],['item' => function($index, $label, $name, $checked, $value) {
                $return = '<div class="radio radio-info inline-block m-r-10">';
                $return .= '<input class="filter" type="radio" id="'.$name.$value.'" name="' . $name . '" value="' . $value . '" '.(($checked == 1)? "checked" : "").' >';
                $return .= '<label  for="'.$name.$value.'">' . ucwords($label) . '</label>';
                $return .= '</div>';
                return $return;
              },])->label("Period") ?>
    </div>
    <div class="col-md-4">
      <br>
      <span class="date-range" style="display:<?= ($model->duration == 'custom')?'block':'none'; ?>;">
      <span class="input-daterange input-group" id="date-range" style="">
        <input type="text" autocomplete="off" class="form-control mob-m-t-10" name="TrackingSearch[from_date]" placeholder="From" id="trackingsearch-from_date" value="<?= $model->from_date ?>"/>
        <span class="input-group-addon bg-info hidden-xs b-0 text-white">to</span>
        <input type="text" autocomplete="off" class="form-control mob-m-t-10"  name="TrackingSearch[to_date]" placeholder="To" id="trackingsearch-to_date" value="<?= $model->to_date ?>"/>
        <!-- <span class="input-group-addon bg-primary b-0 text-white mob-inline-block m-t-10" id="apply" style="cursor:pointer;">Apply</span> -->
      </span>
      </span>
    </div>
    <div class="form-group m-t-20 m-b-0 pull-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-inverse','onclick'=>'window.location ="/tracking/manage/index"']) ?>
        <?php $params = json_encode(Yii::$app->request->queryParams,true); ?>
        <?= Html::a('CVS', ['download-tracking','params'=>$params], ['class' => 'btn btn-primary','data-method'=>"post"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$this->registerJs("

	$('#date-range').datepicker({ toggleActive: true,todayHighlight: true,format:'d-m-yyyy' });
	$('.filter').click(function(){
		var fval = $(this).val();
		if(fval == 'custom'){
       $('.date-range').show();
     }else{
       // $(this).parents('form').submit();
     }
	});
	$('#apply').click(function(){
		var sdate = $('#trackingsearch-from_date').val(); var edate = $('#trackingsearch-to_date').val();
    console.log(sdate);
    console.log(edate);
		if(sdate !== '' & edate !== ''){
      $(this).parents('form').submit();
    }
	});
");
?>

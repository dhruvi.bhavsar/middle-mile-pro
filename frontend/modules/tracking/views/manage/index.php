<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\tracking\models\TrackingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-index">

  <div class="white-box">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'options' => ['class'=>'white-box table-responsive'],
        'tableOptions' => ['class'=>'table'],
        'emptyText' => '<center>No records</center>',
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'=>'Order Id',
                'attribute' => 'number',
                'format'=>'raw',
                'value' => function($model){
                    return '<label class="hidden-lg hidden-md text-muted">Order Id : </label>'.$model['number'];
                }
            ],
            [
                'label'=>'Customer',
                'attribute' => 'custName',
                'format'=>'raw',
                'value' => function($model){
                    return '<label class="hidden-lg hidden-md text-muted">Customer : </label>'.$model['custName'];
                }
            ],
            [
                'label'=>'Trip Date',
                'attribute' => 'trip_date',
                'format'=>'raw',
                'value' => function($model){
                    if (empty($model['trip_date'])){ return ''; }
                    return '<label class="hidden-lg hidden-md text-muted">Trip Date : </label>'.date('d-m-Y',strtotime($model['trip_date']));
                }
            ],
            [
                'label'=>'POD Receival',
                'attribute' => 'pod_receival_date',
                'format'=>'raw',
                'value' => function($model){
                    if (empty($model['pod_receival_date'])){ return ''; }
                    return '<label class="hidden-lg hidden-md text-muted">POD Receival : </label>'.date('d-m-Y',strtotime($model['pod_receival_date']));
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{shareTracking}',
                'buttons'  => [
                  'view' => function ($url, $model) {
                    $url = Url::to(['view', 'id' => md5($model['id'])]);
                    return Html::a('<span class="mdi mdi-map"></span>', $url, ['title' => 'View Tracking','style'=>'line-height: 0.8;','class'=>'btn btn-circle btn-flat waves-effect']);
                  },
                  'shareTracking'=>function ($url, $model) {
                    return Html::a('<span class="mdi mdi-share-variant"></span>',"#modalShareTracking",
                    [
                      'title' => 'Share Tracking',
                      'class'=>'btnShareTracking btn btn-circle btn-flat waves-effect',
                      'style'=>'line-height: 0.8;',
                      'data'=>[
                                'id'=>$model['id'],
                                'number'=>$model['number'],
                                'customer_email'=>$model['customer_email'],
                                'subject'=>'Tracking report of order Id : '.$model['number'],
                                'toggle'=>'modal',
                              ],
                    ]);
                  },
                ]
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>
</div>
<?= $this->render('_modal_share_tracking'); ?>

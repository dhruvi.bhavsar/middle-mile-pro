<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

?>
<div class="modal fade" id="modalShareTracking" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"><h4 class="modal-title" id="">Share Tracking : <span class="trackingOrderId"></span></h4></div>
      <div class="modal-body">
        <div class="hidden" hidden>
          <input type="text" class="trackingTripId" value="">
        </div>
        <div class="row">
          <div class="col-md-12">
            <label class="control-label" for="trackingSubject">Subject</label>
            <input type="text" class="trackingSubject form-control" name="trackingSubject" required value="">
            <div class="help-block text-danger"></div>
          </div>
          <div class="col-md-12">
            <br>
            <label class="control-label" for="trackingEmail">Receiver Email ID</label>
            <input type="text" class="trackingEmail form-control" name="trackingEmail" required value="">
            <div class="help-block text-danger"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btnSubmitSendTracking">Send</button>
      </div>
    </div>
  </div>
</div>


<?php $this->registerJs("
  $(window).on('load pjax:end',function(){
    $('.btnShareTracking').click(function(){
      var data = $(this).data();
      $('.trackingOrderId').text(data.number);
      $('.trackingTripId').val(data.id);
      $('.trackingSubject').val(data.subject);
      $('.trackingEmail').val(data.customer_email);
    });

    $('.btnSubmitSendTracking').click(function(){

      var id = $('.trackingTripId').val();
      var subject = $('.trackingSubject').val();
      if (subject == null || subject == ' '|| subject == '') {
        $('.trackingSubject').next('.help-block').text('Subject Required.');
        return false;
      } else {
        $('.trackingSubject').next('.help-block').text('');
      }
      var email = $('.trackingEmail').val();
      if (email == null || email == ' '|| email == '') {
        $('.trackingEmail').next('.help-block').text('Receiver Email Address Required.');
        return false;
      } else {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var array = email.split(',');
        for (var i = 0; i < array.length; i++) {
          if (reg.test(array[i]) == false){
            $('.trackingEmail').next('.help-block').text('Invalid Email Address.');
            return false;
          }
        }
        $('.trackingEmail').next('.help-block').text('');
      }
      var data = {'id': id,'subject':subject,'email':email};
      $.post('/tracking/manage/share', {data:data},function(result) {
          if (result == 1) {
            alertSnackbar('Email Sent.','success');
            $('#modalShareTracking').modal('hide');
          } else {
            alert(result);
          }
      });
    });
  });
",\yii\web\View::POS_END); ?>

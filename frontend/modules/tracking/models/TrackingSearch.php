<?php

namespace frontend\modules\tracking\models;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use frontend\modules\trip\models\Trip;
use frontend\modules\master\models\VehicleType;
use frontend\modules\customer\models\Customer;
use frontend\modules\master\models\City;
/**
 * TrackingSearch represents the model behind the search form about `frontend\modules\trip\models\Trip`.
 */
class TrackingSearch extends Trip
{
    /**
     * @inheritdoc
     */
    // public $duration;
    // public $from_date;
    // public $to_date;
    public function rules()
    {
        return [
            [['id', 'vendor_id', 'company_id', 'contact_person_id', 'vehicle_id', 'vehicle_type_id', 'buying_rate_incl_handling_charges', 'trip_type', 'service_type', 'status'], 'safe'],
            [['number', 'vendor_bids', 'origin', 'loading_point', 'destination', 'unloading_point', 'trip_date', 'recipients', 'created_at', 'updated_at', 'negotiation_remark', 'cancellation', 'remarks', 'unloaded_datetime', 'pod_receival_date', 'final_bill_date', 'last_notified_at'], 'safe'],
            [['targated_rate', 'targated_buying', 'buying_rate', 'advance_buying_charges', 'selling_rate', 'advance_selling_charges', 'origin_handling_charges', 'destination_handling_charges', 'origin_lat', 'origin_lon', 'destination_lat', 'destination_lon', 'notification_duration','duration','from_date','to_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      $query = Trip::find()->from([Trip::tableName().' T'])
                           ->select(['T.*','C.name custName','Org.name origin_name','Dest.name destination_name','(IF(T.buying_rate_incl_handling_charges IS NULL,(T.selling_rate - T.buying_rate - origin_handling_charges - destination_handling_charges),(T.selling_rate - T.buying_rate))) profit','C.email customer_email'])
                           ->leftJoin(Customer::tableName().' C','C.id = T.company_id ')
                           ->leftJoin(City::tableName().' Org','Org.id = T.origin')
                           ->leftJoin(City::tableName().' Dest','Dest.id = T.destination')
                           ->andWhere(['<>','T.remarks','']);

      if (\Yii::$app->user->identity->role == 0) {
        $query = $query->andWhere(['company_id'=>\Yii::$app->user->identity->customer_id]);
        if (empty(\Yii::$app->user->identity->is_admin)) {
          $query = $query->andWhere(['contact_person_id'=>\Yii::$app->user->identity->id]);
        }
      }

      $this->load($params);
      if(!empty($this->duration))
      {
        switch($this->duration){
            case 'currentmonth':
                $sdate = date('Y-m-d 00:00:00',strtotime('-30 Day'));
                $edate = date('Y-m-d 23:59:59');
                $query = $query->andWhere(['>','T.trip_date',$sdate]);
                $query = $query->andWhere(['<','T.trip_date',$edate]);
                break;
            case 'currentweek':
                $sdate = date('Y-m-d 00:00:00',strtotime('-7 Day'));
                $edate = date('Y-m-d 23:59:59');
                $query = $query->andWhere(['>','T.trip_date',$sdate]);
                $query = $query->andWhere(['<','T.trip_date',$edate]);
                break;
            case 'custom':
                 $sdate = date('Y-m-d 00:00:00',strtotime((!empty($this->from_date)?$this->from_date:'1-Jan-1980 00:00:00')));
                 $edate = date('Y-m-d 23:59:59',strtotime((!empty($this->to_date)?$this->to_date:'1-Jan-2900 23:59:59')));
                 $query = $query->andWhere(['>=','T.trip_date',$sdate]);
                 $query = $query->andWhere(['<=','T.trip_date',$edate]);
                break;
        }
      }

      $query->andFilterWhere([
          'T.company_id'=>$this->company_id,
      ]);

      if (!empty($this->trip_date)) {
        $query->andFilterWhere(['like', 'T.trip_date',date('Y-m-d',strtotime($this->trip_date))]);
      }

      $query->andFilterWhere(['like', 'T.number', $this->number])
            ->andFilterWhere(['like', 'T.vendor_id', $this->vendor_id])
            ->andFilterWhere(['like', 'T.vehicle_type_id', $this->vehicle_type_id])
            ->groupBy(['T.id']);



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendor_id' => $this->vendor_id,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            'sort'=> [
                  'attributes' => ['id'=>'id','number'=>'number','custName'=>'custName','trip_date'=>'trip_date','pod_receival_date'=>'pod_receival_date',],
                  'defaultOrder' => ['id'=>SORT_ASC]
            ]
        ]);

        return $dataProvider;
    }
}

<?php
use yii\helpers\Html;
// SerialColumn
\Yii::$container->set('yii\grid\SerialColumn',['contentOptions'=>['class'=>'hidden-xs hidden-sm'],'headerOptions'=>['class'=>'hidden-xs hidden-sm'],'footerOptions'=>['class'=>'hidden-xs hidden-sm']]);
// LinkPager
\Yii::$container->set('yii\widgets\LinkPager', ['maxButtonCount' => 5]);
// ActionColumn
\Yii::$container->set('yii\grid\ActionColumn', [
    'contentOptions'=>['class' => 'grid-action-column text-center'],
    'buttonOptions'=>['class'=>'btn btn-circle btn-flat waves-effect','style'=>'line-height: 0.8;'],
    'buttons'=>[
        'view'=>function ($url, $model, $key) {
          return Html::a('<i class="mdi mdi-eye" style="line-height: 0.8;"></i>', $url,['class'=>'btn btn-circle btn-flat waves-effect','title' => 'View','data-pjax'=>'0']);
        },
        'update'=>function ($url, $model, $key) {
          return Html::a('<i class="mdi mdi-pencil" style="line-height: 0.8;"></i>', $url,['class'=>'btn btn-circle btn-flat waves-effect','title' => 'Edit','data-pjax'=>'0']);
        },
        'delete'=>function ($url, $model, $key) {
          return Html::a('<i class="mdi mdi-delete text-danger" style="line-height: 0.8;"></i>', $url,['class'=>'btn btn-circle btn-flat waves-effect','data'=>['method'=>'post','pjax'=>0,'confirm'=>'Are you want to delete this item?'],'title' => 'Delete']);
        },
    ],
  ]);

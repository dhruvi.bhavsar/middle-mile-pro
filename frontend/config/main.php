<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    "modules"=>[
            'master' => ['class' => 'frontend\modules\master\Master'],
            'order' => ['class' => 'frontend\modules\order\Order','defaultRoute'=>'manage'], // Currently no table avalibl for this module
            'vendor' => ['class' => 'frontend\modules\vendor\Vendor','defaultRoute'=>'manage'],
            'customer' => ['class' => 'frontend\modules\customer\Customer','defaultRoute'=>'manage'],
            'trip' => ['class' => 'frontend\modules\trip\Trip','defaultRoute'=>'manage'],
            'staff' => ['class' => 'frontend\modules\staff\Staff','defaultRoute'=>'manage'],
            'account' => ['class' => 'frontend\modules\account\Account','defaultRoute'=>'manage'],
            'pod' => ['class' => 'frontend\modules\pod\Pod','defaultRoute'=>'manage'],
            'tracking' => ['class' => 'frontend\modules\tracking\Tracking','defaultRoute'=>'manage'],
            'inventory' => ['class' => 'frontend\modules\inventory\Inventory','defaultRoute'=>'tyre'],
            'notification' => ['class' => 'frontend\modules\notification\Notification','defaultRoute'=>'manage'],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
//                '<controller:\w+>/<action:\w([a-z 0-9\+\-])+>' => '<controller>/<action>',
//                '<controller:\w+>/<id:\w([a-z0-9\+\-])+>' => '<controller>/view',
//                '<alias:master|order>/<controller:\w+>/<action:\w([a-z 0-9\+\-])+>'=>'<alias>/<controller>/<action>',
//                '<controller:\w+>/<action:\w([a-z 0-9\+\-])+>/<slug:\w([a-z 0-9\+\-])+>' => '<controller>/<action>',
            ],
        ],
        'activity' => ['class' => 'frontend\components\Log'],
        'assetManager' => [
          'class' => 'yii\web\AssetManager',
          'bundles' => [
              'yii\web\JqueryAsset' => ['js' => ['jquery.min.js']],
              //'yii\widgets\ActiveFormAsset' => ['js' => ['yii.activeForm.min.js']],
              'yii\bootstrap\BootstrapAsset' => ['css' => ['css/bootstrap.min.css',]],
              'yii\bootstrap\BootstrapPluginAsset' => ['js' => ['js/bootstrap.min.js',],],
              /*
              'kartik\select2\Select2KrajeeAsset'=>[
                'js' => ['js/select2-krajee.min.js'],
                'css' => ['css/select2-addl.min.css'],
              ],
              'kartik\select2\Select2Asset' => [
                    //'sourcePath' => null,   // do not publish the bundle if cdn
                    'js' => ['js/select2.full.min.js'],
                    // 'js' => ['/js/custom.select2.full.js','js/select2-krajee.min.js'],
                    'css' => ['css/select2.min.css'],
              ],
              'kartik\select2\ThemeKrajeeAsset' => ['css' => ['css/select2-krajee.min.css'],],
              'kartik\select2\ThemeKrajeeBs4Asset' => ['css' => ['css/select2-krajee-bs4.min.css'],],
              'kartik\base\WidgetAsset'=>[
                  'js' => ['js/kv-widgets.min.js'],
                  'css' => ['css/kv-widgets.min.css'],
              ]
              */
            ],
        ],
    ],
    'params' => $params,
];

<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$trackingLink = \Yii::$app->params['frontendUrl'].Yii::$app->urlManager->createUrl(['tracking/manage/view', 'id' => md5($id)]);
$trackingLink = str_replace("//-/","/", $trackingLink);
?>
<div class="password-reset">
    <p>Hello,</p>

    <p>Follow the link below to view your tracking report</p>

    <p><?= Html::a(Html::encode($trackingLink), $trackingLink) ?></p>
</div>

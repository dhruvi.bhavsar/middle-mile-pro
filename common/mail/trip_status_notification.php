<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
// $invoiceLink = \Yii::$app->params['frontendUrl'].Yii::$app->urlManager->createUrl(['trip/manage/viewbookingslip', 'id' => $id]);
// $invoiceLink = str_replace("//-/","/", $invoiceLink);
// <p><?= Html::a(Html::encode($invoiceLink), $invoiceLink) ?>
<div class="password-reset">
    <p>Hello,</p>

    <p>Kindly find the below trip status of <?= $modelTrip->number; ?>.</p>

    <?php foreach ($remarks as $remark) { ?>
      <div style="margin:10px">
        <p style="margin:0px"><?= $remark['remark']; ?></p>
        <small style="font-size:10px">Event : <?= \Yii::$app->params['event_status'][$remark['trip_event']]; ?></small>
        <small style="font-size:10px">On : <?= $remark['datetime'] ?></small>
      </div>
    <?php } ?>
</div>

<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$invoiceLink = \Yii::$app->params['frontendUrl'].Yii::$app->urlManager->createUrl(['trip/manage/viewpayslip', 'id' => $id]);
$invoiceLink = str_replace("//-/","/", $invoiceLink);
?>
<div class="password-reset">
    <p>Hello,</p>

    <p>Follow the link below to get your invoice</p>

    <p><?= Html::a(Html::encode($invoiceLink), $invoiceLink) ?></p>
</div>

<?php
date_default_timezone_set("Asia/Kolkata");
ini_set('max_execution_time', 600);
return [
    'name'=>"MiddleMilePro",
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'commonFunction' => [
            'class' => 'common\components\CommonFunctions',
        ],
        'mailer' =>[
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.middlemilepro.com',
                'username' => 'notifications@middlemilepro.com',
                'password' => 'notifications@123',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'activity' => ['class' => 'frontend\components\Log'],
    ],

];

<?php
return [
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'frontendUrl' => 'http://www.middlemilepro.com',
    'adminEmail' => 'notifications@middlemilepro.com',
    'smsKey' => '214691A4n0d0fYh5af3b7a9',
    'gmapKey' => 'AIzaSyDQabb_bvwiVmXKpQE6Zr0AIEPGOo2GTFs',  // omkra9990@gmail.com /  project - middlemilepro
    // TRIP DETAILS
    'tripType' => ['1' => 'Direct', '2' => 'Multistop'],
    // 'tripType'=>['1'=>'Direct','2'=>'Multi Stop Pickup','3'=>'Multistop Delivery','4'=>'Rotation'],
    'serviceType' => ['1' => 'TC Express', '2' => 'Express', '3' => 'Standard'],
    // 'serviceType'=>['1'=>'Conventional','2'=>'Express','3'=>'Expedite'],
    // Bank account types
    'bankAccountTypes' => [1 => 'Savings', 2 => 'Current'],
    'tripStatus' => [
        '1' => 'Vendor Assignment Pending',
        '2' => 'Order Closed/Dropped',
        '3' => 'Under Negotiation',
        '5' => 'Customer Confirmation Pending',
        '8' => 'Vendor Confirmation Pending',
        '10' => 'Order Confirmed',
        '20' => 'Order Delivered'
    ],
    'driverRates' => [
        'loadTrip' => 15,
        'ferryTrip' => 10
    ],
    'tripStatusLabel' => [
        '1' => '<span class="label label-danger label-rouded">Vendor Assignment Pending</span>',
        '2' => '<span class="label label-danger label-rouded">Order Closed</span>',
        '3' => '<span class="label label-danger label-rouded">Under Negotiation</span>',
        '5' => '<span class="label label-warning label-rouded">Customer Confirmation Pending</span>',
        '8' => '<span class="label label-info label-rouded">Vendor Confirmation Pending</span>',
        '10' => '<span class="label label-success label-rouded">Order Confirmed</span>',
        '20' => '<span class="label label-success label-rouded">Order Delivered</span>',
    ],
    'tyreStatus' => [
        '1' => 'Stock',
        '2' => 'Deleted',
        '3' => 'Installed',
        '5' => 'Stepney',
        '8' => 'Repair',
        '9' => 'Re-installed',
        '10' => 'Scraped'
    ],
    'tyreStatusDropdown' => [
        '1' => 'Stock',
        '3' => 'Installed',
        '5' => 'Stepney',
        '8' => 'Repair',
        '9' => 'Re-installed',
        '10' => 'Scraped'
    ],
    'tyreStatusLabel' => [
        '1' => '<span class="label label-info label-rouded">Stock</span>',
        '2' => '<span class="label label-danger label-rouded">Deleted</span>',
        '3' => '<span class="label label-success label-rouded">Installed</span>',
        '5' => '<span class="label label-info label-rouded">Stepney</span>',
        '8' => '<span class="label label-warning label-rouded">Repair</span>',
        '9' => '<span class="label label-success label-rouded">Re-installed</span>',
        '10' => '<span class="label label-danger label-rouded">Scraped</span>'
    ],
    // Staff Role used to create user
    'staffRole' => ['9' => 'General manager', '8' => 'Manager','6'=>'Executive', '3' => 'Operator',
        '1' => 'Driver'],
    // User Role
    'role' => ['10' => 'Owner', '9' => 'General manager', '8' => 'Manager','6'=>'Executive', '3' => 'Operator',
        '1' => 'Driver'],
    // Team
    'team' => ['1' => 'Customer Service', '2' => 'Traffic', '3' => 'Tracking', '4' => 'Sales',
        '5' => 'Accounts','6'=>'Back office'],
    // User Status and Access Rules
    'userStatus' => ['1' => 'Active', '0' => 'Deactive'],
    'accessRules' => [
        '10' => [
            'dashboard' => [],
            'tripRequest' => ['createTrip', 'updateTrip', 'viewTrip', 'deleteTrip'],
            'viewTracking' => [],
            'customers' => [],
            'vendors' => ['vendorList', 'vendorType'],
            'staff' => [],
            'masterSettings' => ['states', 'city', 'route', 'vehicleType', 'stops'],
            'accounts' => ['account', 'pod'],
            'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
        ],
        '8' => [
            '1' => [
                'dashboard' => [],
                'tripRequest' => ['createTrip', 'updateTrip', 'viewTrip', 'deleteTrip',
                    'customerConfirm'],
                'viewTracking' => [],
                'customers' => [],
                'vendors' => ['vendorList', 'vendorType'],
                'staff' => [],
                'masterSettings' => ['states', 'city', 'route', 'vehicleType', 'stops'],
                'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
            ],
            '2' => [
                'dashboard' => [],
                'tripRequest' => ['createTrip', 'updateTrip', 'viewTrip', 'deleteTrip',
                    'assignVendor', 'vendorConfirm'],
                'viewTracking' => [],
                'customers' => [],
                'vendors' => ['vendorList', 'vendorType'],
                'staff' => [],
                'masterSettings' => ['states', 'city', 'route', 'vehicleType', 'stops'],
                'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
            ],
            '3' => [
                'dashboard' => [],
                'tripRequest' => ['createTrip', 'updateTrip', 'viewTrip', 'deleteTrip'],
                'viewTracking' => [],
                'customers' => [],
                'vendors' => ['vendorList', 'vendorType', 'vendorVehicle', 'vendorType'],
                'staff' => [],
                'masterSettings' => ['states', 'city', 'route', 'vehicleType', 'stops'],
                'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
            //'accounts'=>['account','pod'],
            ]
        ],
        '3' => [
            '1' => [
                'dashboard' => [],
                'tripRequest' => ['createTrip', 'updateTrip', 'viewTrip', 'customerConfirm'],
                'viewTracking' => [],
                'customers' => [],
                'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
            ],
            '2' => [
                'dashboard' => [],
                'tripRequest' => ['viewTrip', 'assignVendor', 'vendorConfirm'],
                'viewTracking' => [],
                'vendors' => ['vendorList', 'vendorType'],
                'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
            ],
            '3' => [
                'dashboard' => [],
                'tripRequest' => ['viewTrip'],
                'viewTracking' => [],
                'customers' => [],
                'vendors' => ['vendorList', 'vendorType'],
                'masterSettings' => ['stops'],
                'inventory' => ['viewInventory', 'manageInventory', 'updateInventoryStatus'],
            ]
        ],
        '1' => [
            '0' => [
                'driverView' => [],
            ],
        ],
        '0' => [
            '0' => [
                'viewTracking' => [],
            ],
        ],
    ],
    'event_status' => [
        0 => 'Assigned',
        9 => 'On way to loading Point',
        1 => 'Reported at loading point',
        2 => 'Loaded',
        10=>'Reported at intermittent point',
        3 => 'Departed from origin',
        11=>'Departed from intermittent  point',
        4 => 'Running',
        5 => 'Breakdown',
        6 => 'Reported Unloading Point',
        7 => 'Unloaded',
        8 => 'POD Uploaded',
        12 => 'POD Collected',
        13 => 'POD couriered',
        14 => 'POD Received',
    ],
    'documents' => [
        1 => 'LR',
        2 => 'Eway Bill',
        3 => 'D/L',
        4 => 'R/C',
        5 => 'Memo',
        6 => 'POD',
        7 => 'Final Bill',
    ],
    'transaction_type' => [
        // transaction for vendor
        // 1=>'Advance',
        2 => 'Paid',
        3 => 'Debit',
        // transaction from customer
        33 => 'Advance Received',
        4 => 'Payment Received',
        5 => 'Write off',
        // Below status are applicable for both the parties ancillary charges payable and receivable
        6 => 'Origin Detention Charges',
        7 => 'Destination Detention Charges',

        8 => 'Toll',
        9 => 'Permit',
        10 => 'Adblue',
        11 => 'Punctures',
        12 => 'Repair and maintenance',
        13 => 'Diesel (Cash)',
        31 => 'Diesel (Credit)',
        14 => 'Challan',
        23=>'Overload penalty',
        19=>'Loading charges',
        20=>'Unloading charges',

        21=>'State entry permit',
        22=>'Weigh bridge (kanta)',

        24=>'Food allowance',
        25=>'Parking',
        26=>'Puncture',
        27=>'Towing charges',
        28=>'Repair and Maintenance',
        29=>'Toll  Cash',
        30=>'Toll  (Fastag)',

        15=>'Commission',
        16=>'Mamul',
        17=>'TDS',
        18=>'Debit',
        32=>'Balance brought forward',

        // Continue index from 34
    ],
    'transactionsApproval' => [
        '0' => 'Pending',
        '1' => 'Approved',
        '2' => 'Rejected'
    ],
    'transactionStatus' => [
        0 => 'Account status pending',
        1 => 'Completed',
        2 => 'Rejected by accounts',
        5 => 'Deleted'
    ],
    'mamul_type' => [
        1 => 'Advance Mamul',
        2 => 'Balance Mamul',
    ],
    // Entry of mamual amount should be in desending order. compartable with getMamaul()
    'advance_mamul' => [
        '9999999999999999' => 1000, // This is max amount no limit
        '100000' => 500,
        '50000' => 400,
        '40000' => 300,
        '30000' => 200,
        '20000' => 100,
        '0' => 100, // This is default/min amount
    ],
    // Entry of mamual amount should be in desending order. compartable with getMamaul()
    'balance_mamul' => [
        '3000' => 100,
        '0' => 50, // This is default/min amount
    ],
    'payment_type' => [
        1 => 'Origin Detention Charges',
        2 => 'Destination Detention Charges',
        3 => 'Toll',
        4 => 'Permit',
        5 => 'Adblue',
        6 => 'Punctures',
        7 => 'Repair and maintenance',
        8 => 'Diesel',
        9 => 'Others'
    ],
    'zone' => [
        1 => 'North',
        2 => 'East',
        3 => 'West',
        4 => 'South',
        5 => 'Central',
        6 => 'North East',
    ],
    'load_type' => [
        1 => 'Boxes',
        2 => 'Pallets',
        3 => 'Drums',
        4 => 'Others'
    ],
    'vehicle_status_color_code' => [
        0 => ['color' => '#0070c0', 'label' => 'Assigned'], // assigned
        1 => ['color' => '#92d050', 'label' => 'Reported Origin'], // running
        2 => ['color' => '#92d050', 'label' => 'Loaded'], // running
        3 => ['color' => '#92d050', 'label' => 'Departed'], // running
        4 => ['color' => '#92d050', 'label' => 'Running'], // running
        5 => ['color' => '#d2e730', 'label' => 'Breakdown'], // maintenance
        6 => ['color' => '#e5b8b7', 'label' => 'Reported Unloading Point'], // unloading
        7 => ['color' => '#ff0000', 'label' => 'Unloaded'], // available
        8 => ['color' => '#ff0000', 'label' => 'Available'], // available POD/Uploaded
        12 => ['color' => '#ff0000', 'label' => 'Available'], // available POD/Uploaded
        13 => ['color' => '#ff0000', 'label' => 'Available'], // available POD/Uploaded
        14 => ['color' => '#ff0000', 'label' => 'Available'], // available POD/Uploaded
        '' => ['color' => '#ff0000', 'label' => 'Available'], // assigned
        9 => ['color' => '#92d050', 'label' => 'Running'], // running
    ],
];

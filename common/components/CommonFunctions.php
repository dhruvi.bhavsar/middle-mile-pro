<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use frontend\modules\trip\models\Billing;
use frontend\modules\trip\models\Transactions;

// use yii\web\Request;

class CommonFunctions extends Component
{

    public function accessToElement($action)
    {
        $paramsRULE = Yii::$app->params['accessRules'];
        if (isset(Yii::$app->user->identity->role)) {
            $role = Yii::$app->user->identity->role;
            $team = Yii::$app->user->identity->team;
            if ($role === 10) {
                return true;
            }
            if (!array_key_exists($role, $paramsRULE)) {
                return false;
            }
            if (!array_key_exists($team, $paramsRULE[$role])) {
                return false;
            }

            $RULE = $paramsRULE[$role][$team];
            foreach ($RULE as $mainMenu => $subMenu) {
                if (!strcasecmp($action, $mainMenu)) {
                    return true;
                } else {
                    foreach ($subMenu as $k => $v) {
                        if (!strcasecmp($action, $v)) {
                            return true;
                        }
                    }
                }
            }
        }
    }

    public function accessToAction($action)
    {
        // var_dump($action); die();
        // $actionList = ['vendorVehicle','vendorLocation'];
        // if(in_array($action,$actionList)){
        // throw new NotFoundHttpException("Invalid Request");
        // }
        if (!$this->accessToElement($action)) {
            return Yii::$app->getResponse()->redirect('/site/index');
        }
    }

    function getIndianCurrency($num)
    {
        $number        = (float) $num;
        $decimal       = round($number - ($no            = floor($number)), 2) * 100;
        $hundred       = null;
        $digits_length = strlen($no);
        $i             = 0;
        $str           = array();
        $words         = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits        = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number  = floor($no % $divider);
            $no      = floor($no / $divider);
            $i       += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural  = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str []  = ($number < 21) ? $words[$number].' '.$digits[$counter].$plural.' '.$hundred
                        : $words[floor($number / 10) * 10].' '.$words[$number % 10].' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise  = ($decimal) ? ".".($words[$decimal / 10]." ".$words[$decimal % 10]).' Paise'
                : '';
        return ($Rupees ? 'Rupees '.$Rupees : '').$paise;
    }

    // sms using MSG 91 api

    public function sendSMSV2($mobiles, $message)
    {

        $curl = curl_init();

        $postData = [
            'sender' => 'MMPLLP',
            'route' => "4",
            'country' => "91",
            'sms' => [
                [
                    'message' => $message,
                    'to' => [$mobiles]
                ]
            ],
        ];

        curl_setopt_array($curl,
            array(
            CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($postData, true),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "authkey: ".Yii::$app->params['smsKey'],
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);
        var_dump($response);
        die();
        if ($err) {
            return "cURL Error #:".$err;
        } else {
            return 'response'.$response;
        }
        // return true;
    }

    public function sendSMS($mobiles, $message)
    {

        //Prepare you post parameters
        // for testing
        //$mobiles = '8983528097';

        $postData = array(
            'authkey' => Yii::$app->params['smsKey'],
            'mobiles' => $mobiles,
            'message' => urlencode($message),
            'sender' => 'MMPLLP', // MMPLLP  TESTIN
            'route' => 4
        );

        //API URL
        $url = "https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch,
            array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //get response
        $output = curl_exec($ch);
        curl_close($ch);
        return true;
    }

    public function logEntry()
    {
        
    }

    public function lowestBid($bids)
    {
        $out = 0;
        if (!empty($bids)) {
            foreach ($bids as $bid) {
                $out = ($out > $bid || $out == 0) ? $bid : $out;
            }
        }
        return $out;
    }

    public function exportCSV($header = NULL, $content, $footer = NULL,
                              $fileName, $location = NULL)
    {
        if (empty($location)) {
            $location = 'download/invoices/';
        }

        $f    = $location.strtotime('now').Yii::$app->user->id.'-'.$fileName;
        $file = fopen($f, 'w');

        // save the column headers
        if (isset($header) && !empty($header)) {
            if (is_array($header[0])) {
                foreach ($header as $row) {
                    fputcsv($file, $row);
                }
            } else {
                fputcsv($file, $header);
            }
        }

        // save each row of the data
        foreach ($content as $rows) {
            if (is_array($rows[0])) {
                foreach ($rows as $row) {
                    fputcsv($file, $row);
                }
            } else {
                fputcsv($file, $rows);
            }
        }
        // save the column footer
        if (isset($footer) && !empty($footer)) {
            fputcsv($file, $footer);
        }

        fclose($file);
        return $f;
    }

    public function getMamul($amount, $mamulType)
    {
        $dataMamul = \Yii::$app->params[$mamulType];
        foreach ($dataMamul as $range => $mamulAmount) {
            if ((float) $range <= $amount) {
                return $mamulAmount;
            }
        }
    }

    public function newInvoiceNumber()
    {
        $billing = Billing::find()->select(['MAX(id) as id'])->asArray()->all();
        return ($billing[0]['id'] + 1);
    }

    public function getReceivables($tripId)
    {
        $transactions = Transactions::find()
                ->select([
                    'SUM(IF(payment_type = '.Transactions::ORIGIN_DETENTION_CHARGES.',amount,0)) origin_detention_charges',
                    'SUM(IF(payment_type = '.Transactions::DESTINATION_DETENTION_CHARGES.',amount,0)) destination_detention_charges',
                    'SUM(IF(payment_type = '.Transactions::OTHER.',amount,0)) other_charges',
                    'SUM(IF(payment_type = '.Transactions::TOLL.',amount,0)) toll_charges',
                    'SUM(IF(payment_type = '.Transactions::ORIGIN_DETENTION_CHARGES.',detention_days,0)) origin_detention_days',
                    'SUM(IF(payment_type = '.Transactions::DESTINATION_DETENTION_CHARGES.',detention_days,0)) destination_detention_days',
                ])
                ->andWhere([
                    'trip_id' => $tripId,
                    'transaction_type' => Transactions::TRANSACTION_TYPE_RECEIVABLE,
                    'status' => Transactions::STATUS_ACTIVE
                ])->asArray()->all();

        $paid     = Transactions::find()
                ->select(['SUM(amount) amount_paid'])
                ->andWhere([
                    'trip_id' => $tripId,
                    'transaction_type' => Transactions::RECEIVED,
                    'status' => Transactions::STATUS_ACTIVE
                ])->asArray()->all();
        // 'SUM(IF(payment_type = '.Transactions::WRITE_OFF.',amount,0)) write_off',
        $writeOff = Transactions::find()
                ->select(['SUM(amount) write_off'])
                ->andWhere([
                    'trip_id' => $tripId,
                    'transaction_type' => Transactions::WRITE_OFF,
                    'status' => Transactions::STATUS_ACTIVE
                ])->asArray()->all();
        if (isset($transactions[0])) {
            $transactions = $transactions[0];
        }
        if (isset($writeOff[0])) {
            $writeOff = $writeOff[0];
        }
        if (isset($paid[0])) {
            $paid = $paid[0];
        }

        $transactions['total_detention_charges'] = $transactions['origin_detention_charges']
            + $transactions['destination_detention_charges']+ $transactions['other_charges'];

        $transactions['total_receivables']       = $transactions['total_detention_charges']
            + $transactions['toll_charges'];
        $transactions['total_detention_days']    = $transactions['origin_detention_days']
            + $transactions['destination_detention_days'];
        $transactions['amount_paid']             = $paid['amount_paid'];
        $transactions['write_off']               = $writeOff['write_off'];

        return $transactions;
    }
}
?>
